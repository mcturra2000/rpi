#define STM32F411xE
#include "stm32f4xx.h"

#include "delay.h"
#include "gpio.h"

#define CS 	GPIOA, 4
#define LED 	GPIOB, 12

/*
void spi_send(SPI_TypeDef *spi, uint16_t val)
{
	spi->DR = val; // wite 8/16 bits, depending on setup
	while(!(spi->SR & SPI_SR_TXE)); // wait for transfer to finish
	while((spi->SR & SPI_SR_BSY)); // wait until not busy
}
*/

// seems to conform closely with reference manual
void spi_send16(SPI_TypeDef *spi, uint16_t val)
{

	spi->CR1       |= SPI_CR1_SPE; // enable SPI
	spi->DR = val;
	while(!(spi->SR & SPI_SR_TXE)); // wait for transfer to finish
	//while(n-->0) {
	//	spi->DR = *data++; // wite 8/16 bits, depending on setup
	//	while(!(spi->SR & SPI_SR_TXE)); // wait for transfer to finish
	//}
	while((spi->SR & SPI_SR_BSY)); // wait until not busy
	spi->CR1       &= ~SPI_CR1_SPE; // disable SPI
}

void spi1_init(void)
{
	//gpio_out(CS);
	gpio_alt(CS,  5); // PA4 CS setup
	gpio_pullup(CS); // pull up CS (although you could use an external resistor instead)
	gpio_alt(GPIOA, 5, 5); // PA5 clk setup
	gpio_alt(GPIOA, 7, 5); // PA7 mosi setup

	RCC->APB2ENR	|= RCC_APB2ENR_SPI1EN; // enable SPI1 clock

	SPI1->CR1	|= SPI_CR1_MSTR // enable master mode
		//| SPI_CR1_SSM
 		//| SPI_CR1_SSI
		;
	//SPI1->CR1 |= 0b100 << 3; // baud d_pclk/32
	SPI1->CR1 |= SPI_CR1_DFF; // 16-bit mode (SPI must not be enabled)
	SPI1->CR2 |= SPI_CR2_SSOE; // SS output enabled
}


void zseg_send(uint8_t address, uint8_t value)
{
	//gpio_set(LED);
	//gpio_clr(CS);
	//uint8_t data[] = { address, value };
	uint16_t val =  ((uint16_t) address << 8) | value;
	//spi_sendn(SPI1, data, 2);
	spi_send16(SPI1, val);
	//gpio_set(CS);
	//gpio_clr(LED);
}


void zseg_init(void)
{
	zseg_send(0x0F, 0x00);
	zseg_send(0x09, 0xFF); // Enable mode B
	zseg_send(0x0A, 0x0F); // set intensity (page 9)
	zseg_send(0x0B, 0x07); // use all pins
	zseg_send(0x0C, 0x01); // Turn on chip
}

void zseg_show_count(int count)
{
	uint32_t num = count;
	for (uint8_t i = 0; i < 8; ++i)
	{
		uint8_t c = num % 10;
		num /= 10;
		uint8_t sep = 0; // thousands separator

		// add in thousands separators
		if ((i > 0) && (i % 3 == 0))
		{
			sep = 1 << 7;
		}

		// blank if end of number
		if ((c == 0) && (num == 0) && (i > 0))
		{
			sep = 0;
			c = 0b1111;
		}

		c |= sep;

		zseg_send(i + 1, c);
		//delay(1);
	}

}

int main (void) 
{   
	gpio_out(LED);
	spi1_init();
	zseg_init();
	int count = 0;
	while (1) 
	{
		zseg_show_count(1*count++);
		delayish(1000);
		delayish(1);
	}
}
