//#define F_CPU 8000000UL

#include <avr/io.h>
//#include <util/delay.h>

#include <gpio.h> // my HAL
#include <delay.h>


#define BIT3 GPIOA, (1<<0) // 
#define BIT2 GPIOA, (1<<1) // 
#define BIT1 GPIOA, (1<<2) // 
#define BIT0 GPIOA, (1<<3) // PA0 . Physical pin 13



int main()
{
	gpio_out(BIT0);
	gpio_out(BIT1);
	gpio_out(BIT2);
	gpio_out(BIT3);

	uint8_t count = 0;
	for(;;) {
		gpio_put(BIT0, count & 1);
		gpio_put(BIT1, count & 0b10);
		gpio_put(BIT2, count & 0b100);
		gpio_put(BIT3, count & 0b1000);
		count++;
		delayish(10000);
	}
}
