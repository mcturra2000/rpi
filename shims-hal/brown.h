/*
 * brown.h
 *
 *  Created on: Dec 23, 2023
 *      Author: pi
 */

#ifndef INC_BROWN_H_
#define INC_BROWN_H_

#include <shims.h>

extern uint32_t brown_pwm_took;
void brown_init();

#endif /* INC_BROWN_H_ */
