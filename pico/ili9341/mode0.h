#pragma once

#include <stdint.h>

#ifdef __cplusplus
    extern "C" {
#endif


// alows you set your own custom filler
typedef void(* ili3941_fill_handler_t)(uint16_t*, int);
void ili9341_set_filler(ili3941_fill_handler_t handler);


// ARNE-16 palette converted to RGB565 -- https://lospec.com/palette-list/arne-16
typedef enum {
    MODE0_BLACK,
    MODE0_BROWN,
    MODE0_RED,
    MODE0_BLUSH,
    MODE0_GRAY,
    MODE0_DESERT,
    MODE0_ORANGE,
    MODE0_YELLOW,
    MODE0_WHITE,
    MODE0_MIDNIGHT,
    MODE0_DARK_SLATE_GRAY,
    MODE0_GREEN,
    MODE0_YELLOW_GREEN,
    MODE0_BLUE,
    MODE0_PICTON_BLUE,
    MODE0_PALE_BLUE
} mode0_color_t;

#define TEXT_HEIGHT 24
#define TEXT_WIDTH 53
extern const uint8_t font_data[95][12];
extern mode0_color_t screen_bg_color;
extern mode0_color_t screen_fg_color;
extern int cursor_x;
extern int cursor_y;
extern uint8_t screen[TEXT_HEIGHT * TEXT_WIDTH];
extern uint8_t colors[TEXT_HEIGHT * TEXT_WIDTH];
extern uint8_t show_cursor;
extern uint16_t palette[16];


void mode0_clear(mode0_color_t color);
void mode0_draw_screen();
void mode0_draw_region(uint8_t x, uint8_t y, uint8_t width, uint8_t height);
void mode0_fill_slice(uint16_t *buffer, int slice);
void mode0_scroll_vertical(int8_t amount);
void mode0_set_foreground(mode0_color_t color);
void mode0_set_background(mode0_color_t color);
void mode0_set_cursor(uint8_t x, uint8_t y);
uint8_t mode0_get_cursor_x();
uint8_t mode0_get_cursor_y();
void mode0_print(const char *s);
void mode0_write(const char *s, int len);
void mode0_putc(char c);
void mode0_show_cursor();
void mode0_hide_cursor();

// Won't redraw until the matching _end is invoked.
void mode0_begin();
void mode0_end();

void mode0_refresh_screen();
void ili9341_start_dma(void *buffer, int bytes);
bool ili9341_is_busy();
void ili9341_print(const char *format, ...);
void ili9341_println(const char *format, ...);
void ili9341_printchar (char c);


#ifdef __cplusplus
    }
#endif

