# gfx-hat

gfx-hat contains the following components:
* cap1186 - capacitive buttons and leds
* sn3218  - backlights to illuminate LCD
* st7567  - LCD


## See also

* p63 - notes on getting things working
* [Adafruit_CAP1188_Library](https://github.com/adafruit/Adafruit_CAP1188_Library/blob/master/Adafruit_CAP1188.h)
* [CAP1188 interfacing](https://electropeak.com/learn/interfacing-cap1188-8-key-capacitive-touch-sensor-with-arduino/)
* [pimoroni/gfx-hat](https://github.com/pimoroni/gfx-hat/blob/master/library/gfxhat/st7567.py) - st7567.py
* [pinout](https://pinout.xyz/pinout/gfx_hat)



## Status

2024-06-10	Started. Seems to work pretty well
