//#include "basal.h"
#include <delay.h>


// Assumes clock speed of 8MHz
void delayish(int ms)
{
	for(int j = 0; j < ms; j++) {
		for(int i = 0; i< 157; i++) nop(); // about 1ms
	}
}

void delayish_us(uint8_t n)
{
	for(uint8_t i = 0; i < n; i++) nop();
}
