#include "../../exports.h"

#define LED 17



// base: whereabouts the prog is located 
// not sure if attribute is necessary now that we've specified an entry point
int  __attribute__ ((section (".text.prog")))  prog()
{
	pl_gpio_out(LED);

	// blink 5 times, then exit
	for(int i = 0; i<5; i++) {
		pl_gpio_set(LED);
		pl_sleep_ms(100);
		pl_gpio_clr(LED);
		pl_sleep_ms(900);
	}
	
	return 0;
}
