#pragma once

#include <shims.h>
#include <hardware/spi.h>

#ifdef __cplusplus
extern "C" {
#endif  




typedef struct {
	spi_inst_t *hspi;

	// for CS
	//GPIO_TypeDef* GPIOx;
	uint16_t GPIO_Pin;
} spi_t;


void spi_send_bytes(spi_t *spi, const uint8_t *data, size_t len);

#ifdef __cplusplus
}
#endif

