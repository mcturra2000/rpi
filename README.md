# rpi
My Arduino, ATTiny85, ESP8266/32, RPi (Raspberry Pi), RP2040, STM8 and STM32 projects

**Important** From 2022-04, I am going to try to unify the repo a little.
You should therefore set up an `RPI` environment variable to point to the 
root of this repo. E.g. add the following to `.bashrc`:
```
export RPI=$HOME/repos/rpi
```

2022-05-20 The project migrated from GitHub to GitLab


## Best practices

From best to worst.

* SPI on linux (v. incomplete) gfx-hat	- 2024-06-14
* I2C: rpi/i2c.c/h      - 2024-06-12
* gfx-hat for Makefile  - 2024-06-12


## General

* [audio](audio)
* [circuits](circuits.md) - various electrical circuits
* [linking](linking)
* [logic analyser](logic-analyser.md)
* [serial](serial)
* [software](software.md)
* [Twiddle-factor FFT for mcus](https://blog.podkalicki.com/twiddle-factor-based-fft-for-microcontrollers/)


## IDE

* [IDE](Arduino-IDE.md) : general setup, particularly about ATTiny85 setup.
* [platformio](platformio.md)


## Other repos

* pwm-blackpill - in STM32CubeIDE/workspace\_1.5.0


## Projects

* [0clock](0clock): nano, ds3231, zeroseg clock with timer
* [abaclock](abaclock): clock made with a RTC clock and an Arduino LED backpack
* [crunky](crunky): start of a unikernel for Raspberry Pi 3
* [dev01](dev01): meditation timer
* [dev02](dev02): projects for the DEV02 home-made board: green\_red and mhz85
* [dev03](dev03): esp32 clock
* [dev04](dev04): arduino nano clock (uses arduino-cli)
* [dev05](dev05): ATTiny85 programmer for the Pi
* [dev06](dev06): tiny xmas lights for ATtiny85
* [dev07](dev07): temperature display
* [dev08](dev08): 555 fixed tone generator
* [dev09](dev09): voltage generator. esp32 mcp4921 simplecli, timer interrupts
* [dev10](dev10): ds3231 clock getter and setter
* [dev11](dev11): ATtiny85 blink sketch
* [dev12](dev12): brown noise generator
* [dev13](dev13): stm32f401 toy (schematic: db10.6)
* [ds](ds): DS3231 setter/getter
* [minitone85](minitone85): working version of tone() for ATtiny85
* [mins30-85](mins30-85): 30 minute timer for ATTiny85, programmable from the Pi (`DEV01`). 
* [synth](synth): sound synthesizer/tone-maker


## Protocols

* [i2c](i2c): Arduino and RPi3 master, ATTiny85 slave
* [i2s](i2s)
* [spi](spi)
* [xmodem](xmodem.md)


## Hardware


* [Piezo sensors](https://www.ebay.co.uk/itm/360719728982?mkcid=16&mkevt=1&mkrid=711-127632-2357-0&ssspo=amaKdA7UR5e&sssrc=2047675&ssuid=Vx9wLz5rQfS&widget_ver=artemis&media=COPY)
* [SN74HCxx](sn74hcxx.md) chips
* [TZT RP2040](https://www.temu.com/uk/tzt-rp2040-zero-rp2040-for-raspberry-pi-microcontroller-pico-development-board-module-dual-core-cortex-m0-processor-2mb-flash-g-601099518844423.html?top_gallery_url=https%3A%2F%2Fimg.kwcdn.com%2Fproduct%2FFancyalgo%2FVirtualModelMatting%2F0e7441bcdb1d8e902e7b9d1fc8f86ece.jpg&spec_gallery_id=4004802154&_x_vst_scene=adg&_x_ads_sub_channel=shopping&_x_ns_prz_type=-1&_x_gmc_catalog=1085647&_x_ns_sku_id=17592226045156&_x_ads_channel=bing&_x_gmc_account=3429411&_x_ads_creative_id=82463741636229&_x_ns_device=c&_x_ads_account=176202708&_x_ns_match_type=e&_x_ns_msclkid=64cdd8a548881432d650bc18477de31d&_x_ads_set=519109389&_x_ns_source=s&_x_ads_id=1319416250382694&refer_page_name=kuiper&refer_page_id=13693_1690112006289_p7jq16b6e8&refer_page_sn=13693&_x_sessn_id=gefi4zibf9) Small chip on Temu site
* [VGA breakout board](https://www.aliexpress.com/item/32601693326.html)
* [Zseg](zseg) SPI - 8-digit 7-segment display using MAX7219

---
![](11b-ft232h/ft232h.jpg) [11b - ft232h](11b-ft232h) - Adafruit FTDI
---
![](1306/1306.jpg) [1306](1306) I2C. WROOM 


---

![](8x8/8x8.jpg) [8x8](8x8) I2C

---

![](audio.jpg) Audio Jack, stereo. G=GND, L=Left speaker, R=Right speaker, 1=unknown, 2=unknown.
Underside shown.

---

![](bme688/bme688.jpg) [bme688](bme688) air quality breakout. 4-in-1

---

![](doglcd/doglcd.jpg) [doglcd](doglcd) quasi-SPI. NANO/PICO/STM32/WROOM

--- 

![](drh08/drh08.jpg) [drh08](drh08) drum hat

---

![](gfx-hat/gfx-hat.jpg) [gfx-hat](gfx-hat)

---

![](gps/gps.jpg) [gps](gps)

---

![](mcp4921/mcp4921.jpg) [mcp4921](mcp4921) 12-bit DAC. SPI

---

![](mike.jpg) [mike](https://alexnld.com/product/5pcs-microphone-sound-sensor-module-voice-sensor-high-sensitivity-sound-detection-module-whistle-module-for-arduino/) sound sensor(?), rather than an amplifier per se

---

![](sdcard-spi.jpg) [sdcard-spi](sdcard-spi)

---

![](tm1637.jpg) [tm1637](tm1637.md)

## Links to other sites

* [level-shifting without components](https://mcturra2000.wordpress.com/2021/01/08/arduino-and-raspberrypi-componentless-level-shifting/)

