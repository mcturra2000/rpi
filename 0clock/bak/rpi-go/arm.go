// +build arm

package main


import (
	"log"
	"fmt"
	"image"
	"time"

	"periph.io/x/conn/i2c"
	"periph.io/x/conn/i2c/i2creg"
	"periph.io/x/devices/ssd1306"
	"periph.io/x/devices/ssd1306/image1bit"
	//"periph.io/x/host"
	"github.com/nfnt/resize"

	//<F7>  "golang.org/x/image/font"
	"golang.org/x/image/font"
	"golang.org/x/image/font/basicfont"
	//"golang.org/x/image/font/gofont/gomono"
	"golang.org/x/image/math/fixed"

	"periph.io/x/conn/gpio"
	"periph.io/x/host"
	"periph.io/x/host/rpi"
)



//var buz_pin = gpioreg.ByName("GPIO16");
var buzzer =  rpi.P1_36 // BCM16

func InitPlatform() {
	if _, err := host.Init(); err != nil { log.Fatal(err) }
	initDisplay()
	buzzer.Out(gpio.Low)
	button.In(gpio.PullUp, gpio.NoEdge)
}


func BuzzOn() { buzzer.Out(gpio.High) }
func BuzzOff() { buzzer.Out(gpio.Low) }


var button = rpi.P1_16 // BCM23

func Debounce() {
	count := 0
	prev := 0
	max := 8
	press_num := 0
	for {
		if button.Read() {
			count++
			if count > max { count = max }
		} else {
			count--
			if count < 0 { count = 0 }
		}
		if count == 0 && prev != count {
			//fmt.Println("Debounce: press detected num", press_num)
			ButtonPressed()
			press_num++
		}
		prev = count
		time.Sleep(4* time. Millisecond)
	}
}


//var img *image1bit.VerticalLSB
var dev *ssd1306.Dev
//var drawer font.Drawer
var bus i2c.BusCloser

func initDisplay() {
	var err error
	bus, err = i2creg.Open("")
	if err != nil { log.Fatal(err) }
	var opts = ssd1306.DefaultOpts;
	opts.H = 32;
	opts.Sequential = true; // fix for half the pixels missing

	dev, err = ssd1306.NewI2C(bus, &opts)
	if err != nil { log.Fatal(err) }

}

func Displaystr(str string) {
	img := image1bit.NewVerticalLSB(dev.Bounds())
	f := basicfont.Face7x13
	drawer := font.Drawer{
		Dst:  img,
		Src:  &image.Uniform{image1bit.On},
		Face: f,
		//Dot:  fixed.P(0, img.Bounds().Dy()-1-f.Descent),
		Dot:  fixed.P(0, 9),

	}
	drawer.Dot = fixed.P(0, 9)
	drawer.DrawString(str)
	img1 := resize.Resize(448, 108, img, resize.Lanczos3)

	if err := dev.Draw(dev.Bounds(), img1, image.Point{}); err != nil {
		log.Fatal(err)
	}

}

func DisplayDeinit() {
	fmt.Println("DisplayDeinit:called");
	bus.Close()
}
