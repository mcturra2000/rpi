#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
//#include "hardware/adc.h"
//#include "hardware/clocks.h"
//#include "hardware/flash.h"
#include "hardware/gpio.h"
//#include "hardware/irq.h"
//#include "hardware/pwm.h"
//#include "hardware/spi.h"
// #include "tusb.h" // if you want to use tud_cdc_connected()

#include <ili9341.h>
#include <FreeRTOS.h>
#include <task.h>

#include "pluto.h"
#include "exports.h"

#include "pi.h"

#include <stdarg.h>

void hi(int dummy)
{
	println("This is a test function which says 'hi'");
}

static void gpio_set(uint gpio) { gpio_put(gpio, 1); }

static void ili9341_reset_filler() {
	ili9341_set_filler(&mode0_fill_slice); // go back to console screen mode
}

const pl_exports_t __attribute__((section(".pluto_exports")))  pl_exports = {
	.magic = 0xbaada555, 
	.version = 0,
	.hi = (fptr) hi,
	.memcpy = (fptr) memcpy,
	.ili9341_set_filler = (fptr) ili9341_set_filler,
	.gpio_out = (fptr) pi_gpio_out,
	.gpio_set = (fptr) gpio_set,
	.gpio_clr= (fptr) pi_gpio_clr,
	.sleep_ms = (fptr) sleep_ms,
	.ili9341_reset_filler = (fptr) ili9341_reset_filler 
};



void populate_exports ()
{
/*
	//extern void cmd_hi();
	exports.hi = (fptr) &hi;
	exports.memcpy = (fptr) &memcpy;
	exports.ili9341_set_filler = (fptr) &ili9341_set_filler;
	exports.gpio_out = (fptr) &pi_gpio_out;
	exports.gpio_set = (fptr) &pi_gpio_set;
	exports.gpio_clr = (fptr) &pi_gpio_clr;
	exports.sleep_ms = (fptr) &sleep_ms;
	*/
//#define pl_hi() ((fptr) ((pl_exports_t *)10000200)->hi) ()
	//fptr fn = ((pl_exports_t *)0x10000200)->hi;
	//fptr fn1 =  (pl_exports_t *)(10000200 + 0x8);
	//((fptr) *(&pl_exports.hi)) ();
	//fn();
#if 0
	pl_hi();
	pl_gpio_out(17);
	pl_gpio_set(17);
#endif
}

/* FN screen_task */
void screen_task ()
{
	mode0_refresh_screen();
}

// update the peripherals
void peri_task ()
{
	for(;;) {
		kbd_task();
		screen_task();
		//screen_task();
		//screen_task();
		vTaskDelay(1);
	}
}

//void cmd_task();

TaskHandle_t cmd_hnd = NULL;
TaskHandle_t peri_hnd = NULL;



int main () 
{
	//stdio_init_all();
	// while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 

	kbd_init();
	stdio_init_all();
	//usb_device_init();
	printf("\n=========================\n79-pluto \n");

	//xSemaphoreCreateMutexStatic(&input_buffer_mtx);
	ili9341_init();
	mode0_set_foreground(MODE0_ORANGE);
	mode0_draw_screen();

	//	systick_init();


	//extern int __binary_info_end;
	//print("binary info end: %x\n", &__binary_info_end);
	//extern int __data_end__;
	//print("data end: %x\n", &__data_end__);
	extern int __flash_binary_end;
	print("__flash_binary_end: %x\n", &__flash_binary_end);

	populate_exports();

	fat32_init();
	fat32_type_partition_table();
	fat32_list_root();

	//pl_gpio_out(17);
	//pl_gpio_set(17);

	xTaskCreate(peri_task, "PERI", 512, NULL, 1, &peri_hnd);
	xTaskCreate(cmd_task, "CMD", 512, NULL, 0, &cmd_hnd);
	vTaskStartScheduler();

}


//int  __attribute__ ((section (".pluto_export")))  prog(struct kexports *exports, uint32_t base)
