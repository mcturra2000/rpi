#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
//#include "hardware/adc.h"
//#include "hardware/clocks.h"
//#include "hardware/flash.h"
#include "hardware/gpio.h"
//#include "hardware/irq.h"
//#include "hardware/pwm.h"
//#include "hardware/spi.h"
#if LIB_PICO_STDIO_USB
#pragma message "USB configured as stdio"
#include "tusb.h" // if you want to use tud_cdc_connected()
#endif


#include "pi.h"

#define TX	8
#if TX==0
#define UART uart0
#endif
#if TX==8
#define UART uart1
#endif

int main() 
{
#if 0
	stdio_init_all();
#if LIB_PICO_STDIO_USB
        while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 
#endif
#endif


	uart_init(UART, 115200);
	//gpio_pull_up(TX); // 2023-01-25. Untested, but I think this is uncessary
	gpio_set_function(TX, GPIO_FUNC_UART);

	int count = 0;
	//uint8_t val = 0b01010101;
	char msg[256];
	for(;;) {
		sprintf(msg, "\033[J\033[H%d", count++);
		//sprintf(msg, "%d", count++);
		uart_write_blocking(UART, msg, strlen(msg));
		sleep_ms(1000);
	}

	return 0;
}

