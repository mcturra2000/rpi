--title RP2040 drum machine test

--date April 2023

--newpage

--heading What

---
This is a project that is a work-in-progress. 
---
I wanted to create a drum machine  controlled over the serial port.
---
The hardware is simple enough: a Pico microcontroller, an output to stereo, an LED to indicate drum activity, and another RP2040 used as a serial port and debugger.
---
Let's listen to a demonstration ...

--newpage

--heading mkdata

A drum sample can be converted into a raw binary, like so:
---
sox  $HOME/Music/2022/snare.wav -c 1 -r 44.1k -e unsigned -b 8 drum.raw

--newpage

--heading Embed data

That binary must be turned into source. 

I use some assembler, "blob.S":
---
        .section .rodata
        .global blob
        .balign 4       
blob:
        .incbin "../drum.raw"
        .global blob_end
blob_end:
        .global blob_size
        .balign 4
blob_size:
        .int blob_end - blob

--newpage

--heading main.cc highlights

In the main() function, I set up PWM whose value is set by an IRQ handler. I use a sampling frequency of 44.1kHz.

Sending anything to UART triggers the drum:
---
  for(;;) {
                if(uart_is_readable(uart0)) {
                        uart_getc(uart0);
                        start = true;
                }
        }

--newpage

--heading the IRQ

Here are the most important parts of the IRQ:
---
void my_pwm_wrap_isr()
{
        if(start) {
                start = false;
                led.on();
                i = 0;
        }

        if(-1 < i && i< LEN) {
                u16 v = TRACK[i++];
                pwm_set_gpio_level(SPK, v);
        } else
                led.off();

        pwm_clear_irq(slice_num); 
}

--newpage
--heading Linux control code

The drum beating is controlled via a serial port. termios is used to do the setup:
---
       struct termios tio;

        const char device[] = "/dev/ttyACM0";
        int fd = open(device, O_RDWR | O_NOCTTY | O_NDELAY);

        tcgetattr(fd, &tio);
        tio.c_lflag &= ~ICANON; // disable canonical mode
        cfsetispeed(&tio, B115200);
        cfsetospeed(&tio, B115200);
        tcsetattr(fd, TCSANOW, &tio);

--newpage
--heading Control loop

The drum beat is specified using a simple encoding:
---
        char beat[] = "!!! ";
"!" starts the beat, Space skips a beat.
---
Beats are processed using a simple loop:
        while(1) {
                for(int i = 0 ; i < sizeof(beat); i++) {
                        char c = 1;
                        if(beat[i] == '!') 
                                write(fd, &c, 1);
                        usleep(500'000);
                }
        }

--newpage
--heading Parting words

I hope you like this video. As a next step, I intend to incorporate more drums. This should be reasonably straightforward now that I have something basic working.
---
A link to the source file is given in the description to this video.
---
If you would like more detailed instructions, then leave a comment, and I will endeavour to flesh out the details.
---
BYE

 
