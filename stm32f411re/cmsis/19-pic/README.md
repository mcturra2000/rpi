# create position independent code


## Write-up

To see everything an action, type:
```
make upload
```

The software comes in two parts:
* a "kernel" (main.c) - which calls a "user program"
* a "user program" (prog.c) - which does what you want it to. In this case, it does blinking

You need to decide where to put the program. I chose address 0x8004000, which lies above the kernel code,
which is stored starting from address 0x8000198, just beyond the interrupt vector table.

main.c defines a function typedef for a function that takes no arguments and returns no result:
```
typedef void (*fptr)(void); // takes no args, returns void
```
It defines a function pointer like so, and then calls it:
```
        fptr fn = (fptr)0x8004001;
	fn();
```
Note that address 0x8004001 is called instead of 0x8004000. This is due to the way that Arm interprets
the mode of the function: either as regular Arm instructions taking 32 bits, or thumb instructions
taking 16 bits. The way it decides is if there is a 1 in the LSB. All instructions must start on 
even-numbered addresses. Although Cortex-M MCUs only support thumb mode, it is still necessary
to honour the convention.

prog.c blinks an LED via a loop(). Auxuliiary functions have been copied to facilitate this. A better
idea would be to use functions already defined in the kernel. This would mean creating an objdump
to find the addresses of functions to call. Note that the addresses will be invalidated if the kernel
is changed. An alternative would be to store useful functions into a structure at a defined location,
in the same way as the ISRs are stored.

It is vital that loop() is stored at address 0x8004000 (or other some specified location). Otherwise
the kernel will be executing some other arbitrary functions, which is almost certainly the wrong thing.

Here's the linker script for the program:
```
MEMORY
{
        PROG   (rx)    : ORIGIN = 0x000000,   LENGTH = 512K
}

SECTIONS
{
        .text :
        {
                . = ALIGN(4);
                *(.text.loop)
                *(.text*)
        } >PROG
}
```

The ORIGIN doesn't really matter. It might as well be 0 because it is going to be placed somewhere else
anyway. We want PIC (Position-Independent Code) anyway.

When GCC compiles the source program, it puts each function into thie own sections. gpio\_set() goes
into section ".text.gpio\_set", gpio\_clr() goes into ".text.gpio\_clr", etc. loop() goes into 
".text.loop", Crucially, we want to ensure that loop() goes to the top of the binary, which is why
the link script has
```
                *(.text.loop)
```
at the beginning. The other .text entries can go in after.

Here are relevant snippets from the Makefile:
```
prog.s : prog.c
        $(CC) $(CFLAGS) -S $< -o $@

prog.o : prog.s
        $(CC) -c $<

prog.bin : prog.o
        $(LD) -T prog.ld -o prog.elf prog.o 
        $(OBJCOPY) -O binary prog.elf prog.bin
        $(OBJDUMP) -D prog.elf > prog.dis

.upload : app.bin prog.bin
        touch .upload

upload : .upload
        st-flash --connect-under-reset write app.bin 0x8000000
        st-flash --connect-under-reset write prog.bin 0x8004000
```

The kernel is compiled in a regular way for my CMSIS projects. The program requires additional work,
turning it into a raw binary that is flashed to the MCU.

Note that the upload occurs in two separate stages: once for the kernel (app.bin) and once for
the program (prog.bin). 

Things have been set up so that you only need to compile and flash the kernel once (assuming it
never changes, of course). It is only the program that you would need to change. So normally
you would only need to perform the second st-flash. 

Flash usually works via "sectors" and "pages". A sector consists of many pages. Sector and page
size will depend on the MCU in question. You need to erase a sector before writing pages into it.
That's just how it works. It means, therefore, that you cannot just flash to arbitrary address.
You need to respect sector boundaries. st-flash will tell when such conditions are violated.




## Status

2023-02-05	Started. Works
