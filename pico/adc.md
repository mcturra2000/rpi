# ADC
Created 2022-07-29

A tip to reduce noise on ADC is to pull GP23 high:
```
pinMode(23, OUTPUT);
digitalWrite(23, HIGH);
```
[YouTube video](https://www.youtube.com/watch?v=F0o1C7nEgw0) (esp. 7m52s)
Equivalently:
```
gpio_set_dir(23, GPIO_OUT);
gpio_put(23, 1);
```




## Projects

* [10-adc](10-adc)
* [72-adc-pwm](72-adc-pwm) - control pwm signal using ADC. It quantises the input to musical notes rather than play in a continuous range. 
* [mcp3008](mcp3008) - using an external ADC, which provides 8x 10-bit ADCs.
