# quit without confirmation
define hook-quit
    set confirm off 
end

set pagination off
file build/dev14-f411.elf
#file build/dev14-f401.elf
#target remote localhost:3333
target extended-remote localhost:3333

# 2023-09-11 added (7f2)
monitor reset halt 

load
#b app.c:126
#b app_rtc_irq
#b HAL_RTC_AlarmAEventCallback
#b brown_init
#b TIM3_IRQHandler
#b usecs.c:30
#b isbst_tm
#b rtc_get_uk_tm
# b HAL_RTC_SetDate
#b app.c:84
#b strftime
#b MX_GPIO_Init
#b assert_failed
#b main
#monitor reset init # 2023-09-11 removed
echo RUNNING...\n
c
