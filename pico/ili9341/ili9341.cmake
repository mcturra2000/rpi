#cmake_minimum_required(VERSION 3.12)
#project(ili9341)
#include("../picomc.cmake")
set(LIBRARY_NAME ili9341)

set(ENV{ILI9341} $ENV{PICOMC}/ili9341)
#set(ENV{ILI9341} /home/pi/repos/rpi/pico/ili9341)
add_library(${LIBRARY_NAME} INTERFACE)
target_sources(${LIBRARY_NAME} INTERFACE
	$ENV{ILI9341}/mode0.c
	#$ENV{ILI9341}/mode1.c
	#$ENV{ILI9341}/mode2.c
	$ENV{ILI9341}/font.c
	$ENV{ILI9341}/ili9341.c
  #${CMAKE_CURRENT_LIST_DIR}/${LIBRARY_NAME}.cpp
)
target_include_directories(${LIBRARY_NAME} INTERFACE
  #${CMAKE_CURRENT_LIST_DIR}/include
  #${CMAKE_CURRENT_LIST_DIR}
  $ENV{ILI9341}
)
target_link_libraries(${LIBRARY_NAME} INTERFACE
  #hardware_i2c
)
