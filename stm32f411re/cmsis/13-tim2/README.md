# TIM2

Experimenting with timer interrupts

We replicate a 440Hz square-wave with 25% duty cycle using a sampling rate of 40kHz.


## See also

* db5.343 - writeup


## Status

2023-02-07	Confirmed working, although it does seem to have quirks

2021-12-17	Started. Works
