#include "hardware/adc.h"
#include "pico/stdlib.h"
#include "ssd1306.h"
#include <stdio.h>
#include "pi.h"

typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

#define BTN 17
#define ADC 26

#define WEAK __attribute__((weak))

void WEAK _vdu_init();
void WEAK _vdu_print(char *str);

#if 1

void _vdu_init()
{
	pi_stdio_init();
}

void _vdu_print(char *str) 
{
	printf(str);
}

#else

/*
void i2c_ship(uint32_t i2c, uint8_t addr, const uint8_t *w, size_t wn)
{
	i2c_write_blocking(i2c0, addr, w, wn, false);
}
*/

void _vdu_init()
{
	int h = 32;
	if(1) h = 64;
	pi_i2c_init(4);
	i2c_set_baudrate(&i2c0_inst, 400000);
	init_display(h, 4);
	fill_scr(0); // empty the screen
}

void _vdu_print(char *str) 
{ 
	ssd1306_print(str);
	show_scr();
}
#endif


void vdu_init() { if(_vdu_init) _vdu_init(); }


void vdu_print(char *str) { if(_vdu_print) _vdu_print(str); }

int main()
{
	vdu_init();

	pi_gpio_init(BTN, INPUT_PULLUP);

	adc_init();
	adc_gpio_init(ADC);
	adc_select_input(0); // for gpio26

	for(;;) {
		u16 val = adc_read();
		char msg[100];
		sprintf(msg, "%d\n", val);
		vdu_print(msg);
		delayish(1000);
		while(pi_gpio_is_low(BTN));
	}

	return 0;
}
