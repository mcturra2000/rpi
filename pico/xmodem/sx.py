# 2023-01-25 Works. As does upload

import serial
from xmodem import XMODEM # pip install xmodem
ser = serial.Serial('/dev/ttyACM0', 115200) # timeout = 0);
def getc(size, timeout=1):
	return ser.read(size) or None

def putc(data, timeout=1):
	return ser.write(data)

modem = XMODEM(getc, putc)

stream = open('main.c', 'rb')
modem.send(stream)


