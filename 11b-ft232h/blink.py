# need to call the collowing from command line before invoking python
# export BLINKA_FT232H=1
from time import sleep
from pyftdi.ftdi import Ftdi

#Ftdi().open_from_url('ftdi:///?')

import board
import digitalio
led = digitalio.DigitalInOut(board.C0)
led.direction = digitalio.Direction.OUTPUT

while True:
	led.value = True
	sleep(0.5)
	led.value = False
	sleep(0.5)

