#pragma once

/* For a demo, see stm32f411re/libopencm3/18-debounce
*/

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#if 0
typedef struct {
	int gpio;
	uint8_t integrator;
	bool falling;
	bool rising;
} debounce_t;
#endif

typedef struct {
	unsigned int init: 1, en : 1, 
		     seek_high :1 ,
		     //ack_falling : 1, ack_rising:1, 
		     falling:1, rising:1, // prev:8, 
		     count:8;
} deb_t;

void deb_update(deb_t *deb, int high);
bool deb_falling(deb_t* deb);
bool deb_rising(deb_t* deb);
//void debounce_init(debounce_t* deb, uint gpio, uint delay);
//bool debounce_falling(debounce_t* deb);
//bool debounce_rising(debounce_t* deb);

#ifdef __cplusplus
}
#endif

