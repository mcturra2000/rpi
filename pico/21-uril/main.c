//#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "uril.h"

//#include "pi.h"


char buf_copy[132];
bool line_end = false;
int line_len = 0;

void uril_eol()
{
	memcpy(buf_copy, uril_buf, uril_len);
	line_end = true;
	line_len = uril_len;
}


int main() 
{
	//pi_stdio_init();
	uril_init();
	gpio_set_function(0, GPIO_FUNC_UART); // TX
	uart_puts(uart0, "21: Echo text a line at a time\r\n");


	//int i = 0;
	for(;;) {
		if(!line_end) continue;
		uril_pause();
		line_end = false;
		uart_write_blocking(uart0, buf_copy, line_len);
		uril_resume();
		uart_write_blocking(uart0, "\r\n", 2);
	}

	return 0;
}

