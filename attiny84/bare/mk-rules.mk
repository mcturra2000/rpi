CXX = avr-g++
CXXFLAGS = -g -Os -Wall -mcall-prologues -mmcu=attiny84
CXXFLAGS += -I..
VPATH += ..
BASE=app
HEX = $(BASE).hex
MCU=attiny84
#AVRDUDEMCU=t84
ELF = $(BASE).elf

OBJS += main.o delay.o

.PHONY: install clean

$(HEX) : $(ELF)
	avr-objcopy -R .eeprom -O ihex $^ $@

$(ELF): $(OBJS)
	$(CXX) -g -mmcu=$(MCU) $^ -o $@ 
	avr-objdump -d $(ELF) >app.txt

.o :.cc
	$(CXX) -c $^ 


.flash : app.hex
	touch .flash

flash : .flash
	install-hex -t 84 -f app.hex

clean :
	rm -f *.hex *.obj *.o *.elf
