/* https://stackoverflow.com/questions/15594988/objcopy-prepends-directory-pathname-to-symbol-name */

	.section .rodata

	.global crown
	.balign 4	
crown:
	.incbin "/home/pi/assets/crown.raw"

	.global flag
	.balign 4
flag:
	.incbin "/home/pi/assets/flag.raw"
