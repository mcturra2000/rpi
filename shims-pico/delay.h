#pragma once
//#include <shims.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif  



void delay(uint32_t ms);
uint32_t millis();
void delay_us(uint64_t us);
//#define delay_ms delay
//#define sleep_us delay_us
#define delay sleep_ms

#ifdef __cplusplus
}       
#endif


