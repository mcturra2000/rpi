#include <stdio.h>
#include <string.h>

#include "pi.h"

int main() 
{
	// debug enable fix
	timer_hw->dbgpause = 0; // https://github.com/raspberrypi/pico-sdk/issues/1152
	pi_stdio_init();

	puts("hello.  Starting off in echo mode");
	puts("^e toggles between echo and inspect mode\n");
	puts("In inspect mode:");
	puts("'n' or 'l' to emit LF(\\n,^J)");
	puts("'r' to emit a CR(\\r,^M),");
	int echo_mode = 1;
	for(;;) {
		int c = getchar();
		if(c == 5) echo_mode = 1 - echo_mode; // ^e
		if(echo_mode) {
			putchar(c);
		} else {						      
			switch(c) {			
			case 'l':	// fallthrough
			case 'n':	putchar('\n'); break;
			case 'r':	putchar('\r'); break;
			case '\n': 	printf("LF(\\n) 10, "); break;
			case '\r':	printf("CR(\\r) 13, "); break;
			default: 	printf("%c %d, ", c, c);
			}
		}
	}

	return 0;
}

