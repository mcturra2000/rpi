/*
 * circ.c
 * 2025-01-21	consider deprecated in favour of usb.c
 * 2024-07-23	mcarter	more functions
 * 2023-11-08	mcarter created
 */

#include <shims.h>

#include "circ.h"

#define LEN 64

static uint8_t cb_data[LEN];
int cb_read_index = 0;
int cb_write_index = 0;
int cb_size = 0;

// will return -1 if nothing avail
int circ_get()
{
	uint8_t value;
	if(circ_read(&value))
		return value;
	else
		return -1;
}
void circ_writen(uint8_t *buf, uint32_t len)
{
	for(int i = 0; i < len; i++) circ_write(buf[i]);

}
bool circ_write(uint8_t value)
{
	if (cb_size == LEN)
		return false;
	cb_data[cb_write_index] = value;
	if (++cb_write_index == LEN)
		cb_write_index = 0;
	cb_size++;
	return true;
}

static bool _circ_read(uint8_t *value)
{
	*value = 0;
	if (cb_size == 0)
		return false;
	*value = cb_data[cb_read_index];
	if (++cb_read_index == LEN)
		cb_read_index = 0;
	cb_size--;
	return true;
}

bool circ_read(uint8_t *value)
{
	HAL_NVIC_DisableIRQ(OTG_FS_IRQn);
	bool status = _circ_read(value);
	HAL_NVIC_EnableIRQ(OTG_FS_IRQn);
	return status;
}

bool circ_avail()
{
	return cb_size > 0;
}

