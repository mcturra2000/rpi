//#include "stm32f4xx_hal.h"
//#include "main.h"
#include <pot/zseg.h>

static spi_t* zspi;


static int zseg_is_inverted = 0;

// 1 for invert, 0 for normal
void zseg_invert(int yes)
{
	zseg_is_inverted = yes;
}


/* set the decode mode 0x09 followed by
   0x00 - no decode for digits 7-0
   0xFF - code B decode for digits 7-0

   Code B are the numbers 0-9-EHLP␣
   */

void zseg_setmode(enum zseg_mode mode)
{
	zseg_tfr(0x09, mode);
}


// FN zseg_show_date 
void zseg_show_date (struct tm *t)
{
	uint32_t val = (uint32_t) t->tm_mday * 1000000 + t->tm_hour*100 + t->tm_min;
	uint8_t mask = t->tm_hour <10 ? 0b11000111 : 0b11001111;
	if(t->tm_mday < 10) mask &= ~0b10000000;
	int thou = 0b100;
	if(zseg_is_inverted) thou = 0b10;
	zseg_show_fixed_number(val, mask, thou);
}

void zseg_show_hhmmss_tm(struct tm *t)
{
	zseg_show_hhmmss(t->tm_hour, t->tm_min, t->tm_sec);
}
void zseg_show_ms_as_mmss(uint32_t ms)
{
	uint32_t secs = ms/1000;
	uint32_t mins = secs / 60;
	secs = secs % 60;
	int thou = 0b100;
	if(zseg_is_inverted) thou = 0b10;
	zseg_show_fixed_number(mins*100+secs, 0b1111, thou);
}

void zseg_show_hhmmss(int hours, int mins, int secs)
{
	uint8_t led_mask = 0b111111;
	if(hours<10) led_mask &= ~0b100000; 
	zseg_show_fixed_number((hours*100 + mins)*100+secs, led_mask, 0b10100);
}

void zseg_tfr(uint8_t address, uint8_t value)
{
	uint8_t data[2] = {address, value};
	spi_send_bytes(zspi, data, 2);
}


// i=0 => right-most
// digit < 0 or = 10 => blank
void zseg_set_digit(int i, int digit, int dot)
{
	static int layout[11] = {
		0b1111110, // 0
		0b0110000, // 1
		0b1101101, // 2
		0b1111001, // 3
		0b0110011, // 4
		0b1011011, // 5
		0b1011111, // 6
		0b1110000, // 7
		0b1111111, // 8
		0b1111011, // 9
		0b0000000 // 10
	};



	if(digit < 0 || digit > 9) digit = 10;
	int pattern = layout[digit];
	if(zseg_is_inverted)
		pattern = ((pattern >> 3) & 0b1110) // abc -> def
			| ((pattern & 0b1110) <<3 ) // def -> abc
			| (pattern & 1); //g
	if (dot&1) pattern |=  (1 << 7);

	if(zseg_is_inverted) i = 7 -i;
	zseg_tfr(i+1, pattern);

}

/* show a fixed number with fixed lighting and dot placements
 * num - number to display
 * lit - bit pattern determining which cells are on
 * dot - bit pattern determining which cells have dots on them
 * For lit and dot, MSB is the first cell, LSB is the last
 * Here "cell" means a 7-segment display cell
 *
 * Example: to show minutes and seconds, call
 *  zseg_show_fixed_number(num, 0b1111, 0b100);
 * You would need to convert mins and secs to a decimal beforehand, though.
 */
void zseg_show_fixed_number(uint32_t num, uint8_t lit, uint8_t dot)
{
	for(int i = 0; i< 8; i++) {
		uint32_t c = -1; // assume blank
		if(lit & 1) {
			c = num % 10;
			//if(dot & 1) c |= (1<<7);
		}
		num /= 10;
		lit >>= 1;
		zseg_set_digit(i, c, dot);
		dot >>= 1;
	}
}



void zseg_show_elapsed_and_hhmm(uint32_t secs, struct tm *t)
{
	uint32_t led_mask = 0b11111111;

	//uint32_t secs = om_secs();
	uint32_t mins = secs / 60;
	secs = secs % 60;
	uint32_t display_val = (mins * 100 + secs) * 10000; // timer value so far
	if (mins < 10) led_mask &= ~0b10000000;

	// add in the time
	display_val += t->tm_hour * 100 + t->tm_min;
	if (t->tm_hour < 10) led_mask &= ~0b1000;

	int thou = 0b00010000;
	if(zseg_is_inverted) thou >>=1;
	zseg_show_fixed_number(display_val, led_mask, thou);

	// zseg_show_ms_as_mmss();

}



void zseg_show_count(int count)
{
	int thou = 0b10001000;
	if(zseg_is_inverted) thou >>= 1;
	uint32_t num = count;
	for (uint8_t i = 0; i < 8; ++i)
	{
		uint8_t c = num % 10;
		num /= 10;

		//uint8_t dot = 0; // thousands separator

		// add in thousands separators
		/*
		   if ((i > 0) && (i % 3 == 0))
		   {
		//sep = 1 << 7;
		dot = 1;
		}
		*/

		// blank if end of number
		if ((c == 0) && (num == 0) && (i > 0))
		{
			//sep = 0;
			thou = 0;
			//c = 0b1111;
			c = -1;
		}

		//c |= sep;

		zseg_set_digit(i, c, thou);
		thou >>= 1;
		//delay(1);
	}

}

void zseg_init(spi_t *spi)
{
	zspi = spi;
	zseg_tfr(0x0F, 0x00);
	//zseg_tfr(0x09, 0xFF); // Enable mode B (as numbers) 25/2
	zseg_tfr(0x09, 0x00); // No decode for digits 7-0 . 25/2
	zseg_tfr(0x0A, 0x0F); // set intensity (page 9)
	zseg_tfr(0x0B, 0x07); // use all pins
	zseg_tfr(0x0C, 0x01); // Turn on chip
}


#if  defined(HAL_SPI_MODULE_ENABLED) // && defined(STM32F411xE)
void zseg_test_loop(void)
{
	extern SPI_HandleTypeDef hspi1;
	spi_t spi = {&hspi1, GPIOA, GPIO_PIN_15};
	zseg_init(&spi);
	zseg_invert(1);
	uint32_t count = 0;
	while(1) {
		zseg_show_count(count++);
		HAL_Delay(1000 / 100);
	}
}
#endif

// FN zseg_set_at 
// pos => 0 from left, assuming not inverted
void zseg_set_at(int pos, uint8_t val)
{
	if(zseg_is_inverted == 0) {
		zseg_tfr(pos+1, val);
		return;
	}

	int dot = val >> 7;

	val = ((val >> 3) & 0b1110) // abc -> def
		| ((val & 0b1110) <<3 ) // def -> abc
		| (val & 1); //g
	if (dot) val |=  (1 << 7);

	zseg_tfr(8-pos, val);
}
