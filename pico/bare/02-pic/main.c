/* 
 * You can skip stdint.h and see how GCC defines things by running
 * arm-none-eabi-gcc -dM -E - < /dev/null
 */
//#include <stdint.h>

#include "registers.h"

typedef __UINT32_TYPE__ uint32_t;
typedef __UINT32_TYPE__ u32;

#define LED 25

volatile extern u32 _user_prog_start;
volatile extern u32 __user_prog_end;

//volatile u32 prog[1000] __attribute__ ((section("user_prog"))) = {0xc0debace};
//volatile u32 prog[1000]  = {0xc0debace};

void delay(int n) // no particular timing
{
	for(int i =0 ; i< n; i++) {
		for(int j = 0; j< 1000; j++) {
			asm volatile ("nop");
		}
	}
}



void gpio_set(u32 pin)
{	
	SIO_GPIO_OUT_SET = 1ul << pin;
}

void gpio_clr(u32 pin)
{
	SIO_GPIO_OUT_CLR = 1ul << pin; // turn off the LED

}



//# should be stored at 0x10000800
void  __attribute__ ((section(".user_prog"))) main_loop()
{
	//uint32_t fptr; // a pointer to a function
	while(1) {
		u32 fptr; // a function pointer
		//SIO_GPIO_OUT_SET = 1ul << LED; 
		//fptr = (u32) &gpio_set;
		fptr = (u32) &gpio_set;
		((void (*)(u32)) fptr)(LED);
		//((void (*)(u32)) &gpio_set)(LED);
		//gpio_set(LED);
		//delay(100);
		fptr = (u32) &delay;
		((void (*)(u32)) fptr)(1000);

		//gpio_clr(LED);
		fptr = (u32) &gpio_clr;
		((void (*)(u32)) fptr)(LED);

		//SIO_GPIO_OUT_CLR = 1ul << LED; // turn off the LED
		delay(1000);

	}
}
int main()
{
	// inspired by Ada. Appears to be necessary, too.
	// Bring up the peripherals we require
	// See s2.14.1
	uint32_t reset_mask = RESETS_PAD_BANK0 | RESETS_IO_BANK0;
	RESETS_RESET &= ~reset_mask; // deassert the peripherals we want to use
	while((RESETS_RESET_DONE & reset_mask) != reset_mask); // wait until they are up again

	//PADS_BANK0_GPIO25 &= ~(1<<7); // clear output disable 
	//PADS_BANK0_GPIO25 &= ~(1<<6); // clear input enable

	IO_BANK0_GPIO25_CTRL = GPIO_FUNC_SIO; // init pin - select function SIO
	SIO_GPIO_OE_SET = 1ul << LED; // allow setting of output

	//extern unsigned char user_stuff, user_stuff_end;
	//volatile u32 prog_len = __user_prog_end - __user_prog_start;
	//volatile u32 prog_len = (&user_stuff_end) - (&user_stuff);

	//main_loop();
	u32 ptr = (u32) &main_loop;
	//((void (*)())ptr) ();
	//extern u32 __user_prog_start;
	//((void (*)())&__user_prog_start) (); // call program
	while(1) {
		((void (*)())0x10001000+1) (); // call program
	}




	//volatile char str[] = "hello world";
	return 0;
}
