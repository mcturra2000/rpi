#pragma once

#include <stdint.h>

void uart2_init(void);
void uart2_send_blocking(const char* str, int n);
uint8_t uart2_recv_blocking(void);
void uart2_set_baudrate(uint32_t baudrate);

