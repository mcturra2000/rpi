#include <stdbool.h>
#include <assert.h>
#include <inttypes.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
//#include <lgpio.h>
#include <inttypes.h>
//#include <sys/time.h>


#include <delay.h>
#include "gpio.h"
#include "ssd1306.h"


#include "clock.h"

void buz_on(void);
void buz_off(void);
void stop_timing(void);

#ifdef USE_MQTT
#include "MQTTClient.h"
#define ADDRESS         "tcp://localhost:1883"
#define CLIENTID        "MYCLOCK"
MQTTClient mqtt_client;

int on_mqtt_message (void *context, char *topicName, int topicLen, MQTTClient_message *message) 
{
	char* payload = message->payload;
	printf("Received operation %s\n", payload);

	if(payload[0] == '0') {
		puts("mqtt command 0");
		stop_timing(); // also turns buzzer off
		//buz_off();
	}

	if(payload[0] == '3') {
		puts("mqtt command 3");
		buz_on();
		//start_alarm();
	}

	MQTTClient_freeMessage(&message);
	MQTTClient_free(topicName);
	return 1;
}


void mqtt_init ()
{
	MQTTClient_create(&mqtt_client, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_NONE, NULL);
	MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
	//conn_opts.username = "<<tenant_ID>>/<<username>>";
	//conn_opts.password = "<<password>>";

	MQTTClient_setCallbacks(mqtt_client, NULL, NULL, on_mqtt_message, NULL);

	int rc;
	if ((rc = MQTTClient_connect(mqtt_client, &conn_opts)) != MQTTCLIENT_SUCCESS) {
		printf("Failed to connect, return code %d\n", rc);
		exit(-1);
	}
	MQTTClient_subscribe(mqtt_client, "myclock", 0);
}

void mqtt_deinit(void)
{
	MQTTClient_disconnect(mqtt_client, 1000);
	MQTTClient_destroy(&mqtt_client);

}
#else
void mqtt_deinit(void) {}
#endif


#ifdef USE_INI
#include <ini.h>
#else
int alarm_hush = 0;
int alarm_timeout_secs = 30*60;
void parse_ini() {}
#endif


uint32_t i2c;
int chip_handle;
//uint64_t prog_start_time_ns;
bool shutdown = false;
int64_t timer_is_running = 0;
millis_t timer_start_time;



//const uint8_t btn_pin = 23;
//const uint8_t buz_pin = 16;

pin_t bzr; // GP16
pin_t btn; // GP23





millis_t elapsed_millis(void)
{
	return millis() - timer_start_time;
}

void delay_ms(int ms) { usleep(ms*1000); }

void buz_off (void) 
{
	if(gpio_get(&bzr)) gpio_clr(&bzr);
}

void buz_on (void) 
{
	if(gpio_get(&bzr) == 0) gpio_set(&bzr);
}

void set_buz(bool on) 
{
	if(on)
		buz_on();
	else
		buz_off();
}

void start_timing (void)
{
	puts("start_timing");
	//mode = timing_unexpired;
	timer_is_running = 1;
	timer_start_time = millis();
}

void stop_timing (void)
{
	puts("stop_timing");
	timer_is_running = 0;
	buz_off();
	//start_wall();
}





void buz_task (void)
{
	if(timer_is_running == 0) return;
	bool on = false;
	millis_t elapsed_ms = elapsed_millis() ;
	bool counting = (elapsed_ms/ 1000) < alarm_timeout_secs;
	millis_t slice_ms = elapsed_ms % 5000;
	if(counting) {
		alarm_hush = true; // keep it quiet
		on = (slice_ms < 500) && (!alarm_hush);
	} else {
		on = (slice_ms <200) || (slice_ms > 400 && slice_ms < 600);
	}
	set_buz(on);
}


void btn_pressed (void)
{		
	if(timer_is_running) 
		stop_timing();
	else
		start_timing();
}



void btn_task (void)
{
	static int press_num = 0;
	static int prev = 1;
	int val = gpio_get(&btn);
	assert(val>=0);
	if(val == prev) return;
	prev = val;
	if(val == 0) {
		printf("btn_task: Press detected:%d\n", press_num++);
		btn_pressed();
	}

}


void oled_task (void)
{
#define LEN 6

	char buf[LEN];
	if(timer_is_running) {
		// show the stopwatch
		millis_t elapsed = elapsed_millis();
		elapsed /= 1000;
		int secs = elapsed % 60;
		int mins = elapsed / 60;
		//secs = 6; mins = secs;
		snprintf(buf, LEN, "%02d.%02d", mins, secs);
	} else {
		// show normal time
		time_t curtime = time (NULL);
		struct tm *loc_time = localtime (&curtime);
		char fmt[] = "%H:%M";
		fmt[2] = loc_time->tm_sec % 2 == 0 ? ':' : ' ';
		strftime(buf, LEN, fmt, loc_time);
	}

	ssd1306_home();
	show_str(buf);
}




void exit_function(void)
{
	int status;
	puts("exit_function:called");
	shutdown = true;
	buz_off();

	gpio_release(&btn);
	//gpio_release(&bzr);

	mqtt_deinit();
	//exit_function();
	puts("exit_function:about to call exit(0)");
	exit(0);
}

void intHandler(int dummy)
{
	//exit_function();
	exit(0);
}



int main ()
{
	int status;
	signal(SIGINT, intHandler);
	//signal(SIGTERM, intHandler);
	atexit(exit_function);

	parse_ini();

	gpio_out(&bzr, 16);
	gpio_debounced(&btn, 23);
	ssd1306_init_display(32, 0x3C);
	ssd1306_show_scr();


	printf("sizeof(time_t):%d\n", sizeof(time_t)); // 4 -- holds seconds since epoch
	mqtt_init(); // seems to cause failure from 2022-04-05


	while(!shutdown) {
		btn_task();
		buz_task();
		oled_task();
		ssd1306_show_scr(); // this actually seems to lower cpu consumption
		usleep(1000);
	}

	return 0;
}
