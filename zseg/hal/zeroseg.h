#pragma once

#include "stm32f4xx_hal.h"


void max7219_tfr(uint8_t address, uint8_t value);
void max7219_show_count(int count);
void max7219_init(void);
void max7219_test_loop(void);
