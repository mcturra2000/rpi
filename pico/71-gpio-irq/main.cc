#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
//#include "hardware/adc.h"
//#include "hardware/clocks.h"
//#include "hardware/flash.h"
#include "hardware/gpio.h"
//#include "hardware/irq.h"
//#include "hardware/pwm.h"

#if LIB_PICO_STDIO_USB
#pragma message "USB configured as stdio"
#include "tusb.h" // if you want to use tud_cdc_connected()
#endif

#include "pi.h"

#define BTN  26 // GPIO number, not physical pin // labelled "A0" on tiny rp2040
#define LED  25 // GPIO of built-in LED

#if 1 // normal mode
#define WAY	INPUT_PULLUP
#define IMASK 	GPIO_IRQ_EDGE_FALL
#else // for touch
#define WAY	INPUT
#define IMASK 	GPIO_IRQ_EDGE_RISE
#endif



volatile int count = 0;

// NB as of 2021-06-21 the GPIO parameter is ignored, and is enabled on any pin. According to SDK
 
void gpio_callback(uint gpio, uint32_t events)
{
	count++;
	pi_gpio_toggle(LED);
}

int main() 
{
	stdio_init_all();
#if LIB_PICO_STDIO_USB
        while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 
#endif

	//pi_max7219_init();
	//pi_max7219_show_count(count);
	puts("Counter");

	pi_gpio_init(LED, OUTPUT);
	pi_gpio_init(BTN, WAY);
	gpio_set_irq_enabled_with_callback(BTN, IMASK, true, &gpio_callback);

	int prev_cnt = -1;
	while(1) {
		int cnt = count;
		if(prev_cnt != cnt) {
			printf("%d\n", cnt);
			prev_cnt = cnt;
		}
		//pi_max7219_show_count(count);
		//busy_wait_us_32(100*1000);
	}

	return 0;
}

