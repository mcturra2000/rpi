include ../../mk-rules.mk
JAGAN = ../../jagan
VPATH += .. $(JAGAN)


DEBUG = 0
ifdef DEBUG
CFLAGS += -O0 -ggdb # turn off optimsations
else
CFLAGS += -O2
endif

POT = $(RPI)/pot
VPATH += $(POT)

#IDIR =jagan
CFLAGS += -I. -I$(JAGAN) -I/$(POT)
CFLAGS += -mthumb -mcpu=cortex-m4
CFLAGS += -mfloat-abi=hard
CFLAGS += -mfpu=fpv4-sp-d16 # doesn't seem to be necessary
#CFLAGS += -I..
CFLAGS += -nostdlib
CFLAGS += -fdata-sections -ffunction-sections # for unused code
#CFLAGS += -std=c2x


CFLAGS += -I$(CMSIS)/Device/ST/STM32L4xx/Include/
CFLAGS += -I$(CMSIS)/Include/

ifdef USE_SSD1306
#CFLAGS += -I../../1306
#VPATH += ../../1306
OBJS += ssd1306.o  i2c.o vsnprintf.o # ssd1306-bsp-l432-bang.o
endif

ifdef USE_SDCARD
CFLAGS += -I../../sdcard-spi
VPATH += ../../sdcard-spi
OBJS += sdcard.o
endif

ifdef USE_ZSEG
CFLAGS += -I../../zeroseg
VPATH += ../../zeroseg
OBJS += zeroseg.o zeroseg-bsp-l432.o spi.o
endif

LDFLAGS = -T ../../linker.ld
#LDFLAGS = -L$(CMSIS)/Lib/GCC

#.PATH : jagan


all: app.bin


OBJS += startup.o string.o init.o main.o gpio.o


#TARG_OBJS = $($(TARG)_OBJS)
#TARG_OBJS = $(common_OBJS) $(OBJS)

app.elf: $(OBJS)
	$(LD) $(LDFLAGS) -o app.elf $(OBJS) -L$(CMSIS)/Lib/GCC  # -larm_cortexM4lf_math
	


startup.o : startup.s
	$(CC) -c $^



