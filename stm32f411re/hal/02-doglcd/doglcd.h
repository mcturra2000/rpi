#pragma once

/*
 * 2021-10-29 Created
 */

#include <stdbool.h>
#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

void doglcd_send_byte(int rs_val, uint8_t val);
//void doglcd_test();
void doglcd_init();
void doglcd_send_cmd(uint8_t cmd);
void doglcd_write_char(char c);
void doglcd_write_str(const char *str);
void doglcd_double_height(bool tall);
void doglcd_cls();
void doglcd_gotoxy(int x, int y);

#ifdef __cplusplus
}
#endif
