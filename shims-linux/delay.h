#pragma once

#include <stdint.h>
//#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef uint64_t millis_t;

millis_t millis();
void delay(uint64_t ms);

#ifdef __cplusplus
}
#endif

