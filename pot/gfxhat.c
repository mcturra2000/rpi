#include "spi.h"
#include "gfxhat.h"


#ifdef RPI
void gfxhat_init()
{
	sn3218_init_all();
	sn3218_all_brightnesses(50);

	spi_t spi;
	spi0_0_init(&spi);
	st7567_init(&spi);
}

void gfxhat_deinit()
{
}
#endif
