#include "tim.h"

extern uint32_t SystemCoreClock;

void tim2_5_init(TIM_TypeDef* TIMx, int freq)
{
	int apb1enr = RCC_APB1ENR_TIM2EN;
	if(TIMx == TIM3) apb1enr = RCC_APB1ENR_TIM3EN;
	else if(TIMx == TIM4) apb1enr = RCC_APB1ENR_TIM4EN;
	else if(TIMx == TIM5) apb1enr = RCC_APB1ENR_TIM5EN;		
	RCC->APB1ENR |= apb1enr; // enable TIM2 clock	
	
	TIMx->PSC = SystemCoreClock/1000000-1; // pre-scale to 1us
	TIMx->ARR = 1000000/freq-1; // Auto Reload Value to a given frequency
	TIMx->EGR |= TIM_EGR_UG; // generate update even to reset timer and apply settings
	TIMx->CR1 |= TIM_CR1_CEN; // enable timer counter
	TIMx->DIER |= TIM_DIER_UIE; // enable update interrupt
	
	int irqn = TIM2_IRQn; // assume tim2
	if(TIMx == TIM3) irqn = TIM3_IRQn;
	else if(TIMx == TIM4) irqn = TIM4_IRQn;
	else if(TIMx == TIM5) irqn = TIM5_IRQn;	
	NVIC_EnableIRQ(irqn);
}
