#include <i2c.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>

void i2c1_init(void)
{
        i2c_reset(I2C1);
        uint32_t mask = GPIO9 | GPIO10;
        rcc_periph_clock_enable(RCC_GPIOA);
        gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_PULLUP, mask);   
        //gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, mask);
        gpio_set_output_options(GPIOA, GPIO_OTYPE_OD, GPIO_OSPEED_LOW, mask); // open drain, fast
        gpio_set_af(GPIOA, GPIO_AF4, mask);
        rcc_periph_clock_enable(RCC_I2C1);
        //i2c_peripheral_disable(I2C1);
        i2c_set_speed(I2C1, i2c_speed_sm_100k, rcc_apb1_frequency / 1000000 );
        i2c_set_speed(I2C1, i2c_speed_fm_400k, rcc_apb1_frequency / 1000000 );
        //i2c_set_speed(I2C1, i2c_speed_sm_100k, rcc_apb1_frequency / 1e6 );
        i2c_peripheral_enable(I2C1);
        //init_display(64, (uint32_t) I2C1);
        //init_display(64, I2C1);
}

void __attribute__((weak)) i2c_ship(uint32_t i2c, uint8_t addr, const uint8_t *w, size_t wn) 
{
	i2c_transfer7(i2c, addr, w, wn, 0, 0);
}
