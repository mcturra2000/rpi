#pragma once

/* For a demo, see dev13

Debouncer has an internal software timer to keep track of periodic updates so that you don't have to.
Caller is responsible for setting GPIO pin to input with pullup.

Typical usage:

deb_t deb;
deb_init(&deb, GPIOB, GPIO_PIN_10);
for(;;) {
	if(deb_falling(&deb)) do_something();
}


*/

#include <stdbool.h>
#include <stdint.h>

#include <gpio.h>
#ifdef __cplusplus
extern "C" {
#endif

//int button_update(int *count, int is_high);


#define DEB_1
#ifdef DEB_1

typedef struct {
	uint8_t grate;
	bool falling;
	bool rising;
	GPIO_TypeDef *GPIOx;
	uint32_t GPIO_Pin;
	//gpio_t gpio;
} deb_t;


void deb_init(deb_t *deb, GPIO_TypeDef *GPIOx, uint32_t GPIO_Pin);
//void deb_update(deb_t *deb);
bool deb_falling(deb_t* deb);
bool deb_rising(deb_t* deb);
//void debounce_init(debounce_t* deb, uint gpio, uint delay);
//bool debounce_falling(debounce_t* deb);
//bool debounce_rising(debounce_t* deb);
#endif // DEB_1

#ifdef __cplusplus
}
#endif

