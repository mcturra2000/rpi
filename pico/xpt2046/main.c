#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
//#include "hardware/adc.h"
//#include "hardware/clocks.h"
//#include "hardware/flash.h"
#include "hardware/gpio.h"
//#include "hardware/irq.h"
//#include "hardware/pwm.h"
//#include "hardware/spi.h"
#if LIB_PICO_STDIO_USB
#pragma message "USB configured as stdio"
#include "tusb.h" // if you want to use tud_cdc_connected()
#endif


#include "pi.h"

#include "xpt2046.h"


int main() 
{
	stdio_init_all();
#if LIB_PICO_STDIO_USB
	while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 
#endif

	puts("xpt2046 demo");

	//pi_gpio_in(IRQ_Pin);
	pi_gpio_init(IRQ_Pin, INPUT_PULLUP);
	pi_gpio_out(SPI_CS_Pin);
	pi_gpio_high(SPI_CS_Pin);
	int spi_speed = 800000;
	spi_init(spi, spi_speed);
	const int PIN_SCK = 6;
	gpio_set_function(PIN_SCK,  GPIO_FUNC_SPI);
	const int PIN_SDO = 7;
	gpio_set_function(PIN_SDO, GPIO_FUNC_SPI);
	const int PIN_SDI = 4;
	gpio_set_function(PIN_SDI, GPIO_FUNC_SPI);


#define LED  25 // GPIO of built-in LED
	gpio_init(LED);
	gpio_set_dir(LED, GPIO_OUT);

	int i = 0;
	uint16_t x, y;
	for(;;) {
		if(XPT2046_TouchPressed()) {
			XPT2046_TouchGetCoordinates(&x, &y);
			printf("i= %5d, x = %d, y= %d\n", i++, x, y);
		}
	}

	return 0;
}

