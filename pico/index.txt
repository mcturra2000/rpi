* => the "canonical" version

adc, with pwm	72

alarm, low level    25


audio
	does not compute, pwm	76
	drum			17


binary, embedded	69
	drum		17


bme688 (temperature/environment)    23
	upython, see ~/bme688


circular buffer	16, 79


cmake
	embedded binary	*69, 78


delayish    24


dma
	spi, zeroseg, blocking	63


doglcd		26
	uart	16


doom screen wipe	18


ds3231			66
	fit		60
	set/read	22


embedded binary	69


FreeRTOS	14
	cli	15

ftdi, see uart

gpio
	irq	71


ili9341			78*
	coronation	18
	font, scaled	60
	pluto		79


irq
	gpio	71
	interrupts.txt
	svc/swi	81
	systick	80
	uart rx	16
	

keyboard, usb host	77


linker, see linker.txt


noise	19


pic (position-independent code)		bare/02


pluto		79


pwm, with adc	72


random	*19


rotary	6

		
sdcard	12
	raw song	70


serial, with linux	17, *39
	ds3231		22


sine	db07.40, db07.42
	mcp4921i, table	7
	pwmi, computed	3


spi
	zeroseg, dma blocking	63


ssd1306		60


svc/swi		81


systick		80


temperature
    bme688      23    
	internal	20
	thermistor	20, 61

thermister	61


timer, see alarm


touch sensor	68


uart
	echo		27
	ftdi clone	83
	rx, irq		16
	rx, irq, line	21*, 22
	serial		39
	tx		74
	
uril	21

usb 
	device, lowlevel	75
	keyboard host		77


zeroseg, dma blocking spi	63
	
