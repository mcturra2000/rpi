#pragma once
/*
 * gpio.h
 *
 *  Created on: Mar 1, 2024
 *      Author: pi
 */

//#include "main.h"
//#include "stm32f4xx_hal_conf.h"
#include <shims.h>
#if defined(HAL_GPIO_MODULE_ENABLED)
typedef GPIO_TypeDef * port_t;
typedef uint16_t pin_t;
typedef struct { port_t port; pin_t pin;} gpio_t;

#define LED_BUILTIN GPIOC, GPIO_PIN_13 // won't work for Nucleo board, of course

void gpio_enable_rcc(GPIO_TypeDef *GPIOx);
void gpio_out(GPIO_TypeDef *GPIOx, uint16_t  GPIO_Pin);
void gpio_in(GPIO_TypeDef *GPIOx, uint16_t  GPIO_Pin);
void gpio_inpull(GPIO_TypeDef *GPIOx, uint16_t  GPIO_Pin); // TODO implement
int gpio_read(GPIO_TypeDef *GPIOx, uint16_t  GPIO_Pin); 
void gpio_write(GPIO_TypeDef *GPIOx, uint16_t  GPIO_Pin, int level);

//void gpio_set_level(gpio_t gpio, uint32_t value);
#define digitalWrite HAL_GPIO_WritePin // really just use an alias
#endif
