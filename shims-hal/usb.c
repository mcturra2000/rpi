/*
 * usb.c
 *
 *  Created on: 8 Nov 2023
 *      Author: pi
 */

#include "usbd_cdc_if.h"

//#include "main.h"
//#include <time.h>

//#include <stdbool.h>
#include <string.h>

#include <usb.h>


// holds chars rec'd from USB
#define BUFF_MAX 132
char usb_linebuff[BUFF_MAX]; // capable of holding more than one line
static int usb_linebuff_len = 0;
static int num_eol = 0;




//#include <pot/cal.h>
int __io_putchar(int ch)
{
	char buf[2];
	buf[0] = ch;
	buf[1] = 0;
	USBD_StatusTypeDef status = USBD_BUSY;
	while (status == USBD_BUSY)
	{
		status = CDC_Transmit_FS((uint8_t *)buf, 1);
	}
	return ch;
}



// TODO check overflow
void usb_pushchar(uint8_t c)
{
	static int prev_char = -1;
	if(c<0) return;
	if(c == '\n' && prev_char == '\r') return; // handle \r\n seq
	prev_char = c;
	if(c == '\r' || c == '\n') {
		c = 0;
		num_eol++;
	}
	usb_linebuff[usb_linebuff_len++] = c;
}

// FN linebuff_is_ready 
// line is complete and ready to process
bool usb_linebuff_is_ready()
{
	return num_eol > 0;
	//return (linebuff_len > 0) && (linebuff[linebuff_len-1] == 0);
}

// FN usb_linebuff_consume 
// consumer has finished reading line
void usb_linebuff_consume()
{
	if(num_eol == 0) return;
	HAL_NVIC_DisableIRQ(OTG_FS_IRQn);
	int sz = strlen(usb_linebuff)+1;
	memmove(usb_linebuff, usb_linebuff+sz, usb_linebuff_len-sz);
	//memset(linebuff, sizeof(linebuff), 0);
	usb_linebuff_len -= sz;
	num_eol--;
	HAL_NVIC_EnableIRQ(OTG_FS_IRQn);

}


void usb_writen(uint8_t *buf, uint32_t len)
{
	for(int i = 0; i < len; i++) usb_pushchar(buf[i]);
}
#if 0
void serial_respond(char *msg);

void set_clock(long long val)
{
	DS3231_set_int(val);
}

/*
   void usb_printf()
   {

   }
   */
void serial_respond(char *msg)
{
	USBD_StatusTypeDef status = USBD_BUSY;
	while (status == USBD_BUSY)
	{
		status = CDC_Transmit_FS((uint8_t *)msg, strlen(msg));
	}
}

void usb_print32(char *hdr, uint32_t took)
{
	char strBuf[30];
	snprintf(strBuf, sizeof(strBuf), "%s:%lu\n", hdr, took);
	serial_respond(strBuf);
}




void print_time()
{
	char strBuf[30];
	struct tm t;
	DS3231_get_uk_tm(&t);

	strftime(strBuf, sizeof(strBuf), "%Y.%m.%d %H:%M:%S %a", &t);
	serial_respond(strBuf);
	snprintf(strBuf, sizeof(strBuf), " bst?(%d)\n", isbst_tm(&t));
	serial_respond(strBuf);
}

void print_info()
{
	printf("INFO\r\n");

	// temperatures
	HAL_ADC_Start(&hadc1);
	HAL_ADC_PollForConversion(&hadc1, 1000);
	uint32_t adc = HAL_ADC_GetValue(&hadc1);

#if 0
	uint32_t adc_lo = adc & 0xFFFF, adc_hi = adc >> 16;
	assert(((adc_hi << 16) | adc_lo) == adc);
#define AVG_SLOPE 2.5f // mV/℃ (4.3F)
#define V_AT_25C  0.76 // V (1.43F)
#define V_REF_INT (1.2F)
	//float V_Ref = (float)((V_REF_INT * 4095.0)/adc_lo);
	//float V_Sense = (float)(adc_hi * V_Ref) / 4095.0;
	float V_Sense = adc/1000.0;
	float Temperature = (V_Sense - V_AT_25C) * 1000.0 /AVG_SLOPE + 25.0;

#endif

	// this is also fairly rubbish
#define SENSOR_ADC_MAX 		4095.0
#define SENSOR_ADC_REFV  		3.0
#define SENSOR_AVG_SLOPE 		2.5 
#define SENSOR_V25  			        0.76

	float sensor_vol = (adc) * SENSOR_ADC_REFV / SENSOR_ADC_MAX;
	float sensor_tmp = ((sensor_vol - SENSOR_V25) *1000 / SENSOR_AVG_SLOPE) + 25.0;


	printf("TEMP: DS %f MCU %f \r\n", DS3231_get_treg(), sensor_tmp);
	puts("FIN");
}


void usb_test()
{
	// u32 start = usecs();
	// DS3231_get(&t);

	serial_respond("\n===Test results\n");

	print_time();
	// usb_print32("DS_get (us)", usecs()-start);
	// usb_print32("rtc_num_alarms", rtc_num_alarms);
}

void process_usb()
{
	// struct ts t;
	static char line[64]; // doesn't check for overflow
	static int len = 0;
	bool eol_detected = false;
	while (circ_avail())
	{
		// 2024-07-23 No need to fiddle with IRQ ∵ circ_read now does that for us
		uint8_t c;
		// HAL_NVIC_DisableIRQ(OTG_FS_IRQn);
		circ_read(&c);
		// HAL_NVIC_EnableIRQ(OTG_FS_IRQn);

		if (c == '\r' || c == '\n')
		{
			eol_detected = true;
			line[len] = 0;
			break;
		}
		line[len++] = c;
	}
	if (eol_detected)
	{
		eol_detected = false;
		if (len > 0)
		{
			char cmd = line[0];

			long long val = 0;
			for (int i = 1; i < len; i++)
			{
				val = val * 10 + line[i] - '0';
			}

			// char strBuf[50];

			switch (cmd)
			{
				case 'b': // turn the buzzer on and off
					HAL_GPIO_WritePin(BZR, val);
					break;
				case 'i':
					print_info();
					break;
				case 'R': // read and print the clock

					print_time();

					break;
				case 'S': // set the clock
					set_clock(val);
					break;
				case 't': // test the timer // seems OK
					usb_test();
					break;
			}
		}
		len = 0;
	}
}
#endif
