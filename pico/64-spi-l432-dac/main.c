#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
//#include "hardware/adc.h"
//#include "hardware/clocks.h"
#include "hardware/gpio.h"
//#include "hardware/irq.h"
#include "hardware/spi.h"
// #include "tusb.h" // if you want to use tud_cdc_connected()

//#include "pi.h"


#define SCK 	2
#define MOSI	3
#define CS	5

#define IN	17 // detects that DAC is writeable

#define LED	25

int main() 
{
	stdio_init_all();
	gpio_init(IN);
	gpio_set_dir(IN, GPIO_IN);
	//pi_gpio_init(LED, OUTPUT);
	puts("test remote dac");

	// set up spi
	gpio_set_function(SCK,  GPIO_FUNC_SPI);
	gpio_set_function(MOSI, GPIO_FUNC_SPI);
	gpio_set_function(CS, GPIO_FUNC_SPI);
	const int MHz = 1000000;
	int baud = 16*MHz; // 16MHz seems OK. 32MHz is too fast
	spi_init(spi0, baud);
	spi_set_format(spi0, 16, SPI_CPOL_0 , SPI_CPHA_0, SPI_MSB_FIRST);

	uint16_t count = 0;
	while(1) {
		while(gpio_get(IN)==0); // wait until input pin is high
		//busy_wait_us(10);
		spi_write16_blocking(spi0, &count, 1);
		count += 1;
		//count += 128;
		if(count>4095) count = count - 4096;
		//printf(".");
	}

}

