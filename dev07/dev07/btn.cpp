#include "main.h"


Btn::Btn(int id)
{
    pinMode(id, INPUT_PULLUP);
    _id = id;
}

bool Btn::update()
{
    uint8_t new_grate = (_grate << 1) | (digitalRead(_id) != 0);
    bool result = (new_grate == 0) && (_grate != 0);
    _grate = new_grate;
    return result;
}
