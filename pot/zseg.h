#pragma once

#include <time.h>
//#include <inttypes.h>
#include <spi.h>

/* See app-zsnake in dev13-14 for "raw" mode*/

/* layout (table 6)

     A
    ---
 F | G | B
    ---
 E |   | C
    ---
     D      DP
   
Register:
D7 D6 D5 D4 D3 D2 D1 D0
DP  A  B  C  D  E  F  G
*/


// See table 4
enum zseg_mode {NODECODE = 0x00, CODEB = 0xFF};
void zseg_setmode(enum zseg_mode mode);

void zseg_tfr(uint8_t address, uint8_t value);
void zseg_show_count(int count);
void zseg_init(spi_t *spi);
void zseg_show_fixed_number(uint32_t num, uint8_t lit, uint8_t dot);
void zseg_show_ms_as_mmss(uint32_t ms);
void zseg_show_hhmmss(int hours, int mins, int secs);
void zseg_test_loop(void);
void zseg_show_hhmmss_tm(struct tm *t);
void zseg_show_date(struct tm *t);
void zseg_invert(int yes);
void zseg_show_elapsed_and_hhmm(uint32_t secs, struct tm *t);
void zseg_set_at(int pos, uint8_t val);


