# SDK version

## Status

2025-01-29  Fixed problem with serial. It could sometimes receive a stray
0 character.

2025-01-19	Started. Fails on serial for some bizarre reason
