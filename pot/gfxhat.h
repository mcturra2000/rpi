#pragma once
#include <stdint.h>

#include "ssd1306.h" // we use some functionality there, like fonts

#include "sn3218.h"
#include "st7567.h"



#ifndef count_of
#define count_of(array) (sizeof(array) / sizeof(array[0]))
#endif

typedef uint8_t u8;

void gfxhat_init();
void gfxhat_deinit();
