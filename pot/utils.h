#pragma once

#include <stdbool.h>
#include <stdint.h>

/* "every" example usage

	do_something();
	for(;;) {
		static every_t every_1_sec = {1000};
		if(every(&every_1_sec)) 
			do_something();

		...
	}
*/

#ifdef __cplusplus
extern "C" {
#endif


typedef struct {
	uint32_t period; // user sets this in ms
	uint32_t last_triggered;
} every_t;

bool every(every_t *ev);

#ifdef __cplusplus
}
#endif

