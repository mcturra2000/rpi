#include <errno.h>

#include <gpio.h>


#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/spi.h>
#include <libopencm3/stm32/usart.h>

#include <ctype.h>
#include <stdio.h>

#include <string.h>


#define USART 		USART2
#define RCC_USART	RCC_USART2
#define USE_USART2

typedef uint32_t u32;

static void init_usart(void);


// https://github.com/libopencm3/libopencm3-examples/blob/master/examples/stm32/f4/nucleo-f411re/usart-stdio/usart-stdio.c

int _write(int fd, char *ptr, int len);

void get_buffered_line(void);

/*
 * This is a pretty classic ring buffer for characters
 */
#define BUFLEN 127

static uint16_t start_ndx;
static uint16_t end_ndx;
static char buf[BUFLEN+1];
#define buf_len ((end_ndx - start_ndx) % BUFLEN)
static inline int inc_ndx(int n) { return ((n + 1) % BUFLEN); }
static inline int dec_ndx(int n) { return (((n + BUFLEN) - 1) % BUFLEN); }


/* back up the cursor one space */
static inline void back_up(void)
{
	init_usart();
	end_ndx = dec_ndx(end_ndx);
	usart_send_blocking(USART, '\010');
	usart_send_blocking(USART, ' ');
	usart_send_blocking(USART, '\010');
}

/*
 * A buffered line editing function.
 */
void
get_buffered_line(void) {
	init_usart();
	char	c;

	if (start_ndx != end_ndx) {
		return;
	}
	while (1) {
		c = usart_recv_blocking(USART);
		if (c == '\r') {
			buf[end_ndx] = '\n';
			end_ndx = inc_ndx(end_ndx);
			buf[end_ndx] = '\0';
			usart_send_blocking(USART, '\r');
			usart_send_blocking(USART, '\n');
			return;
		}
		/* ^H or DEL erase a character */
		if ((c == '\010') || (c == '\177')) {
			if (buf_len == 0) {
				usart_send_blocking(USART, '\a');
			} else {
				back_up();
			}
		/* ^W erases a word */
		} else if (c == 0x17) {
			while ((buf_len > 0) &&
					(!(isspace((int) buf[end_ndx])))) {
				back_up();
			}
		/* ^U erases the line */
		} else if (c == 0x15) {
			while (buf_len > 0) {
				back_up();
			}
		/* Non-editing character so insert it */
		} else {
			if (buf_len == (BUFLEN - 1)) {
				usart_send_blocking(USART, '\a');
			} else {
				buf[end_ndx] = c;
				end_ndx = inc_ndx(end_ndx);
				usart_send_blocking(USART, c);
			}
		}
	}
}


/*
 * Called by the libc stdio fread fucntions
 *
 * Implements a buffered read with line editing.
 */
int _read(int fd, char *ptr, int len);

int _read(int fd, char *ptr, int len)
{
	init_usart();
	int	my_len;

	if (fd > 2) {
		return -1;
	}

	get_buffered_line();
	my_len = 0;
	while ((buf_len > 0) && (len > 0)) {
		*ptr++ = buf[start_ndx];
		start_ndx = inc_ndx(start_ndx);
		my_len++;
		len--;
	}
	return my_len; /* return the length we got */
}


int _write(int fd, char *ptr, int len)
{
	init_usart();
	int i = 0;

	/*
	 * Write "len" of char from "ptr" to file id "fd"
	 * Return number of char written.
	 *
	 * Only work for STDOUT, STDIN, and STDERR
	 */
	if (fd > 2) {
		return -1;
	}
	while (*ptr && (i < len)) {
		usart_send_blocking(USART, *ptr);
		if (*ptr == '\n') {
			usart_send_blocking(USART, '\r');
		}
		i++;
		ptr++;
	}
	return i;
}


int getchar(void)
{
	init_usart();
	return usart_recv_blocking(USART);
}
int putchar(int c)
{
	init_usart();
	usart_send_blocking(USART, c);
	return (unsigned char)c;
}

static void init_usart(void)
{
	static int initialised = 0;
	if(initialised !=0) return;
	initialised = 1;
	
	
	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_USART);
#ifdef USE_USART2
	uint16_t gpios = GPIO2 | GPIO3; // TX and RX resp.
#else
	uint16_t gpios = GPIO9 | GPIO10; // TX and RX resp.
#endif
	gpio_mode_setup(GPIOA, GPIO_MODE_AF,GPIO_PUPD_NONE, gpios);
	gpio_set_af(GPIOA, GPIO_AF7, gpios);

	usart_set_baudrate(USART, 115200);
	usart_set_databits(USART, 8);
	usart_set_stopbits(USART, USART_STOPBITS_1);
	usart_set_parity(USART, USART_PARITY_NONE);
	usart_set_mode(USART, USART_MODE_TX_RX);
	usart_set_flow_control(USART, USART_FLOWCONTROL_NONE);
	usart_enable(USART);
}

#if 0
int _getpid(void)
{
	return 1;
}

int _kill(int pid, int sig)
{
	//errno = EINVAL;
	return -1;
}

void _exit (int status)
{
	_kill(status, -1);
	while (1) {}		/* Make sure we hang here */
}

int _close(int file)
{
	return -1;
}


int _fstat(int file, struct stat *st)
{
	//st->st_mode = S_IFCHR;
	return 0;
}

int _isatty(int file)
{
	return 1;
}

int _lseek(int file, int ptr, int dir)
{
	return 0;
}
#endif

#if 0
// taken from https://github.com/libopencm3/libopencm3/wiki/Using-malloc-with-libopencm3
#define MAX_STACK_SIZE  8192

void local_heap_setup(uint8_t **start, uint8_t **end);

#pragma weak local_heap_setup = __local_ram

/* these are defined by the linker script */
extern uint8_t _ebss, _stack;

static uint8_t *_cur_brk = NULL;
static uint8_t *_heap_end = NULL;

/*
 * If not overridden, this puts the heap into the left
 * over ram between the BSS section and the stack while
 * preserving MAX_STACK_SIZE bytes for the stack itself.
 */
static void
__local_ram(uint8_t **start, uint8_t **end)
{
    *start = &_ebss;
    *end = (uint8_t *)(&_stack - MAX_STACK_SIZE);
}


/* prototype to make gcc happy */
void *_sbrk_r(struct _reent *, ptrdiff_t );

void *_sbrk_r(struct _reent *reent, ptrdiff_t diff)
{
    uint8_t *_old_brk;

    if (_heap_end == NULL) {
        local_heap_setup(&_cur_brk, &_heap_end);
    }

    _old_brk = _cur_brk;
    if (_cur_brk + diff > _heap_end) {
        reent->_errno = ENOMEM;
        return (void *)-1;
    }
    _cur_brk += diff;
    return _old_brk;
}
#endif


