# EXTI

Test of external interrupts on GPIO input.

Uses LED (PA5) and push-button (PB13, not the Nucleo user PC13). Interrupts are triggered
on rising and falling edges. Pressing the button turns the LED on. Releasing it turns
it off.


## Links to other sites

* [Hackaday](https://hackaday.com/2021/03/26/bare-metal-stm32-please-mind-the-interrupt-event/)


## Status

2021-12-11	Working.

2021-12-10	Started.
