#include "pluto.h"

void sd_list_root()
{
	print("Directory contents:\n");
	bds_t bds;
	dir32_t dir;
	dir32_init_root(&dir);
	while(dir32_read(&dir, &bds))
		print("%-11.11s %8d\n", bds.name, bds.size);
}
