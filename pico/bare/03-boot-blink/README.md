# blink sketch using boot sector only

## Lessons learned

**Decompile raw binary:**
```
objdump -b binary -marm -Mforce-thumb -D foo.bin

```

**Default boot stage 2 build:**
```
/home/pi/repos/rpi/pico/00-TEMPLATE/build/pico-sdk/src/rp2_common/boot_stage2
```


## Status

2023-04-23	Fixed for cold boot. Works.

2023-04-22	Started. Works when flashed, but not on cold boot.
