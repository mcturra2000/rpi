#pragma once

#include <shims.h>


#ifndef HAL_I2C_MODULE_ENABLED
typedef unsigned int I2C_HandleTypeDef;
#endif

typedef I2C_HandleTypeDef i2c_t;

void i2c_send_bytes(I2C_HandleTypeDef* i2c, uint8_t sid, const uint8_t *data, size_t len);
void i2c_recv_bytes(I2C_HandleTypeDef* i2c, uint8_t sid, uint8_t *data, size_t len);

