#include "basal.h"
//#include "conf.h"
#include "delay.h"
#include "gpio.h"

#define LED 	GPIOB, 3


#if 0
void SysTick_Handler(void)
{
}
#endif


int main (void) 
{   
	gpio_out(LED);

	const int x = 1;
	while(1) {	
		gpio_set(LED);
		delayish(100*x); //delay in  ms
		gpio_clr(LED);
		delayish(900*x); //delay in  ms
	}
}
