#include <gpio.h>

#include <assert.h>
#include <linux/gpio.h>

#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

//#include <gpiod.h>

// you should 
//gpiod_chip_close(chip);

#define CONSUMER "GPIOD"

static struct gpiod_chip *chip = 0;

//struct gpio_v2_line_request fummy;

// maybe
#if 0
void gpiod_init()
{
	return;
	if(chip) return;
	chip = gpiod_chip_open("/dev/gpiochip0");
	if(!chip)
	{
		perror("gpiod_chip_open");
	}
}
#endif
//#define LED 21

#if 1
//typedef struct gpiohandle_request pin_t;
//typedef struct gpio_v2_line_request pin_t;

void gpio_deinit()
{
}

int gpio_pin_init(pin_t *pin, int pin_num, enum gpio_v2_line_flag flags)
{
	//int fd = open("/dev/gpiochip0", O_RDONLY);
	int fd = open("/dev/gpiochip0", O_RDWR);
	assert(fd>=0);

	memset(pin, 0, sizeof(pin_t));
	pin->offsets[0] = pin_num;
	pin->config.flags = flags;
	pin->num_lines = 1;
	pin->fd = fd;

	int ret = ioctl(fd, GPIO_V2_GET_LINE_IOCTL, pin);
	if(ret != 0) {
		printf("gpio_pin_init failed for pin %d with errno %d\n", pin_num, errno);
	}

	close(fd);
	return ret;

}

int gpio_in(pin_t *pin, int pin_num)
{
	return gpio_pin_init(pin, pin_num, GPIO_V2_LINE_FLAG_INPUT);
}

int gpio_out(pin_t *pin, int pin_num)
{
	return gpio_pin_init(pin, pin_num, GPIO_V2_LINE_FLAG_OUTPUT);
}

int gpio_put(pin_t *pin, int val)
{
	struct gpio_v2_line_values vals;
	vals.bits = (val == 0) ? 0 : 1;
	vals.mask = 1;
	return ioctl(pin->fd, GPIO_V2_LINE_SET_VALUES_IOCTL, &vals);
}

int gpio_get(pin_t *pin)
{

        struct gpio_v2_line_values vals;
	vals.bits = 1;
	vals.mask = (1<<30) -1;
	vals.mask = 1<<17;
	vals.mask = 1;
        int status = ioctl(pin->fd, GPIO_V2_LINE_GET_VALUES_IOCTL, &vals);
	if(status != 0) {
		printf("gpio_get: couldn't GPIO_V2_LINE_GET_VALUES_IOCTL, errno = %d\n", errno);
	}
	return vals.bits;
}

int gpio_toggle(pin_t *pin)
{
	gpio_put(pin, 1 - gpio_get(pin));
}
int gpio_clr(pin_t *pin)
{
	return gpio_put(pin, 0);
}
int gpio_set(pin_t *pin)
{
	return gpio_put(pin, 1);
}
#endif
int gpio_release(pin_t *pin)
{
	int ret;
	__u32 pin_num = pin->offsets[0];
	ret = close(pin->fd);
	if(ret != 0) printf("gpio_release (1) close failed for pin %d with err: %s\n", strerror(errno));
	gpio_in(pin, pin_num);
	ret = close(pin->fd);
	if(ret != 0) printf("gpio_release (2) close failed for pin %d with err: %s\n", strerror(errno));
	return ret;
}
int gpio_debounced(pin_t *pin, int pin_num)
{
	//return gpio_pin_init(pin, pin_num, GPIO_V2_LINE_FLAG_INPUT | GPIO_V2_LINE_FLAG_BIAS_PULL_UP);
	int ret = 0;


	int fd = open("/dev/gpiochip0", O_RDWR);
	assert(fd>=0);

	struct gpio_v2_line_attribute lattr = {
		.id = GPIO_V2_LINE_ATTR_ID_DEBOUNCE, 
		.debounce_period_us = 40*1000
	};

	struct gpio_v2_line_config_attribute cattr = {
		.attr = lattr,
		.mask = 1
	};

	// edge falling doesn't really help unless we set up proper event processing
	struct gpio_v2_line_config cfg = {
		.flags = GPIO_V2_LINE_FLAG_INPUT | GPIO_V2_LINE_FLAG_BIAS_PULL_UP | GPIO_V2_LINE_FLAG_EDGE_FALLING,
		.num_attrs = 1,
		.attrs[0] = cattr
	};

	memset(pin, 0, sizeof(pin_t));
	pin->offsets[0] = pin_num;
	pin->config = cfg;
	pin->num_lines = 1;
	pin->fd = fd;

	ret = ioctl(fd, GPIO_V2_GET_LINE_IOCTL , pin);
	if(ret != 0) {
		printf("gpio_debounced GPIO_V2_GET_LINE_IOCTL failed for pin %d with errno %d\n", pin_num, errno);
	}

	close(fd);
	return ret;
}
#if 0
typedef struct gpiohandle_request pin_t;
//typedef struct gpio_v2_line_request pin_t;
int gpio_out(pin_t *pin, int pin_num)
{
	int fd = open("/dev/gpiochip0", O_RDONLY);
	assert(fd>=0);
	pin->lineoffsets[0] = pin_num;
	pin->lines = 1;
	pin->flags = GPIOHANDLE_REQUEST_OUTPUT;
	pin->fd = fd;

	int ret = ioctl(fd, GPIO_GET_LINEHANDLE_IOCTL, pin);
	close(fd);
	return ret;
}

int gpio_clr(pin_t *pin)
{
	struct gpiohandle_data data;
	data.values[0] = 0;
	return ioctl(pin->fd, GPIOHANDLE_SET_LINE_VALUES_IOCTL, &data);
}
int gpio_set(pin_t *pin)
{
	struct gpiohandle_data data;
	data.values[0] = 1;
	return ioctl(pin->fd, GPIOHANDLE_SET_LINE_VALUES_IOCTL, &data);
}
#endif


#if 0
int gpio_out(pin_t *pin, int pin_num)
{
	gpiod_init();

	struct gpiod_line *line = gpiod_chip_get_line(chip, pin_num);

	gpiod_line_request_input(line, CONSUMER);

	unsigned int offsets[1];
	offsets[0] = pin_num;
	int err = gpiod_chip_get_lines(chip, offsets, 1, pin);
	if(err)
	{
		perror("gpiod_chip_get_lines");
		goto cleanup;
	}

	struct gpiod_line_request_config config;
	memset(&config, 0, sizeof(config));
	config.consumer = 0; // "blink";
	config.request_type = GPIOD_LINE_REQUEST_DIRECTION_OUTPUT;
	config.flags = 0;

	// get the bulk lines setting default value to 0
	int values[1];
	values[0] = 0;
	err = gpiod_line_request_bulk(pin, &config, values);
	if(err)
	{
		perror("gpiod_line_request_bulk");
		goto cleanup;
	}



cleanup:
	//gpiod_chip_close(chip);
	return 0;
}


int gpio_clr(pin_t *pin)
{
	int values[1];
	values[0] = 0;
	int err = gpiod_line_set_value_bulk(pin, values);
	if(err)
	{
		perror("gpiod_line_set_value_bulk");
		goto cleanup;
	}
cleanup:
	return 0;
}

int gpio_set(pin_t *pin)
{
	int values[1];
	values[0] = 1;
	int err = gpiod_line_set_value_bulk(pin, values);
	if(err)
	{
		perror("gpiod_line_set_value_bulk");
		goto cleanup;
	}
cleanup:
	return 0;
}
#endif
