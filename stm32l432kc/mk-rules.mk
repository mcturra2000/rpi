PREFIX	?= arm-none-eabi-
CC	= $(PREFIX)gcc
CXX	= $(PREFIX)g++
LD	= $(PREFIX)gcc
OBJCOPY	= $(PREFIX)objcopy
OBJDUMP	= $(PREFIX)objdump

CC = arm-none-eabi-gcc
#AS = arm-none-eabi-as
LD = arm-none-eabi-ld
BIN = arm-none-eabi-objcopy


CMSIS = ${HOME}/STM32Cube/Repository/STM32Cube_FW_L4_V1.16.0/Drivers/CMSIS

all : app.bin

app.bin: app.elf
	$(Q) $(OBJCOPY) -O binary app.elf app.bin
	$(Q) $(OBJDUMP) -d app.elf >app.dis
	
.flash : app.bin
	touch .flash
	
flash : .flash
	st-flash erase
	st-flash --connect-under-reset write *bin 0x8000000	
	
clean :
	rm -rf *.bin *.dis *.elf *.o .targ build/

