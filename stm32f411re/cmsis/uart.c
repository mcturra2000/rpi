#define STM32F411xE
#include "stm32f4xx.h"
#include <delay.h>
#include <printf.h>
#include <stdbool.h>
#include <uart.h>

static uint32_t uart_SystemCoreClock = 16000000;

#define _USART USART2

static bool uart_initialised = false;

int putchar(int c)
{
	if(!uart_initialised) {
		uart_initialised = true;
		uart_init();
	}
	
	//if(c=='\r') putchar('\n');
	while( !( _USART->SR & USART_SR_TXE ) ) {}; // wait until we are able to transmit
	_USART->DR = c; // transmit the character
	return c;
}

//void _putchar(char c) { putchar(c); }

int puts(const char *s)
{
	while(*s)
		putchar(*s++);
	putchar('\n');
	return 1;
}

int getchar()
{
	while( !( _USART->SR & USART_SR_RXNE ) ) {}; // wait until something received
	return _USART->DR; // find out what it is
}

void uart_init (void) 
{
	set_putchar(putchar);

	// Set pins PA2 (TX) and PA3 (RX) for serial communication
	RCC->AHB1ENR	|= RCC_AHB1ENR_GPIOAEN; //enable RCC for port A
	GPIOA->MODER 	|= GPIO_MODER_MODER2_1; // PA2 is Alt fn mode (serial TX in this case)
	GPIOA->AFR[0] 	|= (7 << GPIO_AFRL_AFSEL2_Pos) ; // That alt fn is alt 7 for PA2
	GPIOA->MODER 	|= GPIO_MODER_MODER3_1; // PA3 is Alt fn mode (serial RX in this case)
	GPIOA->AFR[0] 	|= (7 << GPIO_AFRL_AFSEL3_Pos) ; // Alt fn for PA3 is same as for PA2
	
	RCC->APB1ENR  |=  RCC_APB1ENR_USART2EN; // enable RCC for USART2

	// Set the baud rate, which requires a mantissa and fraction
	uint32_t baud_rate = 115200;
	uint16_t uartdiv = uart_SystemCoreClock / baud_rate;
	_USART->BRR = ( ( ( uartdiv / 16 ) << USART_BRR_DIV_Mantissa_Pos ) |
			( ( uartdiv % 16 ) << USART_BRR_DIV_Fraction_Pos ) );

	// Mpw enable the USART peripheral
	_USART->CR1 |= USART_CR1_RE // enable receive
		| USART_CR1_TE // enable transmit
		| USART_CR1_UE // enable usart
		;
}
