# GPIO

```

GPIO_PinState HAL_GPIO_ReadPin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_13);

void HAL_GPIO_TogglePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);

void HAL_GPIO_WritePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, 
	GPIO_PinState PinState);
HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);
```

## Low-level

```
  LL_GPIO_SetOutputPin(GPIOA, GPIO_PIN_7); // set high
  LL_GPIO_SetOutputPin(GPIOA, GPIO_PIN_7 << 16); // reset
```  
