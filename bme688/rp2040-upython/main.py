from machine import Pin,I2C
#from roboboard import RoboBoard
from time import sleep_ms
from bme688 import *

i2c = I2C(0, sda=Pin(4), scl=Pin(5))

sensor = BME680_I2C(i2c)

i = 0
while True:
    gas = sensor.gas
    temperature = sensor.temperature
    humidity = sensor.humidity
    pressure = sensor.pressure
    print(f'i: {i:4.4d}, Temp: {temperature:.1f}C,  RH: {humidity:.1f}%,  Pressure: {pressure:.1f}kPa,  Gas: {gas:.1f}ppm')
    i += 1
    sleep_ms(5000)