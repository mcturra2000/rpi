#pragma once

#include <shims.h>


#ifndef HAL_SPI_MODULE_ENABLED
typedef unsigned int SPI_HandleTypeDef;
#endif

typedef struct {
	SPI_HandleTypeDef *hspi;

	// for CS
	GPIO_TypeDef* GPIOx;
	uint16_t GPIO_Pin;
} spi_t;


void spi_send_bytes(spi_t *spi, const uint8_t *data, size_t len);
