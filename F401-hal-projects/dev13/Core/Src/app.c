/*
 * app.c
 *
 *  Created on: Feb 27, 2024
 *      Author: pi
 *
 * 2024-11-14 BTNb might be brokwn on F401
 */

#include "main.h"
#include "usb.h"


#if 0
#define BTN_UPD(cnt, btn) button_update(&cnt, HAL_GPIO_ReadPin(btn##_GPIO_Port, btn##_Pin))

#define IF_BTN(prefix)               \
	static int prefix##_cnt = 0; \
if (button_update(&prefix##_cnt, HAL_GPIO_ReadPin(prefix##_GPIO_Port, prefix##_Pin)))
#endif

void do_display()
{
	struct tm t;
	DS3231_get_uk_tm(&t);

	if (om_running()) {
		uint32_t led_mask = 0b11111111;

		uint32_t secs = om_secs();
		uint32_t mins = secs / 60;
		secs = secs % 60;
		uint32_t display_val = (mins * 100 + secs) * 10000; // timer value so far
		if (mins < 10)
			led_mask &= ~0b10000000;

		// add in the time
		display_val += t.tm_hour * 100 + t.tm_min;
		if (t.tm_hour < 10)
			led_mask &= ~0b1000;
		zseg_show_fixed_number(display_val, led_mask, 0b00010000);

		// zseg_show_ms_as_mmss();
	}
	else
	{
		zseg_show_hhmmss_tm(&t);

	}

}



void app_ds3231()
{
	deb_t deby;
	deb_init(&deby, BTNy);
	om_init(1);
	for (;;)
	{
		DS3231_exc_handle_usb();
		om_update();

		static every_t ev_1sec = {1000};
		if (every(&ev_1sec))
		{
			do_display();
		}

		if (deb_falling(&deby))
		{
			HAL_GPIO_TogglePin(LEDy);
			om_toggle();
			do_display();
		}
}
}

void app (void)
{

	spi_t spi = {&hspi1, GPIOA, GPIO_PIN_15};
	zseg_init(&spi);
	DS3231_init(&hi2c1);
	app_ds3231();
}
