#include "gpio.h"
#include "uart.h"

void uart2_init(void)
{
	RCC->APB1ENR1 |= RCC_APB1ENR1_USART2EN;
	gpio_enable_rcc(GPIOA); // not sure this is necessary
	
	// not sure that pull-ups are neessary
	//gpio_pullup(GPIOA, 2);
	gpio_alt(GPIOA, 2, 7);
	USART2->CR1 |= USART_CR1_TE; // enable transmission

	//gpio_pullup(GPIOA, 15);
	gpio_alt(GPIOA, 15, 3);
	USART2->CR1 |= USART_CR1_RE; // enable receive
	
	uart2_set_baudrate(115200);
	USART2->CR1 |= USART_CR1_UE; // enable uart itself
}

void uart2_set_baudrate(uint32_t baudrate)
{
	uint16_t uartdiv = SystemCoreClock / baudrate;
	USART2->BRR = ( ( ( uartdiv / 16 ) << USART_BRR_DIV_MANTISSA_Pos ) |
			( ( uartdiv % 16 ) << USART_BRR_DIV_FRACTION_Pos ) );

}

void uart2_send_blocking(const char* str, int n)
{
	USART2->CR1 |= USART_CR1_TE; //send an idle frame
	//char *str = msg;
	while(n--) {
		while ((USART2->ISR & USART_ISR_TXE) == 0); // wait until transmission buffer is empty
		USART2->TDR = *str++; // send a char
	}
	while((USART2->ISR & USART_ISR_TC) == 0); // wait until transmission complete
}

uint8_t uart2_recv_blocking(void)
{
        while ((USART2->ISR & USART_ISR_RXNE) == 0);
        return USART2->RDR;
}

