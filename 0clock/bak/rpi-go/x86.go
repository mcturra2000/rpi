// +build x86

package main


import (
	"bufio"
	"fmt"
	"os"

)

func InitPlatform() {
	fmt.Printf("\033[2J") // xterm clear
	fmt.Printf("\033[H") // xterm home
	rdr = bufio.NewReader(os.Stdin)
}

func BuzzOn() {
	fmt.Printf("\033[1;10H *"); // goto 1x10 and print
}
func BuzzOff() {
	fmt.Printf("\033[1;10H  "); // goto 1x10 and print
}


var rdr  *bufio.Reader

func Debounce() {
	for {
		rdr.ReadString('\n')
		ButtonPressed()
	}

}

func DisplayDeinit() {}

func Displaystr(str string) {
	fmt.Print("\033[H") // xterm home
	fmt.Print(str)
}

