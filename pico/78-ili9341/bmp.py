from PIL import Image
import struct

iname = 'pluto.bmp'
#oname = 'pluto1.bmp'

img = Image.open(iname).convert("RGB")
img = img.rotate(180, expand=True)
#img = img.transpose(Image.TRANSVERSE)
px = img.load()
#img = img1.convert(mode='P', colors=16)
#img3 = img2.transponse(Image.TRANSPOSE)
#img = img2.rotate(90, expand=True)
#img3.show()
#img3.save(oname)



raw = open("pluto.raw", "wb")
for x in range(img.size[0]): # 320
	for y in range(img.size[1]): # 240
		r,g, b = px[319-x, y]
		r5 = r >> 3
		g6 = g >> 2
		b5 = b >> 3
		v565 = (r5 <<11) + (g6 << 5) + b5
		raw.write(struct.pack('>H', v565)) # unsigned short big-endian 

raw.close()


#print(img.size[0])
