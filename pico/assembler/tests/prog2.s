.address 0x10000294 // special ass command to set base address
delay:
	push	{r7, lr}
	sub	sp, #16
	add	r7, sp, #0
	str	r0, [r7, #4]
	movs	r3, #0
	str	r3, [r7, #12]
	b	L2be //100002be <delay+0x2a>
L2a2:
	movs	r3, #0
	str	r3, [r7, #8]
	b	L2b0 // 100002b0 <delay+0x1c>
L2a8:
	nop			//; (mov r8, r8)
	ldr	r3, [r7, #8]
	adds	r3, #1
	str	r3, [r7, #8]
L2b0:
	ldr	r3, [r7, #8]
	ldr	r2, [pc, #28]	//; (100002d0 <delay+0x3c>)
	cmp	r3, r2
	ble	L2a8 // 100002a8 <delay+0x14>
	ldr	r3, [r7, #12]
	adds	r3, #1
	str	r3, [r7, #12]
L2be:
	ldr	r2, [r7, #12]
	ldr	r3, [r7, #4]
	cmp	r2, r3
	blt	L2a2 // 100002a2 <delay+0xe>
	nop			//; (mov r8, r8)
	mov	sp, r7
	add	sp, #16
	pop	{r7, pc}
	nop			//; (mov r8, r8)
.word	0x000003e7

main:
push    {r7, lr}
sub     sp, #8
add     r7, sp, #0
movs    r3, #144        //; 0x90
lsls    r3, r3, #1
str     r3, [r7, #4]
ldr     r3, L330	//r3, [pc, #76]   ; (10000330 <main+0x5c>)
ldr     r2, [r3, #0]
ldr     r3, [r7, #4]
mvns    r1, r3
ldr     r3, L330	//r3, [pc, #68]   ; (10000330 <main+0x5c>)
ands    r2, r1
str     r2, [r3, #0]
nop                     //; (mov r8, r8)
L2f0:
ldr     r3, L334 	//r3, [pc, #64]   ; (10000334 <main+0x60>)
ldr     r3, [r3, #0]
ldr     r2, [r7, #4]
ands    r3, r2
ldr     r2, [r7, #4]
cmp     r2, r3
bne 	L2f0 		//   100002f0 <main+0x1c>
ldr     r3, L338 	//r3, [pc, #56]   ; (10000338 <main+0x64>)
movs    r2, #5
str     r2, [r3, #0]
ldr     r3, L33c 	//r3, [pc, #52]   ; (1000033c <main+0x68>)
movs    r2, #128        //; 0x80
lsls    r2, r2, #18
str     r2, [r3, #0]
L30c:
ldr     r3, L340	// r3, [pc, #48]   ; (10000340 <main+0x6c>)
movs    r2, #128        //; 0x80
lsls    r2, r2, #18
str     r2, [r3, #0]
movs    r0, #100        //; 0x64
bl      delay 		// 10000294 <delay>
ldr     r3, L344 	//r3, [pc, #40]   ; (10000344 <main+0x70>)
movs    r2, #128        //; 0x80
lsls    r2, r2, #18
str     r2, [r3, #0]
movs    r3, #250        //; 0xfa
lsls    r3, r3, #2
movs    r0, r3
bl      delay 		// 10000294 <delay>
b     L30c 		// 1000030c <main+0x38>
nop                     // (mov r8, r8)
L330: .word   0x4000c000
L334: .word   0x4000c008
L338: .word   0x400140cc
L33c: .word   0xd0000024
L340: .word   0xd0000014
L344: .word   0xd0000018

