include ../../mk-rules.mk

LDFLAGS = -T ../linker.ld

OPENCM3_INC = $(OPENCM3_DIR)/include
POT = $(RPI)/pot

MAL = $(RPI)/stm32l432kc/libopencm3 # My Abstraction Layer

#CFLAGS += -I..
CFLAGS += -I$(OPENCM3_INC)
CFLAGS += -I$(MAL)
CFLAGS += -I$(POT)

CFLAGS += -mthumb -mcpu=cortex-m4
CFLAGS += -mfloat-abi=hard
CFLAGS += -mfpu=fpv4-sp-d16 # doesn't seem to be necessary
CFLAGS += -nostdlib
CFLAGS += -fdata-sections -ffunction-sections # for unused code

VPATH += $(MAL):$(POT)
#VPATH += ..
#CFLAGS += -DSTM32L4

#PROJECT = app
#BUILD_DIR ?= bin
#OPT ?= -Os
#CSTD ?= -std=c11
#DEVICE=stm32l432kc
#BUILD_DIR = build



# Be silent per default, but 'make V=1' will show all compiler calls.
# If you're insane, V=99 will print out all sorts of things.
V?=0
V=1 # added by mcarter
ifeq ($(V),0)
Q	:= @
NULL	:= 2>/dev/null
endif


#POT = $(RPI)/pot

#OOCD	?= openocd

OBJS += main.o conf.o delay.o gpio.o

OBJS += stdio.o uart.o vsnprintf.o string.o # required for uart

ifdef USE_SSD1306
OBJS += ssd1306.o i2c.o
endif



all: app.bin

app.elf : $(OBJS)
	$(Q) $(LD) $(LDFLAGS) -L/home/pi/libopencm3/libopencm3/lib -nostartfiles  $(OBJS) -lopencm3_stm32l4 -o app.elf



%.o : %.c
	$(Q) $(CC) $(CFLAGS) -c $^ -o $@




