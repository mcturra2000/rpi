#include "pico/stdlib.h"
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "hardware/adc.h"

#include "pi.h"
#include "ds3231.h"

typedef uint32_t u32;
typedef uint64_t u64;

#define ALARM 0
#define DELAY (2*1000000)

#define I2C_PORT 8

//#define USE_SSD1306
#ifdef USE_SSD1306
#include "ssd1306.h"
void i2c_ship(uint32_t i2c, uint8_t addr, const uint8_t *w, size_t wn)
{
	i2c_write_blocking(i2c0, addr, w, wn, false);
}

void display_init()
{
	int h = 32;
	if(1) h = 64;
	pi_i2c_init(4);
	i2c_set_baudrate(&i2c0_inst, 400000);
	init_display(h, 4);
	fill_scr(0); // empty the screen
}

void display_refresh(const char *str)
{		
	ssd1306_home();
	print_str_at(str, 1, 0, 0);
	show_scr();
}

#else
#include <ili9341.h>
#include <mode0.h>

// buffer must be of size 6*240*2
void ili9341_filler(uint16_t *buffer, int slice)
{
	constexpr auto scale = 6; // scale the font by the given factor
	//constexpr int nrchars = 320/
	//constexpr auto slice_scale = 6 * scale;
	int x = slice  / scale / 6;
	if(x>=53) { // 53 = floor(320/6)
		memset(buffer, 0, 2*240);
		return;
	}
	//int bit = (slice *scale) % 6;
	int bit = (slice/scale) % 6;
	uint8_t mask = 64>>bit;
	for (int y=TEXT_HEIGHT/scale-1; y>=0; y--) {
		auto off = y*53 +x;
		auto color = colors[off];
		uint16_t fg_color = palette[color >> 4];
		uint16_t bg_color = palette[color & 0xf];

		if (show_cursor && (cursor_x == x) && (cursor_y == y)) {
			bg_color = MODE0_GREEN;
		}

		uint8_t character = screen[off];
		const uint8_t* pixel_data = font_data[character];

		// draw the character into the buffer
		for (int j=10; j>=1; j--) {
			for(int k = 0; k<scale; k++) {
				*buffer++ = (pixel_data[j] & mask) ? fg_color : bg_color;
			}
		}
	}
}

void display_init()
{
	ili9341_set_filler(ili9341_filler);
	ili9341_init();
	mode0_set_foreground(MODE0_ORANGE);
	systick_init();
	mode0_draw_screen();

}

extern "C" void isr_systick(void)
{
	mode0_refresh_screen();
}

void display_refresh(const char *str)
{
	//mode0_clear(MODE0_BLACK);
	mode0_set_cursor(0, 0);
	mode0_print(str);
	//mode0_draw_screen();
}
#endif

static void alarm_0_irq() // unneeded
{
	pi_alarm_rearm(ALARM, DELAY);
	char msg[80];
	static volatile int i = 0;
	sprintf(msg, "Count %d\n", i++);
	//ssd1306_print(msg);
}

#define PIN 17
#define BUZZ 18

void ds3231_init()
{
	pi_i2c_init(I2C_PORT);
	i2c_set_baudrate(&i2c0_inst, 400000);
	DS3231_init(DS3231_CONTROL_INTCN, (uint32_t) &i2c0_inst);
}

int main()
{
	adc_init();
	adc_set_temp_sensor_enabled(true);

	display_init();
	ds3231_init();


	pi_gpio_out(BUZZ); // doesn't seem effective on maker pi
	//GpioOut pin(17); // used to test speed of writing

	pi_alarm_init(ALARM, alarm_0_irq, DELAY);

	u64 prev_secs = 1;
	struct ts t;
	uint32_t t1;
	for(;;) {
		pi_gpio_high(PIN);

		u64 secs = time_us_64()/ 1000000;

		pi_gpio_low(BUZZ);
		if(prev_secs != secs) {
			if(secs == 0 || secs == 40 || secs == 45) {
				pi_gpio_high(BUZZ);
			}
		}
		prev_secs = secs;

		// time
		u32 hrs = secs/60/60;
		u32 mins = (secs/60) % 60;
		secs = secs % 60;

		// temperature
		adc_select_input(4);
		uint16_t raw = adc_read();
		const float conversion_factor = 3.3f / (1<<12);
		float result = raw * conversion_factor;
		float temp = 27 - (result -0.706)/0.001721;
		//temp = roundf(temp);
		//sprintf(str, "\n%d oC", (int) temp);
		static float temp1 = -100;
		if(temp1 == -100) temp1 = temp;
		constexpr float w = 0.95;
		temp1 = w*temp1 + (1-w)*temp;

		// ds3231
		DS3231_get_uk(&t);
		char ds_str[100];
		snprintf(ds_str, sizeof(ds_str), "%02d:%02d:%02d",
				t.hour, t.min, t.sec);

		char str[25];
		//sprintf(str, "%02d:%02d", mins, (u32) secs);
		snprintf(str, sizeof(str), "%02d:%02d:%02d\n%dC\n%s", hrs, mins, (u32) secs, 
				//ds_str,
				(int) roundf(temp1),
				ds_str);
		display_refresh(str);

		pi_gpio_low(PIN);
		delayish(200);
	}

	return 0;
}
