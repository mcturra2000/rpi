# stm32cubeide

## Crashes

**Crashes at startup**:
In your workspace
```
rm /data/data/STM32CubeIDE/workspace_1.14.1/.metadata/.plugins/org.eclipse.e4.workbench/workbench.xmi
```

[Link](https://community.st.com/t5/stm32cubeide-mcu/stm32cubeide-hangs-on-start-today/td-p/127555)


## Data collection deactivate

```
Menu : Window : Prefs : STM32Cube : End User Agreement
```
Turn off data collection.

## Defaults

Periphs in default mode? Yes => has a lot of stuff set up

## See also

db 7.58

* [github integration](https://shadyelectronics.com/how-to-use-github-with-stm32cubeide/)
