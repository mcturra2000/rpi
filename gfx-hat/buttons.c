#include "delay.h"
#include "i2c.h"
#include "cap1xxx.h"

#include <stdio.h>

// Reset Pin is used for I2C or SPI
#define CAP1188_RESET  9



void loop() {
	
	uint8_t touched = cap_touched();

	if (touched == 0) {
		// No touch detected
		return;
	}

	for (uint8_t i=0; i<8; i++) {
		if (touched & (1 << i)) 
			printf("C%d", i+1);
		putchar('\t');
	}
	//Serial.println();
	puts("");
	delay(50);
	
}

int main()
{
	i2c_init();
	cap_begin(0x2c); // for gfx-hat
	for(;;) loop();
	return 0;
}
