#include <stdint.h>
#include "debounce.h"
#include <gpio.h>
#include "utils.h"

int button_update(int *count, int is_high) {
        if (is_high == 0) *count += 1;
        else *count = 0;

        if (*count == 8) return 1; // indicates button down
        if (*count > 9) *count = 9;
        return 0;
}

#define DEB_1
#ifdef DEB_1


#define THRESH 10

void deb_init(deb_t *deb, GPIO_TypeDef *GPIOx, uint32_t GPIO_Pin)
{
	deb->grate = 0xFF;
	deb->falling = false;
	deb->rising = false; 
	deb->GPIOx = GPIOx;
	deb->GPIO_Pin = GPIO_Pin;

}

void deb_update(deb_t* deb)
{
	static every_t ev_4ms = {4};
	if (!every(&ev_4ms)) return;

	uint8_t grate = (deb->grate<<1);
	if(gpio_read(deb->GPIOx, deb->GPIO_Pin)) grate |= 1;
	if(grate == 0 && deb->grate != 0) deb->falling = true;
	if(grate == 0xFF && deb->grate != 0xFF) deb->rising = true;
	deb->grate = grate;
}



bool deb_falling(deb_t* deb)
{
	deb_update(deb);
	if(deb->falling) {
		deb->falling = false;
		return true;
	}
	return false;
}


bool deb_rising(deb_t* deb)
{
	deb_update(deb);
	if(deb->rising) {
		deb->rising = false;
		return true;
	}
	return false;
}
#endif // DEB_1
