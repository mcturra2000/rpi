#include "main.h"
#include <ds3231.h>
#include <pi.h>
#include <cstdio>
#include <stdlib.h>


auto_init_mutex(mtx);

void ds3231_get_uk_with_mtx(struct tm *t)
{
	mutex_enter_blocking(&mtx);
	DS3231_get_uk_tm(t);
	mutex_exit(&mtx);
}

void handle_linebuff();

#define BUFF_MAX 128
char linebuff[BUFF_MAX]; // capable of holding more than one line
static int linebuff_len = 0;
static int num_eol = 0; 

void linebuff_init()
{
	linebuff_len = 0;
	num_eol = 0;
}
// FN linebuff_pushchar 
void linebuff_pushchar(int c)
{
	//printf("linbuff len=%d\r\n",linebuff_len);
	static int prev_char = -1;
	if(c<0) return;
	if(c == '\n' && prev_char == '\r') return; // handle \r\n seq
	prev_char = c;
	if(c == '\r' || c == '\n') {
		c = 0;
		num_eol++;
	}
	linebuff[linebuff_len++] = c;
	//if(c==0) printf("strlen = %d\r\n", strlen(linebuff));
}


// FN linebuff_is_ready 
// line is complete and ready to process
bool linebuff_is_ready()
{
	return num_eol > 0;
	//return (linebuff_len > 0) && (linebuff[linebuff_len-1] == 0);
}

// FN linebuff_consume 
// consumer has finished reading line
void linebuff_consume()
{
	if(num_eol == 0) return;
	int sz = strlen(linebuff)+1;
	memmove(linebuff, linebuff+sz, linebuff_len-sz);
	//memset(linebuff, sizeof(linebuff), 0);
	linebuff_len -= sz;
	num_eol--;
}


// FN prbuf 
// For debugging purposes
void prbuf()
{
	for(int i = 0; i< linebuff_len; i++) {
		printf("%d ", linebuff[i]);
	}
	puts("");
	printf(linebuff);
}

void reader() 
{
	linebuff_init();
	for(;;) {
		int c = getchar();
		if(c>0) linebuff_pushchar(c);
		if(linebuff_is_ready()) {
			//prbuf();

			handle_linebuff();
			linebuff_consume();
		}
	}
}

void  serial_setup() 
{
	//stdio_init_all();

	multicore_launch_core1(reader);
}


void handle_linebuff()
{
	//struct tm t;
	long long val;
	char buff[30];
	//puts(linebuff);
	switch(linebuff[0]) {
		case 'R' :
			mutex_enter_blocking(&mtx);
			DS3231_get_str(buff, sizeof(buff));
			mutex_exit(&mtx);
			printf(buff);
			break;

		case 'S' :
			mutex_enter_blocking(&mtx);
			DS3231_set_from_str(linebuff);
			mutex_exit(&mtx);
			break;
	}
}

