#include "common.h"


label_t* hd_label = 0;
label_t* hd_holes = 0; // unresolved forward branching

/* FN streq */
bool streq (str_t *s1, str_t *s2)
{
	if(s1->len != s2->len) return false;
	char *d1 = s1->data, *d2 = s2->data;
	for(int i = 0; i<s1->len; i++) 
		if(d1[i] != d2[i]) return false;
	return true;
}

void add_list (label_t **hd, char* yystart, int yylen, int address)
{
        label_t *label = malloc(sizeof(label_t));
        if(label==0) syn("Couldn't malloc label");
        label->yytext.data = yystart;
        label->yytext.len = yylen;
        label->prev = *hd;
        label->address = address;
	*hd = label;
}

/*
 * FN add_hole
 */
void add_hole (char *yystart, int yylen, int address, jtype_e jtype)
{
	add_list(&hd_holes, yystart, yylen, address);
	hd_holes->jtype = jtype;
}

/* FN find_label */
label_t  *find_label (str_t *str)
{
	label_t *lab = hd_label;
	while(lab) {
		if(streq(str, &(lab->yytext))) {
			//puts("found label");
			return lab;
		}
		lab = lab->prev;
	}
	return 0;
}
void print_label (label_t *lab, char *post)
{
	printf("<");
	str_t *str = &(lab->yytext);
	for(int i = 0; i< str->len; i++) putchar(str->data[i]);
	printf(">%s", post);
}

void init_labels()
{
	hd_label = 0;
	hd_holes = 0; // unresolved forward branching
}

void deinit_labels()
{
	label_t *lab = hd_label;
	while(lab) {
		label_t *prev = lab->prev;
		free(lab);
		lab = prev;
	}
}

bool matched_label_def ()
{
	if(yytype != T_ID) return false;
	char *offset = yystart;
	int len = yylen;
	yylex();
	if(yytype != ':') return false;
	//puts("matched label");

	// add a label
	add_list(&hd_label, offset, len, address);
	printf("%08x ", address);
	print_label(hd_label, "\n");
	return true;
}

/* FN print_labels */
void print_labels ()
{
	puts("labels:");
	label_t *lab = hd_label;
	while(lab) {
		print_label(lab, "\n");
		lab = lab->prev;
	}

}

/* FN imm11 */
#if 0
int imm11 (int here, int there, int divisor, int width)
{
	here = (here/4)*4 + 4; // the ostensible address of next instruction
	printf("imm11: here=%x, there=%x\n", here, there);
	int offset = (there - here)/divisor;
	int mask = (1 << width) -1; // mash off a certain width of bits
	int res = (unsigned int) offset & mask;
	// TODO check range
	return res;
}
#endif

/* FN fill_holes
 * resolve forward labels
 */

void fill_holes ()
{
	puts("Fill forwar reference");
	for(label_t *hole = hd_holes ; hole; hole = hole->prev) {
		label_t *lab = find_label(&(hole->yytext));
		if(lab ==0) {
			printf("Couldn't find label ");
			print_label(hole, "\n");
			syn("Label unfound");
		}
		int offset = rel_offset(hole->address, lab->address, hole->jtype);
		printf("   @%4x:\t %4x\n", hole->address, offset);
	}
}

/* FN rel_offset */
uint32_t rel_offset (int here, int there, jtype_e jtype)
{
	// TODO perform width checks
	
	if(jtype==0) syn("re-Offset logic error: no jtype sepcified");
	uint32_t width = (jtype ==  J_8) ? 8 : 11;
	uint32_t mask = (1<<width) - 1; // contains a string of 1's, e.g. 0b11111111 for J_8
	uint32_t offset = (there - (here+4))/2; // won't always be 2, and may require alignment

	if(jtype == J_BL) {
		// see A6.7.13 to untangle this monstrosity
		// has great potential to be wrong, and may only manifest in rare cases
		mask = ~(0b1111<<(12+16));
		int S = 0;
		if(offset & (1<<31)) S =1; // i.e. offset is negative
		uint32_t imm11 = offset & ((1<<11)-1);
		uint32_t imm10 = (offset >>11) &((1<<10) -1);
		int I1 = offset & (1<<23) ? 1 : 0;
		int J1 = (I1 == S) ? 1 : 0;
		int I2 = offset & (1<<22) ? 1 : 0;
		int J2 = (I2 == S) ? 1 : 0;
		offset = (S << 10) | imm10;
		offset = (offset<<16) | (J1<<13) | (J2<<11) | imm11;
	}
	offset &= mask;
	return offset;
}
