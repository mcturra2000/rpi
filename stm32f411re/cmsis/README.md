# CMSIS

## In this directory

* [debug](debug.md)
* [gpio](gpio.md) - alternate functions
* [index](index.txt)
* [printf](https://github.com/mpaland/printf.git) - MIT licence

## Links to other sites

* [I2C library](https://stm32f4-discovery.net/2014/05/library-09-i2c-for-stm32f4xx/)
* [I2C using DMA](https://embeddedexpert.io/?p=624)

