#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "pico/stdio.h"
// #include "tusb.h" // if you want to use tud_cdc_connected()

#include "pi.h"
#include "ds3231.h"
//#include "i2c.h"


#define BUFF_MAX 128



int main() 
{
	timer_hw->dbgpause = 0; // https://github.com/raspberrypi/pico-sdk/issues/1152
	pi_stdio_init();
	pi_i2c_init(4); // sda 4, scl 5
	i2c_set_baudrate(&i2c0_inst, 100000);
	DS3231_init(DS3231_CONTROL_INTCN, (uint32_t) &i2c0_inst);
	struct ts t;
	uint32_t t1;


	char buff[BUFF_MAX];
	int line_len = 0;
	memset(buff, 0, BUFF_MAX);
	for(;;) {
		int c = getchar();
		if(c == '\r') c = '\n';
		if(c != '\n') {
			if( line_len + 1 >= BUFF_MAX) continue;
			//putchar(c); if(c == '\n') putchar('\r');
			buff[line_len++] = c;
			continue;
		}

			//puts("hello\r\n");
		//printf("\r\nYou entered the line <%s>\r\n", buff);
		if(line_len == 0) continue;

			//puts("hello\r\n");
		if(buff[0] == 'R') {
			//puts("hello 1\r");
			DS3231_get(&t);
			// 003 = ETX end of text
			printf("%d.%02d.%02d %02d:%02d:%02d\003\r\n", t.year, t.mon, t.mday, t.hour, t.min, t.sec);
		}

		if(buff[0] == 'S') {
			//long long tval = set_timeval;
			//set_timeval = -1;
			//char str[] = "S2023060617520003";
			volatile long long timeval = atoll(buff+1);
			DS3231_set_int(timeval);
		}
		if(buff[0] == 'T') { // set to a test time
			long long timeval = 2023060617520003;
			DS3231_set_int(timeval);
		}

		line_len = 0;
		memset(buff, 0, BUFF_MAX);
	}

	return 0;
}

