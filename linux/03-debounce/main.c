#include <gpio.h>

#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/epoll.h>
#include <poll.h>





//#include <linux/gpio.h>
#include <sys/ioctl.h>



pin_t btn;  
pin_t led20;
pin_t led21;


void exit_function(void)
{
	puts("exit_function called");
	gpio_release(&btn);
	gpio_release(&led20);
	gpio_release(&led21);
	exit(0);
}

void intHandler(int dummy)
{
	exit(0);
}


void btn_task (void)
{
	static int press_num = 0;
	static int prev = 1;
	int val = gpio_get(&btn);
	assert(val>=0);
	if(val == prev) return;
	prev = val;
	if(val == 0) {
		printf("btn_task: Press detected:%d\n", press_num++);
		//btn_pressed();
	}

}


int main()
{
	signal(SIGINT, intHandler);
	atexit(exit_function);

	int ret;

	gpio_debounced(&btn, 17);
	ret = gpio_out(&led20, 20);
	ret = gpio_out(&led21, 21);
	assert(ret != -1);

	for(;;) {
		btn_task();
		usleep(1000);
		//sleep(1);
	}


	return 0;
}
