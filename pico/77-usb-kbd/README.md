# USB keyboard host

Plug a USB keyboard into your Pico.


## Reference

* [Write-up](https://medium.com/@oblate_24476/plugging-a-keyboard-into-an-rp2040-8a51a8489ed2)


## Status

2022-12-26	Started. Works
