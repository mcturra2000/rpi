##################################################
# Set configuration for specific chip
CHIP = f411
CHIP = f401
ifeq ($(CHIP), f411)
CHIP1 = STM32F411xE
STARTUP_OBJ = startup_stm32f411retx.o
endif

ifeq ($(CHIP), f401)
CHIP1 = STM32F401xC
STARTUP_OBJ = startup_stm32f401ccux.o
endif

# You can use the same link script for F4o1 and F411
LINKER_SCRIPT = STM32F411RETX_FLASH.ld

##################################################
CFLAGS += $(DEBUG) -DUSE_HAL_DRIVER -D$(CHIP1)
ROOT = $(RPI)/stm32f411re/hal


INC += -I$(CUBE)/STM32F4xx_HAL_Driver/Inc
INC += -I$(CUBE)/CMSIS/Device/ST/STM32F4xx/Include
INC += -I$(CUBE)/CMSIS/Core/Include

OBJS +=  main.o stm32f4xx_hal_msp.o stm32f4xx_it.o system_stm32f4xx.o

OBJS += $(STARTUP_OBJ)

OBJS +=  stm32f4xx_hal_gpio.o stm32f4xx_hal.o stm32f4xx_hal_cortex.o # stm32f4xx_ll_utils.o
OBJS += stm32f4xx_hal_rcc.o
OBJS += sysmem.o syscalls.o
















#VPATH += $(RPI)/stm32f411re/hal
VPATH += $(CUBE)/STM32F4xx_HAL_Driver/Src

INC += -I$(ROOT)/shims
VPATH += $(ROOT)/shims

include ../../mk-rules.mk

