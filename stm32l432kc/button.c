/* 
 * Switch with or without pullup
 *
 * With input pullup:
 * Attach button to ground
 * Button reads low when switch pressed.
 * Button reads high when up.
 *
 * 2022-02-17	Started. Works
 *
 */

#include "conf.h"
#include "delay.h"
#include "gpio.h"

#define SW	D12

int main (void) 
{   
	//gpio_in(SW);
	gpio_inpull(SW);
	gpio_out(LD3);

	while(1) {
		if(gpio_get(SW))
			gpio_clr(LD3);
		else
			gpio_set(LD3);
		//gpio_set(LD3);
		//delayish(100); //delay in  ms
		//gpio_clr(LD3);
		//delayish(900); //delay in  ms
	}
}
