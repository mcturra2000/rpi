#include <gpio.h>

#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>




pin_t led20;
pin_t led21;

void exit_function(void)
{
	puts("exit_function called");
	gpio_release(&led20);
	gpio_release(&led21);
	exit(0);
}

void intHandler(int dummy)
{
	exit(0);
}


int main()
{
	signal(SIGINT, intHandler);
	atexit(exit_function);

	int ret;
	ret = gpio_out(&led20, 20);
	ret = gpio_out(&led21, 21);
	assert(ret != -1);

	for(;;) {
		gpio_toggle(&led20);
		gpio_clr(&led21);
		sleep(1);
		gpio_set(&led21);
		gpio_toggle(&led20);
		sleep(1);
	}

	return 0;
}
