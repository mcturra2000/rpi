# i2c master rx

Get numbers from a i2c slave. Use the ATtiny85 slave project as the slave.

Uses I2C0 with pins:
* ⚪ SDA0 GP4
* 🟡 SCL0 GP5

## Status

2021-07-21	Started. Working
