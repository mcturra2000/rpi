set(LIBRARY_NAME shims)

#include_directories($ENV{PICO_SDK_PATH}/lib/tinyusb/hw) # needed for bsp/board.h
#include_directories($ENV{PICO_SDK_PATH}/lib/tinyusb/src) # needed for tusb.h

#set(ENV{USB_KBD} $ENV{PICOMC}/usb-kbd)
set(SHIMS $ENV{RPI}/shims-pico)
set(POT $ENV{RPI}/pot)
include_directories(${SHIMS})
include_directories(${POT})

add_library(${LIBRARY_NAME} INTERFACE)
target_sources(${LIBRARY_NAME} INTERFACE
	${SHIMS}/delay.c 
	${SHIMS}/gpio.c 
	${SHIMS}/i2c.c 

	${POT}/debounce.c
	${POT}/debouncepp.cc
	${POT}/om.c
	${POT}/utils.c
	#$ENV{USB_KBD}/hid_app.c
	#$ENV{USB_KBD}/msc_app.c
)
target_include_directories(${LIBRARY_NAME} INTERFACE
 	${SHIMS}
	${POT}
)
#target_link_libraries(app ${LIBRARY_NAME} )
#target_link_libraries(${LIBRARY_NAME} INTERFACE
#	tinyusb_host # for USB
#	tinyusb_board # for USB
#)
