#pragma once
// 2024-06-11	This is the better interface for I2C

#include <stdint.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

extern int i2c_fd;

typedef int I2C_HandleTypeDef;

void i2c_init();
int i2c_grab1_at(uint8_t sid, uint8_t reg, uint8_t *result);
void i2c_ship_val_at(uint32_t sid, uint8_t addr, const uint8_t val);
void i2c_ship_at(uint32_t sid, uint8_t addr, const uint8_t *w, size_t wn);


#ifdef __cplusplus
}
#endif

