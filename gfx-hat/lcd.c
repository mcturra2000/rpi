#include <stdio.h>

#include "sn3218.h"

#include "gfxhat.h"




int main()
{
	gfxhat_init();

	for(int x=0 ; x< 64; x++) {
		st7567_set_pixel(x, x, 1);
		st7567_set_pixel(64 - x, x, 1);
		st7567_set_pixel(x + 2, x, 1);
	}
	st7567_show();

	//char buf[32];
	int contrast = 37;

	while(1) {
		st7567_set_contrast(contrast);
		printf("Enter a contrast  0-63 (current val: %d) ", contrast);
		//int n = read(STDIN_FILENO, buff, sizeof(buf));
		//if(n==0) continue;
		scanf("%d", &contrast);
	}

	gfxhat_deinit();

	return 0;
}
