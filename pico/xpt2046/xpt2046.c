/*
 * XPT2046_touch.c
 *
 *  Created on: 20 sep. 2019 �.
 *      Author: Andriy Honcharenko
 */

#include <stdio.h>
#include <stdlib.h>
#include "xpt2046.h"

#include "pi.h"

#define READ_X 0x90
#define READ_Y 0xD0

#define GPIO_PIN_RESET 0
#define GPIO_PIN_SET 1

void HAL_SPI_TransmitReceive(uint8_t *tx_data, uint8_t *rx_data, int len)
{
	spi_write_read_blocking(spi, tx_data, rx_data, len);
}

void HAL_SPI_Transmit(uint8_t *data, int len)
{
	spi_write_blocking(spi, data, len);
}

/*
void HAL_GPIO_WritePin(int pin, int val)
{
	gpio_put(pin, val);
}
*/
static void XPT2046_TouchSelect()
{
	pi_gpio_low(XPT2046_CS_Pin);
	//HAL_GPIO_WritePin(XPT2046_CS_Pin, GPIO_PIN_RESET);
}

static void XPT2046_TouchUnselect()
{
	pi_gpio_high(XPT2046_CS_Pin);
	//HAL_GPIO_WritePin(XPT2046_CS_Pin, GPIO_PIN_SET);
}

bool XPT2046_TouchPressed()
{
	return gpio_get(XPT2046_IRQ_Pin) == 0;
	//return HAL_GPIO_ReadPin(XPT2046_IRQ_GPIO_Port, XPT2046_IRQ_Pin) == GPIO_PIN_RESET;
}

bool XPT2046_TouchGetCoordinates(uint16_t* x, uint16_t* y)
{
	static const uint8_t cmd_read_x[] = { READ_X };
	static const uint8_t cmd_read_y[] = { READ_Y };
	static const uint8_t zeroes_tx[] = { 0x00, 0x00 };

	XPT2046_TouchSelect();

	uint32_t avg_x = 0;
	uint32_t avg_y = 0;
	uint8_t nsamples = 0;

	for(uint8_t i = 0; i < 16; i++)
	{
		if(!XPT2046_TouchPressed())
			break;

		nsamples++;

		HAL_SPI_Transmit((uint8_t*)cmd_read_y, sizeof(cmd_read_y));
		uint8_t y_raw[2];
		HAL_SPI_TransmitReceive((uint8_t*)zeroes_tx, y_raw, sizeof(y_raw));

		HAL_SPI_Transmit((uint8_t*)cmd_read_x, sizeof(cmd_read_x));
		uint8_t x_raw[2];
		HAL_SPI_TransmitReceive((uint8_t*)zeroes_tx, x_raw, sizeof(x_raw));

		avg_x += (((uint16_t)x_raw[0]) << 8) | ((uint16_t)x_raw[1]);
		avg_y += (((uint16_t)y_raw[0]) << 8) | ((uint16_t)y_raw[1]);
	}

	XPT2046_TouchUnselect();

	if(nsamples < 16)
		return false;

	uint32_t raw_x = (avg_x / 16);
	if(raw_x < XPT2046_MIN_RAW_X) raw_x = XPT2046_MIN_RAW_X;
	if(raw_x > XPT2046_MAX_RAW_X) raw_x = XPT2046_MAX_RAW_X;

	uint32_t raw_y = (avg_y / 16);
	if(raw_y < XPT2046_MIN_RAW_Y) raw_y = XPT2046_MIN_RAW_Y;
	if(raw_y > XPT2046_MAX_RAW_Y) raw_y = XPT2046_MAX_RAW_Y;

	// Uncomment this line to calibrate touchscreen:
	//printf("raw_x = %d, raw_y = %d\r\n", (int) raw_x, (int) raw_y);

	//    *x = (raw_x - XPT2046_MIN_RAW_X) * XPT2046_SCALE_X / (XPT2046_MAX_RAW_X - XPT2046_MIN_RAW_X);
	//    *y = (raw_y - XPT2046_MIN_RAW_Y) * XPT2046_SCALE_Y / (XPT2046_MAX_RAW_Y - XPT2046_MIN_RAW_Y);
	*y = XPT2046_SCALE_Y-(raw_y - XPT2046_MIN_RAW_Y) * XPT2046_SCALE_Y / (XPT2046_MAX_RAW_Y - XPT2046_MIN_RAW_Y);
	*x = (raw_x - XPT2046_MIN_RAW_X) * XPT2046_SCALE_X / (XPT2046_MAX_RAW_X - XPT2046_MIN_RAW_X);
	return true;
}
