# Progs: standalone programs

This section explores how to write programs that call functions exposed by the Pluto kernel. 
This enables programs to continue working even after if the kernel is re-compiled,
thereby changing the addresses at which the functions are stored.


## Example

The exemplar is in project 03. It works based on the assumption that the binary is flashed
to address 0x1001'0000. In the directory, type `make`, and transfer the binary to sd card.
From Pluto, type `load blink.bin` to transfer the file from card to flash. 
Then type `run` to run the program from flash.



## General procedure

An example project is given in folder `01-hi`. Use it as a template, adapting it as required.

```
cd 01-hi
vim prog.c # hack, hack, hack ...
make # produces prog.o prog.bin prog.dis
```

`prog.bin` is a raw binary that needs to end up on the RP2040's flash drive. 
`prog.dis` is a disassembly of the binary file that may be useful for informational purposes.

In `Makefile`, change the `DRIVE` variable to be the name of the SD card. 
Insert the card into your computer and type `make install`. 
Alternatively, just copy the binary manually.

Unmount the SD card. Remove the card, and place it into the SD card reader attached to
Pluto. Reboot Pluto.

Take a note of the "flash end" output. For example, it might read `0x1000bc68`. 
The RP2040's flash works in sectors of 4k (0x1000), so the next available sector on
Pluto's flash drive is `0x1000c000`.
That will be used as an example in the following instructions.
It needs to be changed according to the actual value displayed.


Type `dir` to obtain a directory list. `prog.bin` should be listed.

Copy the binary to the flash drive. For example: `flash 0xc000 prog.bin`. 
Note that `0xc000` is an *offset* from `0x1000000`, not an absolute address.
It is not strictly necessary to flash the program at the first available location.
It must be a multiple of 0x1000, though.
Be careful not to write into a memory location that is used by Pluto.
That will corrupt Pluto, and the kernel will need to be reflashed.

```
call 0x1000c000
```
This executes the function stored on flash. 
Note that is an absolute address.

