from enum import Enum
import datetime
import time

import beep
import button
import oled


def get_now(): return datetime.datetime.now()

class ClkSt(Enum):
	CLOCKING = 1
	TIMING = 2

#st = ClkSt.CLOCKING

#def do_clock(ev):
#	global st
#	if st == ClkSt.CLOCKING:
#		if ev == ClkEv.SWAP:
#			st = ClkSt.TIMING
#	elif st == ClkSt.TIMING

showing_clock = True
swap_action_num = 0
start_time = get_now()
elapsed_secs = 0
def swap_action():
	global showing_clock, swap_action_num, start_time
	print("swap_action called", swap_action_num)
	swap_action_num +=1
	showing_clock =  not showing_clock
	if showing_clock:
		beep.do_buz(beep.BuzEv.STOP)
	else:
		start_time = get_now()

def is_showing_clock(): 
	global showing_clock
	return showing_clock

def refresh_display():
	global showing_clock
	if showing_clock:
		oled.show_clock()
	else:
		oled.show_elapsed()

def get_elapsed():
	global elapsed_secs
	return elapsed_secs

TIMEOUT_SECS = 10 
timed_out = False

tick = 0 # poor approx to ms
def main():
	global  tick, elapsed_secs, start_time, timed_out, TIMEOUT_SECS
	while True:
		#if tick % 2 == 0:
		if button.do_button():
			swap_action()
			refresh_display()
			#do_clock(ClkEv.SWAP)

		elapsed_secs = (get_now() - start_time).seconds
		oled.elapsed_secs = elapsed_secs
		if elapsed_secs >= TIMEOUT_SECS and not showing_clock and not timed_out:
			timed_out = True
			beep.do_buz(beep.BuzEv.START)
		beep.do_buz(beep.BuzEv.TICK)



		if tick % 200 == 0:
			refresh_display()



		time.sleep(0.001)
		tick += 1

if __name__=="__main__":
	main()
