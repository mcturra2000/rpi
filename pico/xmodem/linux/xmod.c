/* 2023-01-24	Started for experimental reasons. It is not a 
 * complete implementation
 */

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>

#include <termios.h>
#include <inttypes.h>

#define SOH 0x01        // start of header
#define EOT 0x04        // end of transmission
#define ACK 0x06        // acknowledge
#define NAK 0x15        // no acknowledge // dec 21
//#define ETB 0x18      // end of tranmssion block
#define CAN 0x18  // cancel // dec 24


int main(void)
{
	struct termios term, term_ori;
	int fd = open("/dev/ttyACM0", O_RDWR);
	if(tcgetattr(fd, &term) < 0) {
		puts("couldn't find device");
		exit(1); // what are the current attributes?
	}
	term_ori = term; // create a copy for later restoration
	term.c_lflag &= ~ICANON; // don't wait for newline
	term.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received bytes

	term.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
	term.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed
	// tty.c_oflag &= ~OXTABS; // Prevent conversion of tabs to spaces (NOT PRESENT IN LINUX)
	// tty.c_oflag &= ~ONOEOT; // Prevent removal of C-d chars (0x004) in output (NOT PRESENT IN LINUX)

	// Set in/out baud rate to be 9600
	//cfsetispeed(&term, B115200);
	//cfsetospeed(&term, B115200);
	cfsetspeed(&term, B115200);
	//cfsetspeed(&term, B9600);
	if(tcsetattr(fd, TCSANOW, &term)) exit(1); // set the attributes how we wish

	// see note 1


	typedef uint8_t u8;
	u8 data[128];


	int c = 0, n;

	int get() {
		int ch;
		int n = read(fd, &ch, 1);
		assert(n==1);
		return ch;
	}


	void put(uint8_t c) {
		usleep(1000);
		u8 c1 = c;
		write(fd, &c1, 1);
	}

	while(c!= NAK) {
		puts("Awaiting NAK");
		n = read(fd, &c, 1);
		//printf("%d ", c);
	}

	//assert(c == NAK);
	//c = SOH;
	//write(fd, &c, 1);
	put(SOH);
	puts("Wrote SOH");

	put(1); // first block
	put(255-1); // 
	for(int i = 0; i<128; i++)
		put(0);

	put(0); // checksum
	c = get();
	//assert(c == ACK);
	printf("got %d\n", c);
	put(EOT);
	c = get();
	printf("Got ACK? %d\n", (c==ACK));

	close(fd);

	//if(tcsetattr(STDIN_FILENO, TCSANOW, &term_ori)) exit(1);
	return 0;
}
