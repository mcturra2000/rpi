#pragma once

#include <stdint.h>

#ifdef __cplusplus
    extern "C" {
#endif


/* choose a region above where the kernel is. PROGBASE must be the same as
 * the origin specified in prog.ld the progs dir
 */
#define PROGBASE 0x10010000


typedef void (*fptr)();

/* functions exported by pluto */

typedef struct {
	int magic;
	int version;
	fptr hi;
	fptr memcpy;
	fptr ili9341_set_filler;
	fptr gpio_out;
	fptr gpio_set;
	fptr gpio_clr;
	fptr sleep_ms;
	fptr ili9341_reset_filler;
}   pl_exports_t;

//#define pl_hi 

// __attribute__((section(".pluto_exports"))) 

//extern struct kexports exports;
extern const pl_exports_t __attribute__((section(".pluto_exports")))  pl_exports;

#define PLUTO_FN(fn_name) (((pl_exports_t *)0x10000200)->fn_name)

#define pl_hi() PLUTO_FN(hi) () 
#define pl_memcpy(dest, src, n) PLUTO_FN(memcpy) (dest, src, n)
#define	pl_ili9341_set_filler(addr) PLUTO_FN(ili9341_set_filler) (addr)
#define pl_gpio_out(gpio) PLUTO_FN(gpio_out) (gpio)
#define pl_gpio_set(gpio) PLUTO_FN(gpio_set) (gpio)
#define pl_gpio_clr(gpio) PLUTO_FN(gpio_clr) (gpio)
#define pl_sleep_ms(ms) PLUTO_FN(sleep_ms) (ms)
#define pl_ili9341_reset_filler() PLUTO_FN(ili9341_reset_filler) ()


#ifdef __cplusplus
    }
#endif
