#include <conf.h>
//#include <inttypes.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/gpio.h>
//#include <libopencm3/stm32/timer.h>
//#include <libopencm3/stm32/spi.h>

#include <delay.h>
#include <gpio.h>
#include <debounce.h>
#include <i2c.h>
#include <ssd1306.h>


#define LED1	GPIOC, GPIO13 // blackpill
#define LED2	GPIOA, GPIO5 // Nucleo board

#define BTNY	GPIOA, GPIO1 // yellow button
deb_t sw_yellow = {0};
#define BTNG	GPIOB, GPIO0 // green button
deb_t sw_green = {0};
#define BTNB	GPIOA, GPIO7 // blue button
deb_t sw_blue = {0};
#define BTNW	GPIOA, GPIO4 // white button
deb_t sw_white = {0};

uint32_t tick = 0;

void sys_tick_handler(void) // mandatory ISR name
{
	tick++;
	if(tick % 4 == 0) {
		deb_update(&sw_yellow, gpio_get(BTNY));
		deb_update(&sw_green, gpio_get(BTNG));
		deb_update(&sw_blue, gpio_get(BTNB));
		deb_update(&sw_white, gpio_get(BTNW));
	}
}

int main(void)
{
	delayish(4);
	i2c1_init();
	init_display(64, I2C1);
	int counter = 0;
	
	gpio_out(LED1);
	gpio_out(LED2);
	gpio_inpull(BTNY); sw_yellow.en = 1;
	gpio_inpull(BTNG); sw_green.en = 1;
	gpio_inpull(BTNB); sw_blue.en = 1;
	gpio_inpull(BTNW); sw_white.en = 1;

	systick_set_frequency(1000, rcc_ahb_frequency);
	systick_counter_enable();
	systick_interrupt_enable();

	ssd1306_puts("Press buttons...");
	while (1) {
		if(deb_falling(&sw_yellow)) ssd1306_printf("%3d : yellow\n", counter++);
		if(deb_falling(&sw_green)) ssd1306_printf("%3d : green\n", counter++);
		if(deb_falling(&sw_blue)) ssd1306_printf("%3d : blue\n", counter++);
		if(deb_falling(&sw_white)) ssd1306_printf("%3d : white\n", counter++);
		show_scr();
	}
}
