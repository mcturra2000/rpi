#include "cal.h"

/*
 * Seems OK.
 * checked against https://www.timeanddate.com/date/durationresult.html?d1=1&m1=1&y1=2000&d2=5&m2=4&y2=2020
 */

/* conventions:
 * Mostly follows time.t conventions
 * Sun = 0
 *
 * see man 3 time
 */
const int dims[12] = {31, 28, 31, 30, 31, 30, 31,31, 30, 31, 30, 31};
const int cum_dims[12] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};
int isleap(int y)
{
        if(y%400 ==0) return 1;
        if(y%100 ==0) return 0;
        if(y%4 ==0) return 1;
        return 0;

}

// get days in month
// mon in [0,11]
int get_dims(int y, int mon0)
{
        int res = dims[mon0];
        if(mon0 == 1) res += isleap(y);
        return res;
}
// mon in [0,11]
int get_cum_dims(int y, int mon)
{
        int tot = cum_dims[mon];
        if(isleap(y) && (mon > 1)) tot++;
        return tot;
}
//int days_in_month(int y, int m)
//{
//      return 

// days since 1 Jan 2000 (2000-01-01 returns 0)
// mday in [1,31] month date
// mon [0, 11] months since jan
// year - absolute (e.g. 2000) starts from year 2000
int mildays(int y, int mon, int mday)
{
        int tot = (y-2000) * 365;
        if(y>2000) tot += (y-2001)/4;
        tot += get_cum_dims(y, mon) + mday;
        return tot;
}


// adapted from 
// http://my-small-projects.blogspot.com/2015/05/arduino-checking-for-british-summer-time.html
// mon0 [0,11]
// day1 [1,31] month day
int isbst(int year, int mon0, int day1, int hr)
{
        int imonth = mon0+1;
        int iday = day1;
        //int hr = hour();

        //January, february, and november are out.
        if (imonth < 3 || imonth > 10) { return 0; }
        //April to September are in
        if (imonth > 3 && imonth < 10) { return 1; }

        // find last sun in mar and oct - quickest way I've found to do it
        // last sunday of march
        int lastMarSunday =  (31 - (5* year /4 + 4) % 7);
        //last sunday of october
        int lastOctSunday = (31 - (5 * year /4 + 1) % 7);

        //In march, we are BST if is the last sunday in the month
        if (imonth == 3) {
                if( iday > lastMarSunday) return 1;
                if( iday < lastMarSunday) return 0;
                if (hr < 1) return 0;
                return 1;

        }
        //In October we must be before the last sunday to be bst.
        //That means the previous sunday must be before the 1st.
        if (imonth == 10) {
                if( iday < lastOctSunday) return 1;
                if( iday > lastOctSunday) return 0;
                if (hr >= 1) return 0;
                return 1;
        }

        return 0; // should never reach here, but keep compiler quiet
}


// adjust for BST if necessary
// mon in [0, 11]
// day1 in [1, 31]
void adjbst(int *yr, int *mon0, int *day1, int *hr)
{
        if(isbst(*yr, *mon0, *day1, *hr) ==0) return;
        *hr += 1;
        if(*hr != 24) return;
        *hr = 0;
        *day1 +=1;
        if(*day1 <  dims[*mon0]) return;
        *day1 = 1;
        *mon0 +=1;
}

