;(define id (make-parameter 42))

(define-syntax-rule (gnat-loop w e ...)
		    (let ((w (lambda (x) (display (+ 1 x)))))
		      e ...))



(gnat-loop w
  (w 20)
  (w 30))
