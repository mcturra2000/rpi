# Touch activation 

Using a transistor to touch-activate a pin. `method2()` seems to work best, and uses a gpio pin.

## Hook-up

* GP16 - LED - 220 ohm - GND
* GP26 - Qc
* 3v - Qe
* 3v - Touch pad - Qb


Q is transistor (with *b*ase, *e*mitter, *c*ollector). 2222 and 3904 works.

Schematic: db07.115

## Notes

The following materials help conductivity:
* E45 cream
* water


## Status

2022-09-29	Started. Works
