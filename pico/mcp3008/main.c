#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
//#include "hardware/adc.h"
//#include "hardware/clocks.h"
//#include "hardware/flash.h"
#include "hardware/gpio.h"
//#include "hardware/irq.h"
//#include "hardware/pwm.h"
#include "hardware/spi.h"
#if LIB_PICO_STDIO_USB
#pragma message "USB configured as stdio"
#include "tusb.h" // if you want to use tud_cdc_connected()
#endif


#include "pi.h"

#if 0
int Adafruit_MCP3008::SPIxADC(uint8_t channel, bool differential) {
	// see datasheet sec 6.1
	buffer[0] = 0x01;
	buffer[1] = ((differential ? 0 : 1) << 7) | (channel << 4);
	spi_dev->beginTransaction();
	digitalWrite(_cs, LOW);
	spi_dev->transfer(buffer, 3);
	digitalWrite(_cs, HIGH);
	spi_dev->endTransaction();
	return (((uint16_t)(buffer[1] & 0x07)) << 8) | buffer[2];
}
#endif

int mcp3008_read(int channel, bool differential)
{
#define SCK 2 
#define MOSI 3
#define MISO 4
#define CS 5

	static spi_inst_t *spi = 0;
	if(spi ==0) {
		spi = spi0;
		int baud = 1000000;
		spi_init(spi0, baud);
		//spi_set_format(spi0, 16, SPI_CPOL_0 , SPI_CPHA_0, SPI_MSB_FIRST);
		gpio_set_function(SCK,  GPIO_FUNC_SPI);
		gpio_set_function(MOSI, GPIO_FUNC_SPI);
		gpio_set_function(MISO, GPIO_FUNC_SPI);
		pi_gpio_init(CS, OUTPUT);
		gpio_put(CS, 1);

	}
	if(channel <0 || channel >7) return -1;
	uint8_t buffer[3], out[3];
	// see datasheet sec 6.1
	buffer[0] = 0x01;
	buffer[1] = ((differential ? 0 : 1) << 7) | (channel << 4);
	gpio_put(CS, 0);
	spi_write_read_blocking(spi, buffer, out, 3);
	gpio_put(CS, 1);
	return (((uint16_t)(out[1] & 0x07)) << 8) | out[2];

}

int main() 
{
	stdio_init_all();
#if LIB_PICO_STDIO_USB
	while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 
#endif


#define BTN  14 // GPIO number, not physical pin
#define LED  25 // GPIO of built-in LED
	gpio_init(BTN);
	gpio_set_dir(BTN, GPIO_IN);
	gpio_pull_up(BTN);
	// gpio_get() gets state of pin

	// maybe stabilise ADC
	gpio_set_dir(23, GPIO_OUT);
	gpio_put(23, 1);

	gpio_init(LED);
	gpio_set_dir(LED, GPIO_OUT);

	int i = 0;
	for(;;) {
		int val = mcp3008_read(0, false);
		printf("val %d = %d\n", i++, val);
#if 1
		gpio_put(LED, 1);
		sleep_ms(100);
		gpio_put(LED, 0);
		sleep_ms(900);
#else
		sleep_ms(1);
#endif
	}

	return 0;
}

