from PIL import Image
import struct


def convert(iname, oname):
	img = Image.open(iname).convert("RGB")
	img = img.rotate(180, expand=True)
	#img = img.rotate(90, expand=True)
	px = img.load()

	raw = open(oname, "wb")
	w = img.size[0]
	assert(w == 320)
	h = img.size[1]
	assert(h == 240)
	for x in range(w): # 320
		for y in range(h): # 240
			r,g, b = px[319-x, y]
			r5 = r >> 3
			g6 = g >> 2
			b5 = b >> 3
			v565 = (r5 <<11) + (g6 << 5) + b5
			raw.write(struct.pack('>H', v565)) # unsigned short big-endian 
	raw.close()
	img.show()


def process(name):
	d = '/home/pi/assets/'
	iname = d + name + ".gif"
	oname = d + name + ".raw"
	convert(iname, oname)
	
#iname = '/home/pi/assets/flag.gif'
#oname = '/home/pi/assets/flag.raw'
#convert(iname, oname)

process("flag")
process("crown")
