#include "basal.h"
#include "delay.h"


/* very approximately, 10 nops take about 4.5us (likely less) at 80MHz
*/


/* approx delay in ms without systick */
void __attribute__((optimize("O0"))) delayish (uint32_t ms)
{    
	// TODO SystemCoreClock needs to be a denominator, not numerator
	//uint32_t n = SystemCoreClock / 16000 * 10 /15; // calibrated using logic analyser
	static uint32_t n = 3303; // based on a clock speed of 80MHz
	//if(n==0) n = 100000 / (SystemCoreClock / 1000000);
	for(uint32_t i=0; i<ms; i++)
		nops(n);
}
