/*
 * Init code
 *
 * 2022-02-19	mcarter started
 */

#include <stddef.h>

#include "basal.h"


/*
void __attribute__((optimize("O0"))) nops(uint32_t n)
{
	for(uint32_t i = 0; i < n; i++) nop();
}
*/


//uint32_t SystemCoreClock = 4000000;

uint32_t SystemCoreClock = 80000000;


void SystemInit(void)
{
	/* FPU settings ------------------------------------------------------------*/
#if (__FPU_PRESENT == 1) && (__FPU_USED == 1)
	SCB->CPACR |= ((3UL << 10*2)|(3UL << 11*2));  /* set CP10 and CP11 Full Access */
#endif
	/* Reset the RCC clock configuration to the default reset state ------------*/
	/* Set MSION bit */
	RCC->CR |= RCC_CR_MSION;

	/* Reset CFGR register */
	RCC->CFGR = 0x00000000;

	/* Reset HSEON, CSSON , HSION, and PLLON bits */
	RCC->CR &= (uint32_t)0xEAF6FFFF;

	/* Reset PLLCFGR register */
	RCC->PLLCFGR = 0x00001000;

	/* Reset HSEBYP bit */
	RCC->CR &= (uint32_t)0xFFFBFFFF;

	/* Disable all interrupts */
	RCC->CIER = 0x00000000;

	/* Configure the Vector Table location add offset address ------------------*/
	#define VECT_TAB_OFFSET  0x00 // mcarter added
#ifdef VECT_TAB_SRAM
	SCB->VTOR = SRAM_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal SRAM */
#else
	SCB->VTOR = FLASH_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal FLASH */
#endif
}



#if !defined  (HSE_VALUE)
  #define HSE_VALUE    ((uint32_t)8000000) /*!< Value of the External oscillator in Hz */
#endif /* HSE_VALUE */

#if !defined  (MSI_VALUE)
  #define MSI_VALUE    ((uint32_t)4000000) /*!< Value of the Internal oscillator in Hz*/
#endif /* MSI_VALUE */

#if !defined  (HSI_VALUE)
  #define HSI_VALUE    ((uint32_t)16000000) /*!< Value of the Internal oscillator in Hz*/
#endif /* HSI_VALUE */

  const uint8_t  AHBPrescTable[16] = {0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 6, 7, 8, 9};
  const uint8_t  APBPrescTable[8] =  {0, 0, 0, 0, 1, 2, 3, 4};
  const uint32_t MSIRangeTable[12] = {100000, 200000, 400000, 800000, 1000000, 2000000, \
                                      4000000, 8000000, 16000000, 24000000, 32000000, 48000000};
                                      
                                      
void SystemCoreClockUpdate(void)
{
	uint32_t tmp = 0, msirange = 0, pllvco = 0, pllr = 2, pllsource = 0, pllm = 2;

	/* Get MSI Range frequency--------------------------------------------------*/
	if((RCC->CR & RCC_CR_MSIRGSEL) == RESET)
	{ /* MSISRANGE from RCC_CSR applies */
		msirange = (RCC->CSR & RCC_CSR_MSISRANGE) >> 8;
	}
	else
	{ /* MSIRANGE from RCC_CR applies */
		msirange = (RCC->CR & RCC_CR_MSIRANGE) >> 4;
	}
	/*MSI frequency range in HZ*/
	msirange = MSIRangeTable[msirange];

	/* Get SYSCLK source -------------------------------------------------------*/
	switch (RCC->CFGR & RCC_CFGR_SWS)
	{
		case 0x00:  /* MSI used as system clock source */
			SystemCoreClock = msirange;
			break;

		case 0x04:  /* HSI used as system clock source */
			SystemCoreClock = HSI_VALUE;
			break;

		case 0x08:  /* HSE used as system clock source */
			SystemCoreClock = HSE_VALUE;
			break;

		case 0x0C:  /* PLL used as system clock  source */
			/* PLL_VCO = (HSE_VALUE or HSI_VALUE or MSI_VALUE/ PLLM) * PLLN
			   SYSCLK = PLL_VCO / PLLR
			   */
			pllsource = (RCC->PLLCFGR & RCC_PLLCFGR_PLLSRC);
			pllm = ((RCC->PLLCFGR & RCC_PLLCFGR_PLLM) >> 4) + 1 ;

			switch (pllsource)
			{
				case 0x02:  /* HSI used as PLL clock source */
					pllvco = (HSI_VALUE / pllm);
					break;

				case 0x03:  /* HSE used as PLL clock source */
					pllvco = (HSE_VALUE / pllm);
					break;

				default:    /* MSI used as PLL clock source */
					pllvco = (msirange / pllm);
					break;
			}
			pllvco = pllvco * ((RCC->PLLCFGR & RCC_PLLCFGR_PLLN) >> 8);
			pllr = (((RCC->PLLCFGR & RCC_PLLCFGR_PLLR) >> 25) + 1) * 2;
			SystemCoreClock = pllvco/pllr;
			break;

		default:
			SystemCoreClock = msirange;
			break;
	}
	/* Compute HCLK clock frequency --------------------------------------------*/
	/* Get HCLK prescaler */
	tmp = AHBPrescTable[((RCC->CFGR & RCC_CFGR_HPRE) >> 4)];
	/* HCLK clock frequency */
	SystemCoreClock >>= tmp;
}

#define MODIFY_REG(REG, CLEARMASK, SETMASK)  WRITE_REG((REG), (((READ_REG(REG)) & (~(CLEARMASK))) | (SETMASK)))

__STATIC_INLINE void LL_RCC_PLL_ConfigDomain_SYS(uint32_t Source, uint32_t PLLM, uint32_t PLLN, uint32_t PLLR)
{
  MODIFY_REG(RCC->PLLCFGR, RCC_PLLCFGR_PLLSRC | RCC_PLLCFGR_PLLM | RCC_PLLCFGR_PLLN | RCC_PLLCFGR_PLLR,
             Source | PLLM | (PLLN << RCC_PLLCFGR_PLLN_Pos) | PLLR);
}

// to 80MHz
void SystemClock_Config(void)
{
	FLASH->ACR &= ~0b111;
	FLASH->ACR |= 4; // Set flash latency to 4. Apparently necessary.
	while(((FLASH->ACR) & 0b111) != 4); // Are we there yet?

	MODIFY_REG(PWR->CR1, PWR_CR1_VOS, 1<<9); // set voltage scaling
	RCC->CR |= RCC_CR_MSION; // enable MSI oscillator
	while((RCC->CR & RCC_CR_MSIRDY) ==0); // wait until it's ready
	
	SET_BIT(RCC->CR, RCC_CR_MSIRGSEL); // enable msi range selection
	MODIFY_REG(RCC->CR, RCC_CR_MSIRANGE, 6<<4); // set msi range to 6
	MODIFY_REG(RCC->ICSCR, RCC_ICSCR_MSITRIM, 0 << RCC_ICSCR_MSITRIM_Pos); // set calibration trimming
	LL_RCC_PLL_ConfigDomain_SYS(1, 0, 40, 0);
	SET_BIT(RCC->PLLCFGR, RCC_PLLCFGR_PLLREN); // PLL enable domain sys
	//RCC->PLLCFGR = (40<<RCC_PLLCFGR_PLLN_Pos) | RCC_PLLCFGR_PLLSRC_MSI; // Confiure PLL to use MSI, inc freq
	//RCC->PLLCFGR |= RCC_PLLCFGR_PLLREN; // Enable PLL
	RCC->CR |= RCC_CR_PLLON; // Turn on the PLL.
	while((RCC->CR & RCC_CR_PLLRDY) == 0); // Wait until it is ready. So many steps!
	MODIFY_REG(RCC->CFGR, RCC_CFGR_SW, 0b11); // set sys clk source as PLL
	//RCC->CFGR = 0b111; // set PLL as system clock
	while((RCC->CFGR & RCC_CFGR_SWS) == 0); // wait until PLL has been set up
	
	return;
	// init systick
	SysTick->LOAD  = (uint32_t)((80000000 / 1000) - 1UL);  /* set reload register */
	SysTick->VAL   = 0UL;                                       /* Load the SysTick Counter Value */
	SysTick->CTRL  = SysTick_CTRL_CLKSOURCE_Msk |
                   	SysTick_CTRL_ENABLE_Msk;                   /* Enable the Systick Timer */
}

void jagan_init1(void)
{
	SystemInit();
}

void jagan_init2(void)
{


	
	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	volatile uint32_t tmp;
	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
	tmp = RCC->APB2ENR; // a no-op
	RCC->APB1ENR1 |= RCC_APB1ENR1_PWREN;
	tmp = RCC->APB1ENR1; // a kind of no-op
	#define NVIC_PRIORITYGROUP_4         ((uint32_t)0x00000003) // mcarter added
	NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
	
	SystemClock_Config();
	SystemCoreClockUpdate();
	//SysTick_Config(SystemCoreClock/1000); // set tick to every 1ms
	
}

