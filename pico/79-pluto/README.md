# Pluto : Pico Light Unikernel Tinkering Oddity

MIGHT BE A PLANET, MIGHT JUST BE A BIG BALL OF ICE.


PLUTO is an attempt at writing a small DOS-like kernel for the Raspberry Pi Pico RP2040 MCU (Microcontroller).
It hopes to be small and fast, comparable in power to an early 80's computer like the ZX Spectrum.
Its features include a command prompt, SD card support, audio and USB keyboard/uart.

## Table of Contents

* [Progs: standalone programs](progs/README.md)
* [Status](#status)


## References

* [Position-Independent Code](https://mcuoneclipse.com/2021/06/05/position-independent-code-with-gcc-for-arm-cortex-m/)

## Status

2023-01-03	Started. Working, with keyboard and TFT.
