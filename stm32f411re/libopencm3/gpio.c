#include "gpio.h"

void gpio_enable_rcc(uint32_t gpioport)
{
	enum rcc_periph_clken clken;
	switch(gpioport) { // possibly incomplete
		case GPIOA: clken = RCC_GPIOA; break;
		case GPIOB: clken = RCC_GPIOB; break;
		case GPIOC: clken = RCC_GPIOC; break;
		case GPIOD: clken = RCC_GPIOD; break;
	}
	rcc_periph_clock_enable(clken);
}

void gpio_out(uint32_t gpioport, uint16_t gpios)
{
	gpio_enable_rcc(gpioport);
	gpio_mode_setup(gpioport, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, gpios);
}


void gpio_inpull(uint32_t gpioport, uint16_t gpios)
{
	gpio_enable_rcc(gpioport);
	gpio_mode_setup(gpioport, GPIO_MODE_INPUT, GPIO_PUPD_PULLUP, gpios);
}


void gpio_set_level(port_t port, pin_t pin, uint32_t value)
{
	if(value)
		gpio_set(port, pin);
	else
		gpio_clear(port, pin);

}
#if 0





void gpio_set_opendrain(GPIO_TypeDef *port, uint32_t pin)
{
        port->OTYPER |= (1<<pin);
}


void gpio_set_moder(GPIO_TypeDef *port, uint32_t pin, int val)
{
	port->MODER	&= ~(0b11<<(pin*2)); // important to reset it, as its default is analog
	port->MODER	|= (val<<(pin*2));
}

void gpio_set_ospeedr(GPIO_TypeDef *port, uint32_t pin, int val)
{
	port->OSPEEDR	&= ~(0b11<<(pin*2)); 
	port->OSPEEDR	|= (val<<(pin*2));
}

void gpio_set_pupdr(GPIO_TypeDef *port, uint32_t pin, int val)
{
	port->PUPDR	&= ~(0b11<<(pin*2)); 
	port->PUPDR	|= (val<<(pin*2));
}

void gpio_alt(GPIO_TypeDef *port, uint32_t pin, uint8_t alt_fn)
{
	gpio_enable_rcc(port);
	gpio_set_moder(port, pin, 0b10);

	int pos = pin*4;
	uint8_t idx = 0;
	if(pin>7) { // not tested properly
		pos -= 32;
		idx = 1;
	}
	port->AFR[idx] &= ~(0b1111  << pos); // mask out
	port->AFR[idx] |= (alt_fn  << pos);
}



void gpio_in(GPIO_TypeDef *port, uint32_t pin)
{
	gpio_enable_rcc(port);
	gpio_set_moder(port, pin, 0b00);
	gpio_set_pupdr(port, pin, 0b11);
}

void gpio_pullup(GPIO_TypeDef *port, uint32_t pin)
{
	gpio_set_pupdr(port, pin, 0b01);
}

void gpio_inpull(GPIO_TypeDef *port, uint32_t pin)
{
	gpio_in(port, pin);
	gpio_pullup(port, pin);
}
#endif
