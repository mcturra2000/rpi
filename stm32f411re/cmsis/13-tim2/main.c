#define STM32F411xE
#include "stm32f4xx.h"

#include <math.h>

#include <stdbool.h>

#include <delay.h>
#include <gpio.h>
#include <tim.h>

#define OUT 	GPIOA, 0 // speaker ondb7.096


const float dt = 1000000.0/40000.0; // time increment (40kHz)
float t =0; // time, modulo the wave
const float t2 = 1000000.0/440; // period of 440Hz
const float t1 = t2 *0.25;// duty cycle of 25%


void TIM2_IRQHandler(void)
{
	if(TIM2->SR & TIM_SR_UIF) TIM2->SR = ~TIM_SR_UIF; // clear flag

	t += dt; 
	if(t>t2) t -= t2; // modulus the time period
	if(t>t1)
		gpio_set(OUT);
	else
		gpio_clr(OUT);
}




int main(void)
{
	SCB->CPACR |= (0xF<<20); // turn on FPU
	tim2_5_init(TIM2, 40000);
	gpio_out(OUT);

	while(1);

}

