#include "freertos/FreeRTOS.h"
#include <Arduino.h>
#include <Wire.h>

//#include <optional>

#include <TM1637Display.h>
#include <cal.h>
#include <debounce.h>
#include <ds3231.h>
#include <commands.h>
#include <button.h>
#include <state.h>
#include <main.h>
//#include <opt.h>

//#define SPK2 5
#define DBG 23
#define DAC1 25

#define TM1637_CLK 32
#define TM1637_DIO 33

//#define BTN_WHITE 	19

Button btn_green(18);
Button btn_white(19);

void init_date(void)
{
	return;
	struct ts t;
	t.year = 1922;
	t.mon = 5;
	t.mday = 18;
	t.hour = 18;
	t.min = 00;

	DS3231_set(t);
}

TM1637Display display(TM1637_CLK, TM1637_DIO);

extern "C" void i2c_transfer7(uint32_t i2c, uint8_t addr, const uint8_t *w, size_t wn, uint8_t *r, size_t rn)
{
	if (wn)
	{
		Wire.beginTransmission(addr);
		Wire.write(w, wn);
		Wire.endTransmission();
	}

	if (rn)
	{
		Wire.requestFrom(addr, rn);
		// while(Wire.available() )
		Wire.readBytes(r, rn);
	}
}

// see db5.24
const double fc = 500, fs = 16000, alpha = 2.0 * 3.1415926535897 * fc / fs, beta = 1.0 / (1.0 + alpha);
// fpt alpha = fl2fpt(2.0 * 3.1415926535897 * fc/fs);

hw_timer_t *timer = NULL;
hw_timer_t *timer_millis = NULL;

volatile millis_t millis_ticks =0;
void IRAM_ATTR on_timer_millis()
{
	if (millis_ticks % 4 == 0)
	{
		if (btn_white.falling())
			toggle_noise_state();
		if (btn_green.falling())
			toggle_om_state();
	}

	do_om_tick();

#if 0
	if (millis_ticks % 1024 == 0 && false) {
		Serial.printf("millis timer: %d\n", millis_ticks);
	}
#endif
	millis_ticks++;
}
// apparently can't use floats in an isr, which uses FPU, and will cause a crash.
// can use double, though, which is computed in software. Oh well, it is what it is.
void IRAM_ATTR onTimer()
{
	volatile static double vout = 0;
	digitalWrite(DBG, HIGH);

	
	if(play_tone) {
		static uint8_t val = 0;
		vout = 255 - val;
		dacWrite(DAC1, vout);
	} else if(play_noise) 	{
		uint8_t val = esp_random() >> 31 ? 255 : 0;
		vout = beta * (alpha * (double)val + vout);
		dacWrite(DAC1, vout);
	} else {
		dacWrite(DAC1,  0);
	}
	digitalWrite(DBG, LOW);
}

void setup()
{
	pinMode(LED_BUILTIN, OUTPUT);
	pinMode(DBG, OUTPUT);

	timer = timerBegin(1, 80, true);
	timerAttachInterrupt(timer, &onTimer, true);
	timerAlarmWrite(timer, 1000000 / fs, true);
	//timerAlarmWrite(timer, 1000000 / 800, true);
	timerAlarmEnable(timer);

	timer_millis = timerBegin(0, 80, true);
	timerAttachInterrupt(timer_millis, &on_timer_millis, true);
	timerAlarmWrite(timer_millis, 1000000 / 1000, true);
	timerAlarmEnable(timer_millis);

	// pinMode(DAC1, OUTPUT);
	dacWrite(DAC1, 0);
	// pinMode(SPK2, OUTPUT);

	display.setBrightness(0x0f);

	Serial.begin(115200);
	init_state();

	Wire.begin(21, 22); // sda, scl
	DS3231_init(DS3231_CONTROL_INTCN, 0);
	init_date();
	init_picol();
}
void loop()
{
	// return;
	static int num = 0;
	opt<millis_t> om_elapsed_ms = get_om_elapsed();
	if (om_elapsed_ms.has_value())
	{
		num = om_elapsed_ms.value() / 1000;
		num = (num / 60) * 100 + (num % 60);
	}
	else
	{
		char msg[100];
		// Serial.printf("\nCount: %d\n", count++);
		struct ts t;
		DS3231_get_uk(&t);
		// Serial.printf("%d.%02d.%02d %02d:%02d:%02d\n", t.year, t.mon, t.mday, t.hour, t.min, t.sec);
		num = t.hour * 100 + t.min;
		// printf("hello\n");
	}

	display.showNumberDec(num, false);
	delay(1000);
}
