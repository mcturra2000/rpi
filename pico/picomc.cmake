#message("picomc.cmake says hi")

set(PICO_PLATFORM rp2040)
#project(app C CXX ASM) # needs to be in the projects file, I think
set(CMAKE_C_STANDARD 23)
set(CMAKE_CXX_STANDARD 17)



include($ENV{PICO_SDK_PATH}/pico_sdk_init.cmake)
include($ENV{RPI}/shims-pico/shims.cmake)
include_directories($ENV{PICOMC})
#set(SHIMS $ENV{RPI}/shims-pico)
#include_directories(${SHIMS})
#add_subdirectory(${SHIMS} build)
#include_directories($ENV{RPI}/pot)
add_subdirectory($ENV{PICOMC} build)
#pico_sdk_init()
include($ENV{PICOMC}/ili9341/ili9341.cmake)
include($ENV{PICOMC}/usb-kbd/usb-kbd.cmake)



# utility for flashing begin
add_custom_target(flash DEPENDS app.uf2)
#if(EXISTS /run/media/$ENV{USER}) 
#	set(rpi /run/media/$ENV{USER})
#endif()
#if(EXISTS /media/$ENV{USER})
#	set(rpi /media/$ENV{USER})
#endif()
#set(uf2 ${CMAKE_CURRENT_SOURCE_DIR}/build/app.uf2)
add_custom_command(TARGET flash
	USES_TERMINAL
	#	COMMAND  cp app.uf2 ${rpi}/RPI-RP2/app.uf2
	COMMAND sudo picotool load -f app.uf2
)
# utility for flashing end
