#include "i2c.h"

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/i2c.h>

void i2c1_init(void)
{
	rcc_periph_clock_enable(RCC_GPIOB);
	//rcc_periph_clock_enable(RCC_GPIOH);
	//rcc_set_i2c_clock_hsi(I2C1);

	i2c_reset(I2C1);
	/* Setup GPIO pin GPIO_USART2_TX/GPIO9 on GPIO port A for transmit. */
	gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO6 | GPIO7);
	gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO6 | GPIO7);
	//gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_PULLDOWN, GPIO6 | GPIO7);

	// it's important to set the pins to open drain
	gpio_set_output_options(GPIOB, GPIO_OTYPE_OD, GPIO_OSPEED_25MHZ, GPIO6 | GPIO7);
	gpio_set_af(GPIOB, GPIO_AF4, GPIO6 | GPIO7);
	rcc_periph_clock_enable(RCC_I2C1);
	i2c_peripheral_disable(I2C1);	
	//i2c_set_speed(I2C1, i2c_speed_sm_100k, rcc_apb1_frequency/1000000);
	i2c_set_speed(I2C1, i2c_speed_fm_400k, rcc_apb1_frequency/1000000);
	//i2c_set_speed(I2C1, i2c_speed_sm_100k, 8);
	//i2c_set_standard_mode(I2C1); // mcarter added 2021-11-23
	i2c_peripheral_enable(I2C1);
}

/*
this function is equivalent to calling
i2c_transfer7(i2c, addr, w, wn, 0, 0);
*/
void __attribute__((weak)) i2c_ship(uint32_t i2c, uint8_t addr, const uint8_t *w, size_t wn) 
{
	 //i2c_write7_v1(i2c, addr, w, wn) begin:
	 while ((I2C_SR2(i2c) & I2C_SR2_BUSY));
         i2c_send_start(i2c);
 
         /* Wait for the end of the start condition, master mode selected, and BUSY bit set */
         while ( !( (I2C_SR1(i2c) & I2C_SR1_SB)
                 && (I2C_SR2(i2c) & I2C_SR2_MSL)
                 && (I2C_SR2(i2c) & I2C_SR2_BUSY) ));
 
         i2c_send_7bit_address(i2c, addr, I2C_WRITE);
 
         /* Waiting for address is transferred. */
         while (!(I2C_SR1(i2c) & I2C_SR1_ADDR));
 
         /* Clearing ADDR condition sequence. */
         (void)I2C_SR2(i2c);
 
         for (size_t i = 0; i < wn; i++) {
                 i2c_send_data(i2c, *(w+i));
                 while (!(I2C_SR1(i2c) & (I2C_SR1_BTF)));
         }
	 //i2c_write7_v1(i2c, addr, w, wn) end
         
	 i2c_send_stop(i2c);
}
