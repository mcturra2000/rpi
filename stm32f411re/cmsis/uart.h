#pragma once

/*
#define STM32F411xE
#include "stm32f4xx.h"
#include <delay.h>
#include <printf.h>
#include <stdbool.h>
#include <uart.h>
*/

int putchar(int c);
int puts(const char *s);
int getchar();
void uart_init (void);

//void _putchar(char c) { putchar(c); }
