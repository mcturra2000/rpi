//#include <conf.h>
#include <delay.h>
#include <gpio.h>
#include <libopencm3/stm32/usart.h>
//#include "jg_stdio.h"
#include "uart.h"

#define LED	LED_BUILTIN  //PB3



int main(void)
{
	mal_usart_init(); // required
	//mal_usart_init();
	//rcc_periph_clock_enable(RCC_USART2);
	//usart_set_baudrate(USART2, 115200);
	//usart_enable(USART2);

	//gpio_out(LED);
	char* msg = "Example 06-uart-echo 5\n";
	//uart2_send_blocking(msg, strlen(msg));
	mal_usart_print(msg);
	//int count = 0;
	mal_usart_print("I will now echo your typing...\n");
	while (1)
	{
		//gpio_toggle(LED);
		uint8_t c = usart_recv_blocking(USART2);
		usart_send_blocking(USART2, c);
		//delayish(900);
	}
}
