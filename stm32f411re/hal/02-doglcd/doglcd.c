/*
 * doglcd.c
 *
 *  Created on: Oct 29, 2021
 *      Author: pi
 */

#include <string.h>
//#include "stm32f4xx_hal_conf.h"

#include "main.h"
#include "doglcd.h"

#define LOW 0
#define HIGH 1

#define CS 	GPIOA, GPIO_PIN_4
#define RS 	GPIOA, GPIO_PIN_6

extern SPI_HandleTypeDef hspi1;

static void pause(int ms)
{
	return;
	HAL_Delay(ms);
}
/* rs_val = LOW to send command, HIGH to send ASCII char
*/
void doglcd_send_bytes(GPIO_PinState rs_val, uint8_t* data, int size)
{
	HAL_GPIO_WritePin(RS, rs_val);
	pause(1);
	HAL_GPIO_WritePin(CS, LOW);
	HAL_SPI_Transmit(&hspi1, data, size, 100);
	HAL_GPIO_WritePin(CS, HIGH);
	pause(60);
	//sleep_ms(60);
}

void doglcd_send_cmd(uint8_t cmd)
{
	doglcd_send_bytes(LOW, &cmd, 1);
	pause(1);
	//sleep_ms(1);
}

void doglcd_write_char(char c)
{
	doglcd_send_bytes(HIGH, (uint8_t*) &c, 1);
}

void doglcd_write_str(const char *str)
{
	doglcd_send_bytes(HIGH, (uint8_t*) str, strlen(str));
	/*
	digitalWrite(RS, HIGH);
	digitalWrite(CS, LOW);
	spi_write_blocking(SPI_PORT, (unsigned char*) str, strlen(str));
	digitalWrite(CS, HIGH);
*/
}

void doglcd_double_height(bool tall)
{
	// doesn't seem to work properly
	doglcd_send_cmd((1<<4) + (tall ? 0b1000 : 0));
}
void doglcd_home()
{
	// goes to top-left
	doglcd_send_cmd(1<<1);
}

void doglcd_cls()
{
	doglcd_send_cmd(1);
	doglcd_home();
}

// move cursor to position x,y (1st Row and 1st column is position 0,0)
// for a 163 display : x = 0..15   y = 0..2
// 3-line display assumed here. for others, see:
// https://forum.arduino.cc/t/library-for-electronic-assembly-dog-displays/36988

void doglcd_gotoxy(int x, int y) {
	//return;
	int addr = 0x80;
	if (y<1) addr = 0x80+x;
	if (y==1)
		addr = 0x90+x;
	else if (y>1) addr = 0xA0+x;
	doglcd_send_cmd(addr);
}




void doglcd_init()
{
	/*
        spi_init(SPI_PORT, 500 * 1000); // 500K works, 1000K is too fast
        gpio_set_function(SCK, GPIO_FUNC_SPI);
        gpio_set_function(MOSI, GPIO_FUNC_SPI);

        pi_gpio_out(RS);
        pi_gpio_out(CS);
        gpio_put(CS, 1);
*/

#if 1 // prolly for 5V
        const uint8_t contrast = 0x70  | 0b1000; // from 0x7C
        const uint8_t display = 0b1111; // ori 0x0F
        uint8_t cmds[] = {0x39, 0x1D, 0x50, 0x6C, contrast , 0x38, display, 0x01, 0x06};
#else
        uint8_t cmds[] = {0x39, 0x15, 0x55, 0x6e, 0x72, 0x38, 
                0x0F, 0x01, 0x06};
#endif
	for(int i = 0; i< sizeof(cmds); i++) doglcd_send_cmd(cmds[i]);

}
