/* https://stackoverflow.com/questions/15594988/objcopy-prepends-directory-pathname-to-symbol-name */

	.section .rodata
	.global blob
	//.type blob, @object
	.balign 4	
blob:
	//.incbin "pico/69-embedded-binary/main.c"
	//.incbin "../README.md"
	.incbin "../pluto.raw"

	.global blob_end
blob_end:



	.global blob_size
	//.type blob_size, @object
	.balign 4
blob_size:
	.int blob_end - blob

