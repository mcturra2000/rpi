# BME688 4-IN-1 QUALITY BREAKOUT

"The first air quality sensor with artificial intelligence. The BME688 can measure temperature, pressure, humidy but also has enhanced gas scanner capabilities"

PIM575

**rpi-all.py** - performs all realdings on a Raspberry Pi


## Picture

![](bme688.jpg) 

## See also

* dev07 - temperatute display


## References

* [Purchase link](https://shop.pimoroni.com/products/bme688-breakout?variant=39336951709779)


## Status

2023-03-17	Bought
