#include <FreeRTOS.h>
#include <task.h>

#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"


#include "pi.h"

void gpio17_task()
{
	const uint PIN = 17;
	pi_gpio_out(PIN);

	while(1) {
		pi_gpio_set(PIN);
		vTaskDelay(250);
		pi_gpio_clr(PIN);
		vTaskDelay(250);
	}
}

void led_task()
{
	const uint PIN = PICO_DEFAULT_LED_PIN; // 25
	pi_gpio_out(PIN);

	while(1) {
		pi_gpio_set(PIN);
		vTaskDelay(100);
		pi_gpio_clr(PIN);
		vTaskDelay(900);
	}
}


int main() 
{
	xTaskCreate(led_task, "LED", 256, NULL, 1, NULL);
	xTaskCreate(gpio17_task, "LED", 256, NULL, 1, NULL);
	vTaskStartScheduler();
}

