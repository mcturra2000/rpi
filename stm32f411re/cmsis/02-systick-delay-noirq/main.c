#define STM32F411xE
#include "stm32f4xx.h"
#include "gpio.h"

uint32_t SystemCoreClock = 16000000;
//extern uint32_t SystemCoreClock;

void delay_ms(uint32_t ms)
{
	// fishy: I think I should be dividing by 1000 instead of 10000
	uint32_t ticks = SystemCoreClock/1600000*ms;
	//SysTick->CTRL = 0;
	SysTick->LOAD = ticks;
	SysTick->VAL = 0;
	SysTick->CTRL = SysTick_CTRL_ENABLE_Msk;

	// COUNTFLAG is a bit that is set to 1 when counter reaches 0.
	// It's automatically cleared when read.
	while ((SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk) == 0);
	SysTick->CTRL = 0;
}

int main (void) 
{
	gpio_out(PA5);
	gpio_out(PC13);

#if 0
	// init pin PC10 for output
	RCC->AHB1ENR     |= RCC_AHB1ENR_GPIOCEN; //RCC ON
	GPIOC->MODER    |= GPIO_MODER_MODER10_0; //mode out
	GPIOC->OTYPER   = 0;
	GPIOC->OSPEEDR  = 0;
#endif
	while(1) 
	{
		gpio_set(PA5);
		gpio_set(PC13);
		//GPIOC->ODR |= GPIO_ODR_OD10; // Turn PC10 on
		delay_ms(50); // wait 100ms
		//GPIOC->ODR &= ~GPIO_ODR_OD10; // Turn PC10 off
		gpio_set(PA5);
		gpio_set(PC13);
		delay_ms(950); // wait 900ms
	}
}
