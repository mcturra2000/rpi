# DEV02 home-made board

A board that can be used for testing purposes

## Schematics

```
               ⋅   U1
               ┌─────────┐
               ┤RST    3V├─ 3V
               ┤PB3   PB2├─ PZ1 ─ GND
3V ─ R3 ─ SW1 ─┤PB4   PB1├─ D1 ─ R1 ─ GND
          GND ─┤GND   PB0├─ D2 ─ R2 ─ GND
               └─────────┘

D1 - LED green
D2 - LED red

PZ1 - piezo buzzer

R1 - 220 ohm
R2 - 220 ohm
R3 - 10k ohm

SW1 - switch, pushbutton 

U1 - ATtiny85
```

## Subprojects

* [clapper](clapper) - a "clapperboard" that momentarily beeps and turns the green LED on. It is useful for videos.
