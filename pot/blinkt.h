#pragma once
#if defined(USE_HAL_DRIVER)
#include <stdint.h>
#include <gpio.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * You must configure the pins for output yourself
 */
void blinkt_init(port_t port, pin_t dataPin, pin_t clockPin);

void blinkt_show(void);
void blinkt_set_pixel_colour(uint8_t pos , uint8_t r, uint8_t g, uint8_t b);
void blinkt_test();

#ifdef __cplusplus
}
#endif
#endif
