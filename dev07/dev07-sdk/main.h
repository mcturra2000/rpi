#pragma once

#include "pico/multicore.h"

#include <ds3231.h>

#include "cal.h"
#include "delay.h"
#include "gpio.h"


void ds3231_get_uk_with_mtx(struct tm *t);

void bme_setup();
int bme_degC();

void  serial_setup();
void serial_task();

int ds_hhmm();
void ds_get_str(char *msg, int len);
void DS3231_set_int(long long val);

class Btn {
  public:
  Btn(int id);
  bool update();
  private:
  int _id;
  uint8_t _grate = 255;
};
