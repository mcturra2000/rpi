/* hal7a1.c - generate brown noise (see zettel 7a1)
 *  In MX:
 *  	Mode:
 *  		Clock source: internal clock
 *  		Channel 1: PWM Generation CH1
 *  	Parameter Setting:
 *  		Prescaler: 0
 *  		Counter Period: 5000-1 // == 100MHz clock / 20kHz sample frequency
 *  		Mode: PWM Mode
 *  	NVIC:
 *  		Time global interrupt: enable
 *
 *  In main():
 *  	HAL_TIM_PWM_Start_IT (&htim3, TIM_CHANNEL_1);
 *
 *  Output will be on PA6 TIM3_CH1
 */

#include <stdlib.h>
#include "main.h"

void HAL_TIM_PWM_PulseFinishedCallback(TIM_HandleTypeDef *htim)
{
	const double fc = 400; // cutoff frequency
	const double fs = 20000; // sample frequency
	const double K = 2 * 3.14159265 * fc / fs;
	static double vc = 0; // voltage across cap

	int va = 0;
	if(rand() % 2 == 0) va = __HAL_TIM_GET_AUTORELOAD(htim)/2;
	vc = vc + K * ((double)va - vc); // derived from db05.296
	__HAL_TIM_SET_COMPARE(htim, TIM_CHANNEL_1, vc);
}

