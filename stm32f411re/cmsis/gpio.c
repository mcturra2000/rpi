#include "gpio.h"


static void gpio_enable_rcc(GPIO_TypeDef *port)
{
	uint32_t en_val = RCC_AHB1ENR_GPIOAEN;
	if(port == GPIOB) { en_val = RCC_AHB1ENR_GPIOBEN; }
	else if(port == GPIOC) { en_val = RCC_AHB1ENR_GPIOCEN; }
	else if(port == GPIOD) { en_val = RCC_AHB1ENR_GPIODEN; }
	RCC->AHB1ENR	|= en_val;
}

void gpio_alt(GPIO_TypeDef *port, uint32_t pin, uint8_t alt_fn)
{
	gpio_enable_rcc(port);
	port->MODER	|= (0b10<<(pin*2));

	int pos = pin*4;
	uint8_t idx = 0;
	if(pin>7) { // not tested properly
		pos -= 32;
		idx = 1;
	}
	port->AFR[idx] |= (alt_fn  << pos);
}


void gpio_out(GPIO_TypeDef *port, uint32_t pin)
{
	gpio_enable_rcc(port);
	port->MODER	|= (1<<(pin*2));
}

void gpio_in(GPIO_TypeDef *port, uint32_t pin)
{
	gpio_enable_rcc(port);
	//port->MODER	|= (1<<(pin*2)); // should really set it to 0
}

void gpio_pullup(GPIO_TypeDef *port, uint32_t pin)
{
	port->PUPDR	|= (1<<(pin*2));
}
