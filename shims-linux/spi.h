#pragma once
#include <stdint.h>

// see st7567.c for writing to spi

typedef struct spi_t {
	int fd;
} spi_t;

void spi0_0_init(spi_t *spi);
void spi_deinit(spi_t *spi);
