#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
#include "hardware/spi.h"
#include "hardware/gpio.h"
#include <math.h>

#include "pi.h"

#define Os __attribute__((optimize("Os")))

typedef uint8_t u8;
typedef uint16_t u16;

#define	PIN_SCK		10
#define	PIN_MOSI	11

#define	PIN_CS 		13


//#define LDAC		14 // doesn't seem to help

#define PULSE 18
#define ALARM 0


const int fs = 22000; // frequency sample
const int fw = 440; // frequency of wave

#define MHz 1000000
const uint64_t delay = 1*MHz / fs;



static inline void pin_low(uint8_t pin)
{
	gpio_put(pin, 0);
}

static inline void pin_high(uint8_t pin)
{
	gpio_put(pin, 1);
}


void mcp4921_init()
{

#if 0
	pi_gpio_init(PIN_CS, OUTPUT);
	pin_high(PIN_CS);
#else
	gpio_set_function(PIN_CS, GPIO_FUNC_SPI);
#endif
	int spi_speed = 8 * MHz;
	spi_init(spi1, spi_speed);
	spi_set_format(spi1, 16, 0, 0, SPI_MSB_FIRST);
	gpio_set_function(PIN_SCK,  GPIO_FUNC_SPI);
	gpio_set_function(PIN_MOSI, GPIO_FUNC_SPI);
}

static inline void mcp4921_write(uint16_t vol)
{
	if(vol>4095) vol = 4095;
	//const uint16_t ctl =  0b0011 << 12; // A/B  BUF GA SHDN - original settings
	const uint16_t ctl =  0b0111 << 12; // A/B  BUF GA SHDN 
	vol |= ctl;
	//pin_low(PIN_CS);
	//spi_write16_blocking(spi1, &vol, 1);
	//pin_high(PIN_CS);
	spi1_hw->dr = vol;
}


#define N 1024
uint32_t sine[N];
const float pi2 = 6.283185307179586;
const uint32_t dx = N * fw/fs;
//#define SHFT 22
//const uint32_t dx = ((1<<SHFT) /fs) *fw;

static volatile uint32_t x = 0;
static inline void __not_in_flash_func(write_to_dac)(void)
{
	//const float dx = pi2 * fw / fs;
	x += dx;
	if(x>=N) x -= N;
	//float vol = (sin(x) +1.0) * 4095.0/2.0;
	uint32_t vol = sine[x];
	mcp4921_write(vol);
}


static void __not_in_flash_func(alarm_0_irq)(void) 
{
	pi_alarm_rearm(ALARM, delay);
	pi_gpio_high(PULSE);
#ifdef LDAC
	gpio_put(LDAC, 0);
	gpio_put(LDAC, 1);
#endif
	write_to_dac();
	pi_gpio_low(PULSE);
}


int main() 
{
	stdio_init_all();
	mcp4921_init();

	for(int i = 0; i< N; i++ ) { sine[i] = (sin((float)i * pi2/(float)N) +1.0) * 4095.0/2.0; }

#ifdef LDAC
	gpio_init(LDAC);
	gpio_set_dir(LDAC, GPIO_OUT);
	gpio_put(LDAC, 1);
#endif

	gpio_init(PULSE);
	gpio_set_dir(PULSE, GPIO_OUT);

	pi_alarm_init(ALARM, alarm_0_irq, delay);
	for(;;) tight_loop_contents();

	return 0;
}

