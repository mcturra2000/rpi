#pragma once

#include <Arduino.h>
#include <DS3231.h>

#include "cal.h"
//extern DS3231 myRTC;

void bme_setup();
int bme_degC();

void  serial_setup();
void serial_task();

int ds_hhmm();
void ds_get_str(char *msg, int len);
void DS3231_set_int(long long val);

class Btn {
  public:
  Btn(int id);
  bool update();
  private:
  int _id;
  uint8_t _grate = 255;
};
