

class GpioOut {
	public:
		GpioOut(uint gpio);
		void on(void);
		void off(void);
	private:
		uint m_gpio;
};

