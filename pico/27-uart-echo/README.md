# uart echo test

Terminals are a bit peculiar. The Enter key emits a CR (\r). 
The best way to handle things is for the MCU to always accept and emit LF (\n).
On the terminal emulator, convert keystrokes of CR to LF. 
To do this in picocom, "--omap crlf".
That way everything should work as expected.



## See also


## Status

2023-07-21  Started. Works

