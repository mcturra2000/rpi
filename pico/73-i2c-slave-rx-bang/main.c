#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
//#include "hardware/adc.h"
//#include "hardware/clocks.h"
//#include "hardware/flash.h"
#include "hardware/gpio.h"
//#include "hardware/irq.h"
//#include "hardware/pwm.h"
//#include "hardware/spi.h"
#include "pico/multicore.h"


#if LIB_PICO_STDIO_USB
#pragma message "USB configured as stdio"
#include "tusb.h" // if you want to use tud_cdc_connected()
#endif


#include "pi.h"
#define LED  25

#define SID 0x04


// these are are on the SLAVE side
#define SDA 19
#define SCL 18
uint8_t get_byte()
{
	uint8_t val = 0;
	for(int i = 0; i<7; i++) {
		val <<= 1;
		while(gpio_get_out_level(SCL) == 0);
		val  &= (gpio_get_out_level(SDA) != 0);
		while(gpio_get_out_level(SCL) == 1);
	}
	return val;
}

void send_ack(void)
{
	gpio_set_dir(SDA, true); // as an output
	gpio_put(SDA, 0);
	while(gpio_get_out_level(SDA) == 0);
	gpio_set_dir(SDA, false); // back to an input
	gpio_put(SDA, 1); // with level high
	while(gpio_get_out_level(SDA) != 0);
}

void core1_entryX()
{

	// leave them floating
	gpio_set_dir(SDA, false); // input
	//gpio_disable_pulls(SDA);
	gpio_set_dir(SCL, false); // input
	//gpio_disable_pulls(SCL);

	uint32_t count = 0, val;
idle:
	pi_gpio_toggle(LED);
	gpio_put(SDA, 1); // correspnding to a NACK	
	while(gpio_get_out_level(SDA));
	if(gpio_get_out_level(SCL)==0) goto idle; 
	while(gpio_get_out_level(SCL));
	// ok, we've go a start condition

sid:
	val = get_byte();
	if((val>>1) != SID) goto idle; // it's not for us
	send_ack();

data:
	val = get_byte();
	count = val;
	send_ack();

stop:
	while(gpio_get_out_level(SCL) == 0);
	while(gpio_get_out_level(SDA) == 0);

	//pi_max7219_show_count(count);

	goto idle;

}

void slave()
{
	pi_gpio_out(LED);
	gpio_put(LED, 1);

	i2c_init(i2c1, 100*1000);
	i2c_set_slave_mode(i2c1, true, SID);
	gpio_set_function(18, GPIO_FUNC_I2C); // SDA
	gpio_set_function(19, GPIO_FUNC_I2C); // SCL

	pi_max7219_init();
	while(1) {
		uint8_t val;
		i2c_read_raw_blocking(i2c1, &val, 1);
		pi_max7219_show_count(val);
		//val++;

	}
}

void master()
{
	pi_gpio_init(LED, OUTPUT);

	i2c_init(i2c0, 100*1000);
	gpio_set_function(4, GPIO_FUNC_I2C); // SDA
	gpio_set_function(5, GPIO_FUNC_I2C); // SCL

	sleep_ms(20);
	uint8_t count = 0;
	while(1) {
		i2c_write_timeout_us(i2c0, SID, &count, 1, false, 2000);
		count++;
		sleep_ms(1);
	}
}
int main() 
{
	stdio_init_all();
#if LIB_PICO_STDIO_USB
	while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 
#endif

	if(0) 
		slave();
	else
		master();

	return 0;
}

