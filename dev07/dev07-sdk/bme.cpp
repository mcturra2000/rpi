#include "main.h"

#include <Adafruit_BME680.h>

Adafruit_BME680 bme; // I2C

void bme_setup()
{
  
  if (bme.begin(0x76)) {
    // Set up oversampling and filter initialization
    bme.setTemperatureOversampling(BME680_OS_8X);
    bme.setHumidityOversampling(BME680_OS_2X);
    bme.setPressureOversampling(BME680_OS_4X);
    bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
    bme.setGasHeater(320, 150); // 320*C for 150 ms
  } else {
    Serial.println("Could not find a valid BME680 sensor, check wiring!");
    //while (1);
  }
}

int bme_degC()
{
   if (! bme.performReading()) return 666;
  return bme.temperature;
}
