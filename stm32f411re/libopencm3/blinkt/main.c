#include <conf.h>
#include <delay.h>
#include <gpio.h>
#include <blinkt.h>

//#include <inttypes.h>
//#include <libopencm3/stm32/rcc.h>
//#include <libopencm3/stm32/gpio.h>
//#include <libopencm3/cm3/systick.h>
//#include <libopencm3/stm32/timer.h>
//#include <libopencm3/stm32/spi.h>


#define CLK	GPIO8
#define DAT	GPIO9

int main() 
{
	//stdio_init_all();
	blinkt_init(GPIOB, DAT, CLK);
#if 0
	blinkt_set_pixel_colour(0, 0,0, 1);
	blinkt_show();
	for(;;);
#endif
	for(;;) {
		static int red = 0;
		red = 10 - red; // switch between on and off
		for (int i = 0; i < 8; ++i) {
			blinkt_show();
			delayish(100);
			blinkt_set_pixel_colour(i, red, 0, 0);
		}
	}
	return 0;
}

