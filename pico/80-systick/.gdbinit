cd build

# quit without confirmation
define hook-quit
    set confirm off 
end

set pagination off
file app.elf
target remote localhost:3333
load
#b isr_systick
#b main.c:23
#b main.c:28
#b pi_gpio_high
monitor reset init
echo RUNNING...\n
c
