ROOT = $(RPI)/stm32f411re/cmsis

POT = $(RPI)/pot
CFLAGS += -I$(POT)
VPATH += $(POT)





CFLAGS += -I$(CMSIS)/Device/ST/STM32F4xx/Include/
CFLAGS += -I$(CMSIS)/Include/
#CFLAGS += -fmax-errors=5
#CFLAGS += -D__FPU_PRESENT -DARM_MATH_CM4


# ARM FAST MATHS
CFLAGS += -I$(CMSIS)/DSP/Include
#VPATH += $(CMSIS)/DSP/Source/FastMathFunctions
#VPATH += $(CMSIS)/DSP/Source/CommonTables
#OBJS += arm_common_tables.o
#OBJS += arm_sin_f32.o

OBJS += startup_stm32f411retx.o system_stm32f4xx.o main.o basal.o # delay.o # printf.o string.o i2c.o
#OBJS += uart.o printf.o
OBJS += gpio.o

ifdef USE_SSD1306
OBJS += ssd1306.o 
#OBJS += printf.o string.o i2c.o
OBJS += i2c.o
SSD1306_PATH = ../../../1306
VPATH += $(SSD1306_PATH)
CFLAGS += -I$(SSD1306_PATH)
endif

ifdef USE_LEDMAT
#LEDMAT_PATH = ../../../8x8
#CFLAGS += -I$(LEDMAT_PATH)
#VPATH += $(LEDMAT_PATH)
OBJS += ledmat.o i2c.o
endif

#LDFLAGS = -T $(ROOT)/STM32F411RETX_FLASH.ld

include ../../mk-rules.mk

