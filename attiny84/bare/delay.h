#pragma once
#include <basal.h>
//#include <delay.h>
//#include <stdint.h>

void delayish(int ms);

/*
4 nops() by themselves is about 1 us (without use of delayish_us()

Table of calls to delayish_us() by parameter n, microseconds delay time, and equivalent Hz.
n  	us 	Hz
1  	2.6	384.6k
10 	10	100k
20 	17
40 	33
64 	51.5
80 	64 	15.6k
128 	101.5
255 	200 	5.0k

E.g. calling delayish_us(80) results in a 64us delay, equiv of 15.6kHz
*/

void delayish_us(uint8_t n);

