# HAL

You'll maybe want to link in these other paths:
```
/home/pi/repos/rpi/pot
/home/pi/repos/rpi/stm32f411re/hal/shims
```

[blink.c](blink.c) - very very basic program. 
[article](https://mcturra2000.wordpress.com/2021/09/03/k-i-s-s-stm32-hal-blinky-led/)

[pwm-1](pwm-1.c) - fixed frequency of 500Hz at 25% duty cycle to pin PA11. 
[article](https://mcturra2000.wordpress.com/2021/09/04/k-i-s-s-stm32f4-hal-pwm-fixed-frequency-and-duty-cycle/)


## Links to other sites

* [sdcard](https://github.com/afiskon/stm32-sdcard)
