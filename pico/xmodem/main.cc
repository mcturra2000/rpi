#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
//#include "pico/stdio.h"
#include "hardware/flash.h"
//#include "hardware/clocks.h"
//#include "hardware/flash.h"
#include "hardware/gpio.h"
#include "hardware/irq.h"
#include "hardware/sync.h"
//#include "hardware/spi.h"
#if LIB_PICO_STDIO_USB
#pragma message "USB configured as stdio"
#include "tusb.h" // if you want to use tud_cdc_connected()
#endif

#include <stdlib.h>
#include <FreeRTOS.h>
#include <task.h>


//#include "hardware/structs/systick.h"

#include "pi.h"
#include "circular.h"

#define USE_ILI
#ifdef USE_ILI
//#include "mode0.h"
#include <ili9341.h>
#define print ili9341_print
#define printchar ili9341_printchar
#define refresh_screen mode0_refresh_screen
#define init_monitor mode0_init
#else
void uart_print(const char* format, ...)
{
	char str[100];
	va_list ap;
	va_start(ap, format);
	vsnprintf(str, sizeof(str), format, ap);
	va_end(ap);
	uart_puts(uart, str);
}

void uart_printchar(char c)
{
	char str[2];
	str[0] = c;
	str[1] = 0;
	print(str);
}
void init_monitor()
{
	uart_init(uart, 115200);
	gpio_set_function(8, GPIO_FUNC_UART);
	gpio_set_function(9, GPIO_FUNC_UART);
}
#endif

#define SOH 0x01	// start of header
#define EOT 0x04	// end of transmission
#define ACK 0x06	// acknowledge
#define NAK 0x15	// no acknowledge // dec 21
//#define ETB 0x18	// end of tranmssion block
#define CAN 0x18  // cancel // dec 24

#define FATAL 17
#define SUCCESS 18

#define LED  25 // GPIO of built-in LED

typedef uint8_t u8;

#define UART 1
#if UART == 0
uart_inst_t *uart = uart0;
#else
#define TX 8
uart_inst_t *uart = uart1;
#endif

circle<132, char> buf;



#define LABEL() yield ## __LINE__
#define CPOINT() LABEL() : here = &&LABEL() // continuation point

u8 uget()
{
	char c;
	while(!buf.avail());
	irq_set_enabled(UART1_IRQ, false);
	buf.read(&c);
	irq_set_enabled(UART1_IRQ, true);
	return c;
}

void uput(u8 ch)
{
	uart_putc_raw(uart, ch);
}

void write_pkt (u8 *payload, u8  *page, int pkt, int addr)
{
	int pps = 4096/256; // pages per sector
	addr +=  pkt/pps*4096;
	if(pkt % pps == 0) { 
		uint32_t ints = save_and_disable_interrupts();
		flash_range_erase(addr, 4096);
		restore_interrupts (ints);
	}

	// store even-numbered packets. We're not ready to write
	if(pkt % 2 == 0) {
		memcpy(page, payload, 128); // set the left part of the page
		memset(page + 128, 0, 128); // empty the right part
		return;
	}

	// odd-numbered packet, so write the whole page
	memcpy(page + 128, payload, 128); // the right part of the page
	uint32_t ints = save_and_disable_interrupts();
	flash_range_program(addr, page, 256);
	restore_interrupts (ints);
}

//u8 block[3 + 128 + 1];
void xrecv(u8 *block, u8* page, int addr)
{
	int ack = NAK;
	int i;
	int sum;
	int pkt = 0; // current packet
	int ppkt = 0; // previous packet
	char c;

	while(buf.avail()) uget(); // drain input

begin:
	print(".");

	uput(ack);
	if(!uart_is_readable_within_us(uart, 10 * 1000 * 1000)) goto begin;
	block[0] = 0;

	// drain the uart. There might be some stray garbage at start of recv
	/*
	i = 0;
	while(uart_is_readable_within_us(uart, 12 * 1000)) {
		int c =  uart_getc(uart);
		block[i] = c;
		if(c != 0) print("%i %c\n", i, c);
		if(i<sizeof(block)) i++;
	} 
	*/
	for(int i = 0; i< 132; i++)  {
		u8 c = uget();
		printchar(c);
		block[i] = c;
		//print("\n%d\n", i);
	}

	ack = NAK; // assume failure in the first instance, because there are many things to get right

	// we expect to see 132 bytes but we might not get it all, esp at beginning
	if(i<sizeof(block)) {
		if(block[0] == EOT) {
			uput(ACK);
			print("Found EOT, with %d bytes\n", i);
			goto bye;
		}
		if(i!=0)	print("Received only %d bytes, Was SOH? %d\n", i, block[0] == SOH);
		goto begin;
	}

	print("Received all of block (i=%d). Was SOH? %d, pkt = %i\n", i, block[0] == SOH, block[1]);
	if(block[0] != SOH) {
		print("Didn't get SOH. Trying again");
		goto begin;
	}
	if(block[1] + block[2] != 255) {
		print("Block check failed");
		goto begin;
	}

	// verify checksum
	sum = 0;
	for(int i = 3; i< 131;  i++) sum += block[i];	
	if((sum &0xFF) != block[131]) {
		print("checksums don't match. me: %d, sender: %d\n", sum & 0xFF, block[131]);
		goto begin;
	}

	// verify that the packet received makes sense
	if((pkt != ppkt) && (pkt != ppkt+1)) {
		print("Unexpected packet: %d\n", pkt);
		goto begin;
	}
	ppkt = pkt;

	ack = ACK;
	print("Pkt OK\n");
	//write_pkt(block+3, page, pkt, addr);
	goto begin;

bye:
	print("OK: xmodem received everything. Exiting");
	vTaskDelete(NULL);

}

void xrecv_task(void *params)
{
	int addr = *(int*)params;
	addr = (addr/4096)*4096;

	u8 *block = (u8*) malloc(132);
	assert(block);
	u8 *page = (u8*) malloc(256);
	assert(page);
	xrecv(block, page, addr);	
	free(page);
	free(block);
}

void update_screen(void* args)
{
	while(1) { 
		for(int i = 0; i< 2; i++) {
			refresh_screen();
			refresh_screen();
		}
		vTaskDelay(1);
	}
}

void uart_rx_handler()
{
	char c = (uart_get_hw(uart)->dr) & 0xFF;
	buf.write(c);
}

int main() 
{
	int baud = 115200;
	//baud = 9600;
	uart_init(uart, baud);
	gpio_set_function(TX, GPIO_FUNC_UART);
    	gpio_set_function(TX+1, GPIO_FUNC_UART);
	init_monitor();
	//stdio_semihosting_init();
	//stdio_init_all();
	//printf("test for semihosting");
	//puts("\tyo yo yo watup?");

	print("xmodem test\n");


	irq_set_exclusive_handler(UART1_IRQ , uart_rx_handler);
	uart_set_irq_enables(uart, true, false); // handle something in RX buffer
	irq_set_enabled(UART1_IRQ, true); // necessary

	int addr = 0xf000;
	
#if 1
	xTaskCreate(xrecv_task, "XRECV", 256, &addr, 1, NULL); // keep as low priroty, as it can take time
	xTaskCreate(update_screen, "SCR", 512, NULL, 2, NULL); 
	vTaskStartScheduler();
#else
	xrecv_task(&addr);
#endif
}

