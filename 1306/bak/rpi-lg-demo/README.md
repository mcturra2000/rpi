# ssd1306 natively on Pi 0 using lg library

Setup:
```
cd ~/src
git clone https://github.com/joan2937/lg
cd lg
git checkout v0.2 # or whatever version you prefer
make
sudo make install
```

See their EXAMPLES directory; or this directory

[Gihub repo](https://github.com/joan2937/lg)


## See also

* [Overview](http://abyz.me.uk/lg/download.html)

## Status

2022-07-26	Started. Works
