#define STM32F411xE
#include "stm32f4xx.h"
#include <gpio.h>
#include <uart.h>
#include "printf.h"

uint32_t SystemCoreClock = 16000000;
extern uint32_t _edata;


volatile static int magic = 0;

void  __attribute__ ((section (".init"))) premain()
{
    magic = 42;
}

typedef void (*fptr)(void); // takes no args, returns void

int main (void) 
{
	uart_init();
	//gpio_pullup(GPIOA, 2); // doesn't seem to help
	//gpio_pullup(GPIOA, 3); // ditto
	//GPIOA->OSPEEDR |= 0b10100000; // high speed, although seems unhelpful
	USART2->CR1 |= USART_CR1_RXNEIE;
	//USART2->CR1 |= USART_CR1_TCIE;
	//USART2->CR1 |= USART_CR1_TXEIE;
	NVIC_EnableIRQ(USART2_IRQn);

	puts("UART test - if you can read this, then it should be working!");
	printf("_edata = 0x%x\n", &_edata);

	fptr fn = (fptr)0x8004001;
	fn();
	//puts("Now type something, and I will echo it to serial");
	
	for(;;);
}
