#if 1
#define LED 2 // ATtiny84: pin 11, PA2, D2
#else
#define LED 4 // ATiny85, pin3, PB4, D4
#endif


void fastClock(){
  // 8MHz
  noInterrupts();
  CLKPR = 0x80; // enable clock prescale change
  CLKPR = 0;    // no prescale
  interrupts();
  
}

void setup() {
  pinMode(LED, OUTPUT);
  fastClock(); 

#if 0
  for(int p = 0; p<5; p++) {
    pinMode(p, OUTPUT);
    digitalWrite(p, HIGH);    
  }
#endif  
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(LED, HIGH);
  delay(100);
  digitalWrite(LED, LOW);
  delay(1000);

}
