

//#include <Wire.h>
#include <TM1637Display.h>
#include <pi.h>

#include "main.h"
//#include "pulse.h"
#include <om.h>
#include <debounce.hh>
#include <utils.h>

//bool yellow_active = true;  // true to show clock or termperature
//bool yellow_clock = true;   // when yellow is active, show clock (true), or else temprature
//uint32_t swatch_start_ms;   // start time of stopwatch in millis()



// FN show_task 
void show_task() 
{
	if (om_running()) {
		// show stopwatch
		uint32_t secs = om_secs();
		uint32_t mins = secs / 60;
		secs = secs % 60;
		tm1637_show_dec(mins*100 + secs);
	} else {
		struct tm t;
		ds3231_get_uk_with_mtx(&t);
		//DS3231_get_uk_tm(&t);
		tm1637_show_hhmm(&t);
	}
}


// FN iled 
void iled(int n) {
	pinMode(n, OUTPUT);
	digitalWrite(n, 1);
}  // turn off the internal leds




// FN main
int main()
{
	iled(18);
	iled(19);
	iled(20);
	stdio_init_all();
	serial_setup();
	pi_i2c_init(4);
	DS3231_init(i2c0);
	tm1637_init_pico(0, 2);  // CLK, DIO, delay in us (default is 100)
	tm1637_invert_display(1);
	show_task();
	//Pulse t4ms(4);
	//Pulse t250ms(250);
	every_t ev250ms = {250};
	int default_mode = 1;
	om_init(default_mode);

//#define BTNY 0, 7
//#define BTNR 0, 29
	debounce btny(0, 7);
	debounce btnr(0, 29);
	//gpio_inpull(BTNY);
	//deb_t btny;	
	//deb_init(&btny, BTNY);
	for(;;) {
		om_update();
		if(btny.falling()) {
			if(om_running()) {
				om_stop();
			} else {
				om_set_mode(default_mode);
				om_start();
			}
		}

		if(btnr.falling()) {
			default_mode = 3- default_mode; /* 1->2, 2->1 */
			if(om_running()) om_set_mode(default_mode);
		}

		if(every(&ev250ms)) {
			show_task();
		}
	}
	return 0;
}
