/* app.c
 *
 * MX:
 *	Clock: 100MHz
 *	TIM10:
 *		Activated
 *		Parameter Settings:
 *			Counter period: 65535
 *			Auto preload: Enabled
 *
 */
#include "main.h"

extern TIM_HandleTypeDef htim10;

#define PIN GPIOB, GPIO_PIN_12

void app_main()
{
	__HAL_TIM_SET_PRESCALER(&htim10, SystemCoreClock/1000000-1);
	(&htim10)->Instance->EGR  |= TIM_EGR_UG; // update the registers (required!)
	HAL_TIM_Base_Start(&htim10);

	for(;;) {
		HAL_GPIO_WritePin(PIN, GPIO_PIN_SET);

		// delay for 100 microseconds
		// It's safest to use the right cast for the length of the bits in the timer
		// seems to work spot on
		uint16_t cnt_start = __HAL_TIM_GET_COUNTER(&htim10);
		while((uint16_t)(__HAL_TIM_GET_COUNTER(&htim10) - cnt_start) < 100);

		HAL_GPIO_WritePin(PIN, GPIO_PIN_RESET);
		HAL_Delay(10);

	}

}
