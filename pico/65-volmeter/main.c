#include "hardware/adc.h"
#include "pico/stdlib.h"
#include "ssd1306.h"
#include <stdio.h>
#include <string.h>
#include "pi.h"


const uint8_t splash1_data[512];

#define ALARM 0
#define DELAY (2*1000000)

static void alarm_0_irq() 
{
	pi_alarm_rearm(ALARM, DELAY);
}

#define PIN 17

int main()
{
	int h = 32;
	if(1) h = 64;
	pi_i2c_init(4);
	i2c_set_baudrate(&i2c0_inst, 400000);
	init_display(h);


	pi_gpio_out(PIN);
	//GpioOut pin(17); // used to test speed of writing

	pi_alarm_init(ALARM, alarm_0_irq, DELAY);

	adc_init();
	adc_gpio_init(26);

	int yvals[128];
	memset(yvals, 0, 128);
	int xidx = 0;
	for(;;) {
		clear_scr();
		for(int x = 0; x< 128; x++) {
			uint16_t v = adc_read(); // 0-4095
			v = 63 - v/64; // 0-63
			draw_pixel(x, v, 1); // clear out old value from screen
		}
		//yvals[xidx] = v;
		//draw_pixel(xidx, v, 1); // draw new one

		//for(int x = 0; x< 128; x++) {
		//	int color = x<=v ? 1 : 0;
		//	draw_pixel(x, 10, color);
		//}
		show_scr();
		xidx++;
		if(xidx>=128) xidx = 0;
		//pi_gpio_high(PIN);
		//ssd1306_display_cell();
		//pi_gpio_low(PIN);
		//sleep_ms(1);
	}

	return 0;
}
