#include <delay.h>
#include <i2c.h>
#include <ssd1306.h>

int main(void)
{
	delayish(4); // 2022-10-30 see note
	i2c1_init();

	init_display(64, I2C1);
	int counter = 0;
	while(1) {
		ssd1306_printf("Counter = %d\n", counter++);
		show_scr();
		delayish(1000);
	}

}
