#include <stdio.h>

#include <cal.h>

int main()
{
	printf("%d s/b 0\n", cal_wday(2023, 10, 12)); // 12 Nov 2023 is Sun (i.e. 0)
}
