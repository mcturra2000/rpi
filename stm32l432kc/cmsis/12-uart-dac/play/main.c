/*
 * slurp file:
 * https://gist.github.com/w-vi/7e98342181776162b1a3
 */

//#include <asm/termios.h>
#include <termios.h>

#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>


#if 0
#define SRC "/home/pi/Music/song-16k.raw"
#else
//#define SRC "README.md"
#define SRC "../../toc.md"
#endif

uint8_t *source = NULL;
long bufsize = 0;
void slurp()
{
	FILE *fp = fopen(SRC, "r");
	assert(fp != NULL);
	/* Go to the end of the file. */
	if (fseek(fp, 0L, SEEK_END) == 0) {
		/* Get the size of the file. */
		bufsize = ftell(fp);
		if (bufsize == -1) { /* Error */ }

		/* Allocate our buffer to that size. */
		source = malloc(sizeof(char) * (bufsize + 1));

		/* Go back to the start of the file. */
		if (fseek(fp, 0L, SEEK_SET) != 0) { /* Error */ }

		/* Read the entire file into memory. */
		size_t newLen = fread(source, sizeof(char), bufsize, fp);
		if (newLen == 0) {
			fputs("Error reading file", stderr);
		} else {
			source[++newLen] = '\0'; /* Just to be safe. */
		}
	}
	fclose(fp);
}

void sleep_us(int us)
{
	struct timespec req;
	req.tv_nsec = us * 1000;
	req.tv_sec = 0;
	clock_nanosleep(CLOCK_MONOTONIC, 0, &req, NULL);
}

int nread = 0;
void readin(int fd)
{
	while(1) {
		int bytes;
		uint8_t c;
		ioctl(fd, FIONREAD, &bytes);
		if(bytes == 0) break;
		read(fd, &c, 1); // did we succeed?
		printf("%c", c);
		fflush(stdout);
		nread++;
	}
}

int main()
{
	slurp();
	printf("Buffer size: %d\n", bufsize);

	char *s = "/dev/ttyACM0";
	s = "/dev/ttyUSB0";
	int fd = open(s,O_RDWR | O_NOCTTY| O_SYNC );
	assert(fd != -1);

#if 1
	struct termios tty;
	memset (&tty, 0, sizeof tty);
	int status = tcgetattr (fd, &tty);
	assert(status == 0);

	speed_t speed = B230400;
	speed = B115200;
	cfsetispeed(&tty, speed);
	cfsetospeed(&tty, speed); 


	status = tcsetattr (fd, TCSANOW, &tty);
	assert(status == 0);
#endif

	
#if 0
	// try to change stuff using ioctl
	struct termios2 tio;
	int speed = 115200 * 1; // baudrate

	ioctl(fd, TCGETS2, &tio);
	tio.c_cflag &= ~CBAUD;
	tio.c_cflag |= BOTHER;
	tio.c_ispeed = speed;
	tio.c_ospeed = speed;
	ioctl(fd, TCSETS2, &tio);
#endif

	puts("Sending data");
	//system("stty -F 
	for(int i = 0; i < bufsize; i++) {
		//puts("x");
		//printf(".");
		//fflush(stdout);
		uint8_t v = source[i];
		//int nbytes;
try_again:
		write(fd, &v, 1);
		//printf("."); fflush(stdout);
		//sleep_us(3200*2);
		readin(fd);
		//if(v == '0') goto try_again;
	}

	while(nread != bufsize)	readin(fd);
	/*
	   int bytes;
	   ioctl(fd, FIONREAD, &bytes);
	   if(bytes > 0) {
	   char v;
	   read(fd, &v, 1); // did we succeed?
	   printf("%c", v);
	   fflush(stdout);
	   }
	   */
	close(fd);

	return 0;
}
