#include "pico/stdlib.h"
#include "hardware/gpio.h"

#define LED  25 // GPIO of built-in LED

extern void make_call(uint32_t pin, uint32_t level); // defined in do_call.s

void gpio_put1(uint32_t pin, uint32_t val) { gpio_put(pin, val); }

void isr_svcall(void)
{
	    // Get stack pointer, assuming we the thread that generated
    // the svc call was using the psp stack instead of msp
    //volatile unsigned int *stack;
    asm volatile ("\t movs r0, #4\n");
    asm volatile ("\t mov r1, lr\n");
    asm volatile ("\t tst r0, r1\n");
    asm volatile ("beq uses_msp\n");
    //asm volatile ("\t MRS %0, psp\n"  : "=rm" (stack) );   
    asm volatile ("\t MRS r3, psp\n");   
    asm volatile ("\t b isr_svcall_body\n");
    asm volatile ("\t uses_msp:\n");
    // MRS %0, msp\n"  : "=rm" (stack) );
    asm volatile ("\t MRS r3, msp\n");   
    asm volatile ("\t isr_svcall_body:\n");
    asm volatile ("\t ldr r0, [r3, #0x00]\n");

    // Stack frame contains:
    // r0, r1, r2, r3, r12, r14, the return address and xPSR
    // - Stacked R0 = stack[0]
    // - Stacked R1 = stack[1]
    // - Stacked R2 = stack[2]
    // - Stacked R3 = stack[3]
    // - Stacked R12 = stack[4]
    // - Stacked LR = stack[5]
    // - Stacked PC = stack[6]
    // - Stacked xPSR= stack[7]
	//asm volatile ("\t PUSH {R5, R7}\n");
	//asm volatile ("\t MRS R3, PSP\n");
	//asm volatile ("\t LDR R0, [R3, #0x00]\n");
	volatile uint32_t gpio_pin; // = stack[0];
	uint32_t func_num;

	asm volatile ("\t mov %0, r0\n" : "=r" (gpio_pin));
	asm volatile ("\t mov %0, r7\n" : "=r" (func_num));
	switch(func_num) {
		case 0:
			gpio_put1(gpio_pin, 0);
			break;
		case 1:
			gpio_put1(gpio_pin, 1);
			break;
	}
	//asm volatile ("\t POP {R5, R7}\n");
}

int main() 
{
	gpio_init(LED);
	gpio_set_dir(LED, GPIO_OUT);
	for(;;) {
		make_call(LED, 1);
		sleep_ms(100);
		make_call(LED, 0);
		sleep_ms(1000);		
	}

	return 0;
}

