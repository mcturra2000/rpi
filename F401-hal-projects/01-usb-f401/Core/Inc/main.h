/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

//#include <brown.h>
#include <delay.h>
#include <gpio.h>
//#include "om.h"
//#include "rtc.h"
//#include <spi.h>
//#include <pot/cal.h>
//#include <pot/ds3231.h>
//#include <pot/zseg.h>
//#include <pot/blinkt.h>
//#include <pot/ds3231.h>
//#include <pot/debounce.h>
//#include <pot/utils.h>

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
typedef uint32_t u32;

extern ADC_HandleTypeDef hadc1;
extern SPI_HandleTypeDef hspi1;
extern RTC_HandleTypeDef hrtc;
extern I2C_HandleTypeDef hi2c1;
//extern int using_ds3231;
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void app(void);
void app_clock_with_ds3231();
void app_rtc_irq();
void app_zsnake();
void process_usb();
void systick_callback();
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LEDb_Pin GPIO_PIN_13
#define LEDb_GPIO_Port GPIOC
#define LEDy_Pin GPIO_PIN_2
#define LEDy_GPIO_Port GPIOA
#define BTNy_Pin GPIO_PIN_3
#define BTNy_GPIO_Port GPIOA
#define BTNw_Pin GPIO_PIN_0
#define BTNw_GPIO_Port GPIOB
#define BTNb_Pin GPIO_PIN_10
#define BTNb_GPIO_Port GPIOB
#define BZR_Pin GPIO_PIN_6
#define BZR_GPIO_Port GPIOB
#define LEDerr_Pin GPIO_PIN_7
#define LEDerr_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */
#define DEF_PIN(prefix) prefix##_GPIO_Port , prefix##_Pin
#define BTNb DEF_PIN(BTNb)
#define BTNw DEF_PIN(BTNw)
#define BTNy DEF_PIN(BTNy)
#define BZR DEF_PIN(BZR)
#define LEDb DEF_PIN(LEDb)
#define LEDerr DEF_PIN(LEDerr)
#define LEDy DEF_PIN(LEDy)

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
