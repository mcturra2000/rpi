/*
 * Debugger gives problems re Error finishing flash operation. Fix via:
 * 	menu item Project :
 * 	Properties : Run/Debug Settings.
 * 	Edit the launch configuration. Tab Debugger. Change the Reset type behaviour to None.
 *
 * See ref manual RM0383 s3.3 table 4 for sectors
 */

#include <string.h>
#include <stdio.h>
#include "main.h"

extern UART_HandleTypeDef huart2;

#define USE_SEC_3
#ifdef USE_SEC_3
#define SECTOR FLASH_SECTOR_3
const uint32_t FlashAddress = 0x0800C000; // address of sector 3
#endif
#ifdef USE_SEC_7
#define SECTOR FLASH_SECTOR_7
const uint32_t FlashAddress = 0x08060000; // address of sector 7
#endif

void Write_Flash(uint8_t *data, int n)
{
	uint32_t PAGEError = 0;
	FLASH_EraseInitTypeDef EraseInitStruct;
	//EraseInitStruct.TypeErase   = FLASH_TYPEERASE_SECTORS ;
	EraseInitStruct.TypeErase   = FLASH_TYPEERASE_SECTORS ;
	//EraseInitStruct.Banks = FLASH_BANK_1;
	EraseInitStruct.Sector   = SECTOR;
	EraseInitStruct.VoltageRange = FLASH_VOLTAGE_RANGE_3; // was 3
	EraseInitStruct.NbSectors =1;
     HAL_FLASH_Unlock();
     //__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGSERR );
     //FLASH_Erase_Sector(FLASH_SECTOR_3, VOLTAGE_RANGE_3);
     if(HAL_FLASHEx_Erase(&EraseInitStruct, &PAGEError) != HAL_OK)
    	 Error_Handler();
     if(PAGEError != 0xFFFFFFFFU) Error_Handler();

     for(int i = 0; i<n; i++) {
         HAL_FLASH_Program(TYPEPROGRAM_BYTE, FlashAddress+i, data[i]);
     }
     HAL_FLASH_Lock();
}

void Read_Flash(uint8_t *data, int n)
{
	for(int i = 0; i<n; i++) {
		data[i] = *(uint8_t*)(FlashAddress+i);
	}

}

void app()
{
	  int val = 0;
	  Read_Flash((uint8_t*) &val, sizeof(val));

	  val++;
	  if(val < 300) val = 300;
	  if(val>400) val = 300;
	  Write_Flash((uint8_t*) &val, sizeof(val));

	  volatile int val1 = 0;
	  Read_Flash((uint8_t*) &val1, sizeof(val1));
	  char msg[30];
	  int count = 0;
	  while (1)
	  {
		  HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_5);
		  sprintf(msg, "%d\t%d\r\n", count++, val1);
		  HAL_UART_Transmit(&huart2, (uint8_t*) msg, strlen(msg), 1000);
		  HAL_Delay(1000);
	  }
}
