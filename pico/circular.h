
template<int LEN, typename T>
struct circle
{
	T cb_data[LEN];
	int cb_read_index = 0;
	int cb_write_index = 0;
	int cb_size = 0;

	bool write(T value)
	{
		if(cb_size == LEN) return false;
		cb_data[cb_write_index] = value;
		if(++cb_write_index == LEN) cb_write_index = 0;
		cb_size++;
		return true;
	}

	bool read(T* value)
	{
		*value = 0;
		if(cb_size == 0) return false;
		*value = cb_data[cb_read_index];
		if(++cb_read_index == LEN) cb_read_index = 0;
		cb_size--;
		return true;
	}

	bool avail() { return cb_size>0; }
};
