//#include <conf.h>
#include <delay.h>
#include <gpio.h>

//#include <inttypes.h>
//#include <libopencm3/stm32/rcc.h>
//#include <libopencm3/stm32/gpio.h>
//#include <libopencm3/stm32/timer.h>
//#include <libopencm3/stm32/spi.h>

#include <libopencm3/stm32/adc.h>

#include "jg_stdio.h"
//#include "mal.h"

//typedef uint32_t u32;

#define LED	LED_BUILTIN  //PB3

uint8_t channel_array[] = { 1, 1, ADC_CHANNEL_TEMP};

static void adc_setup(void)
{
	rcc_periph_clock_enable(RCC_ADC);
	rcc_periph_clock_enable(RCC_GPIOA);

	gpio_mode_setup(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO0);
	gpio_mode_setup(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO1);

	adc_power_off(ADC1);
	//adc_set_clk_source(ADC1, ADC_CLKSOURCE_ADC);
	adc_calibrate(ADC1);
	//adc_set_operation_mode(ADC1, ADC_MODE_SCAN);
	//adc_disable_external_trigger_regular(ADC1);
	adc_set_right_aligned(ADC1);
	adc_enable_temperature_sensor();
	//adc_set_sample_time_on_all_channels(ADC1, ADC_SMPTIME_071DOT5);
	adc_set_regular_sequence(ADC1, 1, channel_array);
	//adc_set_resolution(ADC1, ADC_RESOLUTION_12BIT);
	//
	//adc_disable_analog_watchdog(ADC1);
	adc_power_on(ADC1);

	/* Wait for ADC starting up. */
	int i;
	for (i = 0; i < 800000; i++) {    /* Wait a bit. */
		__asm__("nop");
	}

}

static void adc_setup1(void)
{
	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_ADC1);
	gpio_mode_setup(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO0);
	//gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_ANALOG, GPIO0);
	//gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_ANALOG, GPIO1);

	/* Make sure the ADC doesn't run during config. */
	//adc_power_off(ADC1);

	/* We configure everything for one single conversion. */
	//adc_disable_scan_mode(ADC1);
	//adc_set_sample_time_on_all_channels(ADC1, ADC_SMPR_SMP_3CYC);
	//adc_power_on(ADC1);

	/*
	   adc_set_single_conversion_mode(ADC1);
	   adc_disable_external_trigger_regular(ADC1);
	   adc_set_right_aligned(ADC1);
	   adc_set_sample_time_on_all_channels(ADC1, ADC_SMPR_SMP_28DOT5CYC);

	   adc_power_on(ADC1);
	   */

	/* Wait for ADC starting up. */
#if 0
	int i;
	for (i = 0; i < 800000; i++) /* Wait a bit. */
		__asm__("nop");

	adc_reset_calibration(ADC1);
#endif
	//adc_calibrate(ADC1);
}



static uint16_t read_adc_naiive(uint8_t channel)
{
	/*
	uint8_t channel_array[16];
	channel_array[0] = channel;
	adc_set_regular_sequence(ADC1, 1, channel_array);
	*/
	adc_start_conversion_regular(ADC1);
	printf2("hi %d\n", 45);
	while (!adc_eoc(ADC1));
	printf2("hi %d\n", 46);
	uint16_t reg16 = adc_read_regular(ADC1);
	return reg16;
}

int main(void)
{
	//rcc_periph_clock_enable(RCC_GPIOB);
	//gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO3);
	gpio_out(LED);
	//gpio_set(LED);
	//gpio_out(LED);

	puts("ADC says hello");
	printf2("hi %d\n", 42);
	printf2("hi %d\n", 43);
	adc_setup();
	printf2("hi %d\n", 44);

	while (1)
	{
		uint16_t adc = read_adc_naiive(0);
		printf2("ADC: %d\n", adc);
		gpio_set(LED);
		delayish(100);
		gpio_clear(LED);
		//gpio_toggle(LED);
		delayish(900);
		//mal_delayish(100);
	}
}
