#include <string.h>
#include "uril.h"
#include "pi.h"

int uril_len = 0;
char uril_buf[132];

void __attribute__ ((weak)) uril_eol();

void uart_rx_handler()
{
	// TODO check for buffer overflow
	char c = (uart_get_hw(uart0)->dr) & 0xFF;
	if(c == '\n' || c == '\r') {
		if(uril_eol) uril_eol();
		uril_len = 0;
		return;
	}
	uril_buf[uril_len++] = c;
	//buf.write(c);
}


void uril_pause()
{
	irq_set_enabled(UART0_IRQ, false);
}

void uril_resume()
{
	irq_set_enabled(UART0_IRQ, true);
}
void uril_init()
{
	memset(uril_buf, 0, sizeof(uril_buf));
	
	//uart_init(uart0, 115200);
	//gpio_set_function(1, GPIO_FUNC_UART); // RX
	pi_stdio_init();
	irq_set_exclusive_handler(UART0_IRQ , uart_rx_handler);
	uart_set_irq_enables(uart0, true, false); // handle something in RX buffer
	uril_resume();
}

