/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink
*/
#include "Wire.h"
#include "ds3231.h"
#include "cal.h"

void i2c_send_bytes(uint32_t i2c, uint8_t sid, const uint8_t *data, size_t len) {
  Wire.beginTransmission(sid);
  for (int i = 0; i < len; i++) {
    Wire.write(data[i]);
  }
  Wire.endTransmission(true);
}

void i2c_recv_bytes(uint32_t i2c, uint8_t sid, uint8_t *data, size_t len) {
  uint8_t bytesReceived = Wire.requestFrom(sid, len);
  Wire.readBytes(data, bytesReceived);
}
// the setup function runs once when you press reset or power the board
#define BUFF_MAX 128
char buff[BUFF_MAX];
int line_len = 0;

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200);
  Wire.begin();
  DS3231_init(DS3231_CONTROL_INTCN, (uint32_t)0);
  memset(buff, 0, BUFF_MAX);
}

// the loop function runs over and over again forever
void loop() {
  for(;;) {

  
  struct ts t;
  if(!Serial.available()) continue;
  int c = Serial.read();
  if (c == '\r') c = '\n';
  if (c != '\n') {
    if (line_len + 1 >= BUFF_MAX) continue;
    //putchar(c); if(c == '\n') putchar('\r');
    buff[line_len++] = c;
    continue;
  }

  //printf("\r\nYou entered the line <%s>\r\n", buff);
  if (line_len == 0) continue;

  if (buff[0] == 'R') {
    DS3231_get(&t);
    char strBuf[50];
    sprintf(strBuf, "%d.%02d.%02d %02d:%02d:%02d\n", t.year, t.mon, t.mday, t.hour, t.min, t.sec);
    Serial.print(strBuf);
  }

  if (buff[0] == 'S') {
    //long long tval = set_timeval;
    //set_timeval = -1;
    //char str[] = "S2023060617520003";
    volatile long long timeval = atoll(buff + 1);
    DS3231_set_int(timeval);
  }

  line_len = 0;
  memset(buff, 0, BUFF_MAX);
}
//DS3231_get_uk(&t);

//Serial.println(t.year);
//digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
//delay(1000);                       // wait for a second
//digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
//delay(1000);                       // wait for a second
}


/*
   DS3231 library for the Arduino.

   This library implements the following features:

   - read/write of current time, both of the alarms, 
   control/status registers, aging register
   - read of the temperature register, and of any address from the chip.

Author:          Petre Rodan <petre.rodan@simplex.ro>
Available from:  https://github.com/rodan/ds3231

The DS3231 is a low-cost, extremely accurate I2C real-time clock 
(RTC) with an integrated temperature-compensated crystal oscillator 
(TCXO) and crystal.

GNU GPLv3 license:

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

static uint32_t i2c;
//extern void i2c_transfer7(uint32_t i2c, uint8_t addr, const uint8_t *w, size_t wn, uint8_t *r, size_t rn);
extern void i2c_send_bytes(uint32_t i2c, uint8_t sid, const uint8_t *data, size_t len);
extern void i2c_recv_bytes(uint32_t i2c, uint8_t sid, uint8_t *data, size_t len);
//#if __has_include(<libopencm3/stm32/i2c.h>)
//#include <libopencm3/stm32/i2c.h>
//#else
//extern void i2c_transfer7(uint32_t i2c, uint8_t addr, const uint8_t *w, size_t wn, uint8_t *r, size_t rn);
//#endif


#include "Wire.h"
/*
void i2c_send_bytes(uint32_t i2c, uint8_t sid, const uint8_t *data, size_t len) {
  Wire.beginTransmission(sid);
  for(int i = 0; i < len; i++) {
    Wire.write(data[i]);
  }  
  Wire.endTransmission(true);  
}

void i2c_recv_bytes(uint32_t i2c, uint8_t sid, uint8_t *data, size_t len) {
  uint8_t bytesReceived = Wire.requestFrom(sid, len);
  Wire.readBytes(data, bytesReceived);

}
*/


//int snprintf(char *str, size_t size, const char *format, ...);
//void delay(unsigned long ms);

#define SID 0x68

/* control register 0Eh/8Eh
   bit7 EOSC   Enable Oscillator (1 if oscillator must be stopped when on battery)
   bit6 BBSQW  Battery Backed Square Wave
   bit5 CONV   Convert temperature (1 forces a conversion NOW)
   bit4 RS2    Rate select - frequency of square wave output
   bit3 RS1    Rate select
   bit2 INTCN  Interrupt control (1 for use of the alarms and to disable square wave)
   bit1 A2IE   Alarm2 interrupt enable (1 to enable)
   bit0 A1IE   Alarm1 interrupt enable (1 to enable)
   */

void DS3231_init(const uint8_t ctrl_reg, uint32_t i2c_port) {
  i2c = i2c_port;
  DS3231_set_creg(ctrl_reg);
  //DS3231_set_32kHz_output(false); // seems to cause problems
}

// get UK local time (BST adjusted where necessary)
void DS3231_get_uk(struct ts *t) {
  // 2022-10-30 Don't pre decrement and post increment mday (month day), because we expect that to be in the range 1..31, not 0..30
  DS3231_get(t);
  int yr = t->year;
  int mon0 = t->mon - 1;
  int day0 = t->mday;
  int hr = t->hour;
  adjbst(&yr, &mon0, &day0, &hr);
  t->mon = mon0 + 1;
  t->mday = day0;
  t->hour = hr;
}

#if 0
void write_data(uint8_t *src, size_t len)
{
	i2c_write_blocking(i2c_port, SID, src, len, false);
}
#endif

void ds3231_bsp_read(uint8_t *dst, size_t len) {
  //i2c_read_blocking(i2c_port, SID, dst, len, false);
  //i2c_transfer7(i2c, SID, 0, 0, dst, len);
  i2c_recv_bytes(i2c, SID, dst, len);
}
void ds3231_bsp_write(uint8_t *src, size_t len) {
  //i2c_write_blocking(i2c_port, SID, src, len, false);
  //i2c_transfer7(i2c, SID, src, len, 0, 0);
  i2c_send_bytes(i2c, SID, src, len);
}







void set_start_addr(uint8_t val) {
  ds3231_bsp_write(&val, 1);
  //i2c_write_blocking(i2c_port, SID, &val, 1, false);
}

void write_section(int addr, uint8_t *src, int len) {
  set_start_addr(addr);
  ds3231_bsp_write(src, len);
}


void read_section(int addr, uint8_t *dst, int len) {
  set_start_addr(addr);
  ds3231_bsp_read(dst, len);
}

int DS3231_reduce(long long *timeval)  //, int scale)
{
  const int scale = 100;
  int val = *timeval % scale;
  *timeval /= scale;
  return val;
}
void DS3231_set_int(long long timeval) {
  int wday = DS3231_reduce(&timeval);
  volatile int sec = DS3231_reduce(&timeval);
  volatile int min = DS3231_reduce(&timeval);
  volatile int hr = DS3231_reduce(&timeval);
  volatile int mday = DS3231_reduce(&timeval);
  volatile int month = DS3231_reduce(&timeval);
  volatile long long year = timeval - 2000;
  //volatile int foo = 0;
  uint8_t TimeDate[] = { DS3231_TIME_CAL_ADDR, sec, min, hr, wday, mday, month, year };
  for (int i = 1; i <= 7; i++) {
    TimeDate[i] = dectobcd(TimeDate[i]);
  }
  TimeDate[6] += 0x80;
  ds3231_bsp_write(TimeDate, sizeof(TimeDate));
  //i2c_send_bytes((uint32_t) &hi2c1, DS3231_I2C_ADDR, TimeDate, sizeof(TimeDate));
  //i2c_write_blocking(i2c0, DS3231_I2C_ADDR, TimeDate, sizeof(TimeDate), false);
  /*
	int sec = timeval % 100;
	timeval /= 100;
	int min = timeval % 100;
	timeval /= 100;
	int
	*/
}

#if 0
void DS3231_set(struct ts t)
{
	uint8_t i, century;

	if (t.year >= 2000) {
		century = 0x80;
		t.year_s = t.year - 2000;
	} else {
		century = 0;
		t.year_s = t.year - 1900;
	}

	uint8_t TimeDate[7] = { t.sec, t.min, t.hour, t.wday, t.mday, t.mon, t.year_s };

	Wire_beginTransmission(DS3231_I2C_ADDR);
	Wire_write(DS3231_TIME_CAL_ADDR);
	for (i = 0; i <= 6; i++) {
		TimeDate[i] = dectobcd(TimeDate[i]);
		if (i == 5)
			TimeDate[5] += century;
		Wire_write(TimeDate[i]);
	}
	Wire_endTransmission();
}
#endif

void DS3231_get(struct ts *t) {
  // According to datasheet:
  // Date: [1, 31]
  uint8_t TimeDate[7];  //second,minute,hour,dow,day,month,year
  uint8_t i;
  //uint8_t n;


#if 0
	uint8_t gotData = false;
	//mcarter 2022-04-02 use a count instead of millis(), so that we don't have to implement it
	//uint32_t start = millis(); // start timeout
	//while(millis()-start < DS3231_TRANSACTION_TIMEOUT){
	for(int i = 0; i < 50; i++) {
		if (Wire_requestFrom(DS3231_I2C_ADDR, 7) == 7) {
			gotData = true;
			break;
		}
		delay(2);
	}
	if (!gotData)
		return; // error timeout
#endif

  read_section(DS3231_TIME_CAL_ADDR, TimeDate, sizeof(TimeDate));

  TimeDate[5] &= 0x1F;  // mask out century overflow flag
  //uint8_t century = 0;
  for (i = 0; i < 7; i++) {
    TimeDate[i] = bcdtodec(TimeDate[i]);
    /*
		//n = Wire_read();
		n = TimeDate[i];
		if (i == 5) {
		//century = (n & 0x80) >> 7;
		century = n>>7;
		} else
		TimeDate[i] = bcdtodec(n);
		*/
  }

  //uint16_t year_full = century ? 2000 : 6000;
  //year_full += TimeDate[6];
  /*
	   if (century == 1) {
	   year_full = 2000 + TimeDate[6];
	   } else {
	   year_full = 1900 + TimeDate[6];
	   }
	   */

  t->sec = TimeDate[0];
  t->min = TimeDate[1];
  t->hour = TimeDate[2];
  t->mday = TimeDate[4];
  t->mon = TimeDate[5];
  t->year = 2000 + TimeDate[6];
  t->wday = TimeDate[3];
  t->year_s = TimeDate[6];
#ifdef CONFIG_UNIXTIME
  t->unixtime = get_unixtime(*t);
#endif
}

#if 1
void DS3231_set_addr(const uint8_t addr, uint8_t val) {
  uint8_t data[] = { addr, val };
  //write_data(data, 2);
  //write_data
  ds3231_bsp_write(data, 2);
  //write_section(addr, &val, 1);
}
#endif

uint8_t DS3231_get_addr(const uint8_t addr) {
  uint8_t rv;
  read_section(addr, &rv, 1);
  return rv;
}



// control register

void DS3231_set_creg(const uint8_t val) {
  DS3231_set_addr(DS3231_CONTROL_ADDR, val);
}

uint8_t DS3231_get_creg(void) {
  uint8_t rv;
  rv = DS3231_get_addr(DS3231_CONTROL_ADDR);
  return rv;
}

// status register 0Fh/8Fh

/*
   bit7 OSF      Oscillator Stop Flag (if 1 then oscillator has stopped and date might be innacurate)
   bit3 EN32kHz  Enable 32kHz output (1 if needed)
   bit2 BSY      Busy with TCXO functions
   bit1 A2F      Alarm 2 Flag - (1 if alarm2 was triggered)
   bit0 A1F      Alarm 1 Flag - (1 if alarm1 was triggered)
   */

void DS3231_set_sreg(const uint8_t val) {
  DS3231_set_addr(DS3231_STATUS_ADDR, val);
}

uint8_t DS3231_get_sreg(void) {
  uint8_t rv;
  rv = DS3231_get_addr(DS3231_STATUS_ADDR);
  return rv;
}

// aging register
#if 0
void DS3231_set_aging(const int8_t val)
{
	uint8_t reg;

	if (val >= 0)
		reg = val;
	else
		reg = ~(-val) + 1;      // 2C

	/*
	 * At 25°C the aging change of:
	 * +1 means -0.1ppm
	 * -1 means -0.1ppm
	 */
	DS3231_set_addr(DS3231_AGING_OFFSET_ADDR, reg);
	/*
	 * A conversion mut be done to forace the new aging value.
	 */
	DS3231_set_creg(DS3231_get_creg()| DS3231_CONTROL_CONV);
}

int8_t DS3231_get_aging(void)
{
	uint8_t reg;
	int8_t rv;

	reg = DS3231_get_addr(DS3231_AGING_OFFSET_ADDR);

	if ((reg & 0x80) != 0)
		rv = reg | ~((1 << 8) - 1);     // if negative get two's complement
	else
		rv = reg;

	return rv;
}
#endif

// temperature register
float DS3231_get_treg() {

  uint8_t data[2];
  read_section(DS3231_TEMPERATURE_ADDR, data, 2);

  uint8_t temp_msb = data[0];
  uint8_t temp_lsb = data[1] >> 6;

  int8_t nint;
  if ((temp_msb & 0x80) != 0)
    nint = temp_msb | ~((1 << 8) - 1);  // if negative get two's complement
  else
    nint = temp_msb;

  float rv = 0.25 * temp_lsb + nint;

  return rv;
}

void DS3231_set_32kHz_output(const uint8_t on) {
  /*
	 * Note, the pin1 is an open drain pin, therfore a pullup
	 * resitor is required to use the output.
	 */
  if (on) {
    uint8_t sreg = DS3231_get_sreg();
    sreg &= ~DS3231_STATUS_OSF;
    sreg |= DS3231_STATUS_EN32KHZ;
    DS3231_set_sreg(sreg);
  } else {
    uint8_t sreg = DS3231_get_sreg();
    sreg &= ~DS3231_STATUS_EN32KHZ;
    DS3231_set_sreg(sreg);
  }
}

// alarms

// flags are: A1M1 (seconds), A1M2 (minutes), A1M3 (hour),
// A1M4 (day) 0 to enable, 1 to disable, DY/DT (dayofweek == 1/dayofmonth == 0)
#if 0
void DS3231_set_a1(const uint8_t s, const uint8_t mi, const uint8_t h, const uint8_t d, const uint8_t * flags)
{
	uint8_t t[4] = { s, mi, h, d };
	uint8_t i;

	Wire_beginTransmission(DS3231_I2C_ADDR);
	Wire_write(DS3231_ALARM1_ADDR);

	for (i = 0; i <= 3; i++) {
		if (i == 3) {
			Wire_write(dectobcd(t[3]) | (flags[3] << 7) | (flags[4] << 6));
		} else
			Wire_write(dectobcd(t[i]) | (flags[i] << 7));
	}

	Wire_endTransmission();
}

void DS3231_get_a1(char *buf, const uint8_t len)
{
	uint8_t n[4];
	uint8_t t[4];               //second,minute,hour,day
	uint8_t f[5];               // flags
	uint8_t i;

	Wire_beginTransmission(DS3231_I2C_ADDR);
	Wire_write(DS3231_ALARM1_ADDR);
	Wire_endTransmission();

	uint8_t gotData = false;
	//mcarter 2022-04-02 use counting instead of millis()
	//uint32_t start = millis(); // start timeout
	//while(millis()-start < DS3231_TRANSACTION_TIMEOUT){
	for(int i = 0; i< 50; i++) {
		if (Wire_requestFrom(DS3231_I2C_ADDR, 4) == 4) {
			gotData = true;
			break;
		}
		delay(2);
	}
	if (!gotData)
		return; // error timeout

	for (i = 0; i <= 3; i++) {
		n[i] = Wire_read();
		f[i] = (n[i] & 0x80) >> 7;
		t[i] = bcdtodec(n[i] & 0x7F);
	}

	f[4] = (n[3] & 0x40) >> 6;
	t[3] = bcdtodec(n[3] & 0x3F);

	snprintf(buf, len,
			"s%02d m%02d h%02d d%02d fs%d m%d h%d d%d wm%d %d %d %d %d",
			t[0], t[1], t[2], t[3], f[0], f[1], f[2], f[3], f[4], n[0],
			n[1], n[2], n[3]);

}

// when the alarm flag is cleared the pulldown on INT is also released
void DS3231_clear_a1f(void)
{
	uint8_t reg_val;

	reg_val = DS3231_get_sreg() & ~DS3231_STATUS_A1F;
	DS3231_set_sreg(reg_val);
}

uint8_t DS3231_triggered_a1(void)
{
	return  DS3231_get_sreg() & DS3231_STATUS_A1F;
}

// flags are: A2M2 (minutes), A2M3 (hour), A2M4 (day) 0 to enable, 1 to disable, DY/DT (dayofweek == 1/dayofmonth == 0) - 
void DS3231_set_a2(const uint8_t mi, const uint8_t h, const uint8_t d, const uint8_t * flags)
{
	uint8_t t[3] = { mi, h, d };
	uint8_t i;

	Wire_beginTransmission(DS3231_I2C_ADDR);
	Wire_write(DS3231_ALARM2_ADDR);

	for (i = 0; i <= 2; i++) {
		if (i == 2) {
			Wire_write(dectobcd(t[2]) | (flags[2] << 7) | (flags[3] << 6));
		} else
			Wire_write(dectobcd(t[i]) | (flags[i] << 7));
	}

	Wire_endTransmission();
}

void DS3231_get_a2(char *buf, const uint8_t len)
{
	uint8_t n[3];
	uint8_t t[3];               //second,minute,hour,day
	uint8_t f[4];               // flags
	uint8_t i;

	Wire_beginTransmission(DS3231_I2C_ADDR);
	Wire_write(DS3231_ALARM2_ADDR);
	Wire_endTransmission();

	Wire_requestFrom(DS3231_I2C_ADDR, 4);
	//mcarter 2022-04-02 use counting instead of millis()
	uint8_t gotData = false;
	//uint32_t start = millis(); // start timeout
	//while(millis()-start < DS3231_TRANSACTION_TIMEOUT){
	for(int i = 0; i< 50; i++) {
		if (Wire_requestFrom(DS3231_I2C_ADDR, 3) == 3) {
			gotData = true;
			break;
		}
		delay(2);
	}
	if (!gotData)
		return; // error timeout



	for (i = 0; i <= 2; i++) {
		n[i] = Wire_read();
		f[i] = (n[i] & 0x80) >> 7;
		t[i] = bcdtodec(n[i] & 0x7F);
	}

	f[3] = (n[2] & 0x40) >> 6;
	t[2] = bcdtodec(n[2] & 0x3F);

	snprintf(buf, len, "m%02d h%02d d%02d fm%d h%d d%d wm%d %d %d %d", t[0],
			t[1], t[2], f[0], f[1], f[2], f[3], n[0], n[1], n[2]);

}

// when the alarm flag is cleared the pulldown on INT is also released
void DS3231_clear_a2f(void)
{
	uint8_t reg_val;

	reg_val = DS3231_get_sreg() & ~DS3231_STATUS_A2F;
	DS3231_set_sreg(reg_val);
}

uint8_t DS3231_triggered_a2(void)
{
	return  DS3231_get_sreg() & DS3231_STATUS_A2F;
}
#endif
// helpers

#ifdef CONFIG_UNIXTIME
const uint8_t days_in_month[12] PROGMEM = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

// returns the number of seconds since 01.01.1970 00:00:00 UTC, valid for 2000..FIXME
uint32_t get_unixtime(struct ts t) {
  uint8_t i;
  uint16_t d;
  int16_t y;
  uint32_t rv;

  if (t.year >= 2000) {
    y = t.year - 2000;
  } else {
    return 0;
  }

  d = t.mday - 1;
  for (i = 1; i < t.mon; i++) {
    d += days_in_month[i - 1];
  }
  if (t.mon > 2 && y % 4 == 0) {
    d++;
  }
  // count leap days
  d += (365 * y + (y + 3) / 4);
  rv = ((d * 24UL + t.hour) * 60 + t.min) * 60 + t.sec + SECONDS_FROM_1970_TO_2000;
  return rv;
}
#endif

uint8_t dectobcd(const uint8_t val) {
  return ((val / 10 * 16) + (val % 10));
}

uint8_t bcdtodec(const uint8_t val) {
  return ((val / 16 * 10) + (val % 16));
}

uint8_t inp2toi(char *cmd, const uint16_t seek) {
  uint8_t rv;
  rv = (cmd[seek] - 48) * 10 + cmd[seek + 1] - 48;
  return rv;
}
