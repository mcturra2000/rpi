//#define STM32L432xx
//#define USE_FULL_LL_DRIVER
#include "stm32l4xx.h"
#include "stm32l4xx_ll_bus.h"
#include "stm32l4xx_ll_rcc.h"
#include "stm32l4xx_ll_gpio.h"
#include "stm32l4xx_ll_dac.h"
#include "stm32l4xx_ll_dma.h"
#include "stm32l4xx_ll_tim.h"



void MX_TIM2_Init(void)
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};

	/* Peripheral clock enable */
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM2);


	TIM_InitStruct.Prescaler = 0;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 624;
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	LL_TIM_Init(TIM2, &TIM_InitStruct);
	LL_TIM_DisableARRPreload(TIM2);
	LL_TIM_SetClockSource(TIM2, LL_TIM_CLOCKSOURCE_INTERNAL);
	LL_TIM_SetTriggerOutput(TIM2, LL_TIM_TRGO_UPDATE);
	LL_TIM_DisableMasterSlaveMode(TIM2);

}

void MX_DAC1_Init(void)
{

	LL_DAC_InitTypeDef DAC_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	/* Peripheral clock enable */
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_DAC1);

	LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOA);
	/**DAC1 GPIO Configuration
	  PA4   ------> DAC1_OUT1
	  */
	GPIO_InitStruct.Pin = LL_GPIO_PIN_4;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/* DAC1 DMA Init */

	/* DAC_CH1 Init */
	LL_DMA_SetPeriphRequest(DMA1, LL_DMA_CHANNEL_3, LL_DMA_REQUEST_6);
	LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_3, LL_DMA_DIRECTION_MEMORY_TO_PERIPH);
	LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_3, LL_DMA_PRIORITY_HIGH);
	LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_3, LL_DMA_MODE_CIRCULAR);
	LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_3, LL_DMA_PERIPH_NOINCREMENT);
	LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_3, LL_DMA_MEMORY_INCREMENT);
	LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_3, LL_DMA_PDATAALIGN_WORD);
	LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_3, LL_DMA_MDATAALIGN_WORD);


	/** DAC channel OUT1 config */
	DAC_InitStruct.TriggerSource = LL_DAC_TRIG_EXT_TIM2_TRGO;
	DAC_InitStruct.WaveAutoGeneration = LL_DAC_WAVE_AUTO_GENERATION_NONE;
	DAC_InitStruct.OutputBuffer = LL_DAC_OUTPUT_BUFFER_ENABLE;
	DAC_InitStruct.OutputConnection = LL_DAC_OUTPUT_CONNECT_GPIO;
	DAC_InitStruct.OutputMode = LL_DAC_OUTPUT_MODE_NORMAL;
	LL_DAC_Init(DAC1, LL_DAC_CHANNEL_1, &DAC_InitStruct);
	LL_DAC_EnableTrigger(DAC1, LL_DAC_CHANNEL_1);

}


int main()
{
	LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOA);
	
	// DMA init 
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA1); /* DMA controller clock enable */
	NVIC_SetPriority(DMA1_Channel3_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
	NVIC_EnableIRQ(DMA1_Channel3_IRQn);

	MX_DAC1_Init();
	MX_TIM2_Init();

#define NS  128

	uint32_t Wave_LUT[NS];
	for(int i = 0; i < NS/2; i++) Wave_LUT[i] = 2*4095*i/NS;
	for(int i = 0; i < NS/2; i++) Wave_LUT[NS-i-1] = Wave_LUT[i];

	TIM2->ARR = 80000000/400/NS;
	LL_DMA_ConfigAddresses(DMA1, LL_DMA_CHANNEL_3, (uint32_t)Wave_LUT,
			LL_DAC_DMA_GetRegAddr(DAC1, LL_DAC_CHANNEL_1, LL_DAC_DMA_REG_DATA_12BITS_RIGHT_ALIGNED), // right
			//DAC1->DHR12R1, //wrong
			LL_DMA_DIRECTION_MEMORY_TO_PERIPH);
	LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_3, NS);
	LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_3); // needed
	LL_DAC_Enable(DAC1, LL_DAC_CHANNEL_1); // needed
	LL_DAC_EnableDMAReq(DAC1, LL_DAC_CHANNEL_1); //needed


	LL_TIM_EnableCounter(TIM2); // This seems adequate for TIM2

	for(;;);

}
