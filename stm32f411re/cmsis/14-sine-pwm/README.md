# 440Hz sine wave using pwm and 40kHz sampling

## Observations

* uses arm\_math.h:atm\_sin\_f32() - which takes a lot of memoty
* good stuff for abstracting
* it is necessary to keep t within one period. If t gets to large, the calcs go haywire


## Status

2021-12-19	Working.

2021-12-18	Started.
