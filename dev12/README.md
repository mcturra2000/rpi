# Brown noise generator

# Schematic

![](dev12.png) 

C1: 100uF. 22uF also works

R1: 100K variable resistor

SPK: 4 ohm, 5W

## Sundry notes

```
C = 1 / ( 2 pi R fc)
```

## See also

* blog: brown noise sleep machine of 3.9.19
* db10.4
