include $(RPI)/Env.mk
# Variables that you will need to configure:
#CUBE = $(HOME)/STM32Cube/Repository/STM32Cube_FW_F4_V1.28.0/Drivers
CUBE = /home/pi/.config/STM32Cube/Repository/STM32Cube_FW_F4_V1.28.0/Drivers


PREFIX	?= arm-none-eabi-
CC	= $(PREFIX)gcc
CXX	= $(PREFIX)g++
LD	= $(PREFIX)gcc
OBJCOPY	= $(PREFIX)objcopy
OBJDUMP	= $(PREFIX)objdump

CC = arm-none-eabi-gcc
#AS = arm-none-eabi-as
#LD = arm-none-eabi-ld
BIN = arm-none-eabi-objcopy

CMSIS = $(CUBE)/CMSIS

CPU = -mthumb -mcpu=cortex-m4  -mfloat-abi=hard -mfpu=fpv4-sp-d16 
CFLAGS += $(CPU)
CFLAGS += -nostdlib
CFLAGS += -fdata-sections -ffunction-sections # for unused code

INC += -I. -I.. -I$(ROOT) -I$(RPI)/pot
CFLAGS += $(INC)
VPATH += $(ROOT)
VPATH += $(RPI)/pot

# -mthumb turns out to be important (for e.g. memset)
LINKER_SCRIPT ?= linker.ld
LDFLAGS = $(CPU) -T ../$(LINKER_SCRIPT)
LDFLAGS += --specs=nosys.specs  -Wl,--gc-sections -static --specs=nano.specs -Wl,--start-group -lc -lm -Wl,--end-group # for newlib

all : app.bin


app.bin: app.elf 
	arm-none-eabi-objcopy -O binary app.elf app.bin
	arm-none-eabi-objdump -S app.elf >app.dis

#libapp.a : $(OBJS)
#	ar -crs $@ $(OBJS)
	
#app.elf: libapp.a
app.elf: $(OBJS)
	@echo "called via 1"
	$(LD) $(LDFLAGS) -o $@ $^ -L$(CMSIS)/Lib/GCC  # -larm_cortexM4lf_math
	
%.o : %.cc
	$(CXX) $(CFLAGS) -c -o $@ $<
	
%.o : %.c
	$(CC) $(CFLAGS) -c -o $@ $<

%.o : %.s
	$(CC) -c $^
		
.flash : app.bin
	touch .flash
	
flash : .flash
	st-flash --connect-under-reset write *bin 0x8000000	
	
clean :
	rm -rf *.bin *.dis *.elf *.o *.d .flash .targ libapp.a build/
	
info :
	@echo INC = $(INC)
	@echo LD = $(LD)
	@echo LDFLAGS = $(LDFLAGS)
	@echo ROOT = $(ROOT)
	@echo VPATH = $(VPATH)
	@echo CFLAGS= $(CFLAGS)
	@echo LINKER_SCRIPT= $(LINKER_SCRIPT)
	@echo OBJS = $(OBJS)

