# quit without confirmation
define hook-quit
    set confirm off 
end

set pagination off
file Debug/app.elf
#target remote localhost:3333
target extended-remote localhost:3333

# 2023-09-11 added (7f2)
monitor reset halt 

load
#b HAL_RTC_SetDate
#b app.c:165
#b MX_GPIO_Init
#b assert_failed
#b doglcd_test
#monitor reset init # 2023-09-11 removed
echo RUNNING...\n
c
