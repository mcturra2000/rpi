# USB "simple" device test

IN: When the host is ready to receive bulk data it issues an IN Token.

OUT: When the host wants to send the function a bulk data packet, it issues an OUT token

Use `install-rules` to make the device readable and writeable by user.

`lsusb` should return a line:
```
Bus 001 Device 046: ID 3aac:0001 Raspberry Pi Pico Test Device
```

You can find more details about its guts: `lsusb -vd 3aac:0001`

Test the device: `python3 test.py`
should return the capitalised version of the input (Hello World!)


This project is taken from 
pico-examples/usb/device/dev_lowlevel
https://github.com/raspberrypi/pico-examples.git


## See also

* https://www.pschatzmann.ch/home/2021/02/19/tinyusb-a-simple-tutorial/

* [rp2040 experimental Pi pinout replicator](https://medium.com/@oblate_24476/rp2040-experimental-pi-pinout-replicator-dc7ee0145f2c) 2023-07-27


## Status

2023-07-27  Build working again. Improved functionality

2023-07-26  Build failure

2022-11-17	Started. Seems to work. Now blinks an LED on command
