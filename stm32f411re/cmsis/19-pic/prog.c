#include "gpio.h"



#define LED GPIOA, 5

/*
static inline void gpio_set(GPIO_TypeDef *port, uint32_t pin)
{
	port->BSRR = (1 << pin);
}


static inline void gpio_clr(GPIO_TypeDef *port, uint32_t pin)
{
	port->BSRR = (1 << (pin+16));
}
*/

void __attribute__((optimize("O0"))) delayish (uint32_t ms);
void gpio_out(GPIO_TypeDef *port, uint32_t pin);


static void gpio_enable_rcc(GPIO_TypeDef *port)
{
	uint32_t en_val = RCC_AHB1ENR_GPIOAEN;
	if(port == GPIOB) { en_val = RCC_AHB1ENR_GPIOBEN; }
	else if(port == GPIOC) { en_val = RCC_AHB1ENR_GPIOCEN; }
	else if(port == GPIOD) { en_val = RCC_AHB1ENR_GPIODEN; }
	RCC->AHB1ENR	|= en_val;
}


void gpio_out(GPIO_TypeDef *port, uint32_t pin)
{
	gpio_enable_rcc(port);
	port->MODER	|= (1<<(pin*2));
}

void __attribute__((optimize("O0"))) delayish (uint32_t ms)
{
	uint32_t i, j;
	for (i=0; i<ms; i++)
		for (j=0; j<1330; j++) // determined using logic analyser
			asm("nop");
}



void loop()
{
	gpio_out(LED);
	for(;;) {
		gpio_set(LED);
		delayish(100);
		gpio_clr(LED);
		delayish(900);
	}
}
