// SPDX-License-Identifier: GPL-2.0
/*
 * USB Skeleton driver - 2.2
 *
 * Copyright (C) 2001-2004 Greg Kroah-Hartman (greg@kroah.com)
 *
 * This driver is based on the 2.6.3 version of drivers/usb/usb-skeleton.c
 * but has been rewritten to be easier to read and use.
 *
 * Source:
 * https://elixir.bootlin.com/linux/latest/source/drivers/usb/usb-skeleton.c
 *
 * lynx /usr/share/doc/linux-doc-6.1/html/index.html
 */

#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/kref.h>
#include <linux/uaccess.h>
#include <linux/usb.h>
#include <linux/mutex.h>
#include <linux/cdev.h>
#include <linux/ioctl.h>
#include <linux/gpio.h>
#include <linux/gpio/driver.h>
#include <linux/err.h>
//#include <sys/ioctl.h>

#include <uapi/linux/gpio.h> // for GPIO_V2_GET_LINE_IOCTL, GPIO_V2_LINE_SET_VALUES_IOCTL


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Blippy");
MODULE_DESCRIPTION("piclone driver");

/* Define these values to match your devices */
#define USB_SKEL_VENDOR_ID	0x3aac
#define USB_SKEL_PRODUCT_ID	0x0001

/* table of devices that work with this driver */
static const struct usb_device_id gpio_table[] = {
	{ USB_DEVICE(USB_SKEL_VENDOR_ID, USB_SKEL_PRODUCT_ID) },
	{ }					/* Terminating entry */
};
MODULE_DEVICE_TABLE(usb, gpio_table);

static struct usb_device *device;
static dev_t first; // /dev/gpiochip0 device char
static struct cdev c_dev; // Global variable for the character device structure
static struct class *cl; // Global variable for the device class

static ssize_t my_write (struct file *f, const char __user *buf, size_t len, loff_t *off);

// https://embetronicx.com/tutorials/linux/device-drivers/ioctl-tutorial-in-linux/
static long my_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	struct gpio_v2_line_values vals;
	struct gpio_v2_line_request req;
	int wrote_cnt, retval;
	char output_val = '0';
	printk("piclone: my_ioctl()\n");
	switch(cmd) {
		case GPIO_V2_GET_LINE_IOCTL :
			// used for pin init
			printk("piclone: GPIO_V2_GET_LINE_IOCTL\n");
			if(copy_from_user(&req, (const void*) arg, sizeof(req)) != 0) {
				printk("piclone: copy_from_user failed\n");
				return -EFAULT;
			}
			printk("piclone: pin number %d\n", req.offsets[0]);
			req.fd = req.offsets[0]; // we need a way to store the pin

			break;
		case GPIO_V2_LINE_SET_VALUES_IOCTL :
			printk("piclone: GPIO_V2_LINE_SET_VALUES_IOCTL\n");
			if(copy_from_user(&vals, (const void*) arg, sizeof(vals)) != 0) {
				printk("piclone: copy_from_user failed\n");
				return -EFAULT;
			}
			//pi_gpio_put(vals.bits);
			//printk("piclone: vals.bits=%d\n", vals.bits);
			//fd =  
			if(vals.bits) output_val = '1';
			//output_val = 'T';
			retval = usb_bulk_msg(device, usb_sndbulkpipe(device, 0x01), &output_val, 1, &wrote_cnt, 5000);
			//my_write(0, &output_val, 1, 0);
			break;
		//case GPIO_V2_LINE_FLAG_OUTPUT :
		//	printk("piclone: GPIO_V2_LINE_FLAG_OUTPUT\n");
		//	break;
		//case GPIO_V2_LINE_FLAG_INPUT :
		//	printk("piclone: GPIO_V2_LINE_FLAG_INPUT\n");
		//	break;
		default:
			printk("piclone: my_ioctl: unrecognised command %d\n", cmd);
	}
	return 0;
}
static int my_open(struct inode *i, struct file *f)
{
	printk(KERN_INFO "Driver: open()\n");
	return 0;
}
static int my_close(struct inode *i, struct file *f)
{
	printk(KERN_INFO "Driver: close()\n");
	return 0;
}
static ssize_t my_read(struct file *f, char __user *buf, size_t len, loff_t *off)
{
	printk(KERN_INFO "Driver: read()\n");
	return 0;
}
static ssize_t my_write (struct file *f, const char __user *buf, size_t len, loff_t *off)
{
	int retval, wrote_cnt;
	char *tmp;
	printk(KERN_INFO "Driver: write()\n");
	tmp = kmalloc(len, GFP_KERNEL);
	if(!tmp) return -ENOMEM;
	retval = copy_from_user(tmp,  buf, len);
	if(retval) return -EFAULT;
	//retval = usb_bulk_msg(device, usb_sndbulkpipe(device, BULK_EP_OUT), bulk_buf, MIN(cnt, MAX_PKT_SIZE), &wrote_cnt, 5000);
	retval = usb_bulk_msg(device, usb_sndbulkpipe(device, 0x01), tmp, len, &wrote_cnt, 5000);
	kfree(tmp);
	printk("piclone: Bulk message returned %d\n", retval);
	if (retval)
	{
		return retval;
	}
	return len;
}

static struct file_operations pugs_fops =
{
	.owner = THIS_MODULE,
	.open = my_open,
	.read = my_read,
	.write = my_write,
	.unlocked_ioctl = my_ioctl,
	.release = my_close,
};





// called when the device is seen (connected)
static int gpio_probe (struct usb_interface *interface, const struct usb_device_id *id)
{
	//printk(KERN_INFO "Pen drive (%04X:%04X) plugged\n", id->idVendor, id->idProduct);
	struct usb_host_interface *iface_desc;
	struct usb_endpoint_descriptor *endpoint;
	int i;
	//int retval, wrote_cnt;
	//static char msg[] = "T";
	printk("piclone: probe\n");

	iface_desc = interface->cur_altsetting;
	printk(KERN_INFO "Pen i/f %d now probed: (%04X:%04X)\n",
			iface_desc->desc.bInterfaceNumber,
			id->idVendor, id->idProduct);
	printk(KERN_INFO "ID->bNumEndpoints: %02X\n",
			iface_desc->desc.bNumEndpoints);
	printk(KERN_INFO "ID->bInterfaceClass: %02X\n",
			iface_desc->desc.bInterfaceClass);

	for (i = 0; i < iface_desc->desc.bNumEndpoints; i++)
	{
		endpoint = &iface_desc->endpoint[i].desc;

		printk(KERN_INFO "ED[%d]->bEndpointAddress: 0x%02X\n",
				i, endpoint->bEndpointAddress);
		printk(KERN_INFO "ED[%d]->bmAttributes: 0x%02X\n",
				i, endpoint->bmAttributes);
		printk(KERN_INFO "ED[%d]->wMaxPacketSize: 0x%04X (%d)\n",
				i, endpoint->wMaxPacketSize,
				endpoint->wMaxPacketSize);
	}

	device = interface_to_usbdev(interface);




	return 0;
}

// called when the device is disconnected, but not when driver uninstalled
static void gpio_disconnect(struct usb_interface *interface)
{
	//printk(KERN_INFO "piclone: disconnect\n");
	printk("piclone: disconnect\n");
}



/* Get a minor range for your devices from the usb maintainer */
#define USB_SKEL_MINOR_BASE	192

/* our private defines. if this grows any larger, use your own .h file */
#define MAX_TRANSFER		(PAGE_SIZE - 512)
/*
 * MAX_TRANSFER is chosen so that the VM is not stressed by
 * allocations > PAGE_SIZE and the number of packets in a page
 * is an integer 512 is the largest possible packet on EHCI
 */
#define WRITES_IN_FLIGHT	8
/* arbitrarily chosen */

struct usb_interface  intf;

/*
//static int  probe (struct usb_interface *intf, const struct usb_device_id *id)
static char  probe (struct usb_interface *intf, const struct usb_device_id *id)
{
return 0;
}

// Callback to provide a naming hint for a possible device node to create.
char * devnode (struct device *dev, umode_t *mode)
{
return "dunno";
}


static struct usb_class_driver  class_driver = { 
.name = "piclone",
.devnode = &devnode,
} ;
*/

#if 0
// Structure to hold all of our device specific stuff 
struct usb_skel {
	struct usb_device	*udev;			/* the usb device for this device */
	struct usb_interface	*interface;		/* the interface for this device */
	struct semaphore	limit_sem;		/* limiting the number of writes in progress */
	struct usb_anchor	submitted;		/* in case we need to retract our submissions */
	struct urb		*bulk_in_urb;		/* the urb to read data with */
	unsigned char           *bulk_in_buffer;	/* the buffer to receive data */
	size_t			bulk_in_size;		/* the size of the receive buffer */
	size_t			bulk_in_filled;		/* number of bytes in the buffer */
	size_t			bulk_in_copied;		/* already copied to user space */
	__u8			bulk_in_endpointAddr;	/* the address of the bulk in endpoint */
	__u8			bulk_out_endpointAddr;	/* the address of the bulk out endpoint */
	int			errors;			/* the last request tanked */
	bool			ongoing_read;		/* a read is going on */
	spinlock_t		err_lock;		/* lock for errors */
	struct kref		kref;
	struct mutex		io_mutex;		/* synchronize I/O with disconnect */
	unsigned long		disconnected:1;
	wait_queue_head_t	bulk_in_wait;		/* to wait for an ongoing read */
};
#define to_skel_dev(d) container_of(d, struct usb_skel, kref)

static struct usb_driver skel_driver;
#endif


static struct usb_driver gpio_driver = {
	.name = "gpio_driver",
	.id_table = gpio_table,
	.probe = gpio_probe,
	.disconnect = gpio_disconnect,
};

static int __init my_init (void) {
	int result = 0, retval;
	struct device *dev_ret;
	printk("piclone: Hello, Kernel!\n");
	result = usb_register(&gpio_driver);
	if (result < 0) {
		//pr_err("piclone: usb_register failed for the %s driver. Error number %d\n", skel_driver.name, result);
		pr_err("piclone: usb_register failed\n");
		return -1;
	}
	/*
	   usb_register(&intf);
	   ret = usb_register_dev(&intf, &class_driver);
	   if(ret != 0) {
	   printk("piclone: usb_register_dev fail");
	   return ret;
	   }
	   */

	if ((result = alloc_chrdev_region(&first, 0, 1, "gpiochip")) < 0)
	{
		return result;
	}
	printk(KERN_INFO "<Major, Minor>: <%d, %d>\n", MAJOR(first), MINOR(first));
	if (IS_ERR(cl = class_create(THIS_MODULE, "chardrv")))
	{
		unregister_chrdev_region(first, 1);
		return PTR_ERR(cl);
	}
	if (IS_ERR(dev_ret = device_create(cl, NULL, first, NULL, "gpiochip0")))
	{
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return PTR_ERR(dev_ret);
	}

	cdev_init(&c_dev, &pugs_fops);
	if ((retval = cdev_add(&c_dev, first, 1)) < 0)
	{
		device_destroy(cl, first);
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return retval;
	}
	printk("piclone: init success\n");
	return 0;
}
module_init(my_init);

// when you rmmod a module
static void __exit my_exit (void) {
	//struct device *dev_ret;
#if 0
	int wrote_cnt, retval;
	char msg[] = "t";

	retval = usb_bulk_msg(device, usb_sndbulkpipe(device, 0x01), msg, sizeof(msg), &wrote_cnt, 5000);
	printk("piclone: Bulk disconnect message returned %d\n", retval);
	if (retval)
	{
		//return retval;
	}
#endif
	printk("piclone: Goodbye, Kernel\n");
	usb_deregister(&gpio_driver);

	cdev_del(&c_dev);
	device_destroy(cl, first);
	class_destroy(cl);
	unregister_chrdev_region(first, 1);
	printk("piclone: my_exit exiting\n");

	//unregister_chrdev_region(gpio_chrdev, 1);
	//usb_deregister_dev(&intf, &class_driver);

}
module_exit(my_exit);

#if 0
static void skel_draw_down(struct usb_skel *dev);

static void skel_delete(struct kref *kref)
{
	struct usb_skel *dev = to_skel_dev(kref);

	usb_free_urb(dev->bulk_in_urb);
	usb_put_intf(dev->interface);
	usb_put_dev(dev->udev);
	kfree(dev->bulk_in_buffer);
	kfree(dev);
}

static int __init usb_skel_init(void)
{
	int result;

	/* register this driver with the USB subsystem */
	result = usb_register(&skel_driver);
	if (result < 0) {
		pr_err("usb_register failed for the %s driver. Error number %d\n",
				skel_driver.name, result);
		return -1;
	}

	return 0;
}
module_init(usb_skel_init);

static void __exit usb_skel_exit(void)
{
	/* deregister this driver with the USB subsystem */
	usb_deregister(&skel_driver);
}
module_exit(usb_skel_exit);

#endif

