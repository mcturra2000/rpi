#include "../../exports.h"

extern const char blob[];

void ili_pluto_filler(uint16_t* buffer, int slice)
{
	pl_memcpy(buffer, blob  + 240*2*slice, 240*2);
}


int __attribute__ ((section (".text.prog"))) prog()
{
	pl_ili9341_set_filler(ili_pluto_filler);
	pl_sleep_ms(5000);
	pl_ili9341_reset_filler();
	return 0;
}
