set(LIBRARY_NAME usb-kbd)

include_directories($ENV{PICO_SDK_PATH}/lib/tinyusb/hw) # needed for bsp/board.h
include_directories($ENV{PICO_SDK_PATH}/lib/tinyusb/src) # needed for tusb.h

set(ENV{USB_KBD} $ENV{PICOMC}/usb-kbd)
add_library(${LIBRARY_NAME} INTERFACE)
target_sources(${LIBRARY_NAME} INTERFACE
	$ENV{USB_KBD}/hid_app.c
	$ENV{USB_KBD}/msc_app.c
)
target_include_directories(${LIBRARY_NAME} INTERFACE
  $ENV{USB_KBD}
)
target_link_libraries(${LIBRARY_NAME} INTERFACE
	tinyusb_host # for USB
	tinyusb_board # for USB
)
