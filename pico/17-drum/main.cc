#include "hardware/gpio.h"
#include "hardware/pwm.h"
#include "hardware/clocks.h"
#include "hardware/irq.h"
#include "pico/time.h"
#include "pi.h"
#include "gpioxx.h"

//#include "../debounce/debounce.h"

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;

extern const unsigned char bass_start[];
extern const int bass_size;
extern const unsigned char crash_start[];
extern const int crash_size;
extern const unsigned char hatc_start[];
extern const int hatc_size;
extern const unsigned char snare_start[];
extern const int snare_size;
extern const unsigned char tom_start[];
extern const int tom_size;

#define SPK 19

bool start = false;
GpioOut builtin(25);
GpioOut led(27);
GpioOut pin(21); // for timing

uint slice_num;

typedef struct track {
	const  unsigned char* data;
	const /*unsigned*/ int size;
	bool start = false;
	int pos = -1;
} track_t;


track_t bass{bass_start, bass_size};
track_t crash{crash_start, crash_size};
track_t hatc{hatc_start, hatc_size};
track_t snare{snare_start, snare_size};
track_t tom{tom_start, tom_size};

track_t *tracks[] = { &bass,  &hatc, &snare, &tom, &crash };
constexpr auto num_voices = count_of(tracks);
//static_assert(count_of(tracks) == num_voices);
#define BD (1<<0)	// bass drum
#define HC (1<<1)	// hit hat, closed
#define SD (1<<2)	// snare
#define T (1<<3)	// tom-tom
#define CR (1<<4)	// crash

unsigned char take(track_t &tr)
{
	if(tr.start) {
		tr.start = false;
		tr.pos = 0;
	}

	if(-1 < tr.pos && tr.pos < tr.size) {
		u8 val = tr.data[tr.pos++];
		return val;
	} else
		return 1*256/2;
}


/*
 * 2023-04-04 takes about 23us ~ 2% of cycle using "pin" to debug
 */
void __not_in_flash_func(my_pwm_wrap_isr)()
{
	static int tick = 0;
	pin.on();
	int vol = 0;
	for(auto t : tracks) vol += take(*t);
	pwm_set_gpio_level(SPK, vol);
	
	tick++;
	if(tick == 16000) {
		tick = 0;
		for(int led = 6; led<10; led++) pi_gpio_clr(led);
	}


	pin.off();
	pwm_clear_irq(slice_num); 
}


/** NB clock divider must be in range 1.f <= value < 256.f
 * top = 255, freq = 44100 => 11.07
*/

constexpr float pwm_divider(float freq, int top)
{
	uint32_t f_sys = 125'000'000; //clock_get_hz(clk_sys); // typically 125'000'000 Hz
	float scale = (top+1) * freq;
	return (float) f_sys / scale;
}

/*
void isr_systick(void)
{
	static int tick = 0;
	tick++;
	if(tick == 128) {
		tick = 0;
		for(int led = 6; led<10; led++) pi_gpio_clr(led);
	}
}
*/

int main() 
{
	pi_stdio_init();
	bool using_uart = false;

	slice_num = pwm_gpio_to_slice_num(SPK);

	constexpr int top = 255 * num_voices;
	constexpr int sampling_freq = 44'100;
	constexpr float divider = pwm_divider(sampling_freq, top);
	static_assert( (1.0 <= divider) && (divider<256.0));
	pwm_set_clkdiv(slice_num, divider);

	gpio_set_function(SPK, GPIO_FUNC_PWM);
	pwm_set_wrap(slice_num, top);
	pwm_set_enabled(slice_num, true);
	pwm_set_gpio_level(SPK, 0);

	pwm_clear_irq(slice_num);
	pwm_set_irq_enabled(slice_num, true);
	irq_set_exclusive_handler(PWM_IRQ_WRAP, my_pwm_wrap_isr);
	irq_set_enabled(PWM_IRQ_WRAP, true);

	for(int led = 6; led<10; led++) pi_gpio_out(led);

	//systick_init();

	const u8 song1[] = { // std 8th note groove, db7.83
		BD | HC,
		HC,
		SD | HC,
		HC
	};
	const u8 song[] = { 
		//HC, HC, HC, 0, HC, 0, HC, 0, 
		HC, HC, HC, HC, HC, HC, HC, HC, 

		BD | HC,
		HC,
		SD | HC,
		HC,
		BD | HC,
		HC,
		SD | HC,
		HC,


		BD | HC,
		HC,
		SD | HC,
		BD | HC,
		BD | HC,
		HC,
		SD | HC,
		BD | HC,


		BD | HC,
		HC,
			SD | T 	| HC,
		BD 		|  HC,
		BD 		| HC,
		HC,
			SD | T |  HC,
		BD 	|  HC,


		BD | HC,
		HC,
		SD | HC | CR,
		HC,
		BD | HC,
		HC,
		SD | HC | CR,
		HC,


		//0, 0, //0 ,0 , 0 , 0 ,0 ,0
	};
	for(;;) {
		for(int i = 0; i<sizeof(song); i++) {

			u8 what = song[i];
			int led = 6;
			for(auto t : tracks) {
				if(what & 1) {
					t->start = true;
					pi_gpio_set(led);
				}
				what >>= 1;
				led++;
			}

			builtin.on();
			sleep_ms(100);
			builtin.off();
			sleep_ms(150);

			//if(what & 0b10) bass.start = true;
			//if(what & 0b01) snare.start = true;
		}
	}

	return 0;
}

