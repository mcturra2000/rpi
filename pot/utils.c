#include <pot/utils.h>
#include <delay.h>



bool every(every_t *ev)
{
	//last_triggered = 0;
	//ms = millis();
	if(millis() - ev->last_triggered >= ev->period) {
		ev->last_triggered +=  ev->period;
		return true;
	}
	return false;

}
