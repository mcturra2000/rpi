#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <ini.h>


int alarm_hush = 0;
int alarm_timeout_secs = 0;

// Example:
// https://github.com/benhoyt/inih/blob/master/examples/ini_example.c

static int secs = 0;
static int mins = 0;

static int handler(void *user, const char *section, const char *name,
            const char *value)
{
	//#define
	#define IS(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
	#define INT(s, n, var) if(IS(s, n)) { var = atoi(value); }
	INT("alarm", "hush", alarm_hush);
	INT("alarm", "secs", secs);
	INT("alarm", "mins", mins);

	//if(IS("alarm", "hush")) { alarm_hush = atoi(value); }
	//if(IS("alarm", "secs")) {
	//	secs = 


	return 1; // assume everything passes

}

void parse_ini(void)
{
	puts("parse_ini: called");
	if (ini_parse("clock.ini", handler, 0) < 0)
		printf("Can't load 'clock.ini', using defaults\n");
	alarm_timeout_secs = mins * 60 + secs;
	printf("alarm timeout = %ds\n", alarm_timeout_secs);
	puts("parse_ini: exit");
}

#ifdef TEST_INI
int main() { parse_ini(); return 0; }
#endif
