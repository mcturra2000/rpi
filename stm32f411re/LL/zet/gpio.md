
Set a pin high:
```
LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_7);
```

Or even a bunch of them:
```
LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_1 | LL_GPIO_PIN_7);

LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_ALL);
```

Set a pin low:
```
LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_7);
```

Toggle pin (mask):
```
LL_GPIO_TogglePinGPIOA, LL_GPIO_PIN_7);
```
