package main

import (
	"fmt"
)

/*
type Findable interface {
	find() int
}
*/

type pwmmap_t struct {
	gpio string;
	tim uint8;
	ccr uint8;  // channel
	altfn uint8;
	//ccmr uint8; // capture/compare mode register

}
var pwmmap  = []pwmmap_t {
	{"PA0", 5, 1, 2}, // wierd
	{"PA2", 5, 3, 2}, // wierd
	{"PB5", 3, 2, 2}, // works
	{"PB6", 4, 1, 2}, // works
	{"PB7", 4, 2, 2},
}

func find_pwm(gpio string) int  {
	for i, v := range pwmmap {
		if v.gpio == gpio { return i }
	}
	panic("PWM unfound")
}

func pwm(port uint8, pin uint8, freq uint32, duty uint32) () {
	gpio :=  fmt.Sprintf("P%c%d", port, pin)
	fmt.Printf("// %s PWM config, freq %dHz, duty cycle %d%%\n", gpio, freq, duty)
	fmt.Printf("RCC->AHB1ENR |= RCC_AHB1ENR_GPIO%cEN; // Enable port %c clock\n", port, port);
	i  := find_pwm(gpio)
	pwm := pwmmap[i]
	fmt.Printf("gpio_alt(GPIO%c, %d, %d); // %s PWM alt fn\n", port, pin, pwm.altfn, gpio);
	fmt.Printf("RCC->APB1ENR |= RCC_APB1ENR_TIM%dEN; // enable time clock\n", pwm.tim)
	timn := fmt.Sprintf("TIM%d", pwm.tim)
	fmt.Printf("%s->CR1 |= TIM_CR1_ARPE; // buffer the ARR register\n", timn)
	fmt.Printf("%s->CR1 |= TIM_CR1_CEN; // enable the counter\n", timn)
	fmt.Printf("%s->PSC = SystemCoreClock/1000000-1; // scale to 1us\n", timn)
	fmt.Printf("%s->ARR = 1000000/%d-1; // set freq to %d Hz\n", timn, freq, freq);
	fmt.Printf("%s->CCR%d = (%s->ARR +1) * %d/100 -1; // Duty cycle on Cap/Compare Reg\n",
		timn, pwm.ccr, timn, duty)

	ccmr_reg := 1
	if(pwm.ccr>2) { ccmr_reg = 2 }
	ccmr := fmt.Sprintf("CCMR%d", ccmr_reg)

	fmt.Printf("%s->%s |= TIM_%s_OC%dPE; // enable preload for channel %d\n",
		timn, ccmr, ccmr, pwm.ccr,  pwm.ccr)
	fmt.Printf("%s->%s |= (0b110 << TIM_%s_OC%dM_Pos); // PWM mode 1\n", timn, ccmr, ccmr, pwm.ccr)
	fmt.Printf("%s->CCER |= TIM_CCER_CC%dE; // Enable Capture/Compare for channel %d\n", 
		timn, pwm.ccr, pwm.ccr)
	fmt.Println()
}

func main() {
	pwm('A' , 0, 440, 20)
	pwm('A' , 2, 440, 20)
	pwm('B' , 5, 500, 50)
	pwm('B' , 6, 600, 40)
}

