/* using native
*/


#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#include <linux/i2c-dev.h>
#include <i2c/smbus.h>


#include <unistd.h>				//Needed for I2C port
#include <sys/ioctl.h>			//Needed for I2C port
#include <stdio.h>


typedef unsigned char   u8;


// https://gist.github.com/JamesDunne/9b7fbedb74c22ccc833059623f47beb7
// (also contains code for i2c_write()
// Read the given I2C slave device's register and return the read value in `*result`:
int i2c_read(int i2c_fd, u8 slave_addr, u8 reg, u8 *result) {
	int retval;
	u8 outbuf[1], inbuf[1];
	struct i2c_msg msgs[2];
	struct i2c_rdwr_ioctl_data msgset[1];

	msgs[0].addr = slave_addr;
	msgs[0].flags = 0;
	msgs[0].len = 1;
	msgs[0].buf = outbuf;

	msgs[1].addr = slave_addr;
	msgs[1].flags = I2C_M_RD | I2C_M_NOSTART;
	msgs[1].len = 1;
	msgs[1].buf = inbuf;

	msgset[0].msgs = msgs;
	msgset[0].nmsgs = 2;

	outbuf[0] = reg;

	inbuf[0] = 0;

	*result = 0;
	if (ioctl(i2c_fd, I2C_RDWR, &msgset) < 0) {
		perror("ioctl(I2C_RDWR) in i2c_read");
		return -1;
	}

	*result = inbuf[0];
	return 0;
}

int main()
{

	puts("FYI: EREMOTEIO 121 is 'Remote I/O error'");
	puts("FYI: EEBADMSG 74 is 'Bad message'");

	int fd = open("/dev/i2c-1", O_RDWR);
	if(fd < 0) {
		puts("Couldn't open I2C");
		exit(1);
	}

	int sid = 0x4;
	if (ioctl(fd, I2C_SLAVE, sid) < 0) {
		/* ERROR HANDLING; you can check errno to see what went wrong */
		puts("Couldn't ioctl slave");
		exit(1);
	}


	// if possible, use i2c_smbus_* methods instead of direct ioctls
	//if(ioctl(I2C_SMBUS

	// enable PEC (Packet Error Checking), because it seems to happen
	if(ioctl(fd, I2C_PEC, 1) < 0) {
		puts("Couldn't neable I2C_PEC");
		exit(1);
	}

	//__u8 reg = 0x10; // Device register to access. Actually irrelevant
	//__s32 res = i2c_smbus_read_word_data(fd, reg);

	for(;;) {
		//__s32 val = i2c_smbus_read_byte_data(fd, 0x0); // with I2C T85, slave sometimes returns -121
		u8 val;
#if 0
		if(i2c_read(fd, sid, 0x00, &val) ==0)
			printf("val returned = %d\n", val);
#else

		// the T85 I'm attach to seems reasonaly unreliable
		if(read(fd, &val, 1) != 1) { // seems the most reliable way of detecting errors
			puts("Read failed");
		}
		printf("val returned = %d\n", val);
		goto sleepy;

		val = i2c_smbus_read_byte(fd); // with I2C T85, slave sometimes returns -121
		if(val <0) {
			printf("Read err %d\n", errno);
		} else {
			printf("val returned = %d\n", val);
		}
#endif
sleepy:
		sleep(1);
	}

	return 0;

}
