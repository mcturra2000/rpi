#include <pico/time.h>

#include <delay.h>


uint32_t millis()
{
	return to_ms_since_boot(get_absolute_time());
}

void delay_us (uint64_t us)
{
        sleep_us(us);
}

void delayish (int ms)
{
        const int top = 25000; // calibrated at 2023-06-27
        for(int i =0 ; i< ms; i++) {
                for(int j = 0; j< top; j++) {
                        asm volatile ("nop");
                }
        }

}



