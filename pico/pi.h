#pragma once

#include "gpio.h"
#include "hardware/i2c.h"
#include "hardware/irq.h"
#include "hardware/uart.h"

#if LIB_PICO_STDIO_USB
//#pragma message "USB configured as stdio"
#include "tusb.h" // if you want to use tud_cdc_connected()
#endif


#ifdef __cplusplus
extern "C" {
#endif

#define LED_BUILTIN	25

//void delay_us (uint64_t us);

void delayish (int ms);

void pi_stdio_init();

void systick_init (); // see project 80 for example. Use void isr_systick(void)

void pi_alarm_init(uint alarm_num, irq_handler_t callback, uint32_t delay_us);

// declaring it static inline seems to fix the fluttering problems
static inline void pi_alarm_rearm(int alarm_num, uint32_t delay_us)
{
	// Clear the alarm irq
	hw_clear_bits(&timer_hw->intr, 1u << alarm_num);

	//uint32_t delay_us = 2 * 1'000'000; // 2 secs
	// Alarm is only 32 bits so if trying to delay more
	// than that need to be careful and keep track of the upper
	// bits
	//uint64_t target = timer_hw->timerawl + delay_us;

	// Write the lower 32 bits of the target time to the alarm which
	// will arm it
	//timer_hw->alarm[alarm_num] = (uint32_t) target;
	
	timer_hw->alarm[alarm_num] = timer_hw->timerawl + delay_us;

}


//typedef enum {INPUT, OUTPUT, INPUT_PULLUP, INPUT_PULLDOWN} pi_gpio_mode_e;
void pi_gpio_init(uint gpio, pi_gpio_mode_e mode);
static inline void pi_gpio_high(uint gpio) { gpio_put(gpio, 1); }
static inline void pi_gpio_set(uint gpio) { gpio_put(gpio, 1); }
static inline void pi_gpio_low(uint gpio) { gpio_put(gpio, 0); }
static inline void pi_gpio_clr(uint gpio) { gpio_put(gpio, 0); }
void pi_gpio_in(uint gpio);
void pi_gpio_out(uint gpio);
static inline int pi_gpio_is_high(uint gpio) { if(gpio_get(gpio) == 0) return 0; return 1; }
static inline int pi_gpio_is_low(uint gpio) { if(gpio_get(gpio) == 0) return 1; return 0; }
void pi_gpio_toggle(uint gpio);


void pi_pwm_init(uint gpio, uint freq, uint duty);
extern i2c_inst_t *pi_i2c_default_port;
void pi_i2c_init(int sda);

void pi_max7219_init(void);
void pi_max7219_show_count(int count);
void pi_max7219_tfr(uint8_t address, uint8_t value);

void pi_spi_init_std(void);

#ifdef __cplusplus
}


#endif // __cplusplus
