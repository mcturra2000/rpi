#!/usr/bin/env python3

#
# Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
#
# SPDX-License-Identifier: BSD-3-Clause
#

from time import sleep

import usb.core # sudo pip3 install pyusb
import usb.util

# find our device
dev = usb.core.find(idVendor=0x3AAC, idProduct=0x0001)

# was it found?
if dev is None:
    raise ValueError('Device not found')

# get an endpoint instance
cfg = dev.get_active_configuration()
intf = cfg[(0, 0)]

def matcher(d):
	def el(e):
		x = usb.util.endpoint_direction(e.bEndpointAddress) == d
		x = x and (usb.util.endpoint_type(e.bmAttributes) == usb.util.ENDPOINT_TYPE_BULK)
		return x
	return el

outep = usb.util.find_descriptor(
    intf,
    # match the first OUT endpoint
    custom_match= \
        lambda e: \
            usb.util.endpoint_direction(e.bEndpointAddress) == \
            usb.util.ENDPOINT_OUT)

outep = usb.util.find_descriptor(intf, custom_match = matcher(usb.util.ENDPOINT_OUT))
#print(outep)

inep = usb.util.find_descriptor(
    intf,
    # match the first IN endpoint
    custom_match= \
        lambda e: \
            usb.util.endpoint_direction(e.bEndpointAddress) == \
            usb.util.ENDPOINT_IN)

inep = usb.util.find_descriptor(intf, custom_match = matcher(usb.util.ENDPOINT_IN))

assert inep is not None
assert outep is not None

#outep.write("GCO\x19") # set pin 25 to output
#outep.write("GW\x19\x01") # set pin high
outep.write([71, 67, 79, 25]) # G C O 25 : set pin 25 to output
while True:
	outep.write([71, 87, 25, 1]) # G W 25 1 : set pin 25 high
	sleep(0.1)
	outep.write([71, 87, 25, 0]) # G W 25 1 : set pin 25 low
	sleep(1.1)

#while True:
#	x = input()
#	print(bytes(x, "utf-8"))
#	val = 1 if x == "1" else 0
#	print("writing")
#	outep.write([val])
#	print("reading")
#	inep.read(2)

