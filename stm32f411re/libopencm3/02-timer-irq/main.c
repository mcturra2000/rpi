//#include <libopencm3/stm32/rcc.h>
//#include <libopencm3/stm32/gpio.h>
#include <gpio.h>
#include <mal.h>


typedef uint32_t u32;

//#define LED GPIOC, GPIO13
#define LED1	GPIOC, GPIO13 // blackpill
#define LED2	GPIOA, GPIO5 // Nucleo board

void tim2_isr(void) // the standard ISR name for TIM2
{
	timer_clear_flag(TIM2, TIM_SR_UIF); // hmmm, seems to matter that it's at the top
	volatile static int count = 0;
	count++;
	if(count == 1000) {
		gpio_toggle(LED1);
		gpio_toggle(LED2);
		count = 0;
	}
}


int main(void)
{
	gpio_out(LED1);
	gpio_out(LED2);
	int freq = 1000;
	mal_timer_init(TIMER2, freq);
	while(1);
}
