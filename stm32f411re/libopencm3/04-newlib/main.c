#include <errno.h>

#include <gpio.h>


#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/spi.h>
#include <libopencm3/stm32/usart.h>

#include <ctype.h>
#include <stdio.h>

#include <string.h>



int main(void)
{

	char msg[] = "I will echo everything you type...\r\n";
	char *str = msg;
	puts(msg);
	
	char copy[100];
	memcpy(copy, msg, strlen(msg)); // let's see if we can copy a string using newlib
	printf("This string printed using the %s command\r\n", "printf()");
	sprintf(copy, "The meaning of of is %d\r\n", 42);
	str = copy;
	printf(str);
	puts("This is a test of puts()");


	char local_buf[32];
	printf("Enter some text, pressing return afterwards :");
	fflush(stdout);
	fgets(local_buf, 32, stdin);
	printf("You entered '%s'\r\n", local_buf);

	printf("I will echo everything you type...\r\n");


	while (1)
	{
		//uint16_t c = usart_recv_blocking(USART1);
		int c = getchar();
		//usart_send_blocking(USART1, c);
		putchar(c);
	}
}
