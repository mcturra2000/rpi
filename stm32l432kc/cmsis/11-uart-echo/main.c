#include "conf.h"

#include "delay.h"
#include "uart.h"

int main()
{
	uart2_init();
	//uart2_set_baudrate(115200*2);

	char msg[] = "hello world\n";
	//uart2_send_blocking(msg, sizeof(msg));
	while(1) {
		uint8_t c = uart2_recv_blocking();
		uart2_send_blocking(&c, 1);
	}

}
