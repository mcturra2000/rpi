# Pico sin wave of 440Hz outputting to DAC

Output is actually approx 454 Hz, although it is configured for 440Hz.


## Status


2022-12-17	Used sine table. Didn't help reduce hitching, but IRQ timing reduced to 375ns, a HUGE difference.

2022-12-17	Used direct SPI hardware writing instead of blocking. 
		Reduced timing from 22.9us to 20us. Didn't help.

2022-12-07	Used __not_in_flash(), but problems persist

2022-02-05	Tweaks, but it still has a peculiar buzz

2021-07-07	Towards greater modularity to conform to other stuff. Sees to work, but I'm not 100% happy.

2021-04-03 	Started. Working

