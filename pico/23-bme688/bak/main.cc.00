#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
//#include "hardware/adc.h"
//#include "hardware/clocks.h"
//#include "hardware/flash.h"
#include "hardware/gpio.h"
//#include "hardware/irq.h"
//#include "hardware/pwm.h"
#include "hardware/i2c.h"
// #include "tusb.h" // if you want to use tud_cdc_connected()
//#include <pico/time.h>

#include "pi.h"

#define LED  25 // GPIO of built-in LED


typedef i2c_inst_t* i2c_t;
typedef uint8_t* bytes;
typedef uint8_t ubyte;
typedef uint32_t u32;


void i2c_send_bytes_at(i2c_t _i2c, int sid, ubyte reg, bytes values, int size)
{
	i2c_write_blocking(_i2c, sid, &reg, 1, true);
	i2c_write_blocking(_i2c, sid, values, size, false);
}


void i2c_send_byte_at(i2c_t _i2c, int _address, int reg, ubyte value)
{
	i2c_send_bytes_at(_i2c, _address, reg, &value, 1);

}

void i2c_recv_bytes_at(i2c_t _i2c, int sid, ubyte reg , bytes values, int size)
{
	i2c_write_blocking(_i2c, sid, &reg, 1, true);
	i2c_read_blocking(_i2c, sid, values, size, false);
}

ubyte i2c_recv_byte_at(i2c_t _i2c, int sid, ubyte reg )
{
	ubyte value;
	i2c_recv_bytes_at(_i2c, sid, reg, &value, 1);
	return value;
}

u32 millis() { return time_us_32(); }

/*
   void time_sleep(float secs)
   {
   sleep_ms(secs * 1000.00);
   }
   */

#define _BME680_CHIPID 0x61
#define _BME680_REG_CHIPID 0xD0
#define _BME680_BME680_COEFF_ADDR1 0x89
#define _BME680_BME680_COEFF_ADDR2 0xE1
#define _BME680_BME680_RES_HEAT_0 0x5A
#define _BME680_BME680_GAS_WAIT_0 0x64
#define _BME680_REG_SOFTRESET 0xE0
#define _BME680_REG_CTRL_GAS 0x71
#define _BME680_REG_CTRL_HUM 0x72
#define _BME680_REG_STATUS 0xF3
#define _BME680_REG_CTRL_MEAS 0x74
#define _BME680_REG_CONFIG 0x75
#define _BME680_REG_PAGE_SELECT 0x73
#define _BME680_REG_MEAS_STATUS 0x1D

#define _BME680_REG_CONFIG  0x75
#define _BME680_REG_PAGE_SELECT 0x73
#define _BME680_REG_MEAS_STATUS 0x1D
#define _BME680_REG_PDATA 0x1F
#define _BME680_REG_TDATA 0x22
#define _BME680_REG_HDATA 0x25
constexpr auto _BME680_SAMPLERATES = (0, 1, 2, 4, 8, 16);
constexpr auto _BME680_FILTERSIZES = (0, 1, 3, 7, 15, 31, 63, 127);
#define  _BME680_RUNGAS 0x10
constexpr auto _LOOKUP_TABLE_1 = (2147483647.0, 2147483647.0, 2147483647.0, 2147483647.0, 2147483647.0,
		2126008810.0, 2147483647.0, 2130303777.0, 2147483647.0, 2147483647.0,
		2143188679.0, 2136746228.0, 2147483647.0, 2126008810.0, 2147483647.0,
		2147483647.0);
constexpr auto  _LOOKUP_TABLE_2 = (4096000000.0, 2048000000.0, 1024000000.0, 512000000.0, 255744255.0, 127110228.0,
		64000000.0, 32258064.0, 16016016.0, 8000000.0, 4000000.0, 2000000.0, 1000000.0,
		500000.0, 250000.0, 125000.0);

#if 1
float _read24(bytes arr, int len)
{
	float ret = 0.0;
	for(int i = 0; i< len; i++) {
		ret *= 256.0;
		ret += (float)(arr[i] & 0xFF);
	}
	return ret
}
#endif

class BME680_I2C {
	public:
		i2c_t _i2c = i2c0;
		int _sid = 0x76;
		bool _debug = false;
		u32 _last_reading = millis();
		int _adc_pres, _adc_temp, _adc_hum, _adc_gas, _gas_range, _t_fine;
		int _pressure_oversample, _temp_oversample, _humidity_oversample, _filter;
		int _heat_range , _heat_val , _sw_err ;

		float _level_pressure;

		int _refresh_rate = 10;
		int _min_refresh_time = 1000 / _refresh_rate;
		void _read(int reg, bytes values, int length) {
			//result = bytearray(length);
			//self._i2c.readfrom_mem_into(_address, reg & 0xff, result);
			i2c_recv_bytes_at(_i2c, _sid, reg & 0xff, values, length);

			// if self._debug: print("\t${:x} read ".format(register), " ".join(["{:02x}".format(i) for i in result]))
			//return result;
		};

		ubyte  _read(int reg) {
			ubyte value;
			_read(reg, &value, 1);
			return value;
		}

		void _write(int reg, bytes values, int n) {
			//if self._debug: print("\t${:x} write".format(register), " ".join(["{:02x}".format(i) for i in values]))
			for(int i = 0; i < n; i++) {
				ubyte value = values[i] & 0xff;
				//self._i2c.writeto_mem(self._address, reg, bytearray([value & 0xFF]));
				i2c_send_byte_at(_i2c, _sid, reg, value);
				reg += 1;
			}
		};
		void _write(int reg, ubyte value) {
			_write(reg, &value, 1);
		}

		void _read_calibration() {
			coeff = _read(_BME680_BME680_COEFF_ADDR1, 25);
			coeff += _read(_BME680_BME680_COEFF_ADDR2, 16);
			coeff = list(struct.unpack('<hbBHhbBhhbbHhhBBBHbbbBbHhbb', bytes(coeff[1:39])));
			coeff = [float(i) for i in coeff];
			_temp_calibration = [coeff[x] for x in [23, 0, 1]];
			_pressure_calibration = [coeff[x] for x in [3, 4, 5, 7, 8, 10, 9, 12, 13, 14]];
			_humidity_calibration = [coeff[x] for x in [17, 16, 18, 19, 20, 21, 22]];
			_gas_calibration = [coeff[x] for x in [25, 24, 26]];
			_humidity_calibration[1] *= 16;
			_humidity_calibration[1] += _humidity_calibration[0] % 16;
			_humidity_calibration[0] /= 16;
			_heat_range = (_read(0x02) & 0x30) / 16;
			_heat_val = _read(0x00);
			_sw_err = (_read(0x04) & 0xF0) / 16;
		};

		BME680_I2C (){ 
			_write(_BME680_REG_SOFTRESET, 0xB6);
			delayish(5);
			volatile auto chip_id = _read(_BME680_REG_CHIPID); // should return 0x61, 97

			delayish(0);
			//chip_id = self._read_byte(_BME680_REG_CHIPID);
			//if (chip_id != _BME680_CHIPID) throw 666;
			//  raise RuntimeError('Failed 0x%x' % chip_id)
#if 1 
			_read_calibration();
			_write(_BME680_BME680_RES_HEAT_0, 0x73);
			_write(_BME680_BME680_GAS_WAIT_0, 0x65);
			_level_pressure = 1013.25;
			_pressure_oversample = 0b011;
			_temp_oversample = 0b100;
			_humidity_oversample = 0b010;
			_filter = 0b010;
			/*
			   _adc_pres = None;
			   _adc_temp = None;
			   _adc_hum = None;
			   _adc_gas = None;
			   _gas_range = None;
			   _t_fine = None;
			   */
			_last_reading = millis();
			_min_refresh_time = 1000 / _refresh_rate;
#endif
		};
		/*
		   int pressure_oversample() {

		   return _BME680_SAMPLERATES[self._pressure_oversample];
		   }
		   @pressure_oversample.setter
		   def pressure_oversample(self, sample_rate):
		   if sample_rate in _BME680_SAMPLERATES:
		   self._pressure_oversample = _BME680_SAMPLERATES.index(sample_rate)
else:
raise RuntimeError("Invalid")
@property
def humidity_oversample(self):
return _BME680_SAMPLERATES[self._humidity_oversample]
@humidity_oversample.setter
def humidity_oversample(self, sample_rate):
if sample_rate in _BME680_SAMPLERATES:
self._humidity_oversample = _BME680_SAMPLERATES.index(sample_rate)
else:
raise RuntimeError("Invalid")
@property
def temperature_oversample(self):
return _BME680_SAMPLERATES[self._temp_oversample]
@temperature_oversample.setter
def temperature_oversample(self, sample_rate):
if sample_rate in _BME680_SAMPLERATES:
self._temp_oversample = _BME680_SAMPLERATES.index(sample_rate)
else:
raise RuntimeError("Invalid")
@property
def filter_size(self):
return _BME680_FILTERSIZES[self._filter]
@filter_size.setter
def filter_size(self, size):
if size in _BME680_FILTERSIZES:
self._filter = _BME680_FILTERSIZES[size]
else:
raise RuntimeError("Invalid")
@property
*/

#if 1 
		void _perform_reading() {
			if (millis() - _last_reading 	< _min_refresh_time) return;
			_write(_BME680_REG_CONFIG, _filter << 2);
			_write(_BME680_REG_CTRL_MEAS,(_temp_oversample << 5)|(_pressure_oversample << 2));
			_write(_BME680_REG_CTRL_HUM, _humidity_oversample);
			_write(_BME680_REG_CTRL_GAS, _BME680_RUNGAS << 1);
			int ctrl = _read_byte(_BME680_REG_CTRL_MEAS);
			ctrl = (ctrl & 0xFC) | 0x01;
			_write(_BME680_REG_CTRL_MEAS, ctrl);
			new_data = false;
			ubyte data[17];
			while(! new_data) {
				data = _read(_BME680_REG_MEAS_STATUS, 17);
				new_data = data[0] & 0x80 != 0;
				delayish(5);
			}
			_last_reading = millis();
			//_adc_pres = _read24(data[2:5]) / 16;
			//_adc_temp = _read24(data[5:8]) / 16;
			_adc_temp = _read24(data+5, 3) / 16;
			//_adc_hum = struct.unpack('>H', bytes(data[8:10]))[0];
			//_adc_gas = int(struct.unpack('>H', bytes(data[15:17]))[0] / 64);
			//_gas_range = data[16] & 0x0F;
			int var1 = (_adc_temp / 8) - (_temp_calibration[0] * 2);
			int var2 = (var1 * _temp_calibration[1]) / 2048;
			int var3 = ((var1 / 2) * (var1 / 2)) / 4096;
			var3 = (var3 * _temp_calibration[2] * 16) / 16384;
			_t_fine = var2 + var3;
		};

		int temperature() {
			_perform_reading();
			auto calc_temp = (((_t_fine * 5) + 128) / 256);
			return calc_temp / 100;
		};
#endif
		/*
		   @property
		   def pressure(self):
		   self._perform_reading()
		   var1 = (self._t_fine / 2) - 64000
		   var2 = ((var1 / 4) * (var1 / 4)) / 2048
		   var2 = (var2 * self._pressure_calibration[5]) / 4
		   var2 = var2 + (var1 * self._pressure_calibration[4] * 2)
		   var2 = (var2 / 4) + (self._pressure_calibration[3] * 65536)
		   var1 = (((((var1 / 4) * (var1 / 4)) / 8192) *
		   (self._pressure_calibration[2] * 32) / 8) +
		   ((self._pressure_calibration[1] * var1) / 2))
		   var1 = var1 / 262144
		   var1 = ((32768 + var1) * self._pressure_calibration[0]) / 32768
		   calc_pres = 1048576 - self._adc_pres
		   calc_pres = (calc_pres - (var2 / 4096)) * 3125
		   calc_pres = (calc_pres / var1) * 2
		   var1 = (self._pressure_calibration[8] * (((calc_pres / 8) * (calc_pres / 8)) / 8192)) / 4096
		   var2 = ((calc_pres / 4) * self._pressure_calibration[7]) / 8192
		   var3 = (((calc_pres / 256) ** 3) * self._pressure_calibration[9]) / 131072
		   calc_pres += ((var1 + var2 + var3 + (self._pressure_calibration[6] * 128)) / 16)
		   return calc_pres/100
		   @property
		   def humidity(self):
		   self._perform_reading()
		   temp_scaled = ((self._t_fine * 5) + 128) / 256
		   var1 = ((self._adc_hum - (self._humidity_calibration[0] * 16)) -
		   ((temp_scaled * self._humidity_calibration[2]) / 200))
		   var2 = (self._humidity_calibration[1] *
		   (((temp_scaled * self._humidity_calibration[3]) / 100) +
		   (((temp_scaled * ((temp_scaled * self._humidity_calibration[4]) / 100)) /
		   64) / 100) + 16384)) / 1024
		   var3 = var1 * var2
		   var4 = self._humidity_calibration[5] * 128
		   var4 = (var4 + ((temp_scaled * self._humidity_calibration[6]) / 100)) / 16
		   var5 = ((var3 / 16384) * (var3 / 16384)) / 1024
		   var6 = (var4 * var5) / 2
		   calc_hum = (((var3 + var6) / 1024) * 1000) / 4096
		   calc_hum /= 1000
		   if calc_hum > 100:
		   calc_hum = 100
		   if calc_hum < 0:
		   calc_hum = 0
		   return calc_hum
		   @property
		   def altitude(self):
		   pressure = self.pressure
		   return 44330 * (1.0 - math.pow(pressure / self.sea_level_pressure, 0.1903))
		   @property
		   def gas(self):
		   self._perform_reading()
		   var1 = 262144 >> self._gas_range
		   var2 = self._adc_gas - 512
		   var2 *= 3
		   var2 = 4096 + var2
		   calc_gas_res = (1000 * var1) / var2
		   calc_gas_res = calc_gas_res * 100
		   return int(calc_gas_res)
####
#     var1 = ((1340 + (5 * self._sw_err)) * (_LOOKUP_TABLE_1[self._gas_range])) / 65536
#     var2 = ((self._adc_gas * 32768) - 16777216) + var1
#     var3 = (_LOOKUP_TABLE_2[self._gas_range] * var1) / 512
#     calc_gas_res = (var3 + (var2 / 2)) / var2
#     return int(calc_gas_res)
####
def _read_byte(self, register):
return self._read(register, 1)[0]
def _read(self, register, length):
raise NotImplementedError()
def _write(self, register, values):
raise NotImplementedError()
		*/
};

/*
   class BME680_I2C_XXX : Adafruit_BME680 { 
   public: 
   i2c_t _i2c;
   int _address;
   bool _debug;
   BME680_I2C (i2c_t i2c, int address=0x76, bool debug = false, int refresh_rate=10) {
   _i2c = i2c;
   _address = address;
   _debug = debug;
//super().__init__(refresh_rate=refresh_rate);
_refresh_rate = refresh_rate;
};

};
*/

/*
   void delay(int n) // no particular timing
   {
   for(int i =0 ; i< n; i++) {
   for(int j = 0; j< 10000 * 3; j++) {
   asm volatile ("nop");
   }
   }
   }
   */

int main() 
{
	//stdio_init_all();
	// while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 

	//printf("project 23 \n");
	pi_i2c_init(4);
	pi_gpio_init(LED, OUTPUT);
	const auto SID = 0x76; // 0xd0;
	uint8_t value = 0;
	auto i2c = i2c0;
	int it = 0;

	BME680_I2C bme;

	for(;;) {
		/*
		   printf("Reading %d\n", it++);
		   value = 0xd0; // for chip id
		   i2c_write_blocking(i2c, SID, &value, 1, true);
		   int res = i2c_read_blocking(i2c, SID, &value, 1, false);
		   if(res == 1) 
		   printf("Number returned: 0x%x (s/b 0x61)\n", value);
		   else
		   puts("ERR: no bytes_read");
		   puts("Written");
		//pi_gpio_toggle(LED);
		//sleep_ms(1000); // dosn't seem to work for some reason
		//busy_wait_us(1000 * 1000);
		*/
		delayish(1000);
		//puts("Slept");
	}

	return 0;
}

