#define STM32F411xE
#include "stm32f4xx.h"
#include <gpio.h>
#include <uart.h>

uint32_t SystemCoreClock = 16000000;

#if 0
#define _USART USART2

int putchar(int c)
{
	//if(c=='\r') putchar('\n');
	while( !( _USART->SR & USART_SR_TXE ) ) {}; // wait until we are able to transmit
	_USART->DR = c; // transmit the character
	return c;
}

int puts(const char *s)
{
	while(*s)
		putchar(*s++);
	putchar('\n');
	return 1;
}

int getchar()
{
	while( !( _USART->SR & USART_SR_RXNE ) ) {}; // wait until something received
	return _USART->DR; // find out what it is
}
#endif

void USART2_IRQHandler(void)
{
	static volatile int c = -1;
	if(USART2->SR & USART_SR_RXNE) {
		c = USART2->DR;
		//while(!(USART2->SR & USART_SR_TXE));
		//while((USART2->SR & USART_SR_TXE) != 0);
		//USART2->CR1 |= USART_CR1_TE; // idle chars - prolly unnecessary
		USART2->DR = c;
		//while(!(USART2->SR & USART_SR_TC));
	        //USART2->CR1 &= ~USART_CR1_RXNEIE;
		//return;

	}
}
int main (void) 
{
	uart_init();
	//gpio_pullup(GPIOA, 2); // doesn't seem to help
	//gpio_pullup(GPIOA, 3); // ditto
	//GPIOA->OSPEEDR |= 0b10100000; // high speed, although seems unhelpful
	USART2->CR1 |= USART_CR1_RXNEIE;
	//USART2->CR1 |= USART_CR1_TCIE;
	//USART2->CR1 |= USART_CR1_TXEIE;
	NVIC_EnableIRQ(USART2_IRQn);

	//puts("UART test - if you can read this, then it should be working!");
	//puts("Now type something, and I will echo it to serial");
	for(;;);
}
