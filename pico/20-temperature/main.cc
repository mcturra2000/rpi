#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
//#include "hardware/spi.h"
#include "hardware/gpio.h"
#include "hardware/adc.h"
#if LIB_PICO_STDIO_USB
#pragma message "USB configured as stdio"
#include "tusb.h" // if you want to use tud_cdc_connected()
#endif


#define BTN 14 // GPIO number, not physical pin
#define LED 25 // GPIO of built-in LED

constexpr auto use_thermistor = true;

void print_thermistor()
{
	adc_select_input(0);
	uint16_t thermistor = adc_read();
	printf(", thermistor = %d", thermistor);
}

int main() 
{
	stdio_init_all();
#if LIB_PICO_STDIO_USB
        while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 
#endif

	adc_init();
	adc_set_temp_sensor_enabled(true);
	if constexpr(use_thermistor) adc_gpio_init(26); // thermistor (assuming it is connected)
	

	gpio_init(BTN);
	gpio_set_dir(BTN, GPIO_IN);
	gpio_pull_up(BTN);
	// gpio_get() gets state of pin

	gpio_init(LED);
	gpio_set_dir(LED, GPIO_OUT);

	int i = 0;
	for(;;) {	
	
		// read internal temperature
		adc_select_input(4);
		uint16_t raw = adc_read();
		const float conversion_factor = 3.3f / (1<<12);
		float result = raw * conversion_factor;
		float temp = 27 - (result -0.706)/0.001721;
		
		printf("Reading %5d, Internal temp = %f C", i++, temp);
		if constexpr(use_thermistor) print_thermistor();
		printf("\n");
		sleep_ms(1000);		
	}

	return 0;
}

