##########################################
### This Code is for Raspberry Pi Pico ###
###      copyright 2021 balance19      ###
##########################################




from machine import Pin,I2C
from time import sleep_ms

import ds3231
import time


i2c = I2C(0, sda=Pin(4), scl=Pin(5), freq = 100000)

rtc = ds3231.RTC(i2c, sid = 0x68)

# It is encoded like this: sec min hour week day month year.
# Uncomment the line below to set time. Do this only once otherwise time will be set everytime the code is executed.
# rtc.DS3231_SetTime(b'\x00\x14\x18\x28\x14\x07\x21')

def settime(sec, minute, hour, week, day, month, year):
    packed = b"\x00\x23\x12\x28\x14\x07\x21"
    #rtc = RTC_DS3231.RTC(sda_pin=0, scl_pin=1, port = 0)
    rtc.SetTime(packed)

def settime1():
    settime(0,0,0,0,0,0,0)
  
def gettime():
    #rtc = RTC_DS3231.RTC(sda_pin=0, scl_pin=1, port = 0)
    t = rtc.ReadTime(1)
    print(t)

    

def run():
    while True:
        second, minute, hour, weekday, day, month, year = rtc.ReadTime(0)
        day, hour = ds3231.adjbst(year, month, day, hour)
        print(hour, minute)
        time.sleep(1)

run()
