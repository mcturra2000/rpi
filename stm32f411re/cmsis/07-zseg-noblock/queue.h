#pragma once

#include <stdbool.h>

int peek();
bool isEmpty();
bool isFull();
int size();
void insert(int data);
int removeData();
