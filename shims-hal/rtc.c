#include <rtc.h>


#ifdef HAL_RTC_MODULE_ENABLED
#include <stdbool.h>
#include <pot/ds3231.h>
#include <pot/cal.h>


extern RTC_HandleTypeDef hrtc;
extern void Error_Handler(void);

#if 0
static bool rtc_hr_trigger = false;
uint32_t rtc_num_alarms = 0;



void rtc_do_hourly()
{
#if 0
	if(!rtc_hr_trigger) return;
	save_count();
	rtc_set_from_ds3231(); // sync the RTC
	HAL_GPIO_TogglePin(LEDb); // TODO remove
	bzr_start(); // TODO remove
	rtc_hr_trigger = false;
#endif
}


void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
{
	rtc_num_alarms++;
	rtc_hr_trigger = true;
	//HAL_GPIO_TogglePin(LEDb);
}
#endif

void rtc_set_int(long long timeval)
{
	struct tm t;
	cal_reduce_timevalue(timeval, &t);
	//int wday = DS3231_reduce(&timeval); // 1 is Sun (silly, I know)
	//if(wday == 0) wday = 7; // Sun must be 7
	//volatile int sec = DS3231_reduce(&timeval);
	//volatile int min = DS3231_reduce(&timeval);
	//volatile int hr = DS3231_reduce(&timeval);
	//volatile int mday = DS3231_reduce(&timeval);
	//volatile int month = DS3231_reduce(&timeval);
	//volatile long long year = timeval;
	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;
	sTime.Hours = t.tm_hour;
	sTime.Minutes = t.tm_min;
	sTime.Seconds = t.tm_sec;
	sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	sTime.StoreOperation = RTC_STOREOPERATION_RESET;
	//sTime.TimeFormat = RTC_HourFormat_24;
	if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) != HAL_OK) {
		//_Error_Handler(__FILE__, __LINE__);
		Error_Handler();
	}
	sDate.Date = t.tm_mday;
	sDate.Month = t.tm_mon+1; // month must be in range [1,12]
	sDate.Year = t.tm_year % 100; // year must be in range [0,99]
	sDate.WeekDay = t.tm_wday;
	if(sDate.WeekDay == 0) sDate.WeekDay = 7; // Sun must be 7, not 0

	if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN) != HAL_OK) {
		//_Error_Handler(__FILE__, __LINE__);
		Error_Handler();
	}

}
#if 0
/* get RTC data and pack it into a ds3231 struct, which can be easier to handle */
[[deprecated]] // use rtc_get_tm instead
void rtc_get(struct ts *t)
{
	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;
	HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BIN); // need to do this, even if not used

	 t->hour = sTime.Hours;
	 t->min = sTime.Minutes;
	 t->sec = sTime.Seconds;
	//sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	//sTime.StoreOperation = RTC_STOREOPERATION_RESET;

	 t->mday = sDate.Date;
	 t->mon = sDate.Month;
	 t->year  = sDate.Year +2000;
	 t->wday = sDate.WeekDay;

}
#endif
void rtc_get_tm(struct tm *t)
{
	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate; // 1 is Mon, 7 is Sun, 0 is forbidden
	HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BIN); // need to do this, even if not used

	 t->tm_hour = sTime.Hours;
	 t->tm_min = sTime.Minutes;
	 t->tm_sec = sTime.Seconds;
	//sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	//sTime.StoreOperation = RTC_STOREOPERATION_RESET;

	 //if((sDate.Year < 2000) || (sDate.Year > 2090))	 Error_Handler();

	 t->tm_mday = sDate.Date;
	 t->tm_mon = sDate.Month -1;
	 if(sDate.Year > 99)	 Error_Handler();
	 t->tm_year  = sDate.Year+100; // convert from 0..99 to yrs since 1900
	 t->tm_isdst = -1; // we don't know if it's DST because we use UTC
	 int wday = sDate.WeekDay;
	 if(wday ==7) wday = 0; // RTC uses 7 for Sunday, we use 0
	 t->tm_wday = wday;

}
int rtc_get_uk_tm(struct tm *t)
{
	rtc_get_tm(t);
	return cal_adjbst_tm(t);
}

#if 0
void rtc_display_clock()
{
	struct tm t;
	rtc_get_tm_uk(&t);
	zseg_show_hhmmss_tm(&t);
	blinkt_show();
	//zseg_show_hhmmss(sTime.Hours, sTime.Minutes, sTime.Seconds);
}

void rtc_display_date()
{
	struct tm t;
	rtc_get_tm_uk(&t);
	zseg_show_date(&t);
	blinkt_show();
}
#endif

void rtc_set_tm(struct tm *t)
{
	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;
	sTime.Hours = t->tm_hour;
	sTime.Minutes = t->tm_min;
	sTime.Seconds = t->tm_sec;
	sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	sTime.StoreOperation = RTC_STOREOPERATION_RESET;
	//sTime.TimeFormat = RTC_HourFormat_24;
	if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) != HAL_OK) {
		//_Error_Handler(__FILE__, __LINE__);
		Error_Handler();
	}
	sDate.Date = t->tm_mday;
	sDate.Month = t->tm_mon;
	sDate.Year = t->tm_year % 100; // sDate.Year must be in range 0 .. 99
	sDate.WeekDay = t->tm_wday;
	if(sDate.WeekDay == 0) sDate.WeekDay = 7; // Sun is 1, not 0
	if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN) != HAL_OK) {
		//_Error_Handler(__FILE__, __LINE__);
		Error_Handler();
	}
}

#if 0
[[deprecated]] // use rtc_set_tm instead
void rtc_set(struct ts *t)
{
	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;
	sTime.Hours = t->hour;
	sTime.Minutes = t->min;
	sTime.Seconds = t->sec;
	sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	sTime.StoreOperation = RTC_STOREOPERATION_RESET;
	//sTime.TimeFormat = RTC_HourFormat_24;
	if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) != HAL_OK) {
		//_Error_Handler(__FILE__, __LINE__);
		Error_Handler();
	}
	sDate.Date = t->mday;
	sDate.Month = t->mon;
	sDate.Year = t->year % 100+2000;
	sDate.WeekDay = t->wday;
	if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN) != HAL_OK) {
		//_Error_Handler(__FILE__, __LINE__);
		Error_Handler();
	}
}
#endif

void rtc_set_from_ds3231()
{
	//return;
	struct tm t;
	DS3231_get_uk_tm(&t);
	rtc_set_tm(&t);
}

#endif //HAL_RTC_MODULE_ENABLED
