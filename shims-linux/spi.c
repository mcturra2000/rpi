#include <assert.h>
//#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
//#include <string.h>
//#include <stdio.h>

#include <linux/spi/spidev.h>
#include <sys/ioctl.h>


#include "spi.h"

#define CHIO(err) assert(err != -1);
void spi0_0_init(spi_t *spi)
{
	int fd = open("/dev/spidev0.0", O_RDWR);
	assert(fd>=0);
	spi->fd = fd;
	int err;
	int mode = SPI_MODE_0;
	err = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	CHIO(err);
	int bpw = 8;
	// probably most of this extra suff is unnecessary
	err = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bpw);
	CHIO(err);
	//#define SPI_SPEED_HZ  1000000U
#define SPI_SPEED_HZ  100000U // I don't think this actually works though
	int speed = SPI_SPEED_HZ;
	err = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	CHIO(err);
	err = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	CHIO(err);
}

void spi_deinit(spi_t *spi)
{
	close(spi->fd);
}
