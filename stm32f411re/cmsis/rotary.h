#pragma once
#include <basal.h>
#include <gpio.h>

typedef struct {
	GPIO_TypeDef *port; // assumes both on same port
       	uint32_t pin_minus;
	uint32_t pin_plus;
	//int count;
	int state;
} rotary_t;


void rotary_init(rotary_t* r, GPIO_TypeDef *port, uint32_t pin_minus, uint32_t pin_plus);
int rotary_poll(rotary_t* r);
