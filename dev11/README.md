# ATtiny ISP

dev11.ino is a blink sketch to test the ISP. Note that the ATtiny85 blink 
doesn't work properly. It does seem to upload properly, though.

## Schematic

![](dev11.jpg)

## See also

* db7.82
