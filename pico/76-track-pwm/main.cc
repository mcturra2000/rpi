#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
//#include "hardware/adc.h"
//#include "hardware/clocks.h"
#include "hardware/gpio.h"
#include "hardware/irq.h"
#include "hardware/pwm.h"
//#include "hardware/spi.h"
// #include "tusb.h" // if you want to use tud_cdc_connected()
#include "math.h"

#include "pace.h"
#include "pi.h"
#include "../../audio/track.h"

using i32 = int32_t;

const int rep = 3; // to kill the 8kHz hiss
const auto f_s = rep * 8'000; // sampling frequency; our carrier wave
const auto TOP = 125'000'000/f_s-1;
const int c1 = TOP/256;
static_assert(f_s *(TOP+1) < 125'000'000); // mustn't exceed clock hz

unsigned int slice_num;

#define SPK 19

volatile int idx = 0;

void __not_in_flash_func(pwm_isr)(void)
{

	pwm_clear_irq(slice_num); // reset the irq so that it will fire in future

	int y = c1 * track_raw[idx/rep];
	idx++;
	if(idx>= track_raw_len*rep) idx = 0;
	pwm_set_gpio_level(SPK, y);
}

int main() 
{
	int err = pace_config_pwm_irq(&slice_num, SPK, f_s, TOP, pwm_isr);

	// test how much output amperage we're getting
	const int amp = 2;
	pi_gpio_out(2);
	pi_gpio_high(2);

	for(;;);
}

