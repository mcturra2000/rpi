#include "../rp2040-cmsis-1.h"

#define RESETS_IO_BANK0		(1ul<<5)
#define RESETS_PAD_BANK0	(1ul<<8)

#define GPIO_FUNC_SIO	5

#define LED 25

// no particular timing
void delay(int n) 
{
	for(int i =0 ; i< n; i++) {
		for(int j = 0; j< 1000; j++) {
			asm volatile ("nop");
		}
	}
}

void __attribute__ ((naked)) __attribute__ ((section(".boot2.main"))) main()
{
	REG(RESETS_BASE + 0x00 + 0x3000) = RESETS_IO_BANK0; // 2.14.3 Reset register using atomic clear 0x3000
	while(!(RESETS->RESET_DONE & RESETS_IO_BANK0)); // are we there yet?

	//PADS_BANK0_GPIO25 &= ~(1<<7); // clear output disable
	//PADS_BANK0_GPIO25 &= ~(1<<6); // clear input enable

	IO_BANK0->GPIO25_CTRL = GPIO_FUNC_SIO; // init pin - select function SIO
	SIO->GPIO_OE_SET = 1ul << LED; // allow setting of output

	while(1) {
		SIO->GPIO_OUT_SET = (1ul << LED); // turn on gpio atomically
		delay(100);
		SIO->GPIO_OUT_CLR = (1ul << LED); // turn off gpio atomically
		delay(1000);
	}
}

