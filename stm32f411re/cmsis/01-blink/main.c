#include "basal.h"
#include "delay.h"
#include "gpio.h"

#define LED GPIOA, 5 // LED for nucleo board

int main (void) 
{   
	gpio_out(LED);
	while (1) 
	{
		gpio_set(LED);
		delayish(100);
		gpio_clr(LED);
		delayish(900);
	}
}
