#pragma once

#include "fonts.h"
#include "spi.h"

void st7567_init(spi_t *spi);
void st7567_set_contrast(uint8_t value);
void st7567_show();
void st7567_set_pixel(int x, int y, int value);
void st7567_set_col(int page, int c, int value);
void st7567_putchar(char c);
void st7567_printstr(char* str);
