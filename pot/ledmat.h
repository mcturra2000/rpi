#pragma once

#include <inttypes.h>


#define LEDMAT_SID 0x70

void ledmat_set(uint8_t row, uint8_t col, int on);
void ledmat_init(uint32_t i2c_port);
//uint8_t* ledmat_init(int *len);
 void ledmat_show(void);
//uint8_t*  ledmat_grid(int *len); 
void led_set_row(uint8_t row, uint8_t value);
void ledmat_toggle(uint8_t row, uint8_t col);
void ledmat_copy_rows(uint8_t *rows);
void ledmat_copy_cols(uint8_t *cols);
void ledmat_clear(void);
void ledmat_set_col(uint8_t col, uint8_t value);
