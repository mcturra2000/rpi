# fit

Counter for fitness

## Notes

* 3V supply will last at least 17.25hrs, conservatively.


## Status

2023-05-20	Added ds3231. Works

2023-05-13	Introduced scaling and temperature. Works

2022-01-06	Started. Works

