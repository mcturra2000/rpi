#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"

#include "pi.h"
#include "debounce.h"
#include "rotary.h"

#define SMNS 	21 // minus
#define SPLS 	20 // plus
#define SW	19 // pushbutton

#define LED  25 // GPIO of built-in LED

int main() 
{
	stdio_init_all();
	// while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 
	puts("Rotary encoder test");

	//Rotary rot(SIA, SIB, SW);
	debounce_t deb;
	debounce_init(&deb, SW, 4);
	rotary_t rot;
	rotary_init(&rot, SMNS, SPLS);
	pi_gpio_init(LED, OUTPUT);

	//Pulse pulse(500);
	int count = 0;
	//int count_num = 0;
	int idx = 0;
	while(1) {
		if(debounce_falling(&deb)) 
			printf("%d: rotary button pushed\n", idx++);
		if(debounce_rising(&deb)) 
			printf("%d: rotary button released\n", idx++);
		//if(pulse.expired())
		//	pi_gpio_toggle(LED);
		int chg;
		if(chg = rotary_change(&rot)) {
			count += chg;
			printf("%d: count  = %d\n", idx++, count);
		}
	}
}

