#include <stdio.h>
#include <unistd.h>
//#include <gpiod.h>
#include <lgpio.h>
#include <inttypes.h>
#include <assert.h>
#include <errno.h>
#include <error.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "ssd1306.h"

//static_assert(sizeof(uint32_t) == sizeof(int));

uint32_t i2c;

void i2c_transfer7(uint32_t i2c, uint8_t addr, const uint8_t *w, size_t wn, 
		uint8_t *r, size_t rn)
{
	assert(addr = 0x3C);
	if(wn == 0) return;
	clock_t start = clock();
	uint8_t  reg = *w;
	//printf("Reg=%d\n", reg);
	w++;
	wn --;
	while(wn > 0) {
		//if(wn == 0) return;
		//printf(".");
		int size = 32;
		if(wn < size) size = wn;
		int status = lgI2cWriteI2CBlockData(i2c, reg, w, size);
		assert(status>=0);

		w += size;
		wn -= size;
	}
}

int main()
{
	assert(sizeof(int) == sizeof(int));
	int h = lgGpiochipOpen(0);
	assert(h>=0);
	i2c = lgI2cOpen(1, 0x3C, 0);
	init_display(32, i2c);

	for(int i = 0; i<100; i++) {
		char msg[100];
		sprintf(msg, "Count =%d", i);
		ssd1306_puts(msg);
		show_scr();
	}

	lgI2cClose(i2c);
	lgGpiochipClose(0);

#if 0
	return 0;
	struct gpiod_chip *chip = gpiod_chip_open_by_name("gpiochip0");
	assert(chip);
	assert(i2c>=0);

	gpiod_chip_close(chip);
#endif
	return 0;
}
