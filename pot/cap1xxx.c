
#include "cap1xxx.h"

// you must supply these
extern int i2c_grab1_at(uint8_t sid, uint8_t reg, uint8_t *result);
extern void i2c_ship_val_at(uint32_t sid, uint8_t addr, const uint8_t val);


static uint8_t readRegister(uint8_t reg);
static void writeRegister(uint8_t reg, uint8_t value) ;

int cap_sid = -1;

bool cap_begin(uint8_t sid)
{
	cap_sid = sid;

	//printf("PRODID 0x%x (s/b 0x50)\n", readRegister(CAP1188_PRODID));
	//printf("MANUID 0x%x (s/b 0x5d)\n", readRegister(CAP1188_MANUID));

#if 0
	// Useful debugging info

	Serial.print("Product ID: 0x");
	Serial.println(readRegister(CAP1188_PRODID), HEX);
	Serial.print("Manuf. ID: 0x");
	Serial.println(readRegister(CAP1188_MANUID), HEX);
	Serial.print("Revision: 0x");
	Serial.println(readRegister(CAP1188_REV), HEX);

	if ((readRegister(CAP1188_PRODID) != 0x50) ||
			(readRegister(CAP1188_MANUID) != 0x5D) ||
			(readRegister(CAP1188_REV) != 0x83)) {
		return false;
	}
#endif
	writeRegister(0x2a, 0); // allow multiple touches
	writeRegister(0x72, 0xFF * 0 ); // Disable LEDs following touches (the mapping is usually wrong)
	writeRegister(0x41, 0x30); // speed up a bit
	return true;
}

static uint8_t readRegister(uint8_t reg) 
{
	uint8_t result;
	i2c_grab1_at(cap_sid, reg, &result);
	return result;
}
static void writeRegister(uint8_t reg, uint8_t value) 
{
	i2c_ship_val_at(cap_sid, reg, value);
}
/*!
 *   @brief  Reads the touched status (CAP1188_SENINPUTSTATUS)
 *   @return Returns read from CAP1188_SENINPUTSTATUS where 1 is touched, 0 not
 * touched.
 */
uint8_t cap_touched() 
{
	uint8_t t = readRegister(0x03); // bits for which sensors have been input
	if (t) {
		// clear the interrupt flag in the main control register (0x00)
		writeRegister(0x00, readRegister(0x00) & ~CAP1188_MAIN_INT);
	}
	return t;
}

/*!
 *   @brief  Controls the output polarity of LEDs.
 *   @param  inverted
 *           0 (default) - The LED8 output is inverted.
 *           1 - The LED8 output is non-inverted.
 */
void cap_LEDpolarity(uint8_t inverted) {
	writeRegister(CAP1188_LEDPOL, inverted);
}




