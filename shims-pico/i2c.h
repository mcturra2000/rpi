#pragma once 
#include <stdint.h>
#include <stddef.h>

#include <hardware/i2c.h>

#ifdef __cplusplus
extern "C" {
#endif


typedef i2c_inst_t i2c_t;
typedef i2c_t I2C_HandleTypeDef;
	
void i2c_send_bytes(i2c_t *i2c, uint8_t sid, const uint8_t *data, size_t len);
void i2c_recv_bytes(i2c_t *i2c, uint8_t sid, uint8_t *data, size_t len);
void i2c_send_bytes_at(i2c_t* i2c, int sid, uint8_t reg, const uint8_t * values, int size);
void i2c_send_byte_at(i2c_t* i2c, int _address, int reg, uint8_t value);
void i2c_recv_bytes_at(i2c_t* i2c, int sid, uint8_t reg , uint8_t* values, int size);
uint8_t i2c_recv_byte_at(i2c_t* i2c, int sid, uint8_t reg );

#ifdef __cplusplus
}
#endif
