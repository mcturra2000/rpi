#include "common.h"


typedef struct {
	char *name;
	int cond; // condition code
	jtype_e jtype;
} branch_matcher_t;

/* Conditional exceution obtainable from A6.3 of Arm ref manual, p93
*/
static const branch_matcher_t branches[] = {
	{"b", 	0, J_11},
	{"beq",	0b000, J_8},
	{"bne", 0b0001, J_8},
	{"bcs", 0b0010, J_8},
	{"bcc", 0b0011, J_8},
	{"bpl", 0b0101, J_8},
	{"bvs", 0b0110, J_8},
	{"bvc", 0b0111, J_8},
	{"bhi", 0b1000, J_8},
	{"bls", 0b1001, J_8},
	{"bge", 0b1010, J_8},
	{"blt", 0b1011, J_8},
	{"bgt", 0b1100, J_8},
	{"ble", 0b1101, J_8},
	0};


/* FN emit_any */
void emit_any(uint32_t code, char* opname, jtype_e jtype, int size)
{
	expect(T_ID, "Expected label");
	str_t token= {yystart, yylen};
	char *fwd = " FWD"; // signifies it's a fwd branch
	label_t *lab = find_label(&token);
	if(lab) {
		// backward branch
		fwd = "";
		code |= rel_offset(address, lab->address, jtype);
	} else {
		// must be foward branch
		add_hole(yystart, yylen, address, jtype); // add a forward hole
	}

	char name[10];
	sprintf(name, "%s%s", opname, fwd);
	if(size == 2) emit_opcode(name, code>>16);
	emit_opcode(name, code);
}

/* FN parse_branch */
void parse_branch (branch_matcher_t *k)
{
	jtype_e jtype = k->jtype;
	uint32_t code = (jtype == J_8) ? (0b1101 << 12) : (0b111<<13);
	code |= ((k->cond) <<8); // for blt, bge, etc.
	emit_any(code, k->name, jtype, 1);
}


/* FN match_branch */
bool match_branch ()
{
	if(yyfind("bl")) { // BL is something of s special snowflask
		puts("found bl");
		uint32_t code = (0b1111<<(12+16) ) | (3<<14) | (1<<12); // A6.7.13. Crazy stuff, esp in rel_offset
		emit_any(code, "bl", J_BL, 2);
		return true;
	}

	for(branch_matcher_t *k = (branch_matcher_t*)branches ;  k->name != 0 ; k ++) {
		if(yyfind(k->name)) {
			parse_branch(k);
			return true;
		}
	}
	return false;
}
