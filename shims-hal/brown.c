/*
 * brown.c
 *
 *	CubeIDE:
 *		Timers: TIM3 :
 *			Mode and configuration:
 *				Internal clock : check
 *			 	Channel 2: PWM Generation CH2
 *
 *
 *			Parameter Settings: **IGNORE THIS 2024-05-02**
 *				Counter settings:
 *					Counter Period: 3000-1 [1]
 *			 		Auto reload preload: disable (not important)
 *			 	PWM Generation Channel 2:
 *			 		Pulse: 3000-1 [1]
 *			 		Output compare preload: enable (not sure if this is necessary)
 *
 *
 *			NVIC Settings:
 *				TIM3 global interrupt: enable
 *				Set preemption priority to 8
 *
 *		Output will be on PA7
 *
 *	Notes:
 *	[1] Assume clock 60MHz, sampling frequency 20kHz. Then 60e6/20e3 = 3000
 *
 *	See also hal7a1.c
 *
 *  Created on: Dec 23, 2023
 *      Author: pi
 *
 *
 */
#include "brown.h"

#ifdef HAL_TIM_MODULE_ENABLED

#include <stdlib.h>
//#include "main.h"
#include <tim.h>

extern TIM_HandleTypeDef htim3;
void Error_Handler(void);

uint32_t brown_pwm_took = 0;

static const double fs = 20000; // sample frequency
//static const int vol_scale = 1;
static int arr;




static void set_compare(int val)
{
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, val);
	//(&htim3)->Instance->EGR  |= TIM_EGR_UG; // update the registers (required?)
}


/* Takes about 8us
 * 15kHz ~ 67us
 * 20kHz ~ 50us
 */
void HAL_TIM_PWM_PulseFinishedCallback(TIM_HandleTypeDef *htim)
//void TIM3_IRQHandler_xxx(void)
{
	if(htim != &htim3) return;
	//Error_Handler();
	//return;
	//param_assert(0);
	//Error_Handler();
	const double fc = 400; // cutoff frequency
	const double K = 2.0 * 3.14159265 * fc / fs;
	static double vc = 0; // voltage across cap

	//uint32_t start = usecs();
	int va = 0;
	if(rand() % 2 == 0) va = arr;
	vc = vc + K * ((double)va - vc); // derived from db05.296
	set_compare(vc);
	//__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, val);

	//brown_pwm_took = usecs() - start;

}
void brown_init()
{
	//tim_set_frequency(&htim3, 60000, fs);
	extern uint32_t SystemCoreClock;
	int period = SystemCoreClock/fs-1;
	__HAL_TIM_SET_AUTORELOAD(&htim3, period); // the "Period"
	arr = __HAL_TIM_GET_AUTORELOAD(&htim3)*1/8; // scale it a bit - 8
	set_compare(arr);
	//HAL_NVIC_EnableIRQ(TIM3_IRQn);
	//HAL_NVIC_SetPriority(TIM3_IRQn, 8, 0); // mustn't interfere with USB!
	if(HAL_TIM_PWM_Start_IT(&htim3, TIM_CHANNEL_2) != HAL_OK) {
		Error_Handler();
	}
	brown_pwm_took = 0;

}

#endif //HAL_TIM_MODULE_ENABLED
