# 2023-07-03	Fixed timers suspended for debugging
#		(Removed "init" from "monitor reset init")
# 2023-07-01 	snapshot
cd build

# quit without confirmation
define hook-quit
    set confirm off 
end

set pagination off
file app.elf
#target remote localhost:3333
target extended-remote localhost:3333
load
#b main
#b main.c:30
monitor reset 
echo RUNNING...\n
c
