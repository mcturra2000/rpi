# bare metal blinky with separate programmability

## Patching prog.bin

Put the Pico into BOOTSEL mode. Then run "overlay"


## Misc notes

`nm` can be used to list symbols in object files


## Links to other sites


* [Reliable way to embed data into a binary so it can be patched?](https://forums.raspberrypi.com/viewtopic.php?p=2070044#p2070044)
* [Towards “programs” for an #RP2040 “kernel”](https://medium.com/@oblate_24476/towards-programs-for-an-rp2040-kernel-746d6acede5c)


## Status

2023-01-12	Works

2023-01-08	Started.
