#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
//#include "hardware/adc.h"
//#include "hardware/clocks.h"
//#include "hardware/flash.h"
#include "hardware/gpio.h"
//#include "hardware/irq.h"
//#include "hardware/pwm.h"
//#include "hardware/spi.h"
#if LIB_PICO_STDIO_USB
#pragma message "USB configured as stdio"
#include "tusb.h" // if you want to use tud_cdc_connected()
#endif

//#include <functional>

#include "pi.h"

#define BTN  14 // GPIO number, not physical pin
#define LED  25 // GPIO of built-in LED

void led(int on)
{
	if(on)
		gpio_put(LED, 1);
	else
		gpio_put(LED, 0);
}

volatile void (*fn_ptr)(int) = &led;

void test1(int v)
{
	(*fn_ptr)(v);
}


int main() 
{
	stdio_init_all();
#if LIB_PICO_STDIO_USB
        while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 
#endif


	gpio_init(BTN);
	gpio_set_dir(BTN, GPIO_IN);
	gpio_pull_up(BTN);
	// gpio_get() gets state of pin

	gpio_init(LED);
	gpio_set_dir(LED, GPIO_OUT);

	//test1();
	//for(;;);
	int i = 0;
	for(;;) {
		//printf("Hello number %d\n", i++);
		//gpio_put(LED, 1);
		test1(1);
		sleep_ms(100);
		test1(0);
		//gpio_put(LED, 0);
		sleep_ms(1000);		
	}

	return 0;
}

