/*
 * usecs.h
 *
 *  Created on: Feb 29, 2024
 *      Author: pi
 */

#ifndef INC_USECS_H_
#define INC_USECS_H_

#include "main.h"

#ifdef __cplusplus
extern "C" {
#endif

#define TIM TIM5

void usecs_init();
//extern uint16_t usecs();
//extern void usecs_zero();
void usecs_test();

/*
extern inline void usecs_zero() __attribute__((always_inline));

inline void usecs_zero()
{
	TIM->CNT = 0;
}
*/
#define USECS_ZERO() TIM->CNT = 0
#define USECS() TIM->CNT

#ifdef __cplusplus
}
#endif

#endif /* INC_USECS_H_ */
