# serial

A program for the PC rather than the Pico. 
It echos output from the Pico. 
Useful if you want to programmatically interact with the Pico

## Status

2022-10-04	Re-arranged project. Confirmed working. Uses USB interface.

2021-07-21	Started. Working.
