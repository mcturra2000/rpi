#include <stdint.h>
typedef uint32_t u32;
#include "registers.h"
#include "kernel.h" // may need to "make kernel.h"

#define LED 25

/*
#define gpio_set_loc (0x100002d4 +1)
#define gpio_clr_loc (0x100002f4+1)
#define delay_loc (0x10000294+1)
*/


#define call1(f, x) ((void (*)(u32)) f)(x)

void gpio_set(u32 pin);
void delay(int n);
void gpio_clr(u32 pin);

void myprog() // completely internal
{
	for(int i = 0; i<5; i++) {
		gpio_set(LED);
		delay(100);
		gpio_clr(LED);
		delay(1000);
	}
}

void myprog1() // using external functions, say in a kernel
{
	for(int i = 0; i<5; i++) {
		call1(gpio_set_loc, LED);
		call1(delay_loc, 100);
		call1(gpio_clr_loc, LED);
		call1(delay_loc, 1000);
	}
}

void delay(int n) // no particular timing
{
	for(int i =0 ; i< n; i++) {
		for(int j = 0; j< 1000; j++) {
			asm volatile ("nop");
		}
	}
}



void gpio_set(u32 pin)
{	
	SIO_GPIO_OUT_SET = 1ul << pin;
}

void gpio_clr(u32 pin)
{
	SIO_GPIO_OUT_CLR = 1ul << pin; // turn off the LED

}

