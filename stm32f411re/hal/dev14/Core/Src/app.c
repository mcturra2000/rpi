/*
 * app.c
 *
 *  Created on: Feb 27, 2024
 *      Author: pi
 *
 * 2024-11-14 BTNb might be brokwn on F401
 */

#include "main.h"
#include "usb.h"



void do_display()
{
	struct tm t;
	DS3231_get_uk_tm(&t);

	if (om_running()) {
		zseg_show_elapsed_and_hhmm(om_secs(), &t);
	}
	else
	{
		//zseg_show_hhmmss_tm(&t);
		zseg_show_date(&t);

		// day of week
		zseg_set_at(5, 1 << t.tm_wday);

	}

}



void app_ds3231()
{
	deb_t deby;
#ifdef STM32F411xE
#pragma message "Configuring for STM32F411"
	deb_init(&deby, BTNb);
	zseg_invert(1);
#else
	deb_init(&deby, BTNy);
#endif
	om_init(1);
	//zseg_test_loop();
	for (;;)
	{
		DS3231_exc_handle_usb();
		om_update();

		static every_t ev_1sec = {1000};
		if (every(&ev_1sec))
		{
			do_display();
		}

		if (deb_falling(&deby))
		{
			HAL_GPIO_TogglePin(LEDy);
			om_toggle();
			do_display();
		}
}
}

void app (void)
{

	spi_t spi = {&hspi1, GPIOA, GPIO_PIN_15};
	zseg_init(&spi);
	DS3231_init(&hi2c1);
	app_ds3231();
}
