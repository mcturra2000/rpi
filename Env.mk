# This should mostly work for all types of builds
RPI ?= $(HOME)/repos/rpi
POT = $(RPI)/pot
RLIB = $(RPI)/rpi # for headers and src files for Linux executables

PREFIX = arm-none-eabi-
CC = $(PREFIX)gcc
AS = $(PREFIX)gcc -x assembler-with-cpp
CP = $(PREFIX)objcopy
SZ = $(PREFIX)size

TARGET ?= app
BUILD_DIR ?= .

VPATH += $(RPI)/shims-hal 
VPATH +=  $(RPI)/shims-hal/pot

C_SOURCES +=  $(wildcard Core/Src/*.c)
C_SOURCES += $(wildcard Drivers/STM32F4xx_HAL_Driver/Src/*.c)
C_SOURCES += delay.c gpio.c # we're going to need these all the time

# ASM sources , mcarter altered, because they can be slightly different
ASM_SOURCES =  $(wildcard Core/Startup/startup*.s)
ASMM_SOURCES = 

CPU = -mcpu=cortex-m4
FPU = -mfpu=fpv4-sp-d16
FLOAT-ABI = -mfloat-abi=hard
MCU = $(CPU) -mthumb $(FPU) $(FLOAT-ABI)

.DEFAULT_GOAL := all

#######################################
# build the application
#######################################
# list of objects
OBJECTS = $(addprefix $(BUILD_DIR)/,$(notdir $(C_SOURCES:.c=.o)))
vpath %.c $(sort $(dir $(C_SOURCES)))
# list of ASM program objects
OBJECTS += $(addprefix $(BUILD_DIR)/,$(notdir $(ASM_SOURCES:.s=.o)))
vpath %.s $(sort $(dir $(ASM_SOURCES)))
OBJECTS += $(addprefix $(BUILD_DIR)/,$(notdir $(ASMM_SOURCES:.S=.o)))
vpath %.S $(sort $(dir $(ASMM_SOURCES)))

# default action: build all
all: $(BUILD_DIR)/$(TARGET).elf $(BUILD_DIR)/$(TARGET).hex $(BUILD_DIR)/$(TARGET).bin

$(BUILD_DIR)/%.o: %.c Makefile | $(BUILD_DIR) 
	$(CC) -c $(CFLAGS) -Wa,-a,-ad,-alms=$(BUILD_DIR)/$(notdir $(<:.c=.lst)) $< -o $@

$(BUILD_DIR)/%.o: %.s Makefile | $(BUILD_DIR)
	$(AS) -c $(CFLAGS) $< -o $@
$(BUILD_DIR)/%.o: %.S Makefile | $(BUILD_DIR)
	$(AS) -c $(CFLAGS) $< -o $@

$(BUILD_DIR)/$(TARGET).elf: $(OBJECTS) Makefile
	$(CC) $(OBJECTS) $(LDFLAGS) -o $@
	$(SZ) $@

$(BUILD_DIR)/%.hex: $(BUILD_DIR)/%.elf | $(BUILD_DIR)
	$(HEX) $< $@
	
$(BUILD_DIR)/%.bin: $(BUILD_DIR)/%.elf | $(BUILD_DIR)
	$(BIN) $< $@	
	
$(BUILD_DIR):
	mkdir $@	
	

clean:
	-rm -fR $(BUILD_DIR)

.flash : $(BUILD_DIR)/$(TARGET).bin
	touch .flash
	
flash : .flash
	st-flash --connect-under-reset write  $(BUILD_DIR)/$(TARGET).bin 0x8000000	

# echo out all this stuff
echo :
	@echo "POT=$(POT)"
	@echo "PWD=$(PWD)"
	@echo "RPI=$(RPI)"
