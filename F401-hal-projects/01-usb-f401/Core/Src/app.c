/*
 * app.c
 *
 *  Created on: Feb 27, 2024
 *      Author: pi
 *
 * 2024-11-14 BTNb might be brokwn on F401
 */

#include <usb.h>

#include "main.h"



// FN app 
void app(void)
{
	while(1) {
		if(usb_linebuff_is_ready()) {
			printf("You said: '%s'\r\n", usb_linebuff);
			usb_linebuff_consume();
		}
	}
}
