[f]LEDs:[/f]
LD1 - communication 
LD2 - PA5 - user-programmer LED
LD3 - power indicator

[f]Serial debug[/f] PA13
PC13 - [f]user button[/f]

[f]USART2[/f] for onboard. TX is PA2, RX is PA3 (not that that should matter much)

[f]PINOUT[/f]
pa0	cn7:28
pa1	cn7:30
pa2	cn10:35
pa3	cn10:37
pa4	cn7:32
pa5	cn10:11
pa6	cn10:13
pa7	cn10:15
pa8	cn10:23
pa9	cn10:21
pa10	cn10:33
pa11	cn10:14
pa12	cn10:12
pa13	cn7:13
pa14	cn7:15
pa15	cn7:17

pb0	cn7:34
pb1	cn10:24
pb3	cn10:31
pb4	cn10:27
pb5	cn10:29
pb6	cn10:17
pb7	cn7:21
pb8	cn10:3
pb9	cn10:5
pb10	cn10:25
pb12	cn10:16
pb13	cn10:30
pb14	cn10:28
pb15	cn10:26

pc0	cn7:38
pc1	cn7:36
pc2	cn7:35
pc3	cn7:37
pc4	cn10:34
pc5	cn10:6
pc6	cn10:4
pc7	cn10:19
pc8	cn10:2
pc9	cn10:1
pc10	cn7:1
pc11	cn7:2
pc12	cn7:3
pc13	cn7:23
pc14	cn7:25
pc15	cn7:27

pd2	cn7:4

