#include <unistd.h>
#include "sn3218.h"

#include "i2c.h"

#ifdef RPI // only currently compiles nicely on Raspberry Pi

#define SN3218 0x54

typedef uint8_t u8;
typedef uint32_t u32;
#define SN3218_NUM_CHANNELS 18
#define SN3218_CMD_SET_PWM_VALUES 0x01

void sn3218_update();

void sn3218_ship(uint8_t addr, const uint8_t *w, size_t wn)
{
	i2c_ship_at(SN3218, addr, w, wn);
}
void sn3218_ship_val(uint8_t addr, const uint8_t val)
{
	i2c_ship_val_at(SN3218, addr, val);
}

// start device, enable everything
void sn3218_init_all()
{
	sn3218_ship_val(0x17, 0); // reset
	sn3218_ship_val(0x00, 1); // enable

	// enable all LEDS
	u32 enable_mask = 0b111111111111111111;
	enable_mask = 0x3ffff;
	u8 msk[3] = {enable_mask & 0x3f, (enable_mask>>6) & 0x3f, (enable_mask>>12) & 0x3f};
	sn3218_ship(0x13, msk, 3);
}

void sn3218_update()
{
	sn3218_ship_val(0x16, 0x00); 
}


// Set brightness for all leds and update the display
void sn3218_all_brightnesses(uint8_t brightness)
{
	for(int channel = 0; channel < SN3218_NUM_CHANNELS; channel++ ){
		sn3218_ship_val( SN3218_CMD_SET_PWM_VALUES + channel, brightness);
	}
	sn3218_update();

	/*
#define VAL 0xff
	// set pwms
	u8 vals[18] = { VAL, VAL, VAL, VAL, VAL, VAL, VAL, VAL, VAL, VAL, VAL, VAL, VAL, VAL, VAL, VAL, VAL, VAL};
	sn3218_ship(0x01, vals, 18); // set pwm values
	sn3218_update();
	*/
}

#endif
