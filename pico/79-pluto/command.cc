
#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>


#include "mode0.h"
#include "hardware/flash.h"

#include "pluto.h"
#include "exports.h"
#include "circular.h"

void pchar(char c) { mode0_putc(c); }
//void println(const char* str) { print(str); pchar('\n');}

bool int_str(const char* str, int len, int *val);

//////////////////////////////////////////////////////////////////////////

#define MAX_INPUT_LENGTH 132
#define MAX_OUTPUT_LENGTH 132


static struct circle<MAX_INPUT_LENGTH, int8_t> kbd_buf;
//
// over-rides usb-kbd/hig_app.c:process_char()
extern "C" void  process_char (char ch) 
{
	kbd_buf.write(ch);
}

static char pcInputString[ MAX_INPUT_LENGTH ];

void read_command ()
{
	signed char c;
	int cInputIndex = 0;
	memset(pcInputString, 0x00, MAX_INPUT_LENGTH);
	mode0_print("> "); // command prompt
	while(1) {
		do {
			vTaskDelay(10);
		} while(!kbd_buf.avail());
		
		vTaskSuspend(peri_hnd);
		kbd_buf.read(&c);
		vTaskResume(peri_hnd);

		if(c=='\r') c = '\n';
		if(c == '\n') break;
		if(cInputIndex + 1 == MAX_INPUT_LENGTH) continue;
		pcInputString[cInputIndex++] = c;
		printchar(c);
	}
	println("");
}

//////////////////////////////////////////////////////////////////////////
static BaseType_t cmd_call (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	BaseType_t len, xReturn;
	//println("cmd_del called");
	const char *arg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &len);
	configASSERT(len);

	int addr;
       	if(!int_str(arg, len, &addr)) return pdFALSE;


	//int r0; // return code
	int actual_addr = addr;
	addr |= 1; // ensure we're using thumb address, which requires a 1 in LSB
	//int ret = ((int (*)(uint32_t*, uint32_t))addr) ((uint32_t*)&exports, actual_addr); // call program
	int ret = ((int (*)(uint32_t*, uint32_t))addr) (0, 0); // call program
	print("Return code: %d\n", ret);

	return pdFALSE; // no more data
}

/* FN parse_cls */
static BaseType_t cmd_cls (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	mode0_clear(MODE0_BLACK);
	return pdFALSE; // no more data
}

/* FN cmd_del 
 * erase a sector
 * E.g. 0xf000 will erase a sector at 0x1000_f000
 * */
static BaseType_t cmd_del (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	BaseType_t len, xReturn;
	//println("cmd_del called");
	const char *arg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &len);
	configASSERT(len);

	int addr;
       	if(!int_str(arg, len, &addr)) return pdFALSE;
	
	addr = (addr/4096)*4096; // -0x10000000;

	if(addr + 4096 > PICO_FLASH_SIZE_BYTES) {
		print("Can't erase outside flash size");
		return pdFALSE;
	}


	print("addr = 0x%x\n", addr);

	print("Beginning erase:");
	uint32_t irqs = save_and_disable_interrupts();
	flash_range_erase(addr, 4096);
	restore_interrupts(irqs);

	void *empty = malloc(FLASH_PAGE_SIZE);
	configASSERT(empty);
	memset(empty, 0 , FLASH_PAGE_SIZE);
	for(int i = 0; i < FLASH_SECTOR_SIZE/FLASH_PAGE_SIZE; i++) {
		printchar('.');

		uint32_t irqs = save_and_disable_interrupts();
		flash_range_program(addr, (const uint8_t*) empty, FLASH_PAGE_SIZE);
		restore_interrupts(irqs);

		addr += FLASH_PAGE_SIZE;
	}
	free(empty);
	print("\nDone\n");
	return pdFALSE;
}



static BaseType_t cmd_dir (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	//extern void sd_list_root();
	sd_list_root();
	return pdFALSE;
}


static BaseType_t cmd_run (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	BaseType_t len, xReturn;
	//println("cmd_del called");
	//const char *arg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &len);
	//configASSERT(len);

	//int addr;
       	//if(!int_str(arg, len, &addr)) return pdFALSE;


	//int r0; // return code
	int addr = PROGBASE;
	int actual_addr = addr;
	addr |= 1; // ensure we're using thumb address, which requires a 1 in LSB
	//int ret = ((int (*)(uint32_t*, uint32_t))addr) ((uint32_t*)&exports, actual_addr); // call program
	int ret = ((int (*)())addr) (); // call program
	print("Return code: %d\n", ret);

	return pdFALSE; // no more data
}

typedef void (*fptr)(void); // takes no args, returns void



/* FN print_dump */
void print_dump (int addr)
{
	char hex[9];
	for(int row = 0 ; row < 22; row ++) {
		sprintf(hex, "%08x ", addr);
		print(hex);
		for(int i = 0; i<8; i++) {
			sprintf(hex, "%02x ", *(unsigned char*) addr);
			print(hex);
			if(i == 3 || i == 7 || i == 11) print("| ");
			addr++;
		}
		pchar('\n');
	}
}


bool int_str(const char* str, int len, int *val)
{
	*val = 0;
	int base = 10;
	for(int i = 0; i < len; i++) {
		int c = str[i];
		c = tolower(c);
		if(i== 0 && c == '0') {
			base = 16;
			continue;
		}
		if(i==1 && base == 16) {
		       	if(c == 'x') continue;
			println("Arg conversion error. Expected 'X'");
			return false;
		}

		int digit = c - '0';
		if('a' <= c && c <= 'f') digit = c - 'a' + 10;
		if(digit <0 || digit >= base) {
			println("Arg conversion character unexpected");
			return false;
		}
		*val = *val * base  + digit;
	}
	return true;
}

static BaseType_t cmd_dump (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	BaseType_t len, xReturn;
	const char *arg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &len);
	configASSERT(len);

	int addr;
       	if(int_str(arg, len, &addr)) {
		print_dump(addr);
	} else {
		println("ADDR is not a number");
	}
	return pdFALSE; // no more data
}

static void flash_sector(uint32_t offset, const uint8_t* binary)
{
	static_assert(FLASH_SECTOR_SIZE == 4096);
	uint32_t ints; // interrupts
	ints = save_and_disable_interrupts();
	flash_range_erase(offset, FLASH_SECTOR_SIZE);
	flash_range_program(offset, (const uint8_t*) binary, FLASH_SECTOR_SIZE);
	restore_interrupts (ints);
}


static void read_sector(uint8_t* bin, file32_t* pfile)
{
	//int i = 0;
	uint8_t block[512];
	for(int i = 0; i< 4096/512; i++)
	{
		int n = file32_read(pfile, block);
		if(n == 0) return;
		memcpy(bin + i * 512, block, n);
		if(n<512) return;
	}
}

static BaseType_t cmd_flash (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	BaseType_t len, xReturn;
	const char *arg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &len);
	configASSERT(len);

	int off;
       	if(!int_str(arg, len, &off)) {
		println("OFF is not a number");
		return pdFALSE; // no more data
	}


	const char *name = FreeRTOS_CLIGetParameter(pcCommandString, 2, &len);
	configASSERT(len);
	char name0[13]; // a filename  with a 0 terminator
	print("Filename len: %d\n", len);
	if(len>12) {
		println("Filename too long");
		return pdFALSE;
	}
	strncpy(name0, name, len); // 8.3 filename, 0 terminated
	name0[len] = 0;
	char canon[12]; // a canonical filename
	canfile(canon, name0);
	print("Canonical filename is <%s", canon, ">\n");

	file32_t file;
	file32_init(&file, canon);
	if(!file32_found(&file)) {
		println("File not found");
		return pdFALSE;
	}

	uint8_t *bin = (uint8_t*)malloc(4096); 
	assert(bin);
	//int n;
	uint32_t size = file32_size(&file);
	int nsectors = size/4096;
	if(nsectors * 4096 != size) nsectors++; // overhang
	for(int sectorn = 0; sectorn<nsectors; sectorn++) {
		memset(bin, 0, 4096);
		read_sector(bin, &file);
		flash_sector(off, bin);
		off += 4096;
	}


finis:
	free(bin);
	return pdFALSE; // no more data
}

static BaseType_t cmd_hi (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	BaseType_t xReturn;
	sprintf(pcWriteBuffer, "Pluto says 'Hello'\n");
	return pdFALSE; // no more data
}

static BaseType_t cmd_load (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	BaseType_t len, xReturn;
	const char *arg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &len);
	configASSERT(len);

	/*
	int off;
       	if(!int_str(arg, len, &off)) {
		println("OFF is not a number");
		return pdFALSE; // no more data
	}
	*/


	const char *name = FreeRTOS_CLIGetParameter(pcCommandString, 1, &len);
	configASSERT(len);
	char name0[13]; // a filename  with a 0 terminator
	print("Filename len: %d\n", len);
	if(len>12) {
		println("Filename too long");
		return pdFALSE;
	}
	strncpy(name0, name, len); // 8.3 filename, 0 terminated
	name0[len] = 0;
	char canon[12]; // a canonical filename
	canfile(canon, name0);
	print("Canonical filename is <%s", canon, ">\n");

	file32_t file;
	file32_init(&file, canon);
	if(!file32_found(&file)) {
		println("File not found");
		return pdFALSE;
	}

	uint8_t *bin = (uint8_t*)malloc(4096); 
	assert(bin);
	int off = PROGBASE - 0x1000'0000;
	//int n;
	uint32_t size = file32_size(&file);
	int nsectors = size/4096;
	if(nsectors * 4096 != size) nsectors++; // overhang
	for(int sectorn = 0; sectorn<nsectors; sectorn++) {
		memset(bin, 0, 4096);
		read_sector(bin, &file);
		flash_sector(off, bin);
		off += 4096;
	}


finis:
	free(bin);
	return pdFALSE; // no more data
}



static int pic_addr;
static void ili_pic_filler(uint16_t* buffer, int slice)
{
	int addr = pic_addr + 240*2*slice;
	memcpy(buffer, (void*)addr, 240*2);
}

static BaseType_t cmd_pic (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	BaseType_t len, xReturn;
	const char *arg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &len);
	configASSERT(len);

	int addr;
       	if(!int_str(arg, len, &addr)) {
		println("ADDR is not a number");
		return pdFALSE; // no more data
	}

	pic_addr = addr;
	ili9341_set_filler(&ili_pic_filler); // use picture screen mode

	// wait for user to press a key
	while(!kbd_buf.avail())
		vTaskDelay(10);
	signed char c;
	kbd_buf.read(&c); // eat the char. We dpn't care what it was

	ili9341_set_filler(&mode0_fill_slice); // go back to console screen mode
	return pdFALSE; // no more data
}

static BaseType_t cmd_play (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	BaseType_t len, xReturn;
	const char *arg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &len);
	configASSERT(len);

	play_song(arg);
	return pdFALSE; // no more data
}

//////////////////////////////////////////////////////////////////////////
void cmd_task (void *pvParameters)
{
	//println("Starting cmd_task");

	static const CLI_Command_Definition_t call_def = {
		"call",
		"CALL ADDR: Call a subroutine at ADDR\r\n",
		cmd_call,
		1 };
	FreeRTOS_CLIRegisterCommand(&call_def);


	static const CLI_Command_Definition_t cls_def = {
		"cls",
		"CLS: Clear the screen\r\n",
		cmd_cls,
		0 };
	FreeRTOS_CLIRegisterCommand(&cls_def);


	static const CLI_Command_Definition_t del_def = {
		"del",
		"DEL OFF: Erase sector from OFF\r\n",
		cmd_del,
		1 };
	FreeRTOS_CLIRegisterCommand(&del_def);


	static const CLI_Command_Definition_t dir_def = {
		"dir",
		"DIR: List root directory\r\n",
		cmd_dir,
		0 };
	FreeRTOS_CLIRegisterCommand(&dir_def);


	static const CLI_Command_Definition_t dump_def = {
		"dump",
		"DUMP ADDR: Hexdump starting from ADDR\r\n",
		cmd_dump,
		1 };
	FreeRTOS_CLIRegisterCommand(&dump_def);


	static const CLI_Command_Definition_t flash_def = {
		"flash",
		"FLASH OFF FILE: Flash a FILE to OFFset\r\n",
		cmd_flash,
		2 };
	FreeRTOS_CLIRegisterCommand(&flash_def);


	static const CLI_Command_Definition_t hi_def = {
		"hi",
		"HI: Give a greeting\r\n",
		cmd_hi,
		0 };
	FreeRTOS_CLIRegisterCommand(&hi_def);


	static const CLI_Command_Definition_t load_def = {
		"load",
		"LOAD FILE: Tfr file from flash to progbase\r\n",
		cmd_load,
		1 };
	FreeRTOS_CLIRegisterCommand(&load_def);

	static const CLI_Command_Definition_t pic_def = {
		"pic",
		"PIC ADDR: Show a raw pic at ADDR\r\n",
		cmd_pic,
		1 };
	FreeRTOS_CLIRegisterCommand(&pic_def);

	static const CLI_Command_Definition_t play_def = {
		"play",
		"PLAY FILE: Play a song\r\n",
		cmd_play,
		1 };
	FreeRTOS_CLIRegisterCommand(&play_def);

	static const CLI_Command_Definition_t run_def = {
		"run",
		"RUN: Execute prog stored on flash at std place\r\n",
		cmd_run,
		0 };
	FreeRTOS_CLIRegisterCommand(&run_def);

	static char pcOutputString[ MAX_OUTPUT_LENGTH ];
	BaseType_t more;
	for(;;) {
		read_command();
		if(pcInputString[0] == 0) continue;
		do {
			*pcOutputString = 0;
			more = FreeRTOS_CLIProcessCommand(pcInputString, pcOutputString, MAX_OUTPUT_LENGTH);
			print(pcOutputString);
		} while(more!= pdFALSE);
		println("");
	}

}
