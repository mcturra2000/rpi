##########################################
### This Code is for Raspberry Pi Pico ###
###      copyright 2021 balance19      ###
##########################################

import machine

# calendrical functions
dims = [31, 28, 31, 30, 31, 30, 31,31, 30, 31, 30, 31]
cum_dims = [ 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334]

def isleap(y):
    if y%400 ==0: return True
    if y%100 ==0: return False
    if y%4 ==0:  return True
    return False

# get days in month
# mon in [0,11]
def get_dims(y, mon0):
        res = dims[mon0]
        if mon0 == 1: res += isleap(y)
        return res

# mon in [0,11]
def get_cum_dims(y, mon):
        tot = cum_dims[mon]
        if isleap(y) and (mon > 1): tot += 1
        return tot

# days since 1 Jan 2000 (2000-01-01 returns 0)
# mday in [1,31] month date
# mon [0, 11] months since jan
# year - absolute (e.g. 2000) starts from year 2000
def mildays(y, mon, mday):
        tot = (y-2000) * 365
        if y>2000: tot += (y-2001)/4
        tot += get_cum_dims(y, mon) + mday
        return tot

# adapted from 
# http://my-small-projects.blogspot.com/2015/05/arduino-checking-for-british-summer-time.html
# mon0 [0,11]
# day1 [1,31] month day
def isbst(year, mon0, day1, hr):
        imonth = mon0+1
        iday = day1

        #January, february, and november are out.
        if (imonth < 3 or imonth > 10): return False
        #April to September are in
        if (imonth > 3 and imonth < 10):  return True

        # find last sun in mar and oct - quickest way I've found to do it
        # last sunday of march
        lastMarSunday =  (31 - (5* year /4 + 4) % 7)
        #last sunday of october
        lastOctSunday = (31 - (5 * year /4 + 1) % 7)

        #In march, we are BST if is the last sunday in the month
        if (imonth == 3):
                if( iday > lastMarSunday): return True
                if( iday < lastMarSunday): return False
                if (hr < 1): return False
                return True
        
        #In October we must be before the last sunday to be bst.
        #That means the previous sunday must be before the 1st.
        if (imonth == 10):
                if( iday < lastOctSunday): return True
                if( iday > lastOctSunday): return False
                if (hr >= 1): return False
                return True        

        return False # should never reach here, but keep compiler quiet

#adjust for BST if necessary
# mon in [0, 11]
# day1 in [1, 31]
def adjbst(yr, mon0, day1, hr):
        if(isbst(yr, mon0, day1, hr) !=0):
            hr += 1
            if(hr == 24):
                hr = 0
                day1 +=1
                if(day1 >=  dims[mon0]):
                    day1 = 1
                    mon0 +=1
        return day1, hr



# Class for getting Realtime from the DS3231 in different modes.
class RTC:
    w = ["SAT", "SUN", "MON", "TUE", "WED", "THU", "FRI"]
    # If you want different names for Weekdays, feel free to add. Couple examples below:
    # w = ["FR", "SA", "SU", "MO", "TU", "WE", "TH"]
    # w = ["Friday", "Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday"]
    # w = ["Freitag", "Samstag", "Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag"]
    # w = ["viernes", "sabado", "domingo", "lunes", "martes", "miercoles", "jueves"]

    # Initialisation of RTC object. Several settings are possible but everything is optional.
    # If you meet these standards no parameters are required.
    # sid = slave address
    def __init__(self, i2c, sid=0x68, register=0x00):
        self.rtc_sid = sid  # for using different i2c address
        self.rtc_register = register  # for using different register on device. DON'T change for DS3231
        self.i2c = i2c

    # Method for setting the Time
    def SetTime(self, NowTime=b"\x00\x23\x12\x28\x14\x07\x21"):
        # NowTime has to be in format like b'\x00\x23\x12\x28\x14\x07\x21'
        # It is encoded like this           sec min hour week day month year
        # Then it's written to the DS3231
        self.i2c.writeto_mem(int(self.rtc_sid), int(self.rtc_register), NowTime)

    # DS3231 gives data in bcd format. This has to be converted to a binary format.
    def bcd2bin(self, value):
        return (value or 0) - 6 * ((value or 0) >> 4)

    # Add a 0 in front of numbers smaller than 10
    def pre_zero(self, value):
        pre_zero = True  # Change to False if you don't want a "0" in front of numbers smaller than 10
        if pre_zero:
            if value < 10:
                value = f"0{value}"  # From now on the value is a string!
        return value

    # Read the Realtime from the DS3231 with errorhandling. Currently two output modes can be used.
    def ReadTime(self, mode=0):
        try:
            #print(1)   
            # Read RT from DS3231 and write to the buffer variable. It's a list with 7 entries.
            # Every entry needs to be converted from bcd to bin.
            buffer = self.i2c.readfrom_mem(self.rtc_sid, self.rtc_register, 7)
            #print(buffer)   
            # The year consists of 2 digits. Here 2000 years are added to get format like "2021"
            year = self.bcd2bin(buffer[6]) + 2000
            #print(2)
            month = buffer[5] & 0x1F # masking out century overflow
            month = self.bcd2bin(month)  # Just put the month value in the month variable and convert it.
            day = self.bcd2bin(buffer[4])  # Same for the day value
            #print(3)
            # Weekday will be converted in the weekdays name or shortform like "Sunday" or "SUN"
            #print("buffer 3 = ", buffer[3])
            weekday = self.bcd2bin(buffer[3]) % 7
            weekday = self.w[weekday]
            #print(4)
            # Uncomment the line below if you want a number for the weekday and comment the line before.
            # weekday = self.bcd2bin(buffer[3])
            hour = self.pre_zero(self.bcd2bin(buffer[2]))  # Convert bcd to bin and add a "0" if necessary
            minute = self.pre_zero(self.bcd2bin(buffer[1]))  # Convert bcd to bin and add a "0" if necessary
            second = self.pre_zero(self.bcd2bin(buffer[0]))  # Convert bcd to bin and add a "0" if necessary
            if mode == 0:  # Mode 0 returns a list of second, minute, ...
                return second, minute, hour, weekday, day, month, year
            if mode == 1:  # Mode 1 returns a formated string with time, weekday and date
                time_string = f"{hour}:{minute}:{second}      {weekday} {day}.{month}.{year}"
                return time_string
            # If you need different format, feel free to add

        except Exception as e:
            return (
                "Error: is the DS3231 not connected or some other problem (%s)" % e
            )  # exception occurs in any case of error.
