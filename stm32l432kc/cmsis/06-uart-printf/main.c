/*
 * Transmit-only uart messaging
 *
 * See blog of 2022-02-20 for write-up
 *
 * 2022-02-20	Started. 
 */

#include <jg_stdio.h>

#include "delay.h"

int main()
{
	//uart2_init(); // 2022-03-11 init now taken care of automatically
	puts("uart printf test");

	int count = 0;
	while(1) {
		printf2("count = %d\n", count++);
		delayish(1000);
	}
}
