# Adafruit FTDI (FT232H) - GPIO/I2C/SPI USB converter

## References

* [Adafruit](https://www.adafruit.com/product/2264) product description
* [ExplainingComputers](https://www.youtube.com/watch?v=Rt5xtIyxgco) - **useful** for setting things up using Python

## Status

2024-07-17  Created
