# 2023-07-19	snapshot
# 2023-07-01 	snapshot
cd build

# quit without confirmation
define hook-quit
    set confirm off 
end

set pagination off
file app.elf
#target remote localhost:3333 # old method as at 2023-07-19
target extended-remote localhost:3333
load
#b main
#b main.c:46
monitor reset init
echo RUNNING...\n
c
