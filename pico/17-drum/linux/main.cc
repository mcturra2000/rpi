#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <termios.h>
#include <stdlib.h>


int main()
{
	struct termios tio0, tio;

	const char device[] = "/dev/ttyACM0";
	int fd = open(device, O_RDWR | O_NOCTTY | O_NDELAY);
	if(fd == -1) {
		printf( "failed to open port\n" );
	}

	tcgetattr(fd, &tio0);
	tio = tio0;
	tio.c_lflag &= ~ICANON; // disable canonical mode
	cfsetispeed(&tio, B115200);
	cfsetospeed(&tio, B115200);
	tcsetattr(fd, TCSANOW, &tio);

	char beat[] = "!!! ";
	while(1) {
		for(int i = 0 ; i < sizeof(beat); i++) {
			char c = 1;
			if(beat[i] == '!') 
				write(fd, &c, 1);
			usleep(500'000);
		}
	}
	close(fd);
}
