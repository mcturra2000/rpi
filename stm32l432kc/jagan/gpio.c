#include "gpio.h"

void gpio_set_opendrain(GPIO_TypeDef *port, uint32_t pin)
{
        port->OTYPER |= (1<<pin);
}

void gpio_enable_rcc(GPIO_TypeDef *port)
{
	uint32_t en_val = RCC_AHB2ENR_GPIOAEN;
	//RCC->AHB2ENR	|= en_val; // seems important to ensure GPIOA is always enabled
	if(port == GPIOB) { en_val = RCC_AHB2ENR_GPIOBEN; }
	else if(port == GPIOC) { en_val = RCC_AHB2ENR_GPIOCEN; }
	RCC->AHB2ENR	|= en_val;
	nop(); // CubeIDE seems to think it's useful
}

void gpio_set_moder(GPIO_TypeDef *port, uint32_t pin, int val)
{
	port->MODER	&= ~(0b11<<(pin*2)); // important to reset it, as its default is analog
	port->MODER	|= (val<<(pin*2));
}

void gpio_set_ospeedr(GPIO_TypeDef *port, uint32_t pin, int val)
{
	port->OSPEEDR	&= ~(0b11<<(pin*2)); 
	port->OSPEEDR	|= (val<<(pin*2));
}

void gpio_set_pupdr(GPIO_TypeDef *port, uint32_t pin, int val)
{
	port->PUPDR	&= ~(0b11<<(pin*2)); 
	port->PUPDR	|= (val<<(pin*2));
}

void gpio_alt(GPIO_TypeDef *port, uint32_t pin, uint8_t alt_fn)
{
	gpio_enable_rcc(port);
	gpio_set_moder(port, pin, 0b10);
	//port->MODER	|= (0b10<<(pin*2));

	int pos = pin*4;
	uint8_t idx = 0;
	if(pin>7) { // not tested properly
		pos -= 32;
		idx = 1;
	}
	port->AFR[idx] &= ~(0b1111  << pos); // mask out
	port->AFR[idx] |= (alt_fn  << pos);
}


void gpio_out(GPIO_TypeDef *port, uint32_t pin)
{
	gpio_enable_rcc(port);
	gpio_set_moder(port, pin, 0b01);
	//port->MODER	|= (1<<(pin*2));
}

void gpio_in(GPIO_TypeDef *port, uint32_t pin)
{
	gpio_enable_rcc(port);
	gpio_set_moder(port, pin, 0b00);
	gpio_set_pupdr(port, pin, 0b11);
	//port->PUPDR	= ~(0b11<<(pin*2));
	//port->MODER	|= (1<<(pin*2)); // should really set it to 0
}

void gpio_pullup(GPIO_TypeDef *port, uint32_t pin)
{
	gpio_set_pupdr(port, pin, 0b01);
	//port->PUPDR	= ~(0b11<<(pin*2));
	//port->PUPDR	|= (1<<(pin*2));
}

void gpio_inpull(GPIO_TypeDef *port, uint32_t pin)
{
	gpio_in(port, pin);
	gpio_pullup(port, pin);
}
