# ds3231

You can adjust seconds by clicking blue (down) or white button (up). See db07.96


tail -1 /usr/share/zoneinfo/Europe/London 
GMT0BST,M3.5.0/1,M10.5.0
https://sourceware.org/legacy-ml/newlib/2018/msg00301.html

https://github.com/JChristensen/Timezone


## Status

2022-10-30	Added ability to adjust seconds.

2022-04-04	Started. Works
