#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#if __llvm__  
#define O0 __attribute__((optnone))
#else
#define O0   __attribute__((optimize("O0")))
#endif
  
/* approx delay in ms */
void O0 delayish (uint32_t ms);

#ifdef __cplusplus
}
#endif

