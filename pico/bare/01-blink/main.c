#include "../rp2040-cmsis-1.h"

#define RESETS_PWM		(1ul<<14)
#define RESETS_PAD_BANK0	(1ul<<8)
#define RESETS_IO_BANK0		(1ul<<5)

// §2.14 subsystem resets
// reset a peripheral(s), waiting until the are back up
// you can set several peripherals at once, or iseparately in asequence
// E.g. reset_blocking(RESETS_IO_BANK0 | RESETS_PWM);
void reset_blocking(uint32_t periphs)
{
	REG(RESETS_BASE + 0x00 + 0x3000) = periphs; // 2.14.3 Reset register using atomic clear 0x3000
	while(!(RESETS->RESET_DONE & periphs)); // wait until the peripherals are up
}



#define GPIO_FUNC_SIO	5
#define LED 25

void delay(int n) // no particular timing
{
	for(int i =0 ; i< n; i++) {
		for(int j = 0; j< 1000; j++) {
			asm volatile ("nop");
		}
	}
}



static void gpio_clr_mask(uint32_t mask)
{
		SIO->GPIO_OUT_CLR = mask;
}


int main()
{
	reset_blocking(RESETS_IO_BANK0);

	IO_BANK0->GPIO25_CTRL = GPIO_FUNC_SIO; // init pin - select function SIO
	SIO->GPIO_OE_SET = 1ul << LED; // allow setting of output

	while(1) {
		SIO->GPIO_OUT_SET = (1ul << LED); // turn on gpio atomically 
		delay(100);
		SIO->GPIO_OUT_CLR = (1ul << LED); // turn off gpio atomically
		delay(1000);
	}

	return 0;
}
