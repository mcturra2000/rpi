#pragma once
//  Author: avishorp@gmail.com
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA



#ifdef __cplusplus
extern "C" {
#endif

#include <inttypes.h>
#include <stdbool.h>
#include <time.h>

#include <gpio.h>

#define SEG_A   0b00000001
#define SEG_B   0b00000010
#define SEG_C   0b00000100
#define SEG_D   0b00001000
#define SEG_E   0b00010000
#define SEG_F   0b00100000
#define SEG_G   0b01000000


/* 
     A
    ---
 F | G | B
    ---
 E |   | C
    ---
DP   D
*/
	
// 100, 50, 10  works on rpi
// 5, 1 doesn't work on rpi
#define DEFAULT_BIT_DELAY  10

void tm1637_init_stm32(GPIO_TypeDef* clk_gpio,  uint16_t clk_pin, GPIO_TypeDef* dio_gpio, uint16_t dio_pin);

void tm1637_init_pico(uint8_t clk, uint8_t dio);
//void tm1637_showNumberDec(int num);
void tm1637_show_dec(int num);
void tm1637_test();
void tm1637_invert_display(int val); // 1=> inverted (upside down), 0 => normal orientation
void tm1637_show_hhmm(struct tm *t);				


#ifdef __cplusplus
}
#endif


//#endif // __TM1637DISPLAY__
