#pragma once
#include <debounce.h>

class Button
{
public:
    Button(int pin);
    bool falling(void);

private:
    deb_t deb;
    int _pin;
};