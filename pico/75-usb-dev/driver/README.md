# driver

Talk to the USB device


```
See messages: 
sudo dmesg

list modules:
lsmod | grep piclone

# possibly unnecessary
sudo mknod /dev/gpiochip0 c 242 0
sudo chown :plugdev /dev/gpiochip0
#sudo chmod +g /dev/gpiochip0
sudo setfacl -m g:plugdev:rw /dev/gpiochip0

Add the following to /etc/udev/rules.d//99-tinyusb.rules :
KERNEL=="gpiochip[0-9]*", GROUP="plugdev"


echo 1 > /dev/gpiochip0 # turn pin on
```

## See also

* [tutorial](https://github.com/Johannes4Linux/Linux_Driver_Tutorial), also via [YouTube](https://www.youtube.com/watch?v=4tgluSJDA_E&list=PLCGpd0Do5-I3b5TtyqeF1UdyD4C-S-dMa&index=3)
* [writing a usb driver](https://docs.kernel.org/driver-api/usb/writing_usb_driver.html)


## Status

2-23-08-07  Started
