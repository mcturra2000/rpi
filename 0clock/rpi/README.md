# clock

Fancy clock timer

**CPU usage** 4.2% on Pi0

sudo apt install mosquitto mosquitto-clients libpaho-mqtt-dev at

sudo vim /etc/mosquitto/mosquitto.conf 
```
listener 1883
allow_anonymous true
```


## Alarms


To set clock into alarm mode, issue a command like:
```
mosquitto_pub -h localhost -t myclock -m "3"
```

To automate an alarm using `at`:
```
at 21:00
mosquitto_pub -h localhost -t myclock -m "3"
^D
```

Schedule alarm: beep-at 5 pm

To list the pending alarms: **`atq`**

Remove job: **`atrm`**


* [at](https://phoenixnap.com/kb/linux-at-command)

## Setup


**Install the inih library**:
```
sudo apt install meson
cd ~/src
git clone https://github.com/benhoyt/inih.git
cd inih
meson --prefix=$HOME/.local/lib --libdir=. builddir
cd builddir
meson install
```

[Alerternative INI file libs](https://en.wikipedia.org/wiki/INI_file)



**udev rules**

You need to make systemd tract the availability of i2c devices for the service to work.

Run `sudo vim /etc/udev/rules.d/99-i2c.rules` :
```
SUBSYSTEM=="i2c-dev", TAG+="systemd"
```

After rebooting you can then verify this has worked with: 
```
systemctl --all --full -t device | grep i2c
```

You can then add one the resulting i2c .device units as a dependency in your own unit file e.g.
```
cat /etc/systemd/system/mything.service
[Unit]
...
Requires=dev-i2c\x2d1.device
After=dev-i2c\x2d1.device
...
```

[Forum](https://forums.raspberrypi.com/viewtopic.php?t=221507)

You can reload the rules without booting:
```
sudo udevadm control --reload-rules && sudo udevadm trigger
```

## Links 

* [Paho MQTT C Client Library ](https://www.eclipse.org/paho/files/mqttdoc/MQTTClient/html/_m_q_t_t_client_8h.html)

## Status

2022-10-30	Changed `prog_start_time_ns` from type `double` to `uint64_t`. There was a bug in the display of timer elapsed time.

2022-07-27	Re-work of C. Works.

2021-12-25	Added alarm capabilities

2021-12-24	Confirmed working



