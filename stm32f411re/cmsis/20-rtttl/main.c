#include "basal.h"
#include "delay.h"
#include "gpio.h"
#include "debounce.h"
#include "systick.h"
#include <string.h>
#include <i2c.h>
#include <ssd1306.h>


#include "parser/rtttl.h"

typedef uint32_t u32;

#define LED		GPIOA, 5
#define BTN_DOWN 	GPIOB, 0 // green button
#define BTN_UP		GPIOA, 7 // blue button
#define BTN_PLAY	GPIOA, 4 // white button, right (play)
deb_t deb_btn_down;
deb_t deb_btn_up;
deb_t deb_btn_play;


volatile uint32_t  ticks = 0; // must be volatile to prevent compiler optimisations
volatile bool update_buttons = true;

void  SysTick_Handler(void)
{
	ticks++;
	//	gpio_put(LED, gpio_get(BTN_DOWN));
	if((ticks % 4 == 0) && update_buttons) {
		deb_update(&deb_btn_down, gpio_get(BTN_DOWN));
		deb_update(&deb_btn_up, gpio_get(BTN_UP));
		deb_update(&deb_btn_play, gpio_get(BTN_PLAY));
	}

	//if(ticks%512 == 0)
	//	gpio_toggle(LED);
}


void delay_ms(int ms)
{
	uint32_t started = ticks;
	while((ticks-started)<=ms); // rollover-safe (within limits)
}



void set_freq(int freq, int vol)
{
	if(freq == 0) {
		TIM5->CR1 &= ~TIM_CR1_CEN; // turn note off for awhile
		TIM5->EGR |= TIM_EGR_UG; // Generate update
		return;
	}

	uint32_t arr = 1000000/freq-1;
	TIM5->ARR = arr; // set freq
	uint32_t duty =  (arr+1) *  vol /100 -1; 
	TIM5->CCR1 = duty; // Duty cycle on Cap/Compare Reg
	TIM5->CR1 |= TIM_CR1_CEN; // enable the counter
	TIM5->EGR |= TIM_EGR_UG; // Generate update
}

void set_freq_delay(int freq, int vol, int delay)
{
	set_freq(freq, vol);
	delay_ms(delay);
}

uint32_t *read(int size, uint32_t* addr)
{
	uint32_t * ptr = (uint32_t*) (*addr);
	*addr += size;
	return ptr;
}

u32 max(u32 a, u32 b) 
{
	if(a<b) return a;
	return b;
}

uint32_t song_base;
uint32_t seek_song(int n)
{
	uint32_t addr = song_base;
	for(int i = 0; i< n; i++) {
		addr += 16; // skip song title
		uint16_t num_notes = *(uint16_t*) read(2, &addr);
		addr += num_notes*sizeof(note_t); // skip notes
	}
	return addr;

}

uint32_t num_songs = 0;
int cursor_row  = 0; // which row trhe cursor is on

void print_menu(int start_song_num)
{

	clear_scr();
	uint32_t addr = seek_song(start_song_num);
	print_char_at('>', 1, 0, cursor_row*8);
	for(int i = 0; i< max(8, num_songs); i++ ) {
		setCursory(i);
		setCursorx(2);
		char *title = (char *) read(16, &addr);
		for(int x = 0 ; x< 14; x++) {
			ssd1306_putchar(title[x]);
		}

		uint16_t num_notes = *(uint16_t*) read(2, &addr);
		addr += num_notes*sizeof(note_t);
	}
	show_scr();
}

void play(int song_num)
{
	u32 addr = seek_song(song_num);
	addr += 16; // skip title
	uint16_t num_notes = *(uint16_t*) read(2, &addr);
	for(int i = 0; i< num_notes; i++) {
		note_t *note = (note_t*) read(sizeof(note_t), &addr);
#if 1
		set_freq(note->freq, 5);
		delayish(note->dur * 14/16);
		set_freq(0, 0);
		delayish(note->dur * 2/16);
#else
		// we use attack, sustain and release so as not to create clicking
		int freq = note->freq;
		int dur = note->dur;
		int sixth = dur *6 / 128;
		set_freq_delay(freq, 1, sixth);
		set_freq_delay(freq, 1, sixth);
		set_freq_delay(freq, 3, sixth);
		set_freq_delay(freq, 5, dur * 92/ 128); // = 128 - 6*6
		set_freq_delay(freq, 2, sixth);
		set_freq_delay(freq, 0, sixth);
		set_freq_delay(freq, 0, sixth);
		set_freq_delay(0, 0, 0);
#endif
	}
}

void main (void) 
{	   
	// set up speaker PA0 on TIM5 CH1
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
	gpio_alt(GPIOA, 0, 2); // PB6 PWM alt fn
	RCC->APB1ENR |= RCC_APB1ENR_TIM5EN; // enable time clock
	//memset(TIM5, 0, sizeof(TIM5)); // seems to be important
	TIM5->CR1 |= TIM_CR1_ARPE; // buffer the ARR register
	TIM5->PSC = SystemCoreClock/1000000-1; // scale to 1us
	TIM5->CCMR1 |= (0b110 << TIM_CCMR1_OC1M_Pos); // PWM mode 1
	TIM5->CCMR1 |= TIM_CCMR1_OC1PE; // enable preload for channel 1
	TIM5->CCER |= TIM_CCER_CC1E; // Enable Capture/Compare for channel 1
	TIM5->CR1 |= TIM_CR1_CEN; // enable the counter
	TIM5->CNT = 0;
	volatile TIM_TypeDef * tim5 = TIM5; // for debugging purposes


	delayish(4);
	i2c1_init();
	init_display(64, (uint32_t) I2C1);
	//int counter = 0;

	gpio_out(LED);
	gpio_in(BTN_DOWN); gpio_pullup(BTN_DOWN); deb_btn_down.en = 1;
	gpio_in(BTN_UP); gpio_pullup(BTN_UP); deb_btn_up.en = 1;
	gpio_in(BTN_PLAY); gpio_pullup(BTN_PLAY); deb_btn_play.en = 1;

	systick_init_1ms();


	uint32_t addr = 0x8004000; // start of the songs

	// look for magic number
	char* magic = (char*) read(4, &addr);
	if(strncmp(magic, "RING", 4) !=0) {
		ssd1306_printf("Couldn't find ringtone magic marker\n");
		show_scr();
		return;
	}

	num_songs = *(uint32_t*) read(4, &addr);
	song_base = addr;

	print_menu(0);
	//ssd1306_printf("Num songs: %d\n", num_songs);
	//show_scr();

	int top_song = 0; // the song number at the top of the screen
	while(1) {
		bool update_scr = false;
		if(deb_falling(&deb_btn_down)) {
			if(top_song+cursor_row+1 < num_songs)
				cursor_row++;
			if(cursor_row>=8) {
				cursor_row = 7;
				top_song++;
			}

			update_scr = true;
			//gpio_toggle(LED);
		}

		if(deb_falling(&deb_btn_up)) {
			cursor_row--;
			if(cursor_row<0) {
				cursor_row = 0;
				top_song--;
				if(top_song<0) top_song = 0;
			}
			update_scr = true;
		}

		if(deb_falling(&deb_btn_play)) {
			update_buttons = false;
			play(cursor_row+top_song);
			update_buttons = true;
		}

		if(update_scr)
			print_menu(top_song);
	}



}
