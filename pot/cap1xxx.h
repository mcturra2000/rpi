#pragma once

/* Used by drh08, gfx-hat */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

bool cap_begin(uint8_t sid);
uint8_t cap_touched();
//#define CAP1188_I2CADDR 0x2c ///< The default I2C address

// Some registers we use
#if 0
#define CAP1188_SENINPUTSTATUS                                                 \
  0x3 ///< The Sensor Input Status Register stores status bits that indicate a
      ///< touch has been detected. A value of ‘0’ in any bit indicates that no
      ///< touch has been detected. A value of ‘1’ in any bit indicates that a
      ///< touch has been detected.
#define CAP1188_MAIN                                                           \
  0x00 ///< Main Control register. Controls the primary power state of the
       ///< device.
#define CAP1188_MTBLK                                                          \
  0x2A ///< Multiple Touch Configuration register controls the settings for the
       ///< multiple touch detection circuitry. These settings determine the
       ///< number of simultaneous buttons that may be pressed before additional
       ///< buttons are blocked and the MULT status bit is set. [0/1]
#define CAP1188_LEDLINK                                                        \
  0x72 ///< Sensor Input LED Linking. Controls linking of sensor inputs to LED
       ///< channels
#define CAP1188_STANDBYCFG                                                     \
  0x41 ///< Standby Configuration. Controls averaging and cycle time while in
       ///< standby.

#endif

#define CAP1188_PRODID                                                         \
  0xFD ///< Product ID. Stores a fixed value that identifies each product.
#define CAP1188_MANUID                                                         \
  0xFE ///< Manufacturer ID. Stores a fixed value that identifies SMSC
#define CAP1188_REV                                                            \
  0xFF ///< Revision register. Stores an 8-bit value that represents the part
       ///< revision.
       ///
#define CAP1188_MAIN_INT                                                       \
  0x01 ///< Main Control Int register. Indicates that there is an interrupt.
#define CAP1188_LEDPOL                                                         \
  0x73 ///< LED Polarity. Controls the output polarity of LEDs.


#ifdef __cplusplus
}
#endif
