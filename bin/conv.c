#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>

char *radix_name = 0;
int radix = 0;
int rad(char *str, int r)
{
	if(strcmp(radix_name, str) == 0) {
		radix = r;
		return 1;
	} else 
		return 0;
}

int bits[32];


int main(int argc, char ** argv)
{
	assert(argc == 3);

	radix_name = argv[1];
	char *s = radix_name;
	while(*s) *s++ = tolower(*s);
	rad("bin", 2) || rad("oct", 8) || rad("dec", 10) || rad("hex", 16);
	assert(radix);

	int dec = 0; 
	s = argv[2];
	while(*s) {
		int c1 = tolower(*s++);
		if(isdigit(c1))
			c1 = c1 -'0';
		else if(isxdigit(c1))
			c1 = c1 -'a' + 10;
		else
			continue;
		//printf("%d %d -- ", radix, c1);
		if(c1<radix) dec = dec*radix + c1;
	}

	// decode to binary
	int bin = dec;
	for(int i =0; i<32; i++) {
		bits[i] = bin & 1;
		//if(bin & 1) { bits[i] = 
		bin >>= 1;
	}
	printf("BIN ");
	for(int i =0; i<32; i++) {
		int v = bits[31-i];
		if(v) putchar('1'); else putchar('0');
		if( (i%4==3) && (i!=31) ) putchar('\'');
	}
	puts("");

	printf("OCT %o\n", dec);
	printf("DEC %d\n", dec);
	printf("HEX %x\n", dec);
	/*
	string outstr;
	string raw = args[2]; // input number
	foreach(c; raw) {
		int c1 = std.ascii.toUpper(c);
		if(isDigit(c1)) { 
			c1 = c1 - '0';
		} else if ('A' <= c1 && c1 <= 'F' ) {
			c1 = c1 - 'A' + 10;
		} else continue;

		if(c1 <  radix) dec = dec*radix + c1;
	}

	writeln(format("BIN %,4b", dec));
	writeln(format("OCT %o", dec));
	writeln(format("DEC %d", dec));
	writeln(format("HEX %x", dec));
	*/
	return 0;
}  
