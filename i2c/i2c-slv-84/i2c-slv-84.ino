// pull-up resitors may be required (see 3b)
// git clone https://github.com/rambo/TinyWire
#include <TinyWireS.h>

#define I2C_SLAVE_ADDRESS 0x4

void fastClock(){
  // 8MHz
  noInterrupts();
  CLKPR = 0x80; // enable clock prescale change
  CLKPR = 0;    // no prescale
  interrupts();
  
}

void receiveEvent(uint8_t howMany) {
  uint8_t pat = 0;
  if (howMany < 1) return;
   //if (howMany > TWI_RX_BUFFER_SIZE) return;
   while(howMany--) {
    pat = TinyWireS.receive();
   }
   digitalWrite(0, bitRead(pat, 0));
   digitalWrite(1, bitRead(pat, 1));
   digitalWrite(2, bitRead(pat, 2));
   digitalWrite(3, bitRead(pat, 3));
  
}

void setup() {
  fastClock(); 
  pinMode(0, OUTPUT);
  pinMode(1, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);

  TinyWireS.begin(I2C_SLAVE_ADDRESS); // join i2c network
  TinyWireS.onReceive(receiveEvent);
  //TinyWireS.onRequest(requestEvent);
  //Wire.begin(8);                // join i2c bus with address #8
  //Wire.onRequest(requestEvent); // register event
}

void loop() {
  // This needs to be here
  TinyWireS_stop_check();
  #if 0
  digitalWrite(2, HIGH);
  delay(100);
  digitalWrite(2, LOW);
  delay(900);
  #endif
}
