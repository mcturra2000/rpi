#include "pi.h"

#define PIN  16 // GPIO of built-in LED
		//
int main() 
{
	pi_gpio_out(PIN);

	for(;;) {
		pi_gpio_set(PIN);
		delayish(10);
		pi_gpio_clr(PIN);
		delayish(90);		
	}
}

