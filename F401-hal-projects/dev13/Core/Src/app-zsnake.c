#include "main.h"
/* draw a pretty snake on the zsegment display */

// #define A 0b1000000
#define A (1 << 6)
#define B (1 << 5)
#define C (1 << 4)
#define D (1 << 3)
#define E (1 << 2)
#define F (1 << 1)
#define G (1 << 0)

static void pause()
{
    //HAL_Delay(500);
    delay_ms(500/5);
}

uint8_t zseg_display[8] = {0}; // left to right
void zseg_clear() { memset(zseg_display, 0, sizeof(zseg_display)); }

// You possible want to zseg_setmode(NODECODE); beforehand
void zseg_show()
{
    for (int i = 0; i < 8; i++)
        zseg_tfr(i + 1, zseg_display[7 - i]);
}

static int zrows[4][3] = 
{ 
    {A, 1, B},
    {G, -1, E},
    {D, 1 , C},
    {G, -1, F}
 };

void do_zrow(int zrow[3])
{
    int cell = zrow[0], dir = zrow[1], last = zrow[2];
    for(int i = 0; i< 8; i++) {
        zseg_clear();
        int j = i;
        if(dir<0) {j= 7-i;}
        zseg_display[j] = cell;
        zseg_show();
        pause();
    }

    zseg_clear();
    if(dir>0) {zseg_display[7] = last;} else {zseg_display[0] = last;}
    zseg_show();
    pause();

}

void do_zrows()
{
    for(;;) {
        for(int j = 0; j<4; j++) {
            do_zrow(zrows[j]);
        }
    }
}
void app_zsnake()
{
       
    zseg_setmode(NODECODE);
    do_zrows();

    zseg_clear();
    // zseg_display = {0};
    zseg_show();
    pause();

again:
    int seq[2][5] = {{A, B, G, E, D}, {D, C, G, F, A}};
    for (int j = 0; j < 4; j++)
    {
        for (int i = 0; i < 5; i++)
        {
            zseg_clear();
            zseg_display[j * 2] = seq[0][i];
            zseg_show();
            pause();
        }
        for (int i = 0; i < 5; i++)
        {
            zseg_clear();
            zseg_display[j * 2 + 1] = seq[1][i];
            zseg_show();
            pause();
        }
    }
    goto again;
}