#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
//#include "hardware/adc.h"
//#include "hardware/clocks.h"
//#include "hardware/flash.h"
#include "hardware/gpio.h"
//#include "hardware/irq.h"
//#include "hardware/pwm.h"
#include "hardware/i2c.h"
// #include "tusb.h" // if you want to use tud_cdc_connected()
//#include <pico/time.h>

#include "pi.h"
#include "bme68x.h"
#include "i2c.h"

#define LED  25 // GPIO of built-in LED


/*
typedef i2c_inst_t* i2c_t;
typedef uint8_t* bytes;
typedef uint8_t ubyte;
typedef uint32_t u32;
*/



int main() 
{
	timer_hw->dbgpause = 0;
	pi_stdio_init();
	//stdio_init_all();
	// while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 

	printf("project 23 \n");
	pi_i2c_init(4);
	pi_gpio_init(LED, OUTPUT);
	
	/* BME680 API forced mode test */
	struct bme68x_data data;
	bme68x_start(&data, (uint32_t) i2c0);
	puts("point 1");


	// chip id
	uint8_t chip_id = i2c_recv_byte_at((uint32_t) i2c0, 0x76, 0xD0);
	printf("chip_id = %#x (s/b 0x61)\n", chip_id); // seems to be OK

	int i =0;
	while (1) {
		bme68x_single_measure(&data);
		printf("temp %f\n", data.temperature);

		if (bme68x_single_measure(&data) == 0) {

			// Measurement is successful, so continue with IAQ
			data.iaq_score = bme68x_iaq(); // Calculate IAQ

			// Create a message buffer and clear it.
			char msgBuffer[120];
			for(uint16_t i = 0; i < 120; i++){
				msgBuffer[i] = ' ';
			}

			// Send the data through UART.
			sprintf(msgBuffer,
					"8 Count: %d, Temp (deg C): %.2f, Press(Pa): %.2f, Humid(%%): %.2f, "
					"IAQ: %.1f ,Gas resiste(ohm): %.2f\r\n",
					i++, data.temperature, data.pressure, data.humidity,
					data.iaq_score, data.gas_resistance);
			printf("%s", msgBuffer);

		}

		delayish(2000);
	}



	return 0;
}

