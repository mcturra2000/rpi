# Micropython package for RP2040


## Reference

* [github](https://github.com/CRCibernetica/bme688-i2c-micropython/tree/main) - changed address from 0x77 to 0x76, some tweaks to main.py

## Status

2023-07-03	Started. Works
