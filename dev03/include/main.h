#pragma once

#include <Arduino.h>
//#include <inttypes.h>

typedef int64_t millis_t;
extern volatile millis_t millis_ticks;
extern hw_timer_t *timer;