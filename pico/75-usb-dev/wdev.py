#!/usr/bin/env python3
# Write bytes to endpoint 1

# Examples:
# ./wdev.py "GCO\031" # configure pin 25 for output
# ./wdev.py "GW\031\001" # set pin 25 high
# ./wdev.py "GW\031\000" # set pin 25 low

#from time import sleep
import sys
import usb.core # sudo pip3 install pyusb
#import usb.util

#import binascii

dev = usb.core.find(idVendor=0x3AAC, idProduct=0x0001)
if dev is None: raise ValueError('Device not found')

def main(out):
	out =  out.encode('latin-1').decode('unicode_escape').encode("utf-8")
	dev.write(1, out)
	print("Wrote", len(out), "bytes")

if __name__ == "__main__":
	main(sys.argv[1])
	#main("")
	exit(0)

