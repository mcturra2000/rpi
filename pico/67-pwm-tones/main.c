#include "pi.h"

#define IN1	11
#define IN2	12
#define OUT1 19
#define OUT2 20
#define SPK 21

int main() 
{
	pi_pwm_init(OUT1, 400, 50); // gpio freq duty
	pi_pwm_init(OUT2, 402, 50); // gpio freq duty
	//pi_pwm_init(OUT2, 5, 50); // gpio freq duty
	pi_gpio_init(IN1, INPUT);
	pi_gpio_init(IN2, INPUT);
	pi_gpio_init(SPK, OUTPUT);

	for(;;) {
		//gpio_put(SPK, gpio_get(IN1) && gpio_get(IN2); // NB don't use gpio_get_out_level);
		gpio_put(SPK, gpio_get(OUT1) && gpio_get(OUT2)); // NB don't use gpio_get_out_level);

	}

	return 0;
}

