
//  Author: avishorp@gmail.com
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#if __has_include(<pico/stdlib.h>)
#include <pico/stdlib.h> // will define RASPBERRYPI_PICO
#include <pi.h>
#endif

#include "TM1637Display.h"


#include <delay.h>

void tm1637_test()
{
#ifdef RASPBERRYPI_PICO
	//#pragma message "pico tm1637"
	tm1637_init_pico(16, 17); // CLK, DIO
#else
	tm1637_init_stm32(GPIOB, GPIO_PIN_12, GPIOB, GPIO_PIN_13); // CLK, DIO
#endif
	//tm1637_invert_display(1);

	int i = 0;
	for(;;) {
		tm1637_show_dec(i);
		i++;
		sleep_ms(1);		
	}

}

static GPIO_TypeDef *m_clk_gpio, *m_dio_gpio;
static uint16_t m_pinClk, m_pinDIO;
static uint8_t m_brightness;
static unsigned int m_bitDelay;

void clk_write(int level)
{
	gpio_write(m_clk_gpio, m_pinClk, level);	
}

void dio_write(int level)
{
	gpio_write(m_dio_gpio, m_pinDIO, level);	
}

void dio_out()
{
	gpio_out(m_dio_gpio, m_pinDIO);
}

void dio_in()
{
	gpio_in(m_dio_gpio, m_pinDIO);
}

void clk_out()
{
	gpio_out(m_clk_gpio, m_pinClk);
}

void clk_in()
{
	gpio_in(m_clk_gpio, m_pinClk);
}

//void TM1637Display::bitDelay()
void tm1637_bitDelay()
{
	//delayMicroseconds(m_bitDelay);
	sleep_us(m_bitDelay);
}

//void TM1637Display::start()
void tm1637_start()
{
	dio_out();
	tm1637_bitDelay();
}

void tm1637_stop()
{
	dio_out();
	tm1637_bitDelay();
	clk_in();
	tm1637_bitDelay();
	dio_in();
	tm1637_bitDelay();
}


void tm1637_setBrightness(uint8_t brightness, bool on)
{
	m_brightness = (brightness & 0x7) | (on? 0x08 : 0x00);
}

#define TM1637_I2C_COMM1    0x40
#define TM1637_I2C_COMM2    0xC0
#define TM1637_I2C_COMM3    0x80

bool tm1637_writeByte(uint8_t b)
{
	uint8_t data = b;

	// 8 Data Bits
	for(uint8_t i = 0; i < 8; i++) {
		// CLK low
		clk_out();
		tm1637_bitDelay();

		// Set data bit
		if (data & 0x01)
			dio_in();
		else
			dio_out();

		tm1637_bitDelay();

		// CLK high
		clk_in();
		tm1637_bitDelay();
		data = data >> 1;
	}

	// Wait for acknowledge
	// CLK to zero
	clk_out();
	dio_in();
	tm1637_bitDelay();

	// CLK to high
	clk_in();
	tm1637_bitDelay();
	uint8_t ack = gpio_read(m_clk_gpio, m_pinDIO);
	if (ack == 0) dio_out();


	tm1637_bitDelay();
	clk_out();
	tm1637_bitDelay();

	return ack;
}


static int invert_display = 0; // 1=> inverted
void tm1637_invert_display(int val)
{
	invert_display = val;
}

static void reorient_segments(uint8_t segs[], const uint8_t segments[], uint8_t length)
{
	memcpy(segs, segments, length);
	if(invert_display == 0) return;

	// reverse the array
	for(int i = 0; i < length/2; i++) {
		segs[i] = segments[length-i-1];
		segs[length-i-1] = segments[i];
	}
	//return;

	// turn each segment upside down
	// XGFEDCBA needs to map to
	// XGCBAFED
	for(int i = 0; i < length; i++) {
		segs[i] = (segs[i] & 0b11000000)
			| ((segs[i]>>3) & 0b111) // ..FED... -> .....FED
			| ((segs[i] & 0b111) << 3); // .....CBA -> ..CBA...			 
	}

}

void tm1637_setSegments(const uint8_t segments[], uint8_t length, uint8_t pos)
{
	uint8_t segs[length];
	reorient_segments(segs, segments, length);

	// Write COMM1
	tm1637_start();
	tm1637_writeByte(TM1637_I2C_COMM1);
	tm1637_stop();

	// Write COMM2 + first digit address
	tm1637_start();
	tm1637_writeByte(TM1637_I2C_COMM2 + (pos & 0x03));

	// Write the data bytes
	for (uint8_t k=0; k < length; k++)
		tm1637_writeByte(segs[k]);

	tm1637_stop();

	// Write COMM3 + brightness
	tm1637_start();
	tm1637_writeByte(TM1637_I2C_COMM3 + (m_brightness & 0x0f));
	tm1637_stop();
}




//
//      A
//     ---
//  F |   | B
//     -G-
//  E |   | C
//     ---
//      D
const uint8_t digitToSegment[] = {
	// XGFEDCBA
	0b00111111,    // 0
	0b00000110,    // 1
	0b01011011,    // 2
	0b01001111,    // 3
	0b01100110,    // 4
	0b01101101,    // 5
	0b01111101,    // 6
	0b00000111,    // 7
	0b01111111,    // 8
	0b01101111,    // 9
	0b01110111,    // A
	0b01111100,    // b
	0b00111001,    // C
	0b01011110,    // d
	0b01111001,    // E
	0b01110001     // F
};


static const uint8_t minusSegments = 0b01000000;


uint8_t tm1637_encodeDigit(uint8_t digit)
{
	return digitToSegment[digit & 0x0f];
}


void tm1637_clear()
{
	uint8_t data[] = { 0, 0, 0, 0 };
	tm1637_setSegments(data, 4, 0);
}

void tm1637_showDots(uint8_t dots, uint8_t* digits)
{
	for(int i = 0; i < 4; ++i)
	{
		digits[i] |= (dots & 0x80);
		dots <<= 1;
	}
}



//TM1637Display::TM1637Display(uint8_t pinClk, uint8_t pinDIO, unsigned int bitDelay)
void tm1637_init_stm32(GPIO_TypeDef* clk_gpio,  uint16_t clk_pin, GPIO_TypeDef* dio_gpio, uint16_t dio_pin)
	//void tm1637_init(uint8_t clk, uint8_t dio)
{
	int bitDelay = DEFAULT_BIT_DELAY;
	// Copy the pin numbers
	m_clk_gpio = clk_gpio;
	m_pinClk = clk_pin;
	m_dio_gpio = dio_gpio;
	m_pinDIO = dio_pin;
	m_bitDelay = bitDelay;

	// Set the pin direction and default value.
	// Both pins are set as inputs, allowing the pull-up resistors to pull them up
	gpio_in(clk_gpio, clk_pin);
	gpio_in(dio_gpio, dio_pin);
	clk_write(0);
	dio_write(0);

	tm1637_setBrightness(0xff, true);
	tm1637_clear();
	//uint8_t data[] = {0, 0 ,0 ,0}; // { 0xff, 0xff, 0xff, 0xff };
	//tm1637_setSegments(data, 4, 0);
}

void tm1637_init_pico(uint8_t clk, uint8_t dio)
{
	tm1637_init_stm32(0, clk, 0, dio);
}

void tm1637_showNumberBaseEx(int8_t base, uint16_t num, uint8_t dots, bool leading_zero,
		uint8_t length, uint8_t pos)
{
	bool negative = false;
	if (base < 0) {
		base = -base;
		negative = true;
	}


	uint8_t digits[4];

	if (num == 0 && !leading_zero) {
		// Singular case - take care separately
		for(uint8_t i = 0; i < (length-1); i++)
			digits[i] = 0;
		digits[length-1] = tm1637_encodeDigit(0);
	}
	else {
		for(int i = length-1; i >= 0; --i)
		{
			uint8_t digit = num % base;

			if (digit == 0 && num == 0 && leading_zero == false)
				// Leading zero is blank
				digits[i] = 0;
			else
				digits[i] = tm1637_encodeDigit(digit);

			if (digit == 0 && num == 0 && negative) {
				digits[i] = minusSegments;
				negative = false;
			}

			num /= base;
		}

		if(dots != 0)
		{
			tm1637_showDots(dots, digits);
		}
	}
	tm1637_setSegments(digits, length, pos);
}


void tm1637_showNumberDecEx(int num, uint8_t dots, bool leading_zero,
		uint8_t length, uint8_t pos)
{
	tm1637_showNumberBaseEx(num < 0? -10 : 10, num < 0? -num : num, dots, leading_zero, length, pos);
}

void tm1637_showNumberDec(int num, bool leading_zero, uint8_t length, uint8_t pos)
{
	tm1637_showNumberDecEx(num, 0, leading_zero, length, pos);
}

void tm1637_show_dec(int num)
{
	tm1637_showNumberDecEx(num, 0, 0, 4, 0);
}


void tm1637_showNumberHexEx(uint16_t num, uint8_t dots, bool leading_zero,
		uint8_t length, uint8_t pos)
{
	tm1637_showNumberBaseEx(16, num, dots, leading_zero, length, pos);
}

void tm1637_show_hhmm(struct tm *t)
{
	//void tm1637_showNumberDecEx(int num, uint8_t dots, bool leading_zero,               uint8_t length, uint8_t pos)
	tm1637_showNumberDecEx(t->tm_hour * 100 + t->tm_min,
			1, // dots
			1, /* leading 0 */
			4, /* length */
			0  /* pos */
			);
}








