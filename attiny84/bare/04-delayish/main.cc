//#define F_CPU 8000000UL

#include <avr/io.h>
//#include <util/delay.h>

#include <basal.h>
#include <gpio.h> // my HAL
#include <delay.h> // my HAL

#define LED1 GPIOA, (1<<0) // PA0 . Physical pin 13
#define LED2 GPIOB, (1<<2) // PB2 . Physical pin 5

int main()
{
	gpio_out(LED1);
	gpio_out(LED2);

	for(;;) {
		gpio_set(LED1);
		gpio_set(LED2);
		delayish(100);
		//_delay_ms(100);

		gpio_clr(LED1);
		gpio_clr(LED2);
		delayish(900);
		//_delay_ms(300);
	}
}
