.syntax unified
.cpu cortex-m0plus
.thumb

// See ARM v6 reference manual, section B1.5.6
.EQU    EXC_FRAME_R0_OFFSET,          0x00
.EQU    EXC_FRAME_R1_OFFSET,          0x04
.EQU    EXC_FRAME_R2_OFFSET,          0x08
.EQU    EXC_FRAME_R3_OFFSET,          0x0C
.EQU    EXC_FRAME_R12_OFFSET,         0x10
.EQU    EXC_FRAME_LR_OFFSET,          0x14
.EQU    EXC_FRAME_RETURN_ADDR_OFFSET, 0x18
.EQU    EXC_FRAME_XPSR_OFFSET,        0x1C



//.extern gpio_put1

.align 4
.global svc_handler
.global  isr_svcall2
// r7 must contain the function's call number
svc_handler:
isr_svcall2:
	push {lr}
	//mrs r3, psp
	pop {pc}
	movs r0, #4
	mov r1, lr
	tst r0, r1
	//beq stacking_used_msp
	mrs r0, psp
	bx r1
	PUSH	{LR}
	//MRS	R0, MSP
	//TST	LR, #4
	//MRSEQ	R0, MSP
	//MRSNE	R0, PSP
	POP	{PC}
	push {r5, r7, lr}
	mrs	r5, psp
	ldr r0, [r5, #EXC_FRAME_R0_OFFSET]
	cmp r7, #0
	beq.n isr_svcall_0
	cmp r7, #1
	beq.n isr_svcall_1
isr_svcall_exit:
	pop {r5, r7, pc}
isr_svcall_0: // gpio_put
	movs r1, #0
	//bl gpio_put1
	b.n isr_svcall_exit
isr_svcall_1: // gpio_put
	movs r1, #1
	//bl gpio_put1
	b.n isr_svcall_exit


.align 4
.global make_call
// IN:
//	r0 - pin number to set/clr
//	r1 - function call number:
//		0 : sets pin low
//		1 : sets pin high
make_call:
	push {r7, lr}
	mov r7, r1 // coincidentally the function number is also the level
	//movs r0, #66 // for testing purposes
	SVC 0
	pop {r7, pc}
