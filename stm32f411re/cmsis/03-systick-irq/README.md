# Systick

## See also

* [write-up](https://mcturra2000.wordpress.com/2021/11/18/using-cmsis-and-systick-to-blink-an-led-on-an-stm32f4/)
that explains how it all works.


## Status

2022-12-25	Confirmed working.

2021-11-18	Started. Works.
