//#define F_CPU 8000000UL

#include <avr/io.h>
//#include <util/delay.h>

#include <gpio.h> // my HAL
#include <delay.h>


#define LED1 GPIOA, (1<<0) // PA0 . Physical pin 13
#define LED2 GPIOB, (1<<2) // PB2 . Physical pin 5

int main()
{
	gpio_out(LED1);
	//gpio_out(LED2);

	for(;;) {
		gpio_set(LED1);
		delayish_us(64);

		gpio_clr(LED1);
		delayish_us(1);
	}
}
