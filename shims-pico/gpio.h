#pragma once
#include <stdint.h>
#include "hardware/gpio.h"

#ifdef __cplusplus
extern "C" {
#endif  

#define GPIO_PIN_RESET 0
#define GPIO_PIN_SET 1


typedef int GPIO_TypeDef; // a dummy type to make it compatible with STM32

void gpio_out(GPIO_TypeDef *GPIOx, uint16_t  GPIO_Pin);
void gpio_in(GPIO_TypeDef *GPIOx, uint16_t  GPIO_Pin);
void gpio_inpull(GPIO_TypeDef *GPIOx, uint16_t  GPIO_Pin);
int gpio_read(GPIO_TypeDef *GPIOx, uint16_t  GPIO_Pin);
void gpio_write(GPIO_TypeDef *GPIOx, uint16_t  GPIO_Pin, int level);

typedef enum {INPUT, OUTPUT, INPUT_PULLUP, INPUT_PULLDOWN} pi_gpio_mode_e;

#define HIGH 1
#define LOW 0
//#define INPUT GPIO_IN
//#define OUTPUT GPIO_OUT
//void digitalWrite(unsigned int GPIO_Pin, int value);
void pinMode(unsigned int GPIO_Pin, int mode);
static inline void digitalWrite(uint gpio, int val) { gpio_put(gpio, val); }
static inline bool digitalRead(uint gpio) { return gpio_get(gpio); }


#ifdef __cplusplus
}
#endif

