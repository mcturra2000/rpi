#pragma once

/* Example in project 21 */

#include "hardware/uart.h"
#include "hardware/irq.h"
#include "hardware/gpio.h"


extern int uril_len;
extern char uril_buf[132];
void uart_rx_handler();
void uril_pause();
void uril_resume();
void uril_init();

