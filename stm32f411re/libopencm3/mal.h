/** 
 * @file mal.h
 * @brief Declarations for Mark's Abstraction Layer 
 *
 * mal.h to sort out by mc
 */
#pragma once
#include <conf.h>




#include <stdint.h>

#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/cm3/systick.h>
//#include <libopencm3/stm32/i2c.h>


void mal_init_systick(void);


//void mal_max7219_init(void);
//void mal_max7219_show_count(int count);
//void mal_max7219_tfr(uint8_t address, uint8_t value);

void mal_spi_init_std(void);


#if 1
typedef struct {
	uint32_t port;
	int num;
	//rcc_periph_clken rcc;
	//rcc_periph_clken rcc;
	int rcc;
} pin_t;

extern pin_t pb12, pc13, pc14;

#define PB12 &pb12
#define PC13 &pc13
#define PC14 &pc14
#endif

/**
 * @brief This is doc for mal_max7219_init
 *
 * Formula: Perimeter = 2*PI*r
 * @param[in] pin
 */
void pin_toggle(pin_t* pin);

void pin_out(pin_t* pin);
void pin_high(pin_t* pin);
void pin_low(pin_t* pin);

#if 1 // TODO probably reinstate - but seems to conflict with /usr/include/newlib/sys/types.h
typedef struct {
	uint32_t peri;
	int rcc;
	uint32_t irq;
} mal_timer_t;

extern const mal_timer_t timer2;
#define TIMER2 &timer2
extern const mal_timer_t timer5;
#define TIMER5 &timer5
void mal_timer_init(const mal_timer_t* timer, int freq);
#endif


void mal_usart_init(void);
void mal_usart_print(const char* str);

