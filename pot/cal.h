#pragma once
// calendrical functions
/*
 * struct tm from time.h:
tm_sec	int	seconds after the minute	0-60*
tm_min	int	minutes after the hour	0-59
tm_hour	int	hours since midnight	0-23
tm_mday	int	day of the month	1-31
tm_mon	int	months since January	0-11
tm_year	int	years since 1900
tm_wday	int	days since Sunday	0-6
tm_yday	int	days since January 1	0-365
tm_isdst	int	Daylight Saving Time flag
The Daylight Saving Time flag (tm_isdst) is greater than zero if Daylight Saving Time is in effect, zero if Daylight Saving Time is not in effect, and less than zero if the information is not available.

* tm_sec is generally 0-59. The extra range is to accommodate for leap seconds in certain systems.
* See: https://cplusplus.com/reference/ctime/tm/
*  Functions: mktime(), localtime(), gmtime()
 */

#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

int isleap(int y);
int get_dims(int y, int mon0);
int get_cum_dims(int y, int mon);
int mildays(int y, int mon, int mday);
//int isbst(int year0, int mon0, int day1, int hr);
int isbst_tm(struct tm *t);
//int adjbst(int *yr0, int *mon0, int *day1, int *hr);
int cal_wday (int y, int mon, int mday);
void cal_reduce_timevalue(long long timeval, struct tm *t);
int cal_adjbst_tm(struct tm *t);

#ifdef __cplusplus
}
#endif

