#pragma once

#include "pico/time.h"
//#include "pulse.h"
//#include "debounce.h"

#ifdef __cplusplus
extern "C" {
#endif

#if 0
class Rotary {
	public:
		Rotary(uint sia, uint sib, uint sw);
		bool sw_falling();
		bool sw_rising();
		int change();
	private:
		uint m_sia, m_sib;
		Debounce m_sw;
		Pulse m_pulse;
		int m_prev_state = 0, m_cur_state = 0, m_count_change = 0;
		int state();
};
#endif

typedef struct {
	uint gpio_anti; // anti-clockwise
	uint gpio_clock; // clockwise
	int prev_state;
	int cur_state;
	int count_change;
	uint delay;
	absolute_time_t later;
} rotary_t;


void rotary_init(rotary_t* rot, int gpio_anti, int gpio_clock);
int rotary_change(rotary_t* rot);

#ifdef __cplusplus
}
#endif
