#pragma once

#ifdef __cplusplus
extern "C" {
#endif


#if 1
#include <linux/gpio.h>
//typedef struct gpiohandle_request pin_t;
typedef struct gpio_v2_line_request pin_t;
#else
#include <gpiod.h>
typedef struct gpiod_line_bulk pin_t;
#endif


int gpio_pin_init(pin_t *pin, int pin_num, enum gpio_v2_line_flag flags);
int gpio_in(pin_t *pin, int pin_num);
int gpio_out(pin_t *pin, int pin_num);
int gpio_clr(pin_t *pin);
int gpio_set(pin_t *pin);
int gpio_put(pin_t *pin, int val);
void gpio_deinit();
int gpio_get(pin_t *pin);
int gpio_toggle(pin_t *pin);
int gpio_debounced(pin_t *pin, int pin_num);
int gpio_release(pin_t *pin);

#ifdef __cplusplus
}
#endif

