/*
 *
 * SPI SPEED: 500K works, 1000K is too fast
 */

#if 1
#include <stdio.h>
#include <string.h>

#include "doglcd.h"
#include <delay.h>
#include "gpio.h"
#include "spi.h"

#define LOW 0
#define HIGH 1

#ifdef PICO_BOARD
#include "hardware/spi.h"
#include "pi.h"
// db7.138
// C1 5V
#define RS 4	// C2
#define CS 5	// C3
#define SCK  6	// C4
#define MOSI 7	// C5
// C6 GND

//#define SPI_PORT spi0
spi_t spi = {.hspi = spi0, .GPIO_Pin = CS};
#endif


#ifdef USE_HAL_DRIVER
extern SPI_HandleTypeDef hspi1;
spi_t spi = {.hspi = &hspi1, .GPIOx = GPIOA, .GPIO_Pin = GPIO_PIN_10 };
#define RS 			GPIOA, GPIO_PIN_9
//#define CS 			, GPIO_PIN_10
//#define SPI_PORT 	&hspi1
#endif


// You must set up the pins yourself
void doglcd_test()
{
	  doglcd_init();
	  int count = 0;
	  char str[100];
	  while(1) {
		  //doglcd_gotoxy(0, 0);
		  doglcd_home();
		  //delay(100);
		  sprintf(str, "Count: %d", count++);
		  doglcd_write_str(str);
		  delay(1000);
	  }
}
/* rs_val = LOW to send command, HIGH to send ASCII char
*/
void doglcd_send_byte(int rs_val, uint8_t val)
{
	digitalWrite(RS, rs_val);
	spi_send_bytes(&spi, &val, 1);
	//sleep_ms(60);
}

void doglcd_send_cmd(uint8_t cmd)
{
	doglcd_send_byte(LOW, cmd);
	delay(1); // a small delay seems necessary
}

void doglcd_write_char(char c)
{
	doglcd_send_byte(HIGH, c);	
}

void doglcd_write_str(const char *str)
{
	digitalWrite(RS, HIGH);
	spi_send_bytes(&spi, (const uint8_t *) str, strlen(str));
	//digitalWrite(CS, LOW);
	//spi_write_blocking(SPI_PORT, (unsigned char*) str, strlen(str));
	//digitalWrite(CS, HIGH);

}

void doglcd_double_height(bool tall)
{
	// doesn't seem to work properly
	doglcd_send_cmd((1<<4) + (tall ? 0b1000 : 0));
}
void doglcd_home()
{
	// goes to top-left
	doglcd_send_cmd(1<<1);
}

void doglcd_cls()
{
	doglcd_send_cmd(1);
	doglcd_home();
	//delay(50);
}

// move cursor to position x,y (1st Row and 1st column is position 0,0)
// for a 163 display : x = 0..15   y = 0..2
// 3-line display assumed here. for others, see:
// https://forum.arduino.cc/t/library-for-electronic-assembly-dog-displays/36988

void doglcd_gotoxy(int x, int y) {
	int addr = 0x80;
	if (y<1) addr = 0x80+x;
	if (y==1)
		addr = 0x90+x;
	else if (y>1) addr = 0xA0+x;
	doglcd_send_cmd(addr);
}




void doglcd_init()
{
#if 0
        spi_init(SPI_PORT, 500 * 1000); // 500K works, 1000K is too fast
        gpio_set_function(SCK, GPIO_FUNC_SPI);
        gpio_set_function(MOSI, GPIO_FUNC_SPI);

        pi_gpio_out(RS);
        pi_gpio_out(CS);
        gpio_put(CS, 1);
#endif

#if 1 // prolly for 5V
        const uint8_t contrast = 0x70  | 0b1000; // from 0x7C
        const uint8_t display = 0b1111; // ori 0x0F
        uint8_t cmds[] = {0x39, 0x1D, 0x50, 0x6C, contrast , 0x38, display, 0x01, 0x06};
#else
        uint8_t cmds[] = {0x39, 0x15, 0x55, 0x6e, 0x72, 0x38, 
                0x0F, 0x01, 0x06};
#endif
	for(int i = 0; i< sizeof(cmds); i++) doglcd_send_cmd(cmds[i]);

}

#endif
