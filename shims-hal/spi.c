//#include <stddef.h>
//#include <stdint.h>
#include <spi.h>

#ifdef HAL_SPI_MODULE_ENABLED

void spi_send_bytes(spi_t *spi, const uint8_t *data, size_t len)
{
	HAL_GPIO_WritePin(spi->GPIOx, spi->GPIO_Pin , GPIO_PIN_RESET);
	HAL_SPI_Transmit(spi->hspi, (uint8_t *) data, len, 1000);
	//if(status != HAL_OK) Error_Handler();
	HAL_GPIO_WritePin(spi->GPIOx, spi->GPIO_Pin , GPIO_PIN_SET);
}

#endif // HAL_SPI_MODULE_ENABLED
