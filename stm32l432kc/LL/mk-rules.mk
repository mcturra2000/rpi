#CC = arm-none-eabi-gcc
#LD = arm-none-eabi-ld
#BIN = arm-none-eabi-objcopy
include ../../mk-rules.mk


#SRCS := $(wildcard jagan/*.c) $(wildcard *c)

DEBUG = 1
ifdef DEBUG
CFLAGS += -O0 -ggdb # turn off optimsations
else
CFLAGS += -O2
endif

#IDIR =jagan
#CFLAGS += -I. -Ijagan
CFLAGS += -mthumb -mcpu=cortex-m4
CFLAGS += -mfloat-abi=hard
CFLAGS += -mfpu=fpv4-sp-d16 # doesn't seem to be necessary
#CFLAGS += -I..
CFLAGS += -nostdlib
CFLAGS += -fdata-sections -ffunction-sections # for unused code
#CFLAGS += -std=c2x

#CMSIS = /home/pi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.2/Drivers/CMSIS
CMSIS = /home/pi/STM32Cube/Repository/STM32Cube_FW_L4_V1.16.0/Drivers/CMSIS
CFLAGS += -I$(CMSIS)/Device/ST/STM32L4xx/Include/
CFLAGS += -I$(CMSIS)/Include/
VPATH += ..

CFLAGS += -DSTM32L432xx
CFLAGS += -DUSE_FULL_LL_DRIVER
CFLAGS += -I/home/pi/STM32Cube/Repository/STM32Cube_FW_L4_V1.16.0/Drivers/STM32L4xx_HAL_Driver/Inc/
VPATH += /home/pi/STM32Cube/Repository/STM32Cube_FW_L4_V1.16.0/Drivers/STM32L4xx_HAL_Driver/Src

LDFLAGS = -T ../linker.ld
#LDFLAGS = -L$(CMSIS)/Lib/GCC


TARG := app
#TARG := button
#TARG := clock
#TARG := blink
#TARG := uart_demo
#TARG := uart_printf
#TARG := systick
#TARG := debounce_demo


OBJS += startup.o system_stm32l4xx.o stm32l4xx_ll_utils.o  main.o 

# DEMOS
#app_OBJS = stm32l4xx_ll_rcc.o stm32l4xx_ll_utils.o startup.o system_stm32l4xx.o app.o
#blink_OBJS = $(common_OBJS) delay.o
#button_OBJS = $(common_OBJS)
#clock_OBJS = $(common_OBJS)
#debounce_demo_OBJS = $(common_OBJS) debounce.o uart.o jg_printf.o delay.o
#systick_OBJS = $(common_OBJS) 
#uart_demo_OBJS = $(common_OBJS) uart.o delay.o
#uart_printf_OBJS = $(common_OBJS) uart.o jg_printf.o delay.o


app.elf: $(OBJS)
	$(LD) $(LDFLAGS) -o app.elf $(OBJS) -L$(CMSIS)/Lib/GCC  # -larm_cortexM4lf_math
	

	
startup.o : startup.s
	$(CC) $(CFLAGS) -c $^

#init.o : jagan/init.c
#delay.o : jagan/delay.c
#gpio.o  : jagan/gpio.c
#jg_printf.o : jagan/jg_printf.o
#uart.o  : jagan/uart.c

	

