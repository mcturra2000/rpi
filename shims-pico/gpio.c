#include <gpio.h>
//#include <hardware/gpio.h>
void gpio_out(GPIO_TypeDef *GPIOx, uint16_t  GPIO_Pin)
{
	gpio_init(GPIO_Pin);
	gpio_set_dir(GPIO_Pin, 1);
}

void gpio_in(GPIO_TypeDef *GPIOx, uint16_t  GPIO_Pin)
{
	gpio_init(GPIO_Pin);
	gpio_set_dir(GPIO_Pin, 0);
}

void gpio_inpull(GPIO_TypeDef *GPIOx, uint16_t  GPIO_Pin)
{
	gpio_in(GPIOx, GPIO_Pin);
	gpio_pull_up(GPIO_Pin);
}


int gpio_read(GPIO_TypeDef *GPIOx, uint16_t  GPIO_Pin)
{
	return gpio_get(GPIO_Pin);  
}

void gpio_write(GPIO_TypeDef *GPIOx, uint16_t  GPIO_Pin, int level)
{
	gpio_put(GPIO_Pin, level);
}

void pinMode(unsigned int pin, int mode)
{
	gpio_init(pin);
	gpio_set_dir(pin, mode);
}



