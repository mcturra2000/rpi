#include <rotary.h>






void rotary_init(rotary_t* r, GPIO_TypeDef *port, uint32_t pin_minus, uint32_t pin_plus)
{
	r->port = port;
	r->pin_minus = pin_minus;
	r->pin_plus = pin_plus;
	//r->count = 0;
	r->state = 0;

	gpio_in(port, pin_minus);
	gpio_pullup(port, pin_minus);
	gpio_in(port, pin_plus);
	gpio_pullup(port, pin_plus);

}

int rotary_poll(rotary_t* r)
{
	//int new_state = (gpio_get(r->port, r->pin_minus) << 1) | gpio_get(r->port, r->pin_plus);
	int new_state = (gpio_get(r->port, r->pin_plus) << 1) | gpio_get(r->port, r->pin_minus);
	int delta = 0;
	if((r->state == 2) & (new_state == 3)) {
		delta = 1;
	}
	if((r->state == 1) && (new_state == 3)) {
		delta = -1;
	}
	r->state = new_state;

	return delta;

}

