include ../../mk-rules.mk


#CFLAGS += -DSTM32F411xx
CFLAGS += -DSTM32F411xE -DUSE_FULL_LL_DRIVER
CFLAGS += -I$(RPI)/stm32f411re/LL

CFLAGS += -I$(CUBE)/STM32F4xx_HAL_Driver/Inc
CFLAGS += -I$(CMSIS)/Device/ST/STM32F4xx/Include/
CFLAGS += -I$(CMSIS)/Include/

OBJS += startup_stm32f411retx.o
OBJS += system_stm32f4xx.o stm32f4xx_ll_gpio.o stm32f4xx_ll_utils.o
OBJS += main.o 

# -mthumb turns out to be important (for e.g. memset)
LDFLAGS = -nostdlib $(CPU) -T ../linker.ld

VPATH += ..
VPATH += $(CUBE)/STM32F4xx_HAL_Driver/Src

#arm-none-eabi-gcc -o "08-pwm-dma-sine.elf" @"objects.list"   -mcpu=cortex-m4 -T"/home/pi/STM32CubeIDE/f411/08-pwm-dma-sine/STM32F411RETX_FLASH.ld" --specs=nosys.specs -Wl,-Map="08-pwm-dma-sine.map" -Wl,--gc-sections -static --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -Wl,--start-group -lc -lm -Wl,--end-group

# NEWLIBFLAGS = -Wl,--gc-sections -static --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -Wl,--start-group -lc -lm -Wl,--end-group

all: app.bin

app.elf: $(OBJS)
	arm-none-eabi-gcc $(LDFLAGS)   -o app.elf  $(OBJS)  # $(NEWLIBFLAGS)  # -L$(CMSIS)/Lib/GCC  # -larm_cortexM4lf_math



%.o : %.cc
	$(CXX) $(CFLAGS) -c -o $@ $<
	
%.o : %.c
	$(CC) $(CFLAGS) -c -o $@ $<

%.o : %.s
	$(CC) -c $^
