#include <conf.h>
//#include <libopencm3/stm32/rcc.h>
//#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/i2c.h>
#include <i2c.h>
#include <delay.h>
#include <gpio.h>

#include <stdlib.h>
//#include <string.h>

//#include "mal.h"

#include "ledmat.h"

typedef uint32_t u32;

#define DEMO_WEAK_OVERRIDE
#ifdef DEMO_WEAK_OVERRIDE
void i2c_ship(uint32_t i2c, uint8_t addr, const uint8_t *w, size_t wn) 
{
	gpio_set(LED_BUILTIN);


	//i2c_write7_v1(i2c, addr, w, wn) begin:
	while ((I2C_SR2(i2c) & I2C_SR2_BUSY));
	i2c_send_start(i2c);

	/* Wait for the end of the start condition, master mode selected, and BUSY bit set */
	while ( !( (I2C_SR1(i2c) & I2C_SR1_SB)
				&& (I2C_SR2(i2c) & I2C_SR2_MSL)
				&& (I2C_SR2(i2c) & I2C_SR2_BUSY) ));

	i2c_send_7bit_address(i2c, addr, I2C_WRITE);

	/* Waiting for address is transferred. */
	while (!(I2C_SR1(i2c) & I2C_SR1_ADDR));

	/* Clearing ADDR condition sequence. */
	(void)I2C_SR2(i2c);

	for (size_t i = 0; i < wn; i++) {
		i2c_send_data(i2c, *(w+i));
		while (!(I2C_SR1(i2c) & (I2C_SR1_BTF)));
	}
	//i2c_write7_v1(i2c, addr, w, wn) end

	i2c_send_stop(i2c);
}
#endif //DEMO_WEAK_OVERRIDE


static uint8_t pattern1[] = { 
	0b10000001, 
	0b01000010, 
	0b00100100, 
	0b00010000,
	0b00001000, 
	0b00100100, 
	0b01000010, 
	0b10000001 
};

static uint8_t pattern2[] = { // the letter P, with some orientation frills
	0b11110001, 
	0b10001000, 
	0b10001000, 
	0b11110000,
	0b10000000, 
	0b10000000, 
	0b10000001, 
	0b10000010 
};


void show(void)
{
	int len;
	uint8_t* grid = ledmat_grid(&len);
	i2c_ship(I2C1, LEDMAT_SID, grid, len);
}

void random_pattern(void)
{
	while(1) {
		uint16_t rc = rand();
		uint8_t r = rc, c = rc >> 8;
		ledmat_toggle(r, c);
		ledmat_show();
	}
}

void scrolling_pattern(void)
{
	uint8_t* pattern = pattern1;
	while(1) {
		static int offset = 0; // for scrolling purposes
		for (int r = 0; r < 8; ++r) {
			int r1 = (r + offset) %8;
			led_set_row(r1, pattern[r]);
		}
		offset++;
		ledmat_show();
		delayish(100);
	}
}

int main(void)
{
	gpio_out(LED_BUILTIN);
	//gpio_set(LED_BUILTIN);
	i2c1_init();
	ledmat_init((uint32_t) I2C1);


	if(1) 
		random_pattern();
	else
		scrolling_pattern();
}
