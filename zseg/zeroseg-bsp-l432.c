#include "gpio.h"
#include "spi.h"

#include "zeroseg.h"


#define CS      A3
#define SCK     A4
#define MOSI    A6

void max7219_bsp_tfr(uint8_t address, uint8_t value)
{
        SPI_TypeDef *spi = SPI1;
        uint16_t val = ((uint16_t) address << 8) | value;
        gpio_clr(CS);
        spi->DR = val;
        while(!(spi->SR & SPI_SR_TXE)); // wait for transfer to finish
        while((spi->SR & SPI_SR_BSY)); // wait until not busy
        gpio_set(CS);
}

void max7219_bsp_init(void)
{
        gpio_out(CS);
        gpio_set(CS);
        gpio_alt(SCK, 5); // PA5 clk setup
        gpio_alt(MOSI, 5); // PA7 mosi setup
        RCC->APB2ENR    |= RCC_APB2ENR_SPI1EN; // enable SPI1 clock
        SPI1->CR1       |= SPI_CR1_MSTR; // enable master mode
        spi_size_16(SPI1);
        SPI1->CR2 |= SPI_CR2_SSOE; // SS output enabled
        SPI1->CR1 |= (0b010 << SPI_CR1_BR_Pos); // baud rate: fpclk/8
        SPI1->CR1 |= SPI_CR1_SPE; // enable spi

}

