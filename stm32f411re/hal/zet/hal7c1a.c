/* hal7c1a.c
 *
 * MX:
 *	Clock: 100MHz
 * 	DMA settings:
 * 		Add DMA request I2C1_TX
 * 		Enable all the interrupts (don't know if necessary)
 *	TIM10:
 *		Activate
 *		Enable IRQ. Set priority 5 (relatively low)
 *		PSC: 151
 *		Counter period 65535 (default)
 *		auto-reload: Enable
 *		[Implied frequency is 10.04Hz = 100MHz / 152 / 65536]
 */
#include "main.h"
#include "ssd1306.h"

extern I2C_HandleTypeDef hi2c1;
extern TIM_HandleTypeDef htim10;

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance != TIM10) return;
	if(HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY) return;
	HAL_I2C_Master_Transmit_DMA(&hi2c1, 0x3c<<1, (uint8_t*) &ssd1306_screen, sizeof(ssd1306_screen));
}

void app_main()
{
	// --- std CubeIDE gloop ---
	ssd1306_init_display(64, (uint32_t) &hi2c1);
	HAL_Delay(100); // device seems to need a little time to kick in
	HAL_TIM_Base_Start_IT(&htim10);
	int counter = 0;
	while (1)
	{
	  ssd1306_printf("Counter = %d\n", counter++);
	  HAL_Delay(1000);
	}
}
