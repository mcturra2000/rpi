#include "pico/stdlib.h"
#include "ssd1306.h"
#include <stdio.h>
#include <stdlib.h>
#include "pi.h"
#include "hardware/xosc.h"
#include "hardware/structs/rosc.h"


// From 
// https://www.taurusandscorpio.net/list-of-magic-8-ball-responses/

char *responses[] = {
	"It is certain",
	"Without a doubt",
	"You may rely on it",
	"Yes definitely",
	"It is decidedly so",
	"As I see it, yes",
	"Most likely",
	"Yes",
	"Outlook good",
	"Signs point to\nyes",

	"Reply hazy try\nagain",
	"Better not tell\nyou now",
	"Ask again later",
	"Cannot predict\nnow",
	"Concentrate and \nask again",

	"Don't count on\nit",
	"Outlook not so\ngood",
	"My sources say\nno",
	"Very doubtful",
	"My reply is no"
};

int main()
{
	//init_i2c();

	int h = 32;
	if(0) h = 64;
	int sda = 6;
	sda = 4;
	init_display(h, sda);

	volatile uint32_t b=0;
	for(int j = 0; j < 32; ++j) {
		b <<= 1;
		b |= (rosc_hw->randombit & 1);
	}

	int r = b % count_of(responses);
	ssd1306_print(responses[r]); 
	show_scr();

	for(;;) {
		tight_loop_contents();
	}
	return 0;
}
