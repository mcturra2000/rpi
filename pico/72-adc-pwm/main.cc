/* loop through the ADCs printing their values in a round-robin fashion
 * User ADC inputs are on 0-3 (GPIO 26-29), the temperature sensor is on ADC 4
 */

#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "hardware/adc.h"

#include <pi.h>
#include <notes.h>

#define SPK 17

int main() 
{
	stdio_init_all();

	// suggested ADC stabilisation in adc.md. Benefits seem dubious
#if 1
	gpio_set_dir(23, GPIO_OUT);
	gpio_put(23, 1);
#endif

	adc_init();
	//pi_pwm_init(SPK, 400, 50);
	//for(;;);

	int i;
	for(i=0; i<4; i++) adc_gpio_init(26+i);
	adc_set_round_robin(0x1f); // all of the adcs in a round-robin fashion

	int adc0 = -1, adc1;
	const int DIV = 38; // 38 = 4096 (ADC) / 107 (num notes)

again:
	for(i=0;i<5;i++) {
		//adc_select_input(i);
		int v = adc_read(); // result in range 0-4095 inc.
		if(i==0) adc1 = v / DIV;
		//printf("%5d ", (int) v);
	}
	if(adc0 != adc1) {
		adc0 = adc1;
		//auto index = adc1/38; // 38 = 4096 (ADC) / 107 (num notes)
		pi_pwm_init(SPK, notes[adc0].freq, 50);
	}
	//puts("");
	sleep_ms(200);
	goto again;

	return 0;
}

