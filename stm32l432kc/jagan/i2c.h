#pragma once

#include "conf.h"

void i2c_bang(uint8_t sid, uint8_t *data, int n);
void i2c_init_bang(void);
void i2c_ship(uint32_t i2c, uint8_t addr, const uint8_t *w, size_t wn);

