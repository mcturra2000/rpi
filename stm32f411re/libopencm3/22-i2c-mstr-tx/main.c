#include <conf.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
//#include <libopencm3/stm32/timer.h>
//#include <libopencm3/stm32/spi.h>
//#include <libopencm3/stm32/dma.h>
#include <libopencm3/stm32/i2c.h>

#include <string.h>

#include "mal.h"
#include <delay.h>
#include <i2c.h>


typedef uint32_t u32;

#define LED PC13


static void myputs(const char *str)
{
	mal_usart_print(str);
	mal_usart_print("\r\n");
}


int main(void)
{
	pin_out(LED);
	mal_usart_init();

	myputs("");
	myputs("=============================");
	myputs("I2C example: master");

	i2c1_init();

	const uint8_t addr = 0x4; // set this to whatever your slave address is
	uint8_t data = 0;
	while(1) {
		i2c_transfer7(I2C1, addr, &data, 1, 0, 0); // write
		data++;
		//i2c_send_stop(I2C1); // not sure if necessary
		/*
		   i2c_send_start(I2C1);
		   i2c_send_7bit_address(I2C1, slave, I2C_READ);
		   volatile uint8_t v = i2c_get_data(I2C1);
		   i2c_send_stop(I2C1);
		   */
		//mal_delayish(10);
		//pin_toggle(LED);
		delayish(1);
	}

}
