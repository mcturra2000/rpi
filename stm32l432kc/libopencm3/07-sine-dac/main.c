//#include <conf.h>
#include <delay.h>
#include <gpio.h>
#include <math.h>
//#include <inttypes.h>
#include <libopencm3/stm32/rcc.h>
//#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/dac.h>
//#include <libopencm3/stm32/spi.h>

#include <libopencm3/stm32/l4/nvic.h>
//#include "mal.h"


//#include <math.h>
//typedef uint32_t u32;

#define LED	LED_BUILTIN  //PB3


const uint32_t fs = 22000;

//extern uint32_t SystemCoreClock;


extern const int SINEN;
extern const uint16_t sine[1024];
const float fw = 500.0;
#define SHIFT 5 // shifting doesn't seem to help particularly
const float di = (float)1024 * fw / fs;
const int di_shifted = (int)(di * (float)(1<<SHIFT));
//const int di = 

void tim6_dacunder_isr(void)
{
	timer_clear_flag(TIM6, TIM_SR_UIF ); // TIM6->SR = 0;
	gpio_set(LED);


	volatile static int i_shifted = 0;

	uint16_t val;
	
	val = sine[i_shifted>>SHIFT];
	i_shifted += di_shifted;
	if(i_shifted>=(SINEN<<SHIFT)) i_shifted -= (SINEN<<SHIFT);
	dac_load_data_buffer_single(DAC1, val, DAC_ALIGN_RIGHT12, DAC_CHANNEL1);
	
	gpio_clear(LED);

}

int main(void)
{
	//SystemCoreClockUpdate();
			
	// should be able to figure this out somehow
	//struct rcc_clock_scale 	clock;
	//rcc_clock_setup_pll(&clock);
	//rcc_set_pll_source(1); // sysclk system clock selected
	//rcc_set_pll_source(0b0101); // main pll. didn't seem to help

	// set TIM6 interrupt and DAC
	rcc_periph_clock_enable(RCC_TIM6); // RCC_APB1ENR1 |= RCC_APB1ENR1_TIM6EN;
	timer_set_period(TIM6, SystemCoreClock/fs -1); // TIM6->ARR = SystemCoreClock/fs -1;
	timer_generate_event(TIM6, TIM_EGR_UG); // TIM6->EGR |= TIM_EGR_UG;
	timer_enable_counter(TIM6); // TIM6CR1 |= CR1_CEN;
	timer_enable_irq(TIM6, TIM_DIER_UIE ); // TIM6->DIER |= TIM_DIER_UIE;
	nvic_enable_irq(NVIC_TIM6_DACUNDER_IRQ); // NVIC_EnableIRQ(TIM6_DAC_IRQn);
	rcc_periph_clock_enable(RCC_DAC1); // RCC_APB1ENR1 |= RCC_APB1ENR1_DAC1EN;
	//dac_enable(DAC_CHANNEL1, DAC_CR_EN1); // DAC1->CR |= DAC_CR_EN1;
	dac_enable(DAC1, DAC_CHANNEL1); // DAC1->CR |= DAC_CR_EN1;


	//TIM6->ARR =;
	//TIM6->EGR |= TIM_EGR_UG;
	//rcc_periph_clock_enable(RCC_GPIOB);
	//gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO3);
	gpio_out(LED);
	//gpio_set(LED);
	//gpio_out(LED);

	float v = sin(1.0);
	while (1)
	{
		//gpio_set(LED);
		delayish(100);
		//gpio_clear(LED);
		//gpio_toggle(LED);
		//delayish(900);
		//mal_delayish(100);
	}
}
