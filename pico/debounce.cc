#include "hardware/gpio.h"

#include "debounce.h"

#if 1
Debounce::Debounce(uint gpio, uint delay)
{
	gpio_init(gpio);
	gpio_set_dir(gpio, GPIO_IN);
	gpio_pull_up(gpio);
	_gpio = gpio;
	_delay = delay;
	later = make_timeout_time_ms(0); 
}

void Debounce::update()
{	
	if(absolute_time_diff_us(get_absolute_time(), later)>0) return;
	later = make_timeout_time_ms(_delay);
	uint8_t prev = _integrator;
	uint8_t up = gpio_get(_gpio) ? 0 : 1;
	_integrator <<= 1;
	_integrator |= up;
	if(prev != 0xFF && _integrator == 0xFF) _falling = true;
	if(prev != 0 && _integrator == 0) _rising = true;
}

bool Debounce::falling()
{
	update();
	if(_falling) {
		_falling = false;
		return true;
	}
	return false;
}

bool Debounce::rising()
{
	update();
	if(_rising) {
		_rising = false;
		return true;
	}
	return false;
}

#endif

void debounce_init(debounce_t* deb, uint gpio, uint delay)
{
	gpio_init(gpio);
	gpio_set_dir(gpio, GPIO_IN);
	gpio_pull_up(gpio);
	deb->gpio = gpio;
	deb->integrator = 0xFF;
	deb->falling = false;
	deb->rising = false;
	deb->delay = delay;
	deb->later = make_timeout_time_ms(0);
}


void debounce_update(debounce_t* deb)
{
	if(absolute_time_diff_us(get_absolute_time(), deb->later)>0) return;
	deb->later = make_timeout_time_ms(deb->delay);
	uint8_t prev = deb->integrator;
	uint8_t up = gpio_get(deb->gpio) ? 0 : 1;
	deb->integrator <<= 1;
	deb->integrator |= up;
	if(prev != 0xFF && deb->integrator == 0xFF) deb->falling = true;
	if(prev != 0 && deb->integrator == 0) deb->rising = true;
}



bool debounce_falling(debounce_t* deb)
{
	debounce_update(deb);
	if(deb->falling) {
		deb->falling = false;
		return true;
	}
	return false;
}


bool debounce_rising(debounce_t* deb)
{
	debounce_update(deb);
	if(deb->rising) {
		deb->rising = false;
		return true;
	}
	return false;
}
