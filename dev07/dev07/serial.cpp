#include "main.h"
//#include <Serial.h>

#define BUFF_MAX 128
char buff[BUFF_MAX];

void  serial_setup() {
  Serial.begin(115200);
  memset(buff, 0, BUFF_MAX);
}

int line_len = 0;

void serial_task() {

  char msg[BUFF_MAX];
  if(Serial.available() == 0) return;
  int c = Serial.read();
  if(c == -1) return;
  if(c == '\r') c = '\n';
    if(c != '\n') {
      if( line_len + 1 >= BUFF_MAX) return;
      //putchar(c); if(c == '\n') putchar('\r');
      buff[line_len++] = c;
      return;
    }

    if(line_len == 0) return;

    if(buff[0] == 'R') {
      //puts("hello 1\r");
      //DS3231_get(&t);
      // 003 = ETX end of text
      ds_get_str(msg, BUFF_MAX);
      Serial.print(msg);
    }

if(buff[0] == 'S') {
      //long long tval = set_timeval;
      //set_timeval = -1;
      //char str[] = "S2023060617520003";
      volatile long long timeval = atoll(buff+1);
      DS3231_set_int(timeval);
    }
    if(buff[0] == 'T') { // set to a test time
      long long timeval = 2023060617520003;
      DS3231_set_int(timeval);
    }
    
  line_len = 0;
    memset(buff, 0, BUFF_MAX);
  
}
