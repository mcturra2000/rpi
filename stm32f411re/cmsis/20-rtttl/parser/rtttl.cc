/* ringtone compiler
 * https://en.wikipedia.org/wiki/Ring_Tone_Text_Transfer_Language
 * 2023-02-07	Started
 *
 * PROBLEM CHILDREN - stuff that needed fixing
 *
 * DrNo.txt - had to write slurp() function because proior implementation was problematical
 *
 */
#include <algorithm>
#include <cassert>
#include <cmath>
#include <filesystem>
#include <iostream>
#include <fstream>
#include <vector>


using namespace std;
using namespace std::filesystem;
namespace ranges = std::ranges;


#include "rtttl.h"

int pos = 0;
std::string inp;

int get_num()
{
	int val = -1; // as an error condition
	while(isdigit(inp[pos])) {
		if(val ==-1) val = 0;
		val = val*10 + inp[pos++] - '0'; 
	}
	return val;
}

int get_setting(char expect)
{
	pos++;
	assert(inp[pos++] == expect);
	assert(inp[pos++] == '=');
	int setting = get_num();
	assert(setting>=0);
	cout << "setting " << expect << " = " << setting << "\n";
	return setting;
}


typedef struct {
	string title;
	vector<note_t> notes;
} song_t;

std::string slurp(const char *filename)
{
        std::ifstream in;
        in.open(filename, std::ifstream::in | std::ifstream::binary);
        std::stringstream sstr;
        sstr << in.rdbuf();
        in.close();
        return sstr.str();
}

song_t read_song(const path& p)
{
	pos = 0;
	//inp = "";

	song_t song;
	cout << p << "\n";
	//std::ifstream ifs(p);
	//string in1(std::istreambuf_iterator<char>(ifs), std::istreambuf_iterator<char>());
	string inp1{slurp(p.c_str())};
	inp = "";
	for(char c:inp1) {
		if(c == '\r' || c == '\n') continue;
		inp += c;
	}

	//while(ifs.good()) {
	//	ifs >> inp;
	//}
	//ifs.close();
	//std::cout  << inp;

	string title;
	// read title
	while(inp[pos] != ':') {
		cout << inp[pos];
		title += inp[pos++];
	}
	cout << "title=<" << title << ">\n";
	song.title = title;

	// read settings
	int set_d = get_setting('d'); // default duration. "4" means that each note with no duration is a quarter note
	int set_o = get_setting('o'); // default octive 
	int set_b = get_setting('b'); // default tempo, bpm


	// read notes
	int noten = 0;
	while(pos +1 < inp.size()) {
		// decode note
		pos++;
		int dur = get_num(); // duration specifier
		if(dur == -1) dur = set_d;
		int key = 0;
		if(isalpha(inp[pos])) key = tolower(inp[pos++]);
		int sharp = 0;
		if(inp[pos] == '#') { sharp = 1; pos++; }
		int dot = 0;
		if(inp[pos] == '.') { dot = 1; pos++; }
		int octave = get_num();
		if(octave==-1) octave = set_o;
		printf("note: %d, idur = %d, key = %c, sharp = %d, dot = %d, octave = %d, ",
				++noten, dur, key, sharp, dot, octave);

		// encode note
		int semis[] = {9, 11, 0, 2, 4, 5, 7}; // map a note to a semi-tone (a-g). NB A is > C
		int freq = 0;
		if(key != 'p') {
			// primae facie correct
			int semi = semis[key - 'a'];
			if(sharp) semi++; // sharps are one semi-tone higher
			semi = 12* (octave-4) + semi;
			freq = 262.0 * pow(2.0, (double)semi / 12.0); // 262Hz is middle C (C4)
		}
		printf("%d Hz, ", freq);

		dur = 60 * 1000/set_b *4 /dur; // convert to ms
		if(dot) dur = (3 * dur) /2;
		printf("%dms\n", dur);

		song.notes.push_back(note_t{(int16_t)freq, (int16_t)dur});

	}
	cout << inp[pos];
	cout << "=======================================================\n";
	return song;
}

void write_song(const song_t& s, FILE *fscore)
{
	// write title
	for(int i = 0; i< 16; i++) {
		char c = ' ';
		if(i < s.title.size()) c= s.title[i];
		fwrite(&c, 1, 1, fscore);
	}

	uint16_t num_notes = s.notes.size();
	fwrite(&num_notes, 2, 1, fscore);
	for(auto &note: s.notes) 
		fwrite(&note, sizeof(note_t), 1, fscore);
}


int main()
{
	vector<song_t> songs;
	for(const auto& entry : directory_iterator("songs")) {
		songs.push_back(read_song(entry.path()));
	}

	// sort songs by title, irrespective of case
	struct {
		bool operator() (song_t& s1, song_t& s2) { 
			string s1a = s1.title;
			transform(s1a.begin(), s1a.end(), s1a.begin(), ::tolower);
			string s2a = s2.title;
			transform(s2a.begin(), s2a.end(), s2a.begin(), ::tolower);
			return s1a < s2a; 
		}
	} less;
	ranges::sort(songs, less);
	

	// write score
	FILE *fscore = fopen("score.bin", "wb");

	// magic number
	char magic[] = "RING";
	fwrite(magic, 4, 1, fscore); 

	uint32_t num_songs = songs.size();
	fwrite(&num_songs, 4, 1, fscore);

	for(const song_t& s: songs) 
		write_song(s, fscore);
	fclose(fscore);
	return 0;
}
