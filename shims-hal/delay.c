#include <delay.h>

void delay(uint32_t ms)
{
	HAL_Delay(ms);
}

uint32_t millis()
{
	return HAL_GetTick();
}

// DWT: Data Watchpoint and Trace unit
static void dwt_init()
{	   
	/* Disable TRC */
    	CoreDebug->DEMCR &= ~CoreDebug_DEMCR_TRCENA_Msk; // ~0x01000000;
    	/* Enable TRC */
    	CoreDebug->DEMCR |=  CoreDebug_DEMCR_TRCENA_Msk; // 0x01000000;
 
    	/* Disable clock cycle counter */
    	DWT->CTRL &= ~DWT_CTRL_CYCCNTENA_Msk; //~0x00000001;
    	/* Enable  clock cycle counter */
    	DWT->CTRL |=  DWT_CTRL_CYCCNTENA_Msk; //0x00000001;
 
    	/* Reset the clock cycle counter value */
    	//DWT->CYCCNT = 0;
 
    	/* 3 NO OPERATION instructions */
    	__ASM volatile ("NOP");
    	__ASM volatile ("NOP");
    	__ASM volatile ("NOP");
	
}
void delay_us(uint32_t us)
{
	  
	uint32_t startTick = DWT->CYCCNT;
	if(startTick == DWT->CYCCNT) dwt_init();
	uint32_t delayTicks = us * (SystemCoreClock/1000000);
    
	while (DWT->CYCCNT - startTick < delayTicks);
	DWT->CYCCNT = 0;
}

#if 0
void usecs_init()
{
        RCC->APB1ENR |= RCC_APB1ENR_TIM5EN; //must do this first or you can't set the values
        TIM->PSC = SystemCoreClock / 1000000 -1;
        TIM->CR1 |= TIM_CR1_CEN; // enable counter
        TIM->EGR  |= TIM_EGR_UG; // update the registers (required!)
}


void usecs_test()
{
        usecs_init();
        USECS_ZERO();
        HAL_Delay(50);
        volatile uint16_t cnt = USECS();
        UNUSED(cnt);
}
#endif
