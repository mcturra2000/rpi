# blink LED using SWI (software interrupts)


## See also

* [Blog writeup](https://medium.com/@oblate_24476/swi-and-rp2040-enhancement-39bef96320ba)
* [Exception entry and return](https://developer.arm.com/documentation/dui0662/b/The-Cortex-M0--Processor/Exception-model/Exception-entry-and-return?lang=en)
* [SVC and register guarantees](https://forums.raspberrypi.com/viewtopic.php?t=345176)
* [SVC_Handler for Cortex M0](https://community.arm.com/support-forums/f/armds-forum/1089/svc_handler-for-cortex-m0)


## Status

2023-01-07	Fails

2023-01-05	Added a more sophisticated software interrupt routine that can handle different functions and arguments.

2023-01-04	Started. Works
