#pragma once

#include <stdlib.h>
#include <pico/sync.h>

#include "FreeRTOS.h"
#include "task.h"
#include "FreeRTOS_CLI.h"
//#include <semphr.h>

#include "pi.h"
#include "fat32.h"
#include "../../sdcard-spi/sdcard.h"

#ifdef __cplusplus
    extern "C" {
#endif


#include "mode0.h"	    
//#define print ili9341_print
//#define printchar ili9341_printchar

////////////////////////////////////////////////////////////////////////////
// AUDIO


void play_song (const char *filename);

////////////////////////////////////////////////////////////////////////////
// KEYBOARD
#define KBD_UART
void kbd_init();
void kbd_task();



////////////////////////////////////////////////////////////////////////////
// SD
void sd_list_root();



////////////////////////////////////////////////////////////////////////////
#define print ili9341_print
#define printchar ili9341_printchar
#define println ili9341_println
//void print(const char *format, ...); // print to display
//void printchar(char c);
extern TaskHandle_t cmd_hnd;
extern TaskHandle_t peri_hnd;
void cmd_task (void *pvParameters);

#ifdef __cplusplus
    }
#endif
