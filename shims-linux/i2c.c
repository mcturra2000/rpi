#include "i2c.h"
/*
 * sudo apt install libi2c-dev i2c-tools
 * i2c cobbled from
 * https://github.com/amaork/libi2c
 * */

#include <assert.h>
#include <sys/ioctl.h>
#include <asm/ioctl.h>
#include <assert.h>
#include <string.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
//#include <linux/i2c-smbus.h>

//#include <i2c/smbus.h> // try install i2c-tools
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#include "i2c.h"


int i2c_fd = -1;

//#define SID 0x3C // Slave ID





static void check(int return_val, const char *str)
{
	if(return_val<0) {
		printf("ERR:%s:%s\n", str, strerror(errno));
		exit(1);
	}	
}


void i2c_init()
{
	if(i2c_fd != -1) return; // nothing to do, it's already open
	i2c_fd = open("/dev/i2c-1", O_RDWR);
	check(i2c_fd, "Couldn't open i2c-1");
	
}

void i2c_deinit()
{
	if(i2c_fd == -1) return;
	close(i2c_fd);
	i2c_fd = -1;
}


// read one value at a specified address
int i2c_grab1_at(uint8_t sid, uint8_t reg, uint8_t *result) 
{
        int retval;
        uint8_t outbuf[1], inbuf[1];
        struct i2c_msg msgs[2];
        struct i2c_rdwr_ioctl_data msgset[1];

        msgs[0].addr = sid;
        msgs[0].flags = 0;
        msgs[0].len = 1;
        msgs[0].buf = outbuf;

        msgs[1].addr = sid;
        msgs[1].flags = I2C_M_RD | I2C_M_NOSTART;
        msgs[1].len = 1;
        msgs[1].buf = inbuf;

        msgset[0].msgs = msgs;
        msgset[0].nmsgs = 2;

        outbuf[0] = reg;

        inbuf[0] = 0;

        *result = 0;
        if (ioctl(i2c_fd, I2C_RDWR, &msgset) < 0) {
                perror("ioctl(I2C_RDWR) in i2c_read");
                return -1;
        }

        *result = inbuf[0];
        return 0;
}

// ship a single value
void i2c_ship_val_at(uint32_t sid, uint8_t addr, const uint8_t val)
{
        uint8_t v1 = val;
        i2c_ship_at(sid, addr, &v1, 1);
}

void i2c_ship_at(uint32_t sid, uint8_t addr, const uint8_t *w, size_t wn)
{
	assert(wn<33);
	i2c_init(sid); // provisionally
	check(ioctl(i2c_fd, I2C_SLAVE, sid), "Couldn't set slave id");
	int pktsize = wn+1;
	uint8_t data[pktsize];
	data[0] = addr;
	memcpy(data+1, w, wn);
	ssize_t n = write(i2c_fd, data, pktsize);
	if(n != pktsize) {
		printf("ERR: bsp_write wrote only %d out of %d\n", n, pktsize);
	}
	//i2c_deinit();
}
