/*
 * flash.c
 *
 *  Created on: Dec 29, 2023
 *      Author: pi
 */

//#include "flash.h"

#include <shims.h>
//#include "stm32f4xx_hal_conf.h"
//#include "stm32f4xx_hal_flash_ex.h"

void Error_Handler(void);


#define USE_SEC_5
#ifdef USE_SEC_3
#define SECTOR FLASH_SECTOR_3
const uint32_t FlashAddress = 0x0800C000; // address of sector 3
#endif
#ifdef USE_SEC_5
#define SECTOR FLASH_SECTOR_5
const uint32_t FlashAddress = 0x08020000; // address of sector 7
#endif
#ifdef USE_SEC_7
#define SECTOR FLASH_SECTOR_7
const uint32_t FlashAddress = 0x08060000; // address of sector 7
#endif

/* NB writing to flash suspends interrupts and takes about 1s */
static void _Write_Flash(uint8_t *data, int n)
{
	uint32_t PAGEError = 0;
	FLASH_EraseInitTypeDef EraseInitStruct;
	//EraseInitStruct.TypeErase   = FLASH_TYPEERASE_SECTORS ;
	EraseInitStruct.TypeErase = FLASH_TYPEERASE_SECTORS;
	//EraseInitStruct.Banks = FLASH_BANK_1;
	EraseInitStruct.Sector = SECTOR;
	EraseInitStruct.VoltageRange = FLASH_VOLTAGE_RANGE_3; // was 3
	EraseInitStruct.NbSectors = 1;
	HAL_FLASH_Unlock();
	//__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGSERR );
	//FLASH_Erase_Sector(FLASH_SECTOR_3, VOLTAGE_RANGE_3);
	if (HAL_FLASHEx_Erase(&EraseInitStruct, &PAGEError) != HAL_OK)
		Error_Handler();
	if (PAGEError != 0xFFFFFFFFU)
		Error_Handler();

	for (int i = 0; i < n; i++) {
		HAL_FLASH_Program(TYPEPROGRAM_BYTE, FlashAddress + i, data[i]);
	}
	HAL_FLASH_Lock();
}

/* 2023-12-29 wrapper added because some strange behaviour
 * Actually, this doesn't seem to be the problem
 */
void Write_Flash(uint8_t *data, int n)
{
	uint32_t interrupt_save = __get_PRIMASK();
	__disable_irq();

	_Write_Flash(data, n);

	__set_PRIMASK(interrupt_save);
	return;
	 if (!interrupt_save) {
	        __enable_irq();
	    }
}


void Read_Flash(uint8_t *data, int n)
{
	for (int i = 0; i < n; i++) {
		data[i] = *(uint8_t*) (FlashAddress + i);
	}

}
