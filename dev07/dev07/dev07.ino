

#include <Wire.h>
#include <TM1637Display.h>

//#include "RPi_Pico_TimerInterrupt.h"
//#include "cal.h"
#include "main.h"

#include "mbed.h"


TM1637Display display(0, 2, 400);  // CLK, DIO, delay in us (default is 100)


//RPI_PICO_Timer ITimer(0);
using u32 = uint32_t;

#define BZR 6


bool show_trig = true;



Btn btny(7);   // yellow button controls whether to display clock or temperature
Btn btnr(29);  // red button controls stopwatch

//#define BTN 7
//#define BTNr 29
//bool do_show_time = true;
bool yellow_active = true;  // true to show clock or termperature
bool yellow_clock = true;   // when yellow is active, show clock (true), or else temprature
uint32_t swatch_start_ms;   // start time of stopwatch in millis()



void show_task() {
  if (!show_trig) return;
  show_trig = false;

  if (yellow_active) {
    if (yellow_clock) {
      display.showNumberDecEx(ds_hhmm(), 0b01000000, true, 4);
    } else {
      display.showNumberDec(bme_degC(), false);
    }
  } else {
    // show stopwatch
    uint32_t secs = (millis() - swatch_start_ms) / 1000;
    uint32_t mins = secs / 60;
    secs = secs % 60;
    display.showNumberDecEx(mins * 100 + secs, 0b01000000, true, 4);
  }
}


bool red_active = false;

void buzzer_poll() {
  if (!red_active) {
    digitalWrite(BZR, LOW);
    return;
  };
  u32 elapsed = millis() - swatch_start_ms;
  if (elapsed < 30 * 60* 1000) return;
  ulong segment = elapsed % 5000;
  bool on = segment < 250 || (500 < segment && segment < 750);
  //Serial.print("sound ");
  if (on) {
    digitalWrite(BZR, HIGH);
    //tone(BZR, 2525);  // quindar
    //Serial.println("on");
  } else {
    //noTone(BZR);
    digitalWrite(BZR, LOW);
    //Serial.println("off");
  }
}

//volatile uint32_t tick = 0;

//bool TimerHandler(struct repeating_timer *t) {
void every250ms() {
  show_trig = true;
}

void every4ms() {
  if (btny.update()) {
    if (yellow_active) {
      yellow_clock = !yellow_clock;
    } else {
      yellow_active = true;
    }
    red_active = false;
  }
  if (btnr.update()) {
    yellow_active = false;
    red_active = true;
    swatch_start_ms = millis();
  }

 
}


void iled(int n) {
  pinMode(n, OUTPUT);
  digitalWrite(n, 1);
}  // turn off the internal leds

mbed::Ticker t4ms;
mbed::Ticker t250ms;

void setup() {
  pinMode(BZR, OUTPUT);
  iled(18);
  iled(19);
  iled(20);
  serial_setup();

  Wire.begin();  // Start I2C
  //display.start();
  display.setBrightness(0x0f);
  bme_setup();
  using namespace std::chrono_literals;
  t250ms.attach(&every250ms, 250ms);
  t4ms.attach(&every4ms, 4ms);

  show_task();
}

void loop() {
  serial_task();
  show_task();
  buzzer_poll();
}
