/*
 * om.h
 *
 *  Created on: May 14, 2024
 *      Author: pi
 */

#ifndef INC_OM_H_
#define INC_OM_H_

#ifdef __cplusplus
extern "C" {
#endif

int 		om_get_mode(int mode);
void 		om_init(int mode);
uint32_t	om_millis();
//void 		om_mode_toggle_1_2();
uint32_t 	om_secs();
void 		om_set_mode(int mode);
void 		om_start();
void 		om_stop();
void 		om_toggle();
void 		om_update();
bool 		om_running();

#ifdef __cplusplus
}
#endif


#endif /* INC_OM_H_ */
