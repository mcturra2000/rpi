#include "common.h"

typedef struct {
	char *pattern;
	int parts[4];

} code_t;

const code_t add_codes[] = {
	{"r,s,8", {0b10101 << 11, 8, 0, 0}},
	{"s,7", {0b1011 << 12, -2, 0, 0}},
	0};

const code_t adds_codes[] = {
	{"r,r,3",       {0b1110 << 9, 0, 3, 6}},
	{"r,8",         {0b110 << 11, 8, 0 , 0}},
	0 };

const code_t ands_codes[] = {
	{"r,r",		{1<<14, 0, 3, 0}},
	0};

const code_t bkpt_codes[] = { {"8", {0b10111110<<8, 0, 0, 0}}, 0}; // e.g. bkpt #11 => be0b


const code_t blx_codes[] = { {"R", {0b10001111<<7, 3, 0, 0}}, 0};

const code_t bx_codes[] = {
	{"l",		{0b1000111<<8, 3, 0, 0}},
	{"R",		{0b1000111<<8, 3, 0, 0}},
	0};

const code_t cmp_codes[] = {
	{"r,r",		{0b100001010<<6, 0, 3, 0}}, //e.g. CMP R3, R2 => 0x4293
	0};

const code_t ldr_codes[] = {
	{"r,[p,8]",	{0b1001<<11, 8, -2, 0}},
	{"r,[r,5]",	{0b1101<<11, 0, 3,  6}}, // e.g. LDR R3, [R7, #8]
	{"r,=",		{0b1001<<11, 8, 0,  0}}, // TODO: wrongly implmented. It needs to generate an extra address
	{"r,L",		{0b1001<<11, 8, 0,  0}},
	0};

const code_t lsl_codes[] = {
	{"r,r,5",	{0, 0, 3, 6}},
	0};

#define MOV_S_R (0b100011<<9)
const code_t movs_codes[] = {
	{"r,8",		{1<<13, 8, 0, 0}},
	{"r,r", 	{0b111<<10, 0, 3, 0}}, // generate an ADDS, consistent with gnu asm
	{"s,r",		{MOV_S_R, 3, 0, 0}},
	0 };

const code_t mvns_codes[] = {
	{"r,r",		{0b100001111<<6, 0, 3, 0}},
	0};

const code_t nop_codes[] = {
	//{"",	{0b10111111<<8, 0, 0, 0}}, // per arm ref manual
	{"",	{0x46c0, 0, 0, 0}}, // per decompilation
	0 };

const code_t str_codes[] = {
	{"r,[r,5]",	{0b11<<13, 0, 3, 6}},// e.g. STR R0, [r7, #4]
	0};

const code_t sub_codes[] = {
	{"s,7",		{0b101100001<<7, -2, 0, 0}}, // e.g. SUB SP, #16
	0};

/////////////////////////////////////////////////////////////////////////////////////////////

#if 0
// a nice idea of generalising the table, but I don't think it saves much code
#define IGNORE 0
#define STORE 1

/* FN scantab_t */
typedef struct {
	char pattern; // pattern to match against
	yytype_e yytype; // what type to demand
	int width; // in bits
	bool store; // whether to store the argument or not
} scantab_t;

/* FN scantab */
const scantab_t scantab[] = {
	{'=', T_LABEL, 0, IGNORE},
	{'3', T_IMM, 3, STORE}, // imm3 number
	{'5', T_IMM, 5, STORE}, // imm5 number
	{'7', T_IMM, 0, STORE}, // imm7 number requiring shifting
	{'8', T_IMM, 8, STORE}, // imm7 number
	{'l', T_LR,  0, IGNORE}, // LR
	{'L', T_ID,  0, STORE}, // a label without '=' prepended. E.g. LDR R1, MYVAL
	{'p', T_PC,  0, IGNORE}, // PC
	{'r', T_RN,  3, STORE}, // R0-R7
	{'R', T_RN,  4, STORE}, // R0-R15
	{'s', T_SP,  0, IGNORE}, // SP, but don't store argument
	{'S', T_SP,  0, STORE}, // SP, but we store the argument
	0};

/* FN process_scantab */
bool process_scantab (char pat, int *argnum)
{
	for(scantab_t *row = (scantab_t*) scantab; row; row++)
	{
		if(row->pattern != pat) continue; 
		if(yytype != row->yytype) return false; // wrong type found
		if((row->width >0) && (yylval >= (1<<row->width))) return false; // too wide

		// some special cases
		switch(pat) {
			case '=':  // forward-referencing label
                                // TODO : this implementation is wrong, because extra data needs to be generated
                                // the argument isn't stored, but resolved later by filling in a hole
                                puts("yysubscan found =label");
                                add_hole(yystart+1, yylen-1, address, 4); // we need to generate aligned forwarding
                                print_label(hd_holes, " .. yysubscan\n");
                                syn("TODO: don't use =label construct right now");
                                break;
			case '5':
				yylval  >>= 2;
				break;
			case 'l':
				yylval = 14;
				break;
			case 'L':
				add_hole(yystart, yylen, address, 4); // a forward reference
				break;
			case  'S':
				yylval = 13;
				break;
		}

		if(row->store) arg[*argnum++] = yylval;
		return true;
	}

	// must be some other character that we need to match
	if(isalnum(pat)) syn("yysubscan Logic error, Default shouldn't be alnum");
	return yytype == pat;
}

#endif

/* 
 * FN yysubscan
 * we perform a subscan. It is not guaranteed to match, so we need to potentiall roll back
 * We should never hard fail, because there may be further scans down the chain that pass
 */
bool yysubscan (char *expr)
{
	arg[0] = arg[1] = arg[2] = arg[3] = 0;
	char  *yytop = yystart;
	//yylen = 0;
	int argnum = 0;
	while(*expr) {
		if(yylex() ==0) goto fail;
		switch(*expr) {
			case '=': // forward-referencing label
				// TODO : this implementation is wrong, because extra data needs to be generated
				// the argument isn't stored, but resolved later by filling in a hole
				if(yytype != T_LABEL) goto fail;
				puts("yysubscan found =label");
				add_hole(yystart+1, yylen-1, address, 4); // we need to generate aligned forwarding
				print_label(hd_holes, " .. yysubscan\n");
				syn("TODO: don't use =label construct right now");
				break;
				//arg[argnum++] = 0;
			case '3': // #imm3
				//puts("case 3 in");
				if(yytype != T_IMM || yylval > 0b111) goto fail;
				arg[argnum++] = yylval;
				//puts("case 3 out");
				break;
			case '5': // #imm5
				if(yytype != T_IMM || yylval > 0b11111) goto fail;
				arg[argnum++] = (yylval >>2);
				break;
			case '7': // #imm7 (but it may be shifted
				if(yytype != T_IMM) goto fail;
				arg[argnum++] = yylval;
				break;
			case '8': // #imm8
				if(yytype != T_IMM || yylval > 0b11111111) goto fail;
				arg[argnum++] = yylval;
				break;
			case 'l': // LR
				if(yytype != T_LR) goto fail;
				arg[argnum++] = 14;
				break;
			case 'L' : // a label (without '=' prepended). As in `LDR R1, MYVAL'
				if(yytype != T_ID) goto fail;
				add_hole(yystart, yylen, address, 4); // a forward reference
				break;
			case 'p': //PC
				if(yytype != T_PC) goto fail;
				break;
			case 'r': // r0-7
				//puts("scanning r");
				//print_token();
				if(yytype != T_RN || yylval>7) goto fail;
				arg[argnum++] = yylval;
				//puts("scanning r out");
				break;
			case 'R': // r0-15
				if(yytype != T_RN || yylval>15) goto fail;
				arg[argnum++] = yylval;
				break;
			case 's': // SP
				if(yytype != T_SP) goto fail;
				break;
			case 'S': // SP. Like 's', but we store a register value
				if(yytype != T_SP) goto fail;
				arg[argnum++] = 13;
				break;
			default:
				if(isalnum(*expr)) syn("yysubscan Logic error, Default shouldn't be alnum");
				//puts("found yysubscan default in ");
				if(yytype != *expr) goto fail;
				//puts("found yysubscan default out");
				//syn("Logic error in yysubscan");
		}
		expr++;
	}
	return true;
fail:
	//puts("yysuscan fail");
	yystart = yytop; 
	yylen = 0;
	return false;



}

/* FN process_code */
int process_code (code_t *pcode)
{
	int *ps = pcode->parts;
	int code = ps[0];

	// Groan. We need to do handling of special cases
	if(code == MOV_S_R) {
		code |= (1<<7) | 0b101; // wackily represents Rd as the stack pointer. See A6.7.40
		code |=  arg[0] << 3; // the Rm register
		return code;
	}

	for (int i = 0; i<3; i++) {
		int by = ps[i+1]; // amount to shift by
		if(by<0)
			code += arg[i] >> (-by);
		else
			code += arg[i] << by;
	}
	return code;
}

/*
 * FN parse_encodings
 * void parse_encodings (char *name, code_t *codes) 
 */
void parse_encodings (char *name, code_t *codes) 
{
	for(code_t *code = (code_t*) codes; code->pattern; code++) {
		if(yysubscan(code->pattern)) {
			emit_opcode(name, process_code(code));
			return;
		}
	}
	syn(name);
}

typedef struct {
	char *name;
	//int size; // i.e. either 1 or 2 16-bit opcodes 
	code_t *codes;
} matcher_t;

const	matcher_t keys[]  = { 
	{ "add",	(code_t*) add_codes},
	{ "adds",	(code_t*) adds_codes},
	{ "ands",	(code_t*) ands_codes},
	{ "bkpt",	(code_t*) bkpt_codes}, // but might be 32-bit wide??
	{ "blx",	(code_t*) blx_codes},
	{ "bx",		(code_t*) bx_codes},
	{ "cmp",	(code_t*) cmp_codes},
	{ "ldr",	(code_t*) ldr_codes},
	{ "lsl",	(code_t*) lsl_codes},
	{ "lsls",	(code_t*) lsl_codes}, // at least I think they're the same as previous line
	{ "nop",	(code_t*) nop_codes},
	{ "movs",	(code_t*) movs_codes},
	{ "mov",	(code_t*) movs_codes},
	{ "mvns",	(code_t*) mvns_codes},
	{ "str",	(code_t*) str_codes},
	{ "sub",	(code_t*) sub_codes},
	//{ "push" , &parse_push}, 
	0 };

bool match_code ()
{
	for(matcher_t *k = (matcher_t*)keys ;  k->name != 0 ; k ++) {
		if(yyfind(k->name)) {
			parse_encodings(k->name, k->codes);
			//printf(" // %s: ", k->name);
			return true;
		}
	}
	return false;
}
