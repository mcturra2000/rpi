#include "../rp2040-cmsis.h"
//SIO_GPIO_OUT_SET
typedef __UINT32_TYPE__ uint;
typedef __UINT32_TYPE__ u32;


void gpio_set (uint gpio) { SIO_GPIO_OUT_SET = 1ul << gpio; }

void gpio_clr (uint gpio) { SIO_GPIO_OUT_CLR = 1ul << gpio; }

void gpio_out (uint gpio)
{
	REG(IO_BANK0_BASE + 0x4 + 0x8 * gpio ) = 5; // set proper _CTRL register address to 5 (GPIO)
	SIO_GPIO_OE_SET = 1ul << gpio; // allow setting of output
}

#define LED 25


//void delay(int n); // no particular timing

typedef __UINT32_TYPE__ u32;

#define RESETS_IO_BANK0		(1ul<<5)
#define RESETS_PAD_BANK0	(1ul<<8)
// no particular timing
void delay(int n) 
{
	for(int i =0 ; i< n; i++) {
		for(int j = 0; j< 1000; j++) {
			asm volatile ("nop");
		}
	}
}

// defined in src/rp2040/hardware_regs/include/hardware/regs/pads_qspi.h:
#define PADS_QSPI_GPIO_QSPI_SCLK_DRIVE_LSB 	4
#define PADS_QSPI_GPIO_QSPI_SCLK_SLEWFAST_BITS  0x00000001

void main();

u32 __attribute__ ((section(".isr_vector"))) irqs[] = {
	0x20042000, // top of stack = end flash (0x20000000 + 256k)
	(u32) main
};
	

//void __attribute__ ((naked)) __attribute__ ((section(".boot2.main"))) main()
void main()
{
	/*
	// set pad configuration
	PADS_QSPI_GPIO_QSPI_SCLK = 2 << PADS_QSPI_GPIO_QSPI_SCLK_DRIVE_LSB 
		| PADS_QSPI_GPIO_QSPI_SCLK_SLEWFAST_BITS; // 33
	*/

	// inspired by Ada. Appears to be necessary, too.
	// Bring up the peripherals we require
	// See s2.14.1
	// 2023-04-26 still necessary
	uint32_t reset_mask = RESETS_PAD_BANK0 | RESETS_IO_BANK0;
	RESETS_RESET &= ~reset_mask; // deassert the peripherals we want to use
	while((RESETS_RESET_DONE & reset_mask) != reset_mask); // wait until they are up again

	gpio_out(LED);

	while(1) {
		gpio_set(LED);
		delay(100);
		gpio_clr(LED);
		delay(100);
	}
}

