//#define F_CPU 8000000UL

//#include <util/delay.h>

//#include <avr/iotn4.h> // for CLKPR
#include <avr/io.h>
#include <avr/interrupt.h>

#include <gpio.h> // my HAL
#include <delay.h>



//#define LED1 GPIOA, (1<<0) // PA0 . Physical pin 13
//#define LED2 GPIOB, (1<<2) // PB2 . Physical pin 5

#define GPA(x) GPIOA, (1<<x)
#define GPB(x) GPIOB, (1<<x)

//#define TX 	GPA(5) // for testing purposes
//#define P_INT0 	GPB(2) // PB2, which has a special INT0 interrupt
//#define P_A1	GPA(1) // toggle test for 9600baud (=104us delay). Wire into P_INT0 for a test
//#define P_A2	GPA(2) // copy of PA1
//#define P_A3	GPA(3) // spike when P_ONT0 falls

#define RX	GPB(2)

ISR(TIM0_COMPA_vect)
{
	//gpio_toggle(P_A1);
	//gpio_toggle(P_A2);
}

// whenever P_INT0 falls
ISR(INT0_vect) 	
{
	//start_bit_detected = true;
	GIMSK &= ~(1<<INT0); // disable falling edge detection. Timer must re-enable
	//OCRA
	//gpio_set(P_A3);
	//gpio_clr(P_A3);
}

int main()
{
	// set clock 8MHz
	//cli();
	CLKPR = (1<<7); // prepare for change
	CLKPR = 0;

	//gpio_out(TX);
	//gpio_set(TX);
	//gpio_out(P_A1); 
	//gpio_out(P_A2);
	//gpio_out(P_A3);

	gpio_in(RX);

	// enable timer
	//TCCR1 = 0; // stop the counter
	TCNT0 = 0; // set the register count to 0 (it's 8 bits long)
	//TCCR0A = 0b010; // enable CTC (Counter Timer Clear) when matched with A

	// set the prescaler. The counter is only 8 bit long,
	// so we have to make it high enough
	TCCR0B = 0b10; // set the prescaler to 8. counter will advance at 1 MHz
	OCR0A = 97; //  1MHz / 9600 baud = 104.167
	//TIMSK0 |= (1<< OCIE0A); // enable compare interrupt for timer0,  A
	// ... you must implement ISR(TIM0_COMPA_vect)


	// configure RX for interrupts
	//gpio_in(P_INT0); // master must pull up the line, not us.
	//MCUCR = 0b10; // INT0 generated on falling edge
	//GIMSK = (1<<INT0); // enable INT0 IRQ ISR(INT0_vect)

	//sei(); // enable interrupts

	// PB2 has the special interrupt INT0, so we'll use that


	gpio_out(GPIOA, 0b1111);
	//gpio_out(LED2);

	//gpio_put(P_A1, 33);
	//gpio_set(P_A1);
	//for(;;);
	const uint8_t delay_count = 128;
	//for(;;) gpio_put(P_A1, gpio_get(RX));
	//gpio_set(P_A1);
	for(;;) {
		//gpio_set(TX);
		//gpio_clr(TX);
		uint8_t val = 0;
		while(gpio_get(RX));
		//gpio_clr(P_A1);
		delayish_us(delay_count/2); // skew the start bit
		//unsigned char c = 0;
		for(uint8_t i = 0; i < 8; i++) {
			delayish_us(delay_count);
			val >>= 1;
			if(gpio_get(RX)) val |= (1<<7);
			//gpio_set(P_A1);
			//gpio_clr(P_A1);
		}
		delayish_us(delay_count); // skip the stop bit

		//gpio_set(P_A1);
		while(gpio_get(RX) == 0);
		GPIOA->PORT = (val>>4);
		//delayish(10);


	}
}
