#pragma once
#include <stdint.h>
#include <shims.h>

void delay(uint32_t ms);
uint32_t millis();
void delay_us(uint32_t us);
#define delay_ms delay
#define sleep_us delay_us
#define sleep_ms delay


#if 0
void usecs_init();
void usecs_test();
#define USECS_ZERO() TIM->CNT = 0
#define USECS() TIM->CNT
#endif
