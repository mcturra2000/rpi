package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"strings"
	//"strconv"
	"time"
	//"log"
	//"math"



	"gopkg.in/ini.v1" // go get gopkg.in/ini.v1
)



var alarm_single_beep = true
var alarm_duration  float64 = 0.0 // in mins

func SetupCloseHandler () {
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		fmt.Println("\r- Ctrl+C pressed in Terminal")
		BuzzOff();
		DisplayDeinit()
		os.Exit(0)
	}()
}


/* mare - a function which can kill a process
*/
func mare (c chan bool, n time.Duration)  (bool) {
	c1  := make(chan bool)
	go  func() {
		time.Sleep(n);
		c1 <- false
	}()

	select {
	case  <-c1:
		return false
	case v1, ok := <-c:
		if !ok { return true }
		return v1
	}
	return false
}

var clock_chan = make(chan bool)



var beep_chan = make(chan bool)


func BeepSingle () {
	defer func() {
		BuzzOff()
		beep_chan <- true
	}()

	for {
		BuzzOn()
		if mare(beep_chan, 500 * time.Millisecond) { return }
		BuzzOff()
		if mare(beep_chan, 4500 * time.Millisecond) { return }
	}
}

func BeepDouble () {
	defer func() {
		BuzzOff()
		beep_chan <- true
	}()

	for {
		BuzzOn()
		if mare(beep_chan, 200 * time.Millisecond) { return }
		BuzzOff()
		if mare(beep_chan, 200 * time.Millisecond) { return }
		BuzzOn()
		if mare(beep_chan, 200 * time.Millisecond) { return }
		BuzzOff()
		if mare(beep_chan, 4400 * time.Millisecond) { return }
	}
}

var counter_chan = make(chan bool)

func main () {
	//fmt.Println("ssd test");
	SetupCloseHandler()
	InitPlatform()

	cfg, err := ini.Load("clockg.ini")
	if err != nil { panic("Failed to load clockg.ini") }
	alarm_single_beep =  cfg.Section("alarm"). Key("single_beep").MustBool(true)
	alarm_duration =  float64(cfg.Section("alarm"). Key("secs").MustInt(0)) / 60.0
	alarm_duration +=  float64(cfg.Section("alarm"). Key("mins").MustInt(0))
	//if err!= nil { single_

	go Debounce()
	/*
	go DisplayClock()
	go ButtonChange()
	*/

	var ticks  uint64  = 0
	//quant := 20
	c := time.Tick(10 * time.Millisecond)
	for range c {
		ticks += 10
		if(ticks % 400  == 0) { UpdateDisplay() }
		UpdateCounter()
	}

}

var mode_change = make(chan bool)


func StartCounter() {
	counterStartTime = time.Now()
}

var oldBuzzerState = false
func SetBuzzer(newState bool) {
	if  newState == oldBuzzerState { return }
	oldBuzzerState = newState
	if newState {
		BuzzOn()
	} else {
		BuzzOff()
	}
}

func UpdateCounter() {
	if showingClock { return }
	counterElapsed = time.Since(counterStartTime)

	millis5000 := counterElapsed.Milliseconds() % 5000
	on := false
	//if alarm_single_beep { panic("test unexpected result") }
	if counterElapsed.Minutes() > alarm_duration {
		//panic("here")
		on = (millis5000 < 200) || (millis5000 > 400 && millis5000 < 600)
	} else {
		//panic("x")
		on = (millis5000 < 500) &&  alarm_single_beep
	}
	SetBuzzer(on)

}

func StopCounter() {
	SetBuzzer(false)
}

func ButtonPressed () {
	if showingClock {
		StartCounter()
	} else {
		StopCounter()
	}
	showingClock = ! showingClock
}



var showingClock = true
var counterStartTime  time.Time
var counterElapsed time.Duration

func UpdateDisplay() {
	if showingClock {
		dt := time.Now()
		str := fmt.Sprintf("%02d:%02d", dt.Hour(), dt.Minute())
		if(dt.Second() % 2 == 0) {str = strings.Replace(str, ":", " ", 1)}
		Displaystr(str)
	} else {
		//dt := counterElapsed
		secs := int(counterElapsed.Seconds())
		mins := secs / 60
		secs = secs % 60
		str := fmt.Sprintf("%02d.%02d", mins, secs)
		Displaystr(str)
	}
}




