#pragma once

#define STM32F411xE
#include "stm32f4xx.h"

#ifdef __cplusplus
extern "C" {
#endif

static inline uint32_t get32(uint32_t addr) { return *(volatile uint32_t*)addr; }
static inline void put32(uint32_t addr, uint32_t value) { *(volatile uint32_t*)addr = value; }

// https://rosettacode.org/wiki/Random_number_generator_(included)
// for Batch
// generates 16-bit numbers 0 .. 32767
uint16_t rand16();

#ifdef __cplusplus
}
#endif

