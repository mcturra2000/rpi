//#include <byteswap.h>
#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"

#include "ili9341.h"
#include "mode0.h"

extern int main0(); // text. NB Need to call mode0_refresh_screen() explictly
//extern int main1(); // mario
//extern int main2();  // some kind of graphics


#include "pi.h"

void isr_systick(void)
{
	mode0_refresh_screen();
}

// for timing purposes
void main2()
{
	//mode0_color_t fg = MODE0_WHITE;
	//mode0_color_t bg = MODE0_BLACK;
	mode0_draw_screen();

	while(1) {
		mode0_putc('.');
		sleep_ms(1000);
	}
}

extern const char blob[];
//extern char blob_end[];
extern const unsigned blob_size;

// colours of the form RGB 5-6-5
#define RED	__builtin_bswap16(0b1111100000000000)
#define GREEN 	__builtin_bswap16(0b0000011111100000)
#define BLUE	__builtin_bswap16(0b0000000000011111)

void ili_line_filler(uint16_t* buffer, int slice)
{
	uint16_t data[240];
	static_assert(sizeof(data) == 240*2, "Unexpected size");
	uint16_t col = slice % 8 == 0 ? BLUE : 0;
	for(int i = 0; i<240; i++) data[i] = col;

	memcpy(buffer, data, 240*2);	
}

void ili_pluto_filler(uint16_t* buffer, int slice)
{
	//memcpy((uint16_t*)buffer, blob + (77878-320*240) + 240*2*slice, 240*2);
	memcpy(buffer, blob + 240*2*slice, 240*2);
}
/*
void show_pluto()
{
	ili9341_set_filler(&ili_pluto_filler);

}
*/

int main() 
{
	timer_hw->dbgpause = 0;
	stdio_init_all();
	// while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 

	// always keep the backlight on
	const int ili_led = 8;
	pi_gpio_out(ili_led);
	pi_gpio_high(ili_led);

	ili9341_init();
	systick_init();

	int mode = 3; 
	//if((mode != 1) && (mode != 2)) systick_init();
	switch(mode) {
		case 0: main0(); break;
		case 1: // show vertical lines
			ili9341_set_filler(&ili_line_filler);
			break;
		case 2: main2(); break;
		case 3: // show picture of pluto
			ili9341_set_filler(&ili_pluto_filler);
			break;
	}

	for(;;);
}

