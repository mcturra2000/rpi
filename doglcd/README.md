# 7318 doglcd device

## Layout

![](doglcd.jpg)

Pins are labelled C1, C2, ... C6 left to right in the diagram above.

```
WHAT:   5V  RS¹   CS     CLK    MOSI   GND
PIN:    C1  C2    C3     C4     C5     C6

STM32:  5V  PA6   PA4    PA5    PA7    GND
CN:         10:13 7:32   10:11  10:15

```

[1] **MISO** is not used. **RS** is set low to issue a command, high to issue an ASCII. DO NOT use a regular MISO on ESP8266, use an alternative pin like D4 (GPIO2).

## Implementations

* [stm32f411 hal](../stm32f411re/hal/02-doglcd)
* [19d18a](../F401-hal-projects/19d18a-doglcd)

## See also

* [Datasheet](https://www.lcd-module.com/eng/pdf/doma/dog-me.pdf) EA DOGM163W-A
* [Display-O-Tron LCD](https://shop.pimoroni.com/products/display-o-tron-lcd?gclid=EAIaIQobChMI1qe7iZmU4gIVQpnVCh2DhA0rEAQYAiABEgK1HfD_BwE&utm_campaign=google+shopping&utm_medium=cpc&utm_source=google&variant=2662374913)


