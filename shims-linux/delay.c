#include <stddef.h>
#include <sys/time.h>
#include <unistd.h>

#include "delay.h"

void delay(uint64_t ms)
{
	usleep(ms * 1000);
}

millis_t millis ()
{
        //uint64_t dt = lguTimestamp() - prog_start_time_ns;
        //millis_t dt = lguTimestamp();
        //dt /= 1000000; // convert ns to ms
        struct timeval tv;
        gettimeofday(&tv, NULL);
        return tv.tv_sec * 1000 + tv.tv_usec / 1000;
        //return dt;
}

