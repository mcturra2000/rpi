#pragma once

#include <basal.h>


typedef struct {
	REG(PIN);
	REG(DDR); // Data Direction register
	REG(PORT);

} port_t;

typedef unsigned char pin_t;

//#define GPIOA ((port_t*) 0x1B)
//#define GPIOB ((port_t*) 0x18)
#define GPIOA ((port_t*) 0x39)
#define GPIOB ((port_t*) 0x36)

/* Note that gcc is likely able to collapse these functions into a single instruction, making them very efficient */

static inline void gpio_in(port_t *port, pin_t pin)
{
	port->DDR &= ~pin;
}

static inline void gpio_out(port_t *port, pin_t pin)
{
	port->DDR |= pin;
}

static inline uint8_t gpio_get(port_t *port, pin_t pin)
{
	return ((port->PIN) & pin);
}

static inline void gpio_set(port_t *port, pin_t pin)
{
	port->PORT |= pin;
}

static inline void gpio_clr(port_t *port, pin_t pin)
{
	port->PORT &= ~pin;
}

static inline void gpio_toggle(port_t *port, pin_t pin)
{
	port->PORT ^= pin;
}

static inline void gpio_put(port_t *port, pin_t pin, uint8_t value)
{
	if(value)
		gpio_set(port, pin);
	else
		gpio_clr(port, pin);
}
