#include "basal.h"
#include "stm32f4xx_ll_rcc.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_system.h"
#include "stm32f4xx_ll_exti.h"
#include "stm32f4xx_ll_cortex.h"
#include "stm32f4xx_ll_utils.h"
#include "stm32f4xx_ll_pwr.h"
#include "stm32f4xx_ll_dma.h"
#include "stm32f4xx_ll_tim.h"
#include "stm32f4xx_ll_gpio.h"

#include "delay.h"


#define LED 	GPIOA, LL_GPIO_PIN_5 // nucleo builtin
#define SPK	GPIOA, LL_GPIO_PIN_0

void gpio_out(GPIO_TypeDef *GPIOx, uint32_t PinMask)
{
	uint32_t periph = -1;
	if(GPIOx == GPIOA) periph = LL_AHB1_GRP1_PERIPH_GPIOA;
	else if(GPIOx == GPIOB) periph = LL_AHB1_GRP1_PERIPH_GPIOB;
	else if(GPIOx == GPIOC) periph = LL_AHB1_GRP1_PERIPH_GPIOC;	
	LL_AHB1_GRP1_EnableClock(periph);

	LL_GPIO_ResetOutputPin(GPIOx, PinMask);
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = PinMask;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	LL_GPIO_Init(GPIOx, &GPIO_InitStruct);
}



static void MX_TIM2_Init(void)
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM2);
	NVIC_SetPriority(TIM2_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
	NVIC_EnableIRQ(TIM2_IRQn);

	TIM_InitStruct.Prescaler = 99;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 799;
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	LL_TIM_Init(TIM2, &TIM_InitStruct);

	LL_TIM_EnableARRPreload(TIM2);
	LL_TIM_SetClockSource(TIM2, LL_TIM_CLOCKSOURCE_INTERNAL);
	LL_TIM_SetTriggerOutput(TIM2, LL_TIM_TRGO_RESET);
	LL_TIM_DisableMasterSlaveMode(TIM2);
}

void TIM2_IRQHandler(void)
{
	LL_TIM_ClearFlag_UPDATE(TIM2); // necessary
	LL_GPIO_TogglePin(SPK);
	LL_GPIO_TogglePin(GPIOC, LL_GPIO_PIN_0);
	//LL_GPIO_SetOutputPin(GPIOC, LL_GPIO_PIN_0);
	//LL_GPIO_ResetOutputPin(GPIOC, LL_GPIO_PIN_0);
}


int main()
{
	gpio_out(SPK);
	gpio_out(LED);
	gpio_out(GPIOC, LL_GPIO_PIN_0);

	MX_TIM2_Init();
	LL_TIM_EnableCounter(TIM2);
	LL_TIM_EnableIT_UPDATE(TIM2);

	// let's change the settings
	LL_TIM_SetAutoReload(TIM2, 400);
	LL_TIM_GenerateEvent_UPDATE(TIM2); // probably unecessary

	while(1) {
		LL_GPIO_SetOutputPin(LED);
		delayish(100);
		LL_GPIO_ResetOutputPin(LED);
		delayish(900);
	}

}
