# doglcd

## Layout

![](doglcd.jpg)

Pins are labelled C1, C2, ... C6 left to right in the diagram above.

```
WHAT:   5V  RS¹   CS     CLK    MOSI   GND
PIN:    C1  C2    C3     C4     C5     C6

STM32:  5V  PA6   PA4    PA5    PA7    GND
CN:         10:13 7:32   10:11  10:15

```

[1] **MISO** is not used. **RS** is set low to issue a command, high to issue an ASCII. DO NOT use a regular MISO on ESP8266, use an alternative pin like D4 (GPIO2).

## Reference

* [Description](../doglcd/README.md)

## Status

2024-07-27  Switched to F401. Works

2023-07-02	A reworking of the structure. Not sure I'm terrible happy, mind. 
            Works

2023-04-13	Started. Works. Can almost certainly be simplified, but beware compilation. Test in stages!

