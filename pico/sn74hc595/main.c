#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
//#include "hardware/adc.h"
//#include "hardware/clocks.h"
//#include "hardware/flash.h"
#include "hardware/gpio.h"
//#include "hardware/irq.h"
#include "hardware/timer.h"
#include "hardware/spi.h"
#if LIB_PICO_STDIO_USB
#pragma message "USB configured as stdio"
#include "tusb.h" // if you want to use tud_cdc_connected()
#endif


#include "pi.h"


#define CLK	10 // IC pin 11 SRCLK	- equiv of of SCK
#define DAT	11 // IC pin 14 SER	- equiv of MOSI
#define LATCH	13 // IC pin 12 RCLK	- equiv of CS
// !!NB!! hold IC pin  10 SRCLR high

void using_spi(void)
{
	spi_inst_t *spi = spi1;
	//spi_set_format(spi0, 16, SPI_CPOL_0 , SPI_CPHA_0, SPI_MSB_FIRST);
	spi_init(spi, 1000000);
	gpio_set_function(CLK,  GPIO_FUNC_SPI);
	gpio_set_function(DAT, GPIO_FUNC_SPI);
	gpio_set_function(LATCH, GPIO_FUNC_SPI);
	//spi_set_baudrate(spi, 100000);

	uint8_t count = 0;
	for(;;) {
		spi_write_blocking(spi, &count, 1);
		count++;
		sleep_ms(250);
	}
}

void pause(void)
{
	busy_wait_us_32(50);
	//sleep_ms(1);
}

int main() 
{
	stdio_init_all();
#if LIB_PICO_STDIO_USB
        while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 
#endif

	using_spi(); // comment this line out if you want to use bit-banging

	pi_gpio_out(LATCH);
	gpio_put(LATCH, 1);
	pi_gpio_out(DAT);
	pi_gpio_out(CLK);

	uint8_t count = 0;
	for(;;) {
		gpio_put(LATCH, 0);
		pause();

		uint8_t val = count;
		for(int i = 0; i<8; i++) {
			gpio_put(DAT, val>>7);
			val <<= 1;
			gpio_put(CLK, 1);
			pause();
			gpio_put(CLK, 0);
			gpio_put(DAT, 0);
			pause();
		}
		gpio_put(LATCH, 1);
		pause();
		count++;

		sleep_ms(250);
	}

	return 0;
}

