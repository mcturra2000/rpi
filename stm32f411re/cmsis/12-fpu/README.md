# FPU

Experimentation with FPU

In Makefile:
```
CFLAGS += -mfloat-abi=hard
```

Turn on FPU in main():
```
        SCB->CPACR |= (0xF<<20); // turn on FPU
```


## Links

* [Video](https://www.youtube.com/watch?v=S1XU4bmWwHI)


## Status

2021-12-17	Started. Works
