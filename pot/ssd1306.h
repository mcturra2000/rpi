#pragma once

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

//extern uint8_t ssd1306_scr[1025];

//extern const uint8_t ssd1306_font6x8[];


struct ssd1306_screen_t {
	uint8_t cmd; // the data instruction
	uint8_t pixels[1024];
} __attribute__((packed));
extern struct ssd1306_screen_t ssd1306_screen;


void ssd1306_test();


int font_pixel(char c, int col, int row);
void ssd1306_init_display(int h, uint32_t i2c);
int getx(void);
void setCursorx(int x);
void setCursory(int y);
void setCursorxy(int x, int y);
void ssd1306_print(const char* str);
void ssd1306_printf(const char *format, ...);
void ssd1306_puts(const char* str);
void ssd1306_print_at(int x, int y, const char* str);
void draw_letter_at(uint8_t x, uint8_t y, char c);
void draw_pixel(int16_t x, int16_t y, int color);
void drawBitmap(int16_t x, int16_t y, const uint8_t bitmap[], int16_t w,
		int16_t h, uint16_t color);
void ssd1306_home(void);
void ssd1306_show_scr(void);
void fill_scr(uint8_t v);
void clear_scr(void);
void ssd1306_putchar(char c);
void ssd1306_send_data(uint8_t* data, int nbytes);
void ssd1306_test_loop_with(int h);
void ssd1306_write_cmd(uint8_t cmd);
void ssd1306_display_cell(void);
//void print_char(char c, int scale, int xoff);
//void print_str(char *str, int scale);
void print_char_at(char c, int scale, int xoff, int yoff);
void print_str_at(const char *str, int scale, int x, int y);
void show_str(char* str);
void ssd1306_printf_at(int scale, int x, int y, const char *format, ...);

#ifdef __cplusplus
}
#endif

