#include <gpio.h>


/*
void digitalWrite(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState)
{
	HAL_GPIO_WritePin(GPIOx)
}
*/

void gpio_enable_rcc(GPIO_TypeDef *GPIOx)
{
	//enum rcc_periph_clken clken;
	//GPIO_TypeDef *clken = 0;
	// possibly incomplete
	if(GPIOx == GPIOA) {
		__HAL_RCC_GPIOA_CLK_ENABLE() ;
	} else if(GPIOx == GPIOB) {
		__HAL_RCC_GPIOB_CLK_ENABLE() ;
	} else if(GPIOx == GPIOC) {
		__HAL_RCC_GPIOC_CLK_ENABLE() ;
	} else 	if(GPIOx == GPIOD) {
		__HAL_RCC_GPIOD_CLK_ENABLE() ;
	}
}

// You can set multiple pins on same port
void gpio_out(GPIO_TypeDef *GPIOx, uint16_t  GPIO_Pin)
{
  
	gpio_enable_rcc(GPIOx);
	HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_RESET);
	
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = GPIO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOx, &GPIO_InitStruct);
}
void gpio_in(GPIO_TypeDef *GPIOx, uint16_t  GPIO_Pin)
{
  gpio_enable_rcc(GPIOx);

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  GPIO_InitStruct.Pin = GPIO_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOx, &GPIO_InitStruct);
}

int gpio_read(GPIO_TypeDef *GPIOx, uint16_t  GPIO_Pin)
{
  return HAL_GPIO_ReadPin(GPIOx, GPIO_Pin);
}

void gpio_write(GPIO_TypeDef *GPIOx, uint16_t  GPIO_Pin, int level)
{
  HAL_GPIO_WritePin(GPIOx, GPIO_Pin, level);
}
