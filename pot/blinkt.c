#include "blinkt.h"
#if defined(USE_HAL_DRIVER)
//#define clockPin 14
//#define dataPin  15

#define OUTPUT 	GPIO_OUT
#define INPUT 	GPIO_IN
#define HIGH	1
#define LOW	0

typedef uint8_t u8;

static port_t _port;
static pin_t _dataPin, _clockPin;

//static gpio_t _data_port, _clock_port;
//static GPIO_TypeDef * _gpio;

/*
static inline void digitalWrite1(uint16_t pin, uint32_t value)
{
	HAL_GPIO_WritePin(_port, pin, value);
	//gpio_set_level(gpio_t gpio, value);
}
*/

/* user is responsible for configuring pins. Output should be initialised to low, which is generally the default */
void blinkt_init(port_t port, pin_t dataPin, pin_t clockPin)
//void blinkt_init(GPIO_TypeDef* gpio, pin_t data_port, pin_t clock_port)
{
	_port = port;
	_dataPin = dataPin;
	_clockPin = clockPin;

	/*
	gpio_out(port, dataPin);
	gpio_out(port, clockPin);
	digitalWrite(dataPin , LOW);
	digitalWrite(clockPin, LOW);
	*/
}




#define numLEDs  8
static uint8_t pixels[numLEDs * 3];



static void spi_out(uint8_t n) {
	for (uint8_t i = 8; i--; n <<= 1) {
		digitalWrite(_port, _dataPin, n>>7);
		digitalWrite(_port, _clockPin, HIGH);
		digitalWrite(_port, _clockPin, LOW);
	}
}

#if 0 // for when you want to time it, which is about 1ms
u32 cnt = HAL_GetTick();
blinkt_show();
//blinkt_show();
cnt = HAL_GetTick() - cnt;
zseg_show_count(cnt);
HAL_Delay(1000);
#endif

// takes 4.4ms on f411 using libopencm3
// takes 1ms on f411 using HAL
void blinkt_show() 
{
	uint8_t i;
	uint8_t n = numLEDs;
	uint8_t *ptr = pixels;


	for (i = 0; i < 4; i++) spi_out(0x00); // 4 byte start-frame marker

	do {                               // For each pixel...
		spi_out(0xFF);                   //  Pixel start
		for (i = 0; i < 3; i++) spi_out(*ptr++); // Write R,G,B
	} while (--n);

	for (i = 0; i < ((numLEDs + 15) / 16); i++) spi_out(0xFF); // end-frame marker
}

/** pos in range 0..7 incl . 0 is leftmost*/
void blinkt_set_pixel_colour(uint8_t pos , uint8_t r, uint8_t g, uint8_t b) 
{

	pos *= 3;
	pixels[pos++] = b;
	pixels[pos++] = g;
	pixels[pos] = r;
}

void blinkt_test()
{
	// You need to set B12 and B13 as output pins
	int pos = 0;
	blinkt_init(GPIOB, GPIO_PIN_12, GPIO_PIN_13); // 12 as data, 13 as clock
	while(1) {
		for(int i = 0 ; i< 8; i++) {
			blinkt_set_pixel_colour(i, 0, 0, 0);
		}
		blinkt_set_pixel_colour(pos, 0, 0, 64);
		blinkt_show();
		pos++;
		if(pos>7) pos = 0;
		HAL_Delay(500);
	}
}

#endif
