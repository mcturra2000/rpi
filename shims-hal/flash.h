/*
 * flash.h
 *
 *  Created on: Dec 29, 2023
 *      Author: pi
 */

#ifndef INC_FLASH_H_
#define INC_FLASH_H_

#include <stdint.h>

void Write_Flash(uint8_t *data, int n);
void Read_Flash(uint8_t *data, int n);


#endif /* INC_FLASH_H_ */
