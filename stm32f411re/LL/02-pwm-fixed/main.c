#include <stddef.h>

#include "stm32f4xx_ll_rcc.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_system.h"
#include "stm32f4xx_ll_exti.h"
#include "stm32f4xx_ll_cortex.h"
#include "stm32f4xx_ll_utils.h"
#include "stm32f4xx_ll_pwr.h"
#include "stm32f4xx_ll_dma.h"
#include "stm32f4xx_ll_tim.h"
#include "stm32f4xx_ll_gpio.h"

#include "delay.h"



#define LED 	GPIOA, LL_GPIO_PIN_5 // nucleo builtin

static void MX_GPIO_Init(void)
{
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	/* GPIO Ports Clock Enable */
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOC);
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOH);
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);

	LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_5);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_5;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}




static void MX_TIM5_Init_mc(void)
{

	LL_TIM_InitTypeDef TIM_InitStruct = {0};

	LL_TIM_OC_InitTypeDef TIM_OC_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM5);

	TIM_InitStruct.Prescaler = SystemCoreClock/1000000-1;
	//TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	const int freq = 440;
	TIM_InitStruct.Autoreload = 1000000/freq-1; 
	//TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1; //unneeded
	LL_TIM_Init(TIM5, &TIM_InitStruct);
	LL_TIM_EnableARRPreload(TIM5);
	LL_TIM_OC_EnablePreload(TIM5, LL_TIM_CHANNEL_CH1);
	TIM_OC_InitStruct.OCMode = LL_TIM_OCMODE_PWM1;
	//TIM_OC_InitStruct.OCState = LL_TIM_OCSTATE_DISABLE; //unneeded
	//TIM_OC_InitStruct.OCNState = LL_TIM_OCSTATE_DISABLE; //unneeded
	TIM_OC_InitStruct.CompareValue = 1000000/freq/2-1; // 50% duty cycle
	//TIM_OC_InitStruct.OCPolarity = LL_TIM_OCPOLARITY_HIGH; //unneeded
	LL_TIM_OC_Init(TIM5, LL_TIM_CHANNEL_CH1, &TIM_OC_InitStruct);
	//LL_TIM_OC_DisableFast(TIM5, LL_TIM_CHANNEL_CH1);
	//LL_TIM_SetTriggerOutput(TIM5, LL_TIM_TRGO_RESET); //unneeded
	//LL_TIM_DisableMasterSlaveMode(TIM5); //unneeded
	/* USER CODE BEGIN TIM5_Init 2 */

	/* USER CODE END TIM5_Init 2 */
	//LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
	/**TIM5 GPIO Configuration
	  PA0-WKUP   ------> TIM5_CH1
	  */
	GPIO_InitStruct.Pin = LL_GPIO_PIN_0;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_2;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

int main()
{
	MX_GPIO_Init();
	MX_TIM5_Init_mc();

	LL_TIM_CC_EnableChannel(TIM5, LL_TIM_CHANNEL_CH1);
	LL_TIM_EnableCounter(TIM5);


	while(1) {
		LL_GPIO_SetOutputPin(LED);
		delayish(100);
		LL_GPIO_ResetOutputPin(LED);
		delayish(900);
	}

}
