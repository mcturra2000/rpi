# make notes for D programming language

import re


inp = open('notes.txt', "r")
lines = inp.readlines();
inp.close();
#print(lines)

re1 = re.compile(r'\s+')
outd = open("notes.d", "w")
count = 0

for line in lines:
	line = line.strip();
	name, freq = re1.split(line)
	try:
		slash = name.index('/')
		flat = name[slash+1:]
	except ValueError:
		flat = name

	dstr = "immutable auto %s = %s; // %d\n" %( flat, freq, count)
	count += 1
	outd.write(dstr)
	


outd.close()
