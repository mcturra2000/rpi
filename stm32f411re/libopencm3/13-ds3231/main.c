#include <conf.h>

#include <stdlib.h>
#include <time.h>
#include <libopencm3/cm3/nvic.h>
//#include <libopencm3/stm32/rcc.h>
//#include <libopencm3/stm32/gpio.h>
//#include <libopencm3/stm32/i2c.h>
#include <libopencm3/stm32/timer.h>

//#include <string.h>

#include <delay.h>

#include <gpio.h>
//#include <mal.h>
#include <i2c.h>

#include <stdio.h>
#include <string.h>

#include <debounce.h>
#include <ds3231.h>

#define USE_SSD1306
#ifdef USE_SSD1306
#include <ssd1306.h>
#endif

#define LED1    GPIOC, GPIO13 // blackpill
#define LED2    GPIOA, GPIO5 // Nucleo board

#define BTNY	GPIOA, GPIO1 // yellow button
deb_t sw_y = {0}; // yellow
#define BTNG	GPIOB, GPIO0 // green button
deb_t sw_g = {0}; // green
#define BTNB	GPIOA, GPIO7 // blue button
deb_t sw_b = {0}; // blue
#define BTNW	GPIOA, GPIO4 // white button
deb_t sw_w = {0}; // white

volatile uint32_t tick = 0;

void sys_tick_handler(void) // mandatory ISR name
{
#if 0
	tick++;
	int rem = tick % 1024;
	if(rem == 0) 
		gpio_set(LED2);
	if(rem == 100)
		gpio_clear(LED2);
#endif
}

volatile bool refresh_display = true;
volatile int adjust_secs = 0;

void tim2_isr(void) // the standard ISR name for TIM2
{
	timer_clear_flag(TIM2, TIM_SR_UIF); // hmmm, seems to matter that it's at the top
	tick++;
	int rem;
#if 0
	rem = tick % 1024;
	if(rem == 0) 
		gpio_set(LED2);
	if(rem == 100)
		gpio_clear(LED2);

#endif
	//rem = tick % 512;
	if(tick%215 ==0) refresh_display = true;

	if(tick%4==0) {
		deb_update(&sw_y, gpio_get(BTNY));
		//if(deb_falling(&sw_y)) gpio_toggle(LED2);
		deb_update(&sw_g, gpio_get(BTNG));
		//if(deb_falling(&sw_g)) gpio_toggle(LED2);
		deb_update(&sw_b, gpio_get(BTNB));
		if(deb_falling(&sw_b)) { adjust_secs--;  refresh_display = true;  }
		deb_update(&sw_w, gpio_get(BTNW));
		if(deb_falling(&sw_w)) { adjust_secs++; refresh_display = true;  }
	}
}

typedef struct {
	uint32_t peri;
	int rcc;
	uint32_t irq;
} mal_timer_t;

extern const mal_timer_t timer2;
#define TIMER2 &timer2
extern const mal_timer_t timer5;
#define TIMER5 &timer5
void mal_timer_init(const mal_timer_t* timer, int freq);


//                           peri  rcc       irq
const mal_timer_t timer2 = { TIM2, RCC_TIM2, NVIC_TIM2_IRQ};
const mal_timer_t timer5 = { TIM5, RCC_TIM5, NVIC_TIM5_IRQ};


void mal_timer_init(const mal_timer_t* timer, int freq)
{
	rcc_periph_clock_enable(timer->rcc);
	uint32_t clk_freq = rcc_get_timer_clk_freq(timer->peri);
	timer_set_prescaler(timer->peri, clk_freq/1000000-1); // set the CK_CNT (clock counter) freq to 1MHz
	int period = 1000000/freq -1;
	timer_set_period(timer->peri, period); // twice a second. Equivalent to TIM_ARR(TIM2) = period
	timer_generate_event(timer->peri, TIM_EGR_UG); // send an update to reset timer and apply settings
	//timer_generate_event(TIM2, TIM_EGR_UG | TIM_EGR_TG); // equiv: TIM_EGR(TIM2) |= (TIM_EGR_UG | TIM_EGR_UG)
	//timer_enable_update_event(TIM2); // equiv: TIM_CR1(TIM2) &= ~TIM_CR1_UDIS
	timer_enable_counter(timer->peri); // equiv: TIM_CR1(TIM2) |= TIM_CR1_CEN
	timer_enable_irq(timer->peri, TIM_DIER_UIE); // equiv: TIM_DIER(TIM2) |= TIM_DIER_UIE
	timer_enable_preload (timer->peri);
	nvic_enable_irq(timer->irq);
}


int main(void) 
{
	delayish(4); // ssd1306 needs to see to need a bit a delay
	//mal_usart_init();
	puts("DS3231 STM32 test");
	i2c1_init();
	//puts("i2c initialised");
	DS3231_init(DS3231_CONTROL_INTCN, I2C1);
	//puts("ds3231 initialised");
	init_display(64, I2C1);
	//puts("ssd1306 initialised");


	gpio_inpull(BTNY);  sw_y.en = 1;
	gpio_inpull(BTNG);  sw_g.en = 1;
	gpio_inpull(BTNB);  sw_b.en = 1;
	gpio_inpull(BTNW);  sw_w.en = 1;

	gpio_out(LED2);
	//mal_init_systick(); // seems to cause freezes after awhile
	mal_timer_init(TIMER2, 1000); // this causes stalling, too.

	//char msg[100];
	uint32_t t1;
	for(;;) {
		if(!refresh_display) continue;
		nvic_disable_irq(NVIC_TIM2_IRQ);
		volatile int loc_adjust_secs = adjust_secs;
		adjust_secs = 0;
		refresh_display = false;
		nvic_enable_irq(NVIC_TIM2_IRQ);
		//gpio_toggle(LED2);
		if(loc_adjust_secs) {
			int secs = bcdtodec(DS3231_get_addr(0x00));
			//DS3231_get(&t);
			secs += loc_adjust_secs;
			while (secs<0) secs += 60;
			while (secs>59) secs -= 60;
			//secs = 30;
			DS3231_set_addr(0x00, dectobcd(secs));
			//DS3231_set_addr(0x00, 0);
		}

		struct ts t;
		DS3231_get_uk(&t);
		//sprintf(msg, "%d.%02d.%02d %02d:%02d:%02d\n", t.year, t.mon, t.mday, t.hour, t.min, t.sec);
		//ssd1306_printf(msg);
		ssd1306_printf_at(2, 0, 0, "%02d:%02d:%02d", 
				t.hour, t.min, t.sec);
		//printf("Temp: %dC\n\n", (int) DS3231_get_treg());		
		ssd1306_printf_at(2, 0, 24, "%dC", (int) DS3231_get_treg()); 
		t1 = tick;
		show_scr(); // takes about 23ms
		t1 = tick - t1;
		//t1 = tick;
		uint32_t secs = tick/1000;
		uint32_t mins = secs / 60;
		uint32_t hrs = mins/60;
		ssd1306_printf_at(2, 0, 48, "%02d:%02d:%02d", hrs, mins%60, secs %60); 
		//delayish(1000);		
	}
	return 0;
}







