# bme688

Interface with 4-in-1 temperature sensor

Debugging doesn't seem to work, though. There seems to be a problem setting up the clocks.

SDA PB9 (white)
SCL PB8 (yellow)

## See also 

* [BME68x-STM32-HAL](https://github.com/Squieler/BME68x-STM32-HAL) - basis of this code


## Status

2023-07-10  Started. Works
