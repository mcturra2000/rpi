/*
 * rtc.h
 *
 *  Created on: Feb 27, 2024
 *      Author: pi
 *  See cal.h for struct tm
 */

#ifndef INC_RTC_H_
#define INC_RTC_H_

#ifdef __cplusplus
extern "C" {
#endif


#include <shims.h>
#ifndef HAL_RTC_MODULE_ENABLED
typedef unsigned int RTC_HandleTypeDef;
#endif

#include <time.h>

//extern bool rtc_hr_trigger; // an hourly trigger by rtc
extern uint32_t rtc_num_alarms;
void rtc_display_clock();
void rtc_set_from_ds3231();
void rtc_display_date();
void rtc_do_hourly();
//void rtc_get(struct ts *t);
void rtc_set_tm(struct tm *t);
void rtc_set_int(long long timeval);
void rtc_get_tm(struct tm *t);
int rtc_get_uk_tm(struct tm *t);

#ifdef __cplusplus
}
#endif
#endif /* INC_RTC_H_ */
