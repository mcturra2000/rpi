//#include <gpio.h>
#include <stdio.h>
#include <errno.h>
#include <assert.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <linux/gpio.h>
#include <assert.h>


#include <gpiod.h> // sudo apt install libgpiod-doc libgpiod-dev gpiod


typedef struct gpio_v2_line_request pin_t;

int gpio_pin_init(pin_t* pin, int pin_num, enum gpio_v2_line_flag flags)
{
	//int fd = open("/dev/gpiochip0", O_RDONLY);
	int fd = open("/dev/gpiochip0", O_RDWR);
	assert(fd>=0);

	memset(pin, 0, sizeof(pin_t));
	pin->offsets[0] = pin_num;
	pin->config.flags = flags;
	pin->num_lines = 1;
	pin->fd = fd;

	int ret = ioctl(fd, GPIO_V2_GET_LINE_IOCTL , pin);
	if(ret != 0) {
		printf("gpio_pin_init failed for pin %d with errno %d\n", pin_num, errno);
	}

	//close(fd);
	return ret;

}

int gpio_put(pin_t *pin, int val)
{
	struct gpio_v2_line_values vals;
	vals.bits = (val == 0) ? 0 : 1;
	vals.mask = 1;
	int retval = ioctl(pin->fd, GPIO_V2_LINE_SET_VALUES_IOCTL, &vals);
	if(retval!=0)  {
		printf("gpio_out:ioctl errno %d\n", retval);
	}
	return retval;
}

int gpio_out(pin_t *pin, int pin_num)
{
	return gpio_pin_init(pin, pin_num, GPIO_V2_LINE_FLAG_OUTPUT);
}


int gpio_clr(pin_t *pin)
{
	return gpio_put(pin, 0);
}
int gpio_set(pin_t *pin)
{
	return gpio_put(pin, 1);
}


void sleep_ms(int ms)
{
	usleep(ms * 1000);
}


int main()
{
#if 0
	//struct gpiod_chip *chip = gpiod_chip_open_by_name("gpiochip0");
	struct gpiod_chip *chip = gpiod_chip_open("/dev/gpiochip0");
	assert(chip);

	gpiod_chip_close(chip);
	return 0;
#endif
	pin_t builtin_led;
	puts("calling gpio_out");
	gpio_out(&builtin_led, 25);
	puts("calling gpio_set");
	for(;;) {
		gpio_set(&builtin_led);
		sleep_ms(100);
		gpio_clr(&builtin_led);
		sleep_ms(1000);
	}
	
	return 0;
}
