#define F_CPU 8'000'000UL

#include <SoftwareSerial.h>

#define rxPin 1
#define txPin 2


// Set up a new SoftwareSerial object

SoftwareSerial ser =  SoftwareSerial(rxPin, txPin);

#define LED (1<<0) // physical pin 13
//#define LED 0b11111111

#define PORT	PORTA
#define DDR	DDRA

void uart_putchar(char c)
{
	if(c=='\n') uart_putchar('\r');
	//loop_until_bit_is_set(UCSR, UDRE);
	USIDR = c;
}
void setup()  {

  // set clock to 8MHz
  cli();
  CLKPR = (1<<7); // Enable prescaler change
  CLKPR = 0; // set to 8MHz
  sei();

  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
  ser.begin(115200); // 115200 is OK in 8MHz clock. 38400 is OK in 1MHz clock
}


void loop() {
  static char c = 'A';
    if (ser.available() > 0) {
        ser.write(c++);
        if(c > 'Z') {
          ser.write('\r');
          ser.write('\n');
          c = 'A';
        }
        delay(1000);
    }
}
