# Pico

## Setup

Define the `PICOMC` environment variable. For example, add this
to `.bashrc`:
```
export PICOMC=/home/$USER/repos/rpi/pico
```

## Debugging

```
mkbuild
debug
gdb-multiarch
pc # for serial output
mk # to rebuild
```


Tested with picoprobe (post picoprobe-cmsis-v1.02):
```
commit 3a1887ff06047779d7b267bb2402183ab86bd1ef (HEAD -> master, origin/master, origin/HEAD)
Merge: eb49410 13b420d
Author: Luke Wren <luke@raspberrypi.com>
Date:   Thu Jun 22 16:17:39 2023 +0100
```

```
openocd --version # as supplied with Debian Bookworm
0.12.0
```

```
openocd -f interface/cmsis-dap.cfg -f target/rp2040.cfg -s tcl -c "adapter speed 5000"
```

If getting `LIBUSB_ERROR_ACCESS`:
```
Error: Can't find a picoprobe device! Please check device connections and permissions.
```

then add rule to `/etc/udev/rules.d/60-openocd.rules`:
```
# Raspberry Pi Picoprobe
ATTRS{idVendor}=="2e8a", ATTRS{idProduct}=="000c", MODE="660", GROUP="plugdev", TAG+="uaccess"
```
Then:
```
sudo udevadm control --reload
```

Ensure you're a a member of plugdev.
[Source](https://forums.raspberrypi.com/viewtopic.php?t=312867)

In main.c:
```
int main() 
{
        // debug enable fix
        timer_hw->dbgpause = 0; // https://github.com/raspberrypi/pico-sdk/issues/1152
    ...
}
```

[Hardware timer not working after reset under debugger](https://github.com/raspberrypi/pico-sdk/issues/1152)

## Technical notes

* [adc](adc.md)
* [boot](boot.md) - booting into program mode
* [dma](dma.md)
* [gpio](gpio.md)
* [interrupts](interrupts.txt)
* [pwm](pwm.md)
* [serial](serial.md)


## Also in this directory

* [baby](baby.md) 8 sequencer
* [kicad](kicad) - MCU layout for the KiCad electronic circuit design program


## Elsewhere in this repo

* [oled](../1306/pico-sdk)


## Projects

* [freq](freq) - fixed frequency generator using repeating\_timer\_callback
* [freq-adj](freq-adj) - frequency generator whose values can be 
set up over the USB serial port
* [thermo_lcd.py](thermo_lcd.py) - display on-board temperature to LCD

