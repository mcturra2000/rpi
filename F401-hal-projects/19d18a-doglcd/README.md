# doglcd

Makefile works!

Check the ioc file to see what the connections are


Compatible with dev board 19d18

Connections are as follows:
```
C1  5V
C2  PA9   RS
C3  PA10  CS
C4  PB3   CS
C5  PB5   MOSI
C5  GND
```


## Status

2024-07-29  Compilable Makefile. Works
