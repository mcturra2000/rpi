#pragma once

//#ifndef STM32F411xE
//#define STM32F411xE
//#endif
#include "stm32f4xx.h"

#ifdef __cplusplus
extern "C" {
#endif

//static inline uint32_t get32(uint32_t addr) { return *(volatile uint32_t*)addr; }
//static inline void put32(uint32_t addr, uint32_t value) { *(volatile uint32_t*)addr = value; }

#define nop() asm volatile("nop")

#ifdef __cplusplus
}
#endif

