#include <basal.h>

#include <delay.h>
#include <gpio.h>
#include <i2c.h>
#include <rotary.h>
#include <stdbool.h>
#include <ssd1306.h>
#include <systick.h>
#include <tim.h>



//#define DOWN	GPIOA, 0
#define LED	GPIOB, 9
//#define SPK 	GPIOA, 0
#define SPK 	GPIOB, 0


rotary_t dial1, dial2;
volatile int val1 = 100;
volatile int val2 = 80;


void  SysTick_Handler(void)
{
	val1 += 4*rotary_poll(&dial1);
	if(val1 <0) val1 = 0;
	val2 += 4*rotary_poll(&dial2);
	if(val2<0) val2 = 0;
}



void TIM2_IRQHandler(void)
{
	if(TIM2->SR & TIM_SR_UIF) TIM2->SR = ~TIM_SR_UIF; // clear flag
	static volatile uint32_t time1 =0;
	//static volatile uint32_t ping_time =0;
	static volatile uint32_t time2 =0;
	//static volatile uint32_t oneshot_start_time =0;
	//static volatile bool spk_on = false;

	//tim2++;
	//ping_time++;
	//tim_val2++;

	//int triggered = 0;
	time1++;
	if(time1 >= val1) {
		time1 -= val1;
		//triggered = 1;
		gpio_toggle(SPK);
	}

	time2++;
	if(time2 >= val2) {
		time2 -= val2;
		//triggered = 1;
		gpio_toggle(SPK);
	}

	/*
	return;

	if(triggered && (!spk_on)) {
		spk_on = true;
		gpio_set(SPK);
		//triggered = 0;
		oneshot_start_time = tim2;
	}
	if((tim2-oneshot_start_time >= shot_val) && spk_on) {
		spk_on = false;
		gpio_clr(SPK);
	}
	*/

}



int main (void) 
{   
	gpio_out(LED);
	gpio_out(SPK);
	rotary_init(&dial1, GPIOC, 6, 5); // decr, incr
	rotary_init(&dial2, GPIOC, 1, 0);

	tim2_5_init(TIM2, 100000); // timer updates ever 10us
	systick_init_1ms();

	i2c1_init();
	init_display(64, 0);
	int count = 0;

	while (1) 
	{
		clear_scr();
		ssd1306_printf("COUNT: %4d\nVAL 1: %4d\nVAL 2: %4d\n", count++, val1, val2);
		show_scr();
		delayish(100);
	}
}
