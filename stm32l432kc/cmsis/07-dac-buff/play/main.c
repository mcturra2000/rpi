/*
 * slurp file:
 * https://gist.github.com/w-vi/7e98342181776162b1a3
 */

#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <bcm2835.h>


#define RDY	22 //ready pin

uint8_t *source = NULL;
long bufsize = 0;
void slurp()
{
	FILE *fp = fopen("/home/pi/Music/song-16k.raw", "r");
	assert(fp != NULL);
	/* Go to the end of the file. */
	if (fseek(fp, 0L, SEEK_END) == 0) {
		/* Get the size of the file. */
		bufsize = ftell(fp);
		if (bufsize == -1) { /* Error */ }

		/* Allocate our buffer to that size. */
		source = malloc(sizeof(char) * (bufsize + 1));

		/* Go back to the start of the file. */
		if (fseek(fp, 0L, SEEK_SET) != 0) { /* Error */ }

		/* Read the entire file into memory. */
		size_t newLen = fread(source, sizeof(char), bufsize, fp);
		if (newLen == 0) {
			fputs("Error reading file", stderr);
		} else {
			source[++newLen] = '\0'; /* Just to be safe. */
		}
	}
	fclose(fp);
}

void sleep_us(int us)
{
	struct timespec req;
	req.tv_nsec = us * 1000;
	req.tv_sec = 0;
	clock_nanosleep(CLOCK_MONOTONIC, 0, &req, NULL);
}

int main()
{
	slurp();
	printf("Buffer size: %d\n", bufsize);

	assert(bcm2835_init() != 0);

	bcm2835_gpio_fsel(RDY, BCM2835_GPIO_FSEL_INPT);

	bcm2835_spi_begin();
	bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);      // The default
	bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);                   // The default
	//bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_65536); // The default
	//bcm2835_spi_setClockDivider(32); 
	//bcm2835_spi_setClockDivider(64); 
	bcm2835_spi_setClockDivider(64 *8); // 16 too fast ... for Pi1
	//bcm2835_spi_setClockDivider(64 *4); // 16 too fast ... for Pi1
	bcm2835_spi_chipSelect(BCM2835_SPI_CS0);                      // The default
	bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);      // the default

	// Send a byte to the slave and simultaneously read a byte back from the slave
	// If you tie MISO to MOSI, you should read back what was sent
	//uint8_t data = bcm2835_spi_transfer(0x23);
	//printf("Read from SPI: %02X\n", data);
	for(int i = 0; i < bufsize; i++) {
#if 0
		uint8_t v1 = source[i];
		uint16_t vol = v1;
		vol <<= 4;
#else
		uint16_t vol = source[i] << 4;
#endif
		//vol = __bswap_16(vol);
		while(bcm2835_gpio_lev(RDY)==0) // wait until pin goes high
			sleep_us(10);

		//vol = i;
		//bcm2835_spi_transfer(vol);
		bcm2835_spi_write(vol);
	}

	bcm2835_spi_end();
	bcm2835_close();
	return 0;
}
