#include "../rp2040-cmsis-1.h"
#include <assert.h>

typedef unsigned int uint;
static_assert(sizeof(int) == 4, "Unexpected size of int");
//static_assert( 5, "Oops");

typedef __UINT64_TYPE__ uint64_t;

#define RESETS_TIMER		(1ul<<21)
#define RESETS_PWM		(1ul<<14)
#define RESETS_PAD_BANK0	(1ul<<8)
#define RESETS_IO_BANK0		(1ul<<5)

// §2.14 subsystem resets
// reset a peripheral(s), waiting until the are back up
// you can set several peripherals at once, or iseparately in asequence
// E.g. reset_blocking(RESETS_IO_BANK0 | RESETS_PWM);
void reset_blocking(uint32_t periphs)
{
	//REG(RESETS_BASE + 0x00 + 0x3000) = periphs; // 2.14.3 Reset register using atomic clear 0x3000
	//RESETS->RESET = periph;
	//while(!(RESETS->RESET_DONE & periphs)); // wait until the peripherals are up
	RESETS->RESET &= ~periphs;
	//RESETS->RESET |= periphs;
	while(!(RESETS->RESET_DONE & periphs));

}



#define GPIO_FUNC_SIO	5
//#define LED 25
#define LED 14

// unsafe for 2 cores
uint64_t get_timer()
{
#if 1
	uint32_t lo = TIMER->TIMELR;
	uint32_t hi = TIMER->TIMEHR;
	return ((uint64_t) hi << 32u) | lo;
#else
	uint32_t lo = TIMER->TIMERAWL;
	uint32_t hi = TIMER->TIMERAWH;
	return ((uint64_t) hi << 32u) | lo;
#endif

}

void delay_ms(uint ms)
{
#if 0
	volatile uint64_t start = TIMER->TIMELR;
	uint64_t us = (uint64_t) ms * 1000;
	while((TIMER->TIMELR - start) < us);
#else
	uint64_t start = get_timer();
	uint64_t us = (uint64_t) ms * 1000;
	while(get_timer() - start < us);
#endif
}



static void gpio_clr_mask(uint32_t mask)
{
	SIO->GPIO_OUT_CLR = mask;
}

void gpio_set (uint gpio) { SIO->GPIO_OUT_SET = 1ul << gpio; }

void gpio_clr (uint gpio) { SIO->GPIO_OUT_CLR = 1ul << gpio; }

void gpio_out (uint gpio)
{
	REG(IO_BANK0_BASE + 0x4 + 0x8 * gpio ) = 5; // set proper _CTRL register address to 5 (GPIO)
	SIO->GPIO_OE_SET = 1ul << gpio; // allow setting of output
}


/*
#define XOSC_CTRL_FREQ_RANGE_VALUE_1_15MHZ 666
#define xosc_hw_ctrl XOSC->
void xosc_init(void) {
    // Assumes 1-15 MHz input, checked above.
    //xosc_hw->ctrl = XOSC_CTRL_FREQ_RANGE_VALUE_1_15MHZ;
    xosc_hw_ctrl = XOSC_CTRL_FREQ_RANGE_VALUE_1_15MHZ;

    // Set xosc startup delay
    xosc_hw->startup = STARTUP_DELAY;

    // Set the enable bit now that we have set freq range and startup delay
    hw_set_bits(&xosc_hw->ctrl, XOSC_CTRL_ENABLE_VALUE_ENABLE << XOSC_CTRL_ENABLE_LSB);

    // Wait for XOSC to be stable
    while(!(xosc_hw->status & XOSC_STATUS_STABLE_BITS));
}
*/

int main()
{
	//reset_blocking(RESETS_IO_BANK0 | RESETS_PAD_BANK0);
	//reset_blocking(RESETS_IO_BANK0 | RESETS_PAD_BANK0 | RESETS_TIMER);
	reset_blocking(RESETS_PAD_BANK0);
	reset_blocking(RESETS_IO_BANK0);
	reset_blocking(RESETS_TIMER); 

	//xosc_init();
	CLOCKS->CLK_SYS_RESUS_CTRL |= (1<<8); // enable resus. maybe unnecessary. didn't help

	
	XOSC->CTRL = 0xaa0; // FREQ_RANGE assumed to be 1-15MHz
	//XOSC->STARTUP = 47; // startup delay
	XOSC->STARTUP = 0xc4;
	XOSC->CTRL |= (0xfab << 12); // enable XOSC
	while(!(XOSC->STATUS & (1<<31))); // wait for XOSC to be running and stable

	ROSC->CTRL |= (0xd1e <<12); // disable rosc

	CLOCKS->CLK_REF_CTRL = 0x2; // xosc clock source // seems useful for getting timing right


	CLOCKS->CLK_REF_CTRL = (0x2); // clock ref if xosc
	CLOCKS->CLK_SYS_CTRL = (0x3<<5) ;
	CLOCKS->CLK_PERI_CTRL = (0x4 << 5)  // use xosc_clksrc in AUXSRC
		| (1<< 11) // enable
		;

	//reset_blocking(RESETS_TIMER); 
	//reset_blocking(RESETS_IO_BANK0 | RESETS_PAD_BANK0);

	IO_BANK0->GPIO0_CTRL = GPIO_FUNC_SIO; // init pin - select function SIO
	IO_BANK0->GPIO25_CTRL = GPIO_FUNC_SIO; // init pin - select function SIO
	//SIO->GPIO_OE_SET = 1ul << LED; // allow setting of output
	gpio_out(LED);

	while(1) {
		SIO->GPIO_OUT_SET = (1ul << LED); // turn on gpio atomically 
		delay_ms(10);
		SIO->GPIO_OUT_CLR = (1ul << LED); // turn off gpio atomically
		delay_ms(90);
	}

	return 0;
}
