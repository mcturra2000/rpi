#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
//#include "hardware/adc.h"
//#include "hardware/clocks.h"
//#include "hardware/flash.h"
#include "hardware/gpio.h"
//#include "hardware/irq.h"
//#include "hardware/pwm.h"
//#include "hardware/spi.h"
#if LIB_PICO_STDIO_USB
#pragma message "USB configured as stdio"
#include "tusb.h" // if you want to use tud_cdc_connected()
#endif


#include "pi.h"

/*
#ifdef RASPBERRYPI_PICO // this works
#pragma message "I'm a pico"
#endif
*/

#include "TM1637Display.h"
//TM1637Display tm1637(16, 17); // CLK, DIO


int main() 
{
    timer_hw->dbgpause = 0; // debug pause
    tm1637_test(); // CLK=16, DIO=17
#if 0
    tm1637_init_pico(16, 17); // CLK, DIO

#define LED  25 // GPIO of built-in LED
	pi_gpio_out(LED);

	int i = 0;
	for(;;) {
		tm1637_show_dec(i);
		i++;
		pi_gpio_toggle(LED);
		sleep_ms(1000);		
	}
#endif

	return 0;
}

