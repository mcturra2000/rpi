//#include <stdio.h>
//#include <string.h>
#include "hardware/irq.h"
#include "hardware/uart.h"
#include "pico/sync.h"
//#include "pico/time.h"

#include <pi.h>
#include "doglcd.h"
#include "circular.h"


/*
 * power connections:
 * VIN pin of LCD should go to Pico pin 40 (VBUS). being 5V
 * GND of LCD to any GND pin of Pico
 *
 * 2023-02-02	Confirmed
 */


circle<1024, char> buf;



void uart_rx_handler()
{
	char c = (uart_get_hw(uart0)->dr) & 0xFF;
	buf.write(c);
}


// Blocking receive
int get ()
{
	char c;
	while(!buf.avail());
	irq_set_enabled(UART0_IRQ, false);
	buf.read(&c);
	irq_set_enabled(UART0_IRQ, true);
	return c;
}

int main() 
{
	uart_init(uart0, 115200);
	//gpio_set_function(0, GPIO_FUNC_UART); // TX
	gpio_set_function(1, GPIO_FUNC_UART); // RX


	sleep_ms(10); // a little delay seems to help for the Tiny
	doglcd_init();
	irq_set_exclusive_handler(UART0_IRQ , uart_rx_handler);
	uart_set_irq_enables(uart0, true, false); // handle something in RX buffer
	irq_set_enabled(UART0_IRQ, true); // necessary

	char c;
	for(;;) {
		c = get();

		// check for escape sequencence
		if(c == 0x1b) {
			c = get();
			if(c != '[') continue;
			c = get();
			switch(c) {
				case 'H':
					doglcd_gotoxy(0, 0);
					sleep_ms(2);
					break;		
				case 'J':
					doglcd_cls();
					sleep_ms(2); // seems necessary
					break;
				default: // unrecognised input
					continue;
			}
		} else
			doglcd_write_char(c);
	}
}

