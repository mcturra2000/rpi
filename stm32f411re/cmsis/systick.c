#include "systick.h"

extern uint32_t SystemCoreClock;

/* set systick to trigger every 1 ms
*/

void systick_init_1ms()
{
	SysTick_Config(SystemCoreClock/1000); // set tick to every 1ms
}
