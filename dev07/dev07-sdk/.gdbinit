# 2023-09-11	snapshot.
#		monitor reset init is possibly wrong. See blink sketch in HAL
#		for a likely fix to this
# 2023-07-19	snapshot
# 2023-07-01 	snapshot
cd build

# quit without confirmation
define hook-quit
    set confirm off 
end

set pagination off
file app.elf
#target remote localhost:3333 # old method as at 2023-07-19
target extended-remote localhost:3333
monitor reset halt 
load
#b main
#b main.c:32
#monitor reset init
echo RUNNING...\n
c
