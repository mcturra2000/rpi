# ili9341

New project which separates out ili9341 into a library.

Flexible custom-modes are available.

To produce the raw image, run `bmp.py`, which uses the Python Image Library
and converts a `bmp` into a `raw` file

## Status

2023-03-02	Mostly deprecate mode1 and 2 in favour of custom-made modes.
		Added support for raw binaries

2023-01-22	Breaking change (unincorporated). Mode 0 now needs to call
		mode0_refresh_screen() directly for screen refresh
		
2023-01-02	Started. Working.
