# gpio notes

gpioinfo - prints info about gpiochips and "lines" (i.e. pins)

## References

* [gpio.h](https://github.com/torvalds/linux/blob/master/include/uapi/linux/gpio.h) kernel. Useful reference.


https://www.kernel.org/doc/html/v5.4/i2c/dev-interface.html

https://blog.lxsang.me/post/id/33

## Obsolete

libgpiod doesn't particularly seem to help over ioctl

sudo apt install gpiod libgpiod-dev libgpiod-doc

lynx /usr/share/doc/libgpiod-dev/html/files.html


