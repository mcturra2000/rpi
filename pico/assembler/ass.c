#include "common.h"
//#include <unistd.h> // needed for getopt
#include <getopt.h> // needed for getopt


jmp_buf jmp_syn_error;

uint16_t opcode0; //regular opcode
uint16_t opcode1; // for when the opcodes are 16 bits
int address;

enum yytype_e yytype;

char *source = NULL;
char *yystart; // start of token
long source_len;
int yyline; // line number
int yylen; // length of token
int yylval;
int arg[4]; // arguments to the assembler opcode
static_assert(sizeof(int) >= 4 , "yylval needs to be at least 4 bytes long");


// a syntax eeero has occurred
void syn (char *msg)
{
	printf("%s\n", msg);
	longjmp(jmp_syn_error, 1);
}

uint8_t bin[4096]; // binary output. restricted to 1k for now


/* FN emit_opcode */
void emit_opcode (char * name, uint16_t value)
{
	printf("    %4x:\t %04x // %s \n", address, value, name);
	bin[address] = value;
	bin[address+1] = (value>>8);
	address += 2;

}
/* FN emit_opcode0 */
void emit_opcode0 (char * name) // deprecate
{
	emit_opcode(name, opcode0);
}

/* FN expect */
void expect (enum yytype_e expected_yytype, char *msg)
{
	yylex();
	if(yytype == expected_yytype) return;
	syn(msg);
}



char* slurp(char* filename, long *len)
{
	FILE *fp = fopen(filename, "r");
	if(fp == NULL) return 0;

	char* source = 0;
	*len = -1;

	// find its length
	if (fseek(fp, 0L, SEEK_END) != 0) goto finis;	
	*len = ftell(fp);
	if (*len == -1) goto finis;

	source = malloc(sizeof(char) * (*len + 1));

	/* Go back to the start of the file. */
	if (fseek(fp, 0L, SEEK_SET) != 0) { /* Error */ }

	/* Read the entire file into memory. */
	size_t newLen = fread(source, sizeof(char), *len, fp);
	if (newLen == 0) {
		fputs("Error reading file", stderr);
	} else {
		source[newLen] = '\0'; /* Just to be safe. */
	}

finis:
	fclose(fp);
	return source;
}

typedef void (*fptr)(void);


void match_list(char* name, uint32_t code) // expect to see something like {r4, pc}
{
	yylex();
	if(!yyfind("{")) syn("expected '{'"); // generate a syntax error
	//opcode0 = 0b1011010 << 9;

	while(1) {
		if(yylex() == 0) syn("pushi/pop paramater not understood");
		//printf("yytype = %d\n", yytype);
		if(yytype == T_RN) { // check range
			code |= (1 << yylval);
			//puts("found rn");
		} else  if(yytype == T_LR || yytype == T_PC){
			code |= (1 << 8);
			//puts("found lr/sp");
		} else 
			syn("Expected to find register");


		if(yylex() == 0) syn("unexpected end of parameter list");
		if(yytype == ',') {
			//puts("found comma");
			continue;
		}
		if(yytype == '}') {
			//puts("found }");
			break;
		} 
		syn("Unexpected type in parameter list");
	}
	emit_opcode(name, code);
}

/* FN match_push */
bool match_push ()
{
	if(!yyfind("push")) return false;
	//puts("match_push called");
	match_list("push", 0b1011010<<9);
	//puts("finished push scan");
	//printf("opcode0 = %04x\n", opcode0);
	return true;
}

/* FN match_pop */
bool match_pop ()
{
	if(!yyfind("pop")) return false;
	//puts("match_ppop called");
	match_list("pop", 0b1011110<<9);
	return true;
}

void print_token()
{
	printf("token = <");
	for(int i = 0; i < yylen; i++) putchar(yystart[i]);
	printf(">\n");
}

/* FN yyfind 
 * bool yyfind (char* keyword)
 **/

bool yyfind (char* keyword)
{
	for(int i = 0; i < yylen; i++ ) {
		if(tolower(*(yystart +i)) != *(keyword+i)) return false;
	}
	// ensure further that we have reached the end of the keyword
	if(*(keyword+yylen) != 0) return false;

	yystart += yylen;
	yylen = 0;

	return true;

}

void yyeat()
{
loop:
	while(isspace(*yystart)) {
		if(*yystart == '\n') yyline++;		
		yystart++;
	}
	if(*yystart == '/' && *(yystart+1) == '/') {
		while(*yystart != '\n' && *yystart != 0) yystart++;
		goto loop;
	}

}

/* FN yyscan
 * int yyscan (char *re)
 * a cut-price regex engine
 */
int yyscan (char *re)
{
	//assert(re != 0);
	//printf("yyscan re passed in: %c\n", *re);
	while(*re) {
		char c = *(yystart+yylen);
		switch(*re) {
			case '1': // one or more digits
				if(isdigit(c)) yylen++;
				else goto fail;
				while(isdigit(*(yystart+yylen))) yylen++;
				break;
			case 'a': // one a-zA-Z
				if(isalpha(c)) yylen++;
				else goto fail;
				break;
			case 'b': // zero or more a-zA-Z0-9
				while(isalnum(*(yystart+yylen))) yylen++;
				break;
			case 'X': // one or more hex digits
				if(isxdigit(c)) yylen++;
				else goto fail;
				while(isxdigit(*(yystart+yylen))) yylen++;
				break;
			case 'z': // something that isn't alphanum. It's not included in match, and it ends match
				if(isalnum(c)) goto fail;
				else break;
			case '*': // anything that isn't   \0, \t, \n or space
				if(c == 0 || c == '\t' || c == '\n' || c == ' ') goto fail;
				yylen++;
				break;
			default:
				// will include c, l, p, r, s, =
				//if(c == '=') printf("Found c as '=', *re as %c\n", *re);
				if(tolower(c) == *re) yylen++;
				else goto fail;
		}
		re++;
	}
	return 1;
fail:
	//puts("yyscan fail");
	yylen = 0;
	return 0;


}

/* FN set_yylval16
 * void set_yylval16(int off)
 * off specifies offset in case there is an expected prefix
 * Works in hex
 */
void set_yylval16(int off) 
{
	yylval = 0;
	for(int i = off; i < yylen; i++) { // NB skip the initial '#'
		char c = tolower(yystart[i]);
		int val = c - '0';
		if(!isdigit(c)) val = c -'a' +10;
		yylval = yylval * 16 +val;
		//printf("yylval = %d\n", yylval);
	}
}

/* FN set_yylval
 * void set_yylval(int off)
 * off specifies offset in case there is an expected prefix
 */
void set_yylval(int off) 
{
	yylval = 0;
	for(int i = off; i < yylen; i++) { // NB skip the initial '#'
		yylval = yylval * 10 + yystart[i] - '0';
		//printf("yylval = %d\n", yylval);
	}
}

/*
 * FN yylex
 */
int yylex ()
{
	yystart += yylen;
	yylen = 0;
	yytype = 0;
	yylval = 0;
	yyeat();

	if(yyscan("=abz"))	{ 
		//printf("yylex found a label of len = %d\n", yylen-1);
		return yytype = T_LABEL; 
	} 
	if(yyscan(".abz"))	{ return yytype = T_DIR; } // a directive of some sort
	if(yyscan("#1z"))	{ set_yylval(1); return yytype = T_IMM; }
	if(yyscan("1z"))	{ set_yylval(0); return yytype = T_NUM; }
	if(yyscan("0xXz"))	{ set_yylval16(2); return yytype = T_NUM; }
	if(yyscan("lrz")) 	{ return yytype = T_LR; }
	if(yyscan("r1z")) 	{ set_yylval(1); return yytype = T_RN; }
	if(yyscan("pcz"))	{ return yytype = T_PC; }
	if(yyscan("spz")) 	{ return yytype = T_SP; }
	if(yyscan("ab")) 	{ return yytype = T_ID; }
	if(yyscan("*"))  	{ return yytype = *(yystart); }
	return 0;


}




bool match_directive()
{
	if(yytype != T_DIR) return false;
	//puts("directive!");
	if(yyfind(".align")) {
		expect(T_NUM, "Expected number");
		// add some nops if necessary
		while(address % yylval != 0) {
			opcode0 = 0x46c0; // the nop (although actually mov r8, r8)
			emit_opcode0("align nop");
		}
		//printf("found align: %d\n", yylval);
		return true;
	}
	if(yyfind(".word")) {
		expect(T_NUM, "Expected number");
		emit_opcode("word low", yylval & 0xffff);
		emit_opcode("word high", yylval >>16);
		return true;
	}

	if(yyfind(".address")) {
		expect(T_NUM, "Expected number");
		address = yylval;
		return true;
	}

	syn("Unknown directive");
	return false;
}

void parse_stmt()
{
	if(match_directive() || match_code() || match_push() || match_pop() || match_branch()) return;
	if(matched_label_def()) return;

	syn("unrecognised statement");
}

void parse_top ()
{
	address = 0;
	yystart = source;
	yyline = 1;
	yylen = 0;

	while(yylex()) {
		parse_stmt();
	}
	fill_holes(); // resolve forward labels

	FILE *fbin = fopen("prog.bin", "wb");
	assert(fbin);
	fwrite(bin, address, 1, fbin);
	fclose(fbin);
}


int main (int argc, char *argv[])
{
	//parse_encodings(); return 0;
	int opt;
	while((opt = getopt(argc, argv, "")) != -1) ;
	if(optind >= argc) {
		fprintf(stderr, "Expected argument after options\n");
		exit(EXIT_FAILURE);
	}

	char *srcfile = argv[optind];
	printf("Assembling: %s\n", srcfile);



	source = slurp(srcfile, &source_len);
	//puts("so far so good");

	//list_encodings();
	init_labels();
	if(setjmp(jmp_syn_error) == 0) {
		// try
		parse_top();
	} else {
		// catch
		printf("syntax error in line %d\n", yyline);
	}
	//print_labels();
	deinit_labels();
	free(source);
	return 0;
}
