import smbus, time

bus = smbus.SMBus(1)
addr = 0x4

count = 0
while True:
    count += 1
    b = bus.read_byte(addr)
    print("Count: " + repr( count) +  ", Value: " +  repr(b))
    time.sleep(1)

bus.close()
