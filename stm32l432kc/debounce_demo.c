/* debouncinh
 *
 * 2022-02-211	Started
 */

//#include "conf.h"
#include "debounce.h"
#include "delay.h"
#include "gpio.h"
#include "uart.h"
#include "jg_printf.h"

#define SW 	D12
deb_t sw_deb = {0};

volatile uint32_t  ticks = 0; // must be volatile to prevent compiler optimisations
void SysTick_Handler(void)
{
        ticks++;
	if(ticks % 4 == 0)
		deb_update(&sw_deb, gpio_get(SW));

        if(ticks % 500 == 0)
                gpio_toggle(LD3);
}



int main (void) 
{
	uart2_init();
	printf2("Debounce test\n");
	
	gpio_inpull(D12);
	sw_deb.en = 1;

        SysTick_Config(SystemCoreClock/1000); // set tick to every 1ms


	gpio_out(LD3);

	int count = 0;
	while(1) {
		if(deb_falling(&sw_deb)) printf2("%d:button press detected\n", count++);
		if(deb_rising(&sw_deb)) printf2("%d:button release detected\n", count++);
		continue;
		gpio_set(LD3);
		delayish(100); //delay in  ms
		gpio_clr(LD3);
		delayish(900); //delay in  ms
	}
}
