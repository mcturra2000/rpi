#define STM32F411xE
#include "stm32f4xx.h"

#include "queue.h"

#define CS 	GPIOA, 4

uint32_t SystemCoreClock = 16000000;

volatile uint32_t  ticks = 0; // must be volatile to prevent compiler optimisations

void  SysTick_Handler(void)
{
	ticks++;
}


#define AF5	5 


static inline void gpio_out(GPIO_TypeDef *port, uint32_t pin)
{
	RCC->AHB1ENR	|= (1<< ((port - GPIOA)/(GPIOB - GPIOA)));
	port->MODER	|= (1<<(pin*2));
}


static inline void gpio_alt(GPIO_TypeDef *port, uint32_t pin, uint8_t alt_fn)
{
	port->MODER	|= (0b10<<(pin*2));

	int pos = pin*4;
	uint8_t idx = 0;
	if(pin>7) { // not tested properly
		pos -= 32;
		idx = 1;
	}
	port->AFR[idx] |= (alt_fn  << pos);
}

static inline void gpio_set(GPIO_TypeDef *port, uint32_t pin)
{
	port->BSRR = (1 << pin);
}


static inline void gpio_clr(GPIO_TypeDef *port, uint32_t pin)
{
	port->BSRR = (1 << (pin+16));
}


static inline void gpio_toggle(GPIO_TypeDef *port, uint32_t pin)
{
	if(port->ODR & (1<<pin))
		gpio_clr(port, pin);
	else
		gpio_set(port, pin);
}

void spi_send(void)
{
	static SPI_TypeDef* spi = SPI1;
	static void *where = &&start;
	goto *where;

start:
	if(isEmpty()) return;
	gpio_clr(CS);
	spi->DR = removeData();
	where = &&transmitting;
transmitting:
	if(!(spi->SR & SPI_SR_TXE)) return;
	where = &&busy;
busy:
	if(spi->SR & SPI_SR_BSY) return;
	gpio_set(CS);
	where = &&start;

}


void zseg_send(uint8_t address, uint8_t value)
{
	insert((address << 8 ) | value);
}


void zseg_init(void)
{
	zseg_send(0x0F, 0x00);
	zseg_send(0x09, 0xFF); // Enable mode B
	zseg_send(0x0A, 0x0F); // set intensity (page 9)
	zseg_send(0x0B, 0x07); // use all pins
	zseg_send(0x0C, 0x01); // Turn on chip
}

void zseg_show_count(int count)
{
	uint32_t num = count;
	for (uint8_t i = 0; i < 8; ++i)
	{
		uint8_t c = num % 10;
		num /= 10;
		uint8_t sep = 0; // thousands separator

		// add in thousands separators
		if ((i > 0) && (i % 3 == 0))
		{
			sep = 1 << 7;
		}

		// blank if end of number
		if ((c == 0) && (num == 0) && (i > 0))
		{
			sep = 0;
			c = 0b1111;
		}

		c |= sep;

		zseg_send(i + 1, c);
	}

}


void spi_init(void)
{
	gpio_out(CS); // PA4 CS set up as a normal output pin
	gpio_alt(GPIOA, 5, AF5); // PA5 CLK setup
	gpio_alt(GPIOA, 7, AF5); // PA7 MOSI setup

	RCC->APB2ENR	|= RCC_APB2ENR_SPI1EN; // enable SPI1 clock

	SPI1->CR1	|= 0
		//| (0b111 <<SPI_CR1_BR_Pos) // Baud rate f_pclk/256
		| SPI_CR1_MSTR // master mode
		| SPI_CR1_SSM
		| SPI_CR1_SSI
		| SPI_CR1_DFF // 16-bit mode
		;

	SPI1->CR1	|= SPI_CR1_SPE; // enable spi
}


int main (void) 
{   
	SysTick_Config(SystemCoreClock/1000); // set tick to every 1ms
	spi_init();

	gpio_out(GPIOB, 12); // enable PB12 for timing experiments
	zseg_init();
	while (1) 
	{
		static int prev_secs = -1;
		volatile int secs = ticks/1000;
		if(secs > prev_secs) {
			zseg_show_count(secs);
			prev_secs = secs;
		}

		spi_send();
		gpio_toggle(GPIOB, 12);
	}
}
