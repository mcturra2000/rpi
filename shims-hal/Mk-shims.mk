# mcarter stuff added
C_INCLUDES += -I$(RPI)/shims-hal
C_INCLUDES += -I${RPI}/pot



#VPATH += $(RPI)/shims-hal
#VPATH += $(RPI)/shims-hal/pot

#C_SOURCES += \
#brown.c circ.c delay.c i2c.c rtc.c \
#cal.c ds3231.c blinkt.c debounce.c gpio.c zseg.c \
#om.c spi.c usb.c usecs.c  utils.c \
#app.c

ifneq ($(wildcard Core/Src/app.c),)
	C_SOURCES += app.c
endif

C_SOURCES += $(wildcard $(RPI)/shims-hal/*.c) 
C_SOURCES += $(wildcard $(RPI)/pot/*.c) 

#circ.c

.flash : $(BUILD_DIR)/$(TARGET).bin
	touch .flash
	flash-stm32 -f $(BUILD_DIR)/$(TARGET).bin
	#st-flash --connect-under-reset write $(BUILD_DIR)/$(TARGET).bin 0x8000000
flash : .flash
	true

.DEFAULT_GOAL := all

# Print brief out. use make V=1 for load output 
$(V).SILENT:
