Enable IRQ in GUI

main, to kickoff: 
```
  HAL_SPI_Receive_IT(&hspi5, (uint8_t*) &slave_rx5, 1);
```

```
static volatile uint8_t  slave_rx5 = 0;
void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef * hspi)
{
	if(hspi->Instance == SPI5) {
		HAL_GPIO_TogglePin(YLLW); // for debug purposes
		HAL_SPI_Receive_IT(&hspi5, (uint8_t*) &slave_rx5, 1);
	}
}
```

