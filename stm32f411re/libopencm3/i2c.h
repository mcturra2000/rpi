#pragma once

#include <conf.h>
#include <libopencm3/stm32/i2c.h>

void i2c1_init(void);
void i2c_ship(uint32_t i2c, uint8_t addr, const uint8_t *w, size_t wn);

