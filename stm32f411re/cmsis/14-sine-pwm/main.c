#define STM32F411xE
#include "stm32f4xx.h"

#include <math.h>

#include <stdbool.h>

//#include <inttypes.h>
//#include <stdint.h>
#define ARM_MATH_CM4 1
//#include <math.h>
//#include <cmsis_gcc.h>
#include <arm_math.h>


#include <basal.h>

#include <delay.h>
#include <gpio.h>
#include <uart.h>
#include <printf.h>


uint32_t SystemCoreClock = 16000000;


//const float dt = 1000000.0/40000.0; // time increment (40kHz)
const float samp_freq = 40000.0;
const float dt = 1.0/samp_freq; // time increment (40kHz)
float t =0; // time
//const float t2 = 1000000.0/440; // period of 440Hz
//const float t1 = t2 *0.25;// duty cycle of 25%

//extern float32_t arm_sin_f32(float32_t);


bool counter_empty = true;
volatile float counter_value = 0.1;

void pwm_set_duty(TIM_TypeDef *tim, int ch, float duty);

/*
	TIM_TypeDef *ptr_tim = TIM2; // assume TIM2 for now	
	if(tim == 3) {ptr_tim = TIM3; }
	else if(tim == 4) {ptr_tim = TIM4; }
	else if(tim == 5) {ptr_tim = TIM5; }
*/


int rcc_enable_tim(TIM_TypeDef *tim)
{
	if(tim == TIM2) { RCC->APB1ENR |= RCC_APB1ENR_TIM2EN; }
	else if(tim == TIM3) { RCC->APB1ENR |= RCC_APB1ENR_TIM3EN; }
	else if(tim == TIM4) { RCC->APB1ENR |= RCC_APB1ENR_TIM4EN; }
	else if(tim == TIM5) { RCC->APB1ENR |= RCC_APB1ENR_TIM5EN; }
	else return 1;

	return 0; // we handled it OK
}


void TIM2_IRQHandler(void)
{
	if(TIM2->SR & TIM_SR_UIF)
		TIM2->SR = ~TIM_SR_UIF; // clear flag
	//GPIOC->ODR ^= GPIO_ODR_OD3; // PC3 toggle
	pwm_set_duty(TIM2, 3, counter_value);
	counter_empty = true;
}

void tim_init(TIM_TypeDef *tim)
{
	rcc_enable_tim(tim);
	//RCC->APB1ENR |= RCC_APB1ENR_TIM2EN; // enable TIM2 clock
	tim->PSC = 16000000/1000000-1; // pre-scale to 1us
	tim->ARR = 1000000/40000-1; //40kHz Auto Reload Value
	tim->EGR |= TIM_EGR_UG; // generate update even to reset timer and apply settings
	tim->CR1 |= TIM_CR1_CEN; // enable timer counter
	tim->DIER |= TIM_DIER_UIE; // enable update interrupt

	int irqn = 0;
	if(tim == TIM2) { irqn = TIM2_IRQn;}
	else if(tim == TIM3) { irqn = TIM3_IRQn;}
	else if(tim == TIM4) { irqn = TIM4_IRQn;}
	else if(tim == TIM5) { irqn = TIM5_IRQn;}
	if(irqn) NVIC_EnableIRQ(irqn);
}



void pwm_set_duty(TIM_TypeDef *tim, int ch, float duty)
{
	// Capture/Compare Register
	gpio_set(GPIOC, 2);
	uint32_t cap_comp_val = (tim->ARR+1)*duty;
	if(cap_comp_val<0) cap_comp_val = 0;
	if(ch==1) 	tim->CCR1 = cap_comp_val;
	else if(ch==2) 	tim->CCR2 = cap_comp_val;
	else if(ch==3) 	tim->CCR3 = cap_comp_val;
	else if(ch==4) 	tim->CCR4 = cap_comp_val;
	gpio_clr(GPIOC, 2);
	//uint32_t ccr_addr = (uint32_t) ptr_tim + (ch-1)*4 + 0x34; // one of TIMx -> CCR1, CCR2, CCR2, CCR4
	//put32(ccr_addr, (float)arr * duty -1); // set duty cycle on Capture/Compare register
}

void pwm_init(GPIO_TypeDef *port,int pin, TIM_TypeDef *tim, int ch, int af, uint32_t scale, int freq, float duty)
{
	//RCC->APB1ENR |= 1<< (tim-2); // only handles tims 2..5:
	rcc_enable_tim(tim);
	gpio_alt(port, pin, af);


	tim->CR1 |= TIM_CR1_ARPE; // buffer the ARR register
        tim->CR1 |= TIM_CR1_CEN; // enable the counter
	tim->PSC = SystemCoreClock/scale-1; // scale to whatever
	uint32_t arr = scale/freq;
        tim->ARR = arr-1; // set freq 

	pwm_set_duty(tim, ch, duty);
	uint32_t ccmr_addr = (uint32_t)tim + 0x18; // equiv of TIMx->CCMR1
	uint32_t ccmr_pos = (ch-1)*8; // what a mess
	if(ch>2) {
		ccmr_addr += 4; // equiv of TIMx->CCMR2
		ccmr_pos -= 16;
	}
	uint32_t ccmr_value = get32(ccmr_addr);
	ccmr_value |= (1 << (ccmr_pos+3)); // enable preload for channel
	ccmr_value |= (0b110 << (ccmr_pos+4)); // OCxM set to PWM mode 1
	put32(ccmr_addr, ccmr_value);

	tim->CCER |= 1 << (ch*4-4); // CCnE - capture/compare n output enable




}

int main(void)
{
	SCB->CPACR |= (0xF<<20); // turn on FPU
	tim_init(TIM2);
	gpio_out(GPIOC, 2);
	gpio_out(GPIOC, 3);
	pwm_init(GPIOB, 10, TIM2, 3, 1, 1000000, samp_freq, 0.25); // surprisingly, this works

	puts("hello from 14-sine-pwm");
	printf("sin(pi/2) = %d\n", (int) (arm_sin_f32(0.5)*1000));
	const float freq = 440.0f; // Hz
	const float omega = 2.0f * 3.1412f * freq; // angular frequency
	const float T = 1.0/freq; // period
	while(1) {
		if(counter_empty) {
			gpio_set(GPIOC, 3);
			counter_value = (arm_sin_f32(omega * t) + 1.0f)/2.0f ;
			t += dt;
			if(t>T) t-= T; // to prevent calc drift
			gpio_clr(GPIOC, 3);
			counter_empty = false;
		}
	}

}

