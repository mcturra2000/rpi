#pragma once
// calendrical functions

#ifdef __cplusplus
extern "C" {
#endif

int isleap(int y);
int get_dims(int y, int mon0);
int get_cum_dims(int y, int mon);
int mildays(int y, int mon, int mday);
int isbst(int year, int mon0, int day1, int hr);
void adjbst(int *yr, int *mon0, int *day1, int *hr);

#ifdef __cplusplus
}
#endif

