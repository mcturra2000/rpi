#include <shims.h>

#include <stddef.h>
#include <stdint.h>

#ifdef HAL_I2C_MODULE_ENABLED // this has to come after the include


void i2c_send_bytes(I2C_HandleTypeDef* i2c, uint8_t sid, const uint8_t *data, size_t len)
{
	HAL_StatusTypeDef status = HAL_I2C_Master_Transmit(i2c, sid << 1, (uint8_t *) data, len, 1000);
	(void)status; // stop complaining of unused variable
	//assert_param(0);
	assert_param(status == HAL_OK);
	//if(status != HAL_OK) Error_Handler();
/*
	#if 1
	(void) status;
#else
	if()
	*/
}
void i2c_recv_bytes(I2C_HandleTypeDef* i2c, uint8_t sid, uint8_t *data, size_t len)
{
	HAL_StatusTypeDef status = HAL_I2C_Master_Receive(i2c, sid <<1 , data, len, 1000);
	(void)status; // stop complaining of unused variable
	//assert_param(0);
	assert_param(status == HAL_OK);
}

void i2c_ship(I2C_HandleTypeDef* i2c, uint8_t sid, const uint8_t *data, size_t len)
{
	i2c_send_bytes(i2c, sid, data, len);
}

#else
//void i2c_ship(uint32_t i2c, uint8_t sid, const uint8_t *data, size_t len) {} // dummy compilation

#endif //HAL_I2C_MODULE_ENABLED
