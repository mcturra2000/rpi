#include <basal.h>

#include <delay.h>
#include <gpio.h>
#include <i2c.h>
#include <rotary.h>
#include <ssd1306.h>
#include <systick.h>



//#define DOWN	GPIOA, 0
#define LED	GPIOB, 9



rotary_t dial;
volatile int val = 0;


void  SysTick_Handler(void)
{
	val += rotary_poll(&dial);
}


int main (void) 
{   
	gpio_out(LED);
	rotary_init(&dial, GPIOC, 6, 5); // decr, incr

	systick_init_1ms();

	i2c1_init();
	init_display(64, 0);
	int count = 0;

	while (1) 
	{
		ssd1306_printf("C:%d, Val = %d\n", count++, val);
		show_scr();
		delayish(1000);
	}
}
