# thermometer/clock

PERIS: bme688 tm1637 rp2040

Two versions are available:
* [dev07](dev07) - **PREFERRED** - Arduino code which supports getting/setting of code as supplied [here](../pico/22-ds3231-ser/prog)
* [upython](upython) implementation


Schematics: 19d10

## Status

2023-07-04  Started. Works
