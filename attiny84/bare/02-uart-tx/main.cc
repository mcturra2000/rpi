//#define F_CPU 8000000UL

//#include <util/delay.h>

//#include <avr/iotn4.h> // for CLKPR
#include <avr/io.h>
#include <avr/interrupt.h>

#include <gpio.h> // my HAL
#include <delay.h>



//#define LED1 GPIOA, (1<<0) // PA0 . Physical pin 13
//#define LED2 GPIOB, (1<<2) // PB2 . Physical pin 5

#define GPA(x) GPIOA, (1<<x)
#define GPB(x) GPIOB, (1<<x)

#define TX 	GPA(7) 
//#define P_INT0 	GPB(2) // PB2, which has a special INT0 interrupt
//#define P_A1	GPA(1) // toggle test for 9600baud (=104us delay). Wire into P_INT0 for a test
//#define P_A2	GPA(2) // copy of PA1
//#define P_A3	GPA(3) // spike when P_ONT0 falls

/*
static inline void gpio_put(port_t* port, pin_t pin, int value)
{
	if(value)
		gpio_set(port, pin);
	else
		gpio_clr(port, pin);
}
*/

volatile uint8_t uart_char;
volatile static int bits_transmitted = 0;
volatile uint8_t ticks = 0;

ISR(TIM0_COMPA_vect)
{
	ticks++;
	//volatile static uint8_t val; // this will be shifted
	switch(bits_transmitted++) {
		case 0 : // start bit
			gpio_clr(TX); 
			//val = uart_char; 
			break; 
		case 1 : 
		case 2 : 
		case 3 : 
		case 4 : 
		case 5 : 
		case 6 : 
		case 7 : 
		case 8 : 
			gpio_put(TX, uart_char & 1); // transmit LSB
			uart_char >>=1; // shift next bit to LSB
			break;
		case 9: // stop bit
			gpio_set(TX); // first stop bit
			bits_transmitted--; // finished, prevent from recycling
			//TIMSK0 &= ~(1<< OCIE0A); // clear compare interrupt for timer0,  A
	}

	//gpio_toggle(P_A1);
	//gpio_toggle(P_A2);
}

void uart_init()
{
	// set the prescaler. The counter is only 8 bit long,
	// so we have to make it high enough
	TCCR0B = 0b10; // set the prescaler to 8. counter will advance at 1 MHz
	OCR0A = 97; //  1MHz / 9600 baud = 104.167
	// ... you must implement ISR(TIM0_COMPA_vect)
}

void uart_send(char c)
{
	uart_char = c;
	bits_transmitted = 0;
	// enable timer
	TCNT0 = 0; // set the register count to 0 (it's 8 bits long)
	TCCR0A = 0b010; // enable CTC (Counter Timer Clear) when matched with A
	TIMSK0 |= (1<< OCIE0A); // enable compare interrupt for timer0,  A
}

void message(void)
{
	char msg[] = "Hello from the ATtiny85... ";
	char *ptr = msg;
	for(;;) {
		//gpio_set(P_A2);
		//gpio_clr(P_A2);
		//uart_send('X');
		uart_send(*ptr++);
		if(*ptr == 0) ptr = msg;
		delayish(200);
	}
}

void numbers(void)
{
	gpio_out(GPIOA, 0b1111); //PA0..3
	uint8_t count = 0;
	for(;;) {
		uart_send(count);
		//gpio_put(GPIOA, 0b1111, count>>4); // light up some LEDS
		GPIOA->PORT = (count>>4);
		count++;
		delayish(400);
	}
}

int main()
{
	CLKPR = (1<<7); // prepare for change
	CLKPR = 0;

	gpio_out(TX);
	gpio_set(TX);
	//gpio_out(P_A1); 
	//gpio_out(P_A2);
	//gpio_out(P_A3);

	asm volatile ("nop");


	uart_init();
	sei(); // enable interrupts

	uart_send(0xFF); // clear the decks

	if(1) 
		message();
	else
		numbers();
}
