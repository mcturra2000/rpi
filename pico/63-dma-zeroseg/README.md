# zeroseg display using dma

Synchronous display to zseg.

## Timing

SPI takes 16us @ 10MHz to send 8x16 bits

zseg\_tfrn() takes about 200ns (yes, nanoseconds) to set up the DMA tfr. It is fast!


## Status

2022-01-31	Works

2022-01-30	Started
