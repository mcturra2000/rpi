#include <assert.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

#include <string>
#include <ostream>
#include <iostream>

#include <glob.h>


using namespace std;

void do_help ()
{
	puts("app - set or read ds3231");
	puts("-h   this help");
	puts("-r   read time on ds3231 in UTC");
	puts("-s   send UTC time to ds3231");	
	puts("-t   set to a test time of Jan 21 2021"); // when the RP2040 was introduced
}

void do_read (int fd)
{
	char msg[] = "\nR\n";
	write(fd, msg, strlen(msg));

	while(1) {
		char c;
		int rd = read(fd, &c, 1);
		if(c == '\n' || c == 003) break; // 003 = ETX, end of text
		putchar(c);
	}

	puts(" (utc from device)");
}

void send_text (int fd, const char* out)
{
	printf("%s", out);
	int len = strlen(out);
	int n = write(fd, out, len);
	if(n != len) puts("ERR: incorrect number of bytes written");
	//printf("gmtime is : %s", asctime(tm1));
	do_read(fd);
}

void do_send (int fd)
{
	time_t now = time(0);
	struct tm *tm1 = gmtime(&now);
	char out[100];
	// wday is days since Sunday. [0,6]
	sprintf(out, "\nS%04d%02d%02d%02d%02d%02d%02d\n", 
			tm1->tm_year+1900, tm1->tm_mon+1, tm1->tm_mday, 
			tm1->tm_hour, tm1->tm_min, tm1->tm_sec, tm1->tm_wday);
	send_text(fd, out);
}

void do_test(int fd)
{
	send_text(fd, "\nS2021012100000000\n"); // I don't actually know weekday
}

// signal handler doesn't seem effective
void intHandler(int dummy)
{
	puts("Ctl-C handling");
	exit(1);
}

int main(int argc, char **argv)
{
	signal(SIGINT, intHandler);

	// determine the first suitable device
	string dev;
	glob_t globbuf;
	globbuf.gl_offs = 1;
	glob("/dev/ttyA*", GLOB_NOSORT, NULL, &globbuf);
	glob("/dev/ttyU*", GLOB_APPEND, NULL, &globbuf);
	if(globbuf.gl_pathv) dev = globbuf.gl_pathv[0];
	globfree(&globbuf);
	if(dev.size() == 0) {
		puts("No device found. Exiting");
		return 1;
	}
	cout << "using device: " <<dev << endl;

	//int fd1=open("/dev/ttyACM0", O_RDWR | O_NOCTTY);
	//int fd1=open("/dev/ttyUSB0", O_RDWR | O_NOCTTY);
	int fd1=open(dev.c_str(), O_RDWR | O_NOCTTY);
	assert(fd1>=0);

	struct termios tty;
	if(tcgetattr(fd1, &tty) != 0) {
		printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));
	}

	speed_t speed = B115200;
	cfsetspeed(&tty, speed);
	cfmakeraw(&tty); // make it a raw tty, which fixes \n issues that I was having before

	// Save tty settings, also checking for error
	if (tcsetattr(fd1, TCSANOW, &tty) != 0) {
		printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
		return 1;
	}


	// print time now for reference purposes
	time_t now = time(0);
	struct tm *tm1 = gmtime(&now);
	char out[100];
	sprintf(out, "%04d.%02d.%02d %02d:%02d:%02d", 
			tm1->tm_year + 1900, tm1->tm_mon+1, tm1->tm_mday, 
			tm1->tm_hour, tm1->tm_min, tm1->tm_sec); //, tm1->tm_wday+1);
	printf("%s (actual utc now)\n", out);

	int c;
	while((c = getopt (argc, argv, "hrst")) != -1) 
		switch (c)
		{
			case 'h' : do_help(); break;
			case 'r' : do_read(fd1); break;
			case 's' : do_send(fd1); break;
			case 't' : do_test(fd1); break;
			default: puts("Unrecognised option.");
		}



	close(fd1);
	puts("\n");
	return 0;

}
