#define F_CPU 1000000UL

//#include <Arduino.h>
//#include <Narcoleptic.h>

#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/wdt.h>

void setup() {

  CLKPR = (1<<CLKPCE); // prepare the clock for speed reset
  CLKPR = 0b11; // 1MHz

  ADCSRA &= ~(1<<ADEN); //Disable ADC
  ACSR = (1<<ACD); //Disable the analog comparator

  // ATtiny25/45/85
  //Disable digital input buffers on all ADC0,ADC2,ADC3 + AIN1/0 pins
  DIDR0 = (1<<ADC0D)|(1<<ADC2D)|(1<<ADC3D)|(1<<AIN1D)|(1<<AIN0D);

  USICR = 0; // zap the USI

   // PRR - Power Reduction Register - This will disable certain clocks
   PRR |= (1<<PRTIM1) // shutdown Timer/Counter1
    | (1<<PRUSI) // shutdown USI
    | (1<<PRADC) // shutdown ADC
    ;

  // turn off brown-out enable in software
  MCUCR = bit (BODS) | bit (BODSE);
  MCUCR = bit (BODS); 
  interrupts ();             // guarantees next instruction executed
  sleep_cpu ();              // sleep within 3 clock cycles of above    
  
  for(int i = 0; i<5; i++) {
    pinMode(i, OUTPUT);
  }

}

// take about 2ms
void  blit(unsigned char pin)
{
    digitalWrite(pin, HIGH);
    delay(1);
    digitalWrite(pin, LOW);
    delay(1);
}


// keep all pins on continuously
void pattern1() {
  for(int i = 0; i<5; i++) {
    blit(i);
  }
  delay(2);
}

// keep a random pin on for 1 sec
void pattern2() {
  static int prev_pin= -1;
  int pin;
  while(1) {
    pin = random(5); // [0, 4]
    if(pin != prev_pin) break;
  }
  prev_pin = pin;
  for(int i = 0; i < 500; i++) {
    blit(pin); // takes about 2ms
  }
  delay(2);
  
}
void loop() {
  pattern2();
}
