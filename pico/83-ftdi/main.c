#include "ftdi-config.h"


#define TX1	4
#define RX1	5


void core1_entry() {
	uint8_t c;
	for(;;) {
		//uart_read_blocking(uart0, &c, 1);
		c = getchar();
		uart_write_blocking(uart1, &c, 1);

	}
}

int main() 
{
	stdio_init_all();
#if LIB_PICO_STDIO_USB
        while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 
#endif

	//uart_set_baudrate(uart0, BAUD);
	uart_init(uart1, BAUD);
	//gpio_pull_up(TX1);
	gpio_set_function(TX1, GPIO_FUNC_UART);
	gpio_set_function(RX1, GPIO_FUNC_UART);


	multicore_launch_core1(core1_entry);
	uint8_t c;
	for(;;) {
		uart_read_blocking(uart1, &c, 1);
		//uart_write_blocking(uart0, &c, 1);
		//putchar('.');
		putchar(c);
	}
}

