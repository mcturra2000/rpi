#include "i2c.h"

void i2c_send_bytes(i2c_t *i2c, uint8_t sid, const uint8_t *data, size_t len)
{
	i2c_write_blocking(i2c, sid, data, len, false);
}

void i2c_recv_bytes(i2c_t *i2c, uint8_t sid, uint8_t *data, size_t len)
{
	i2c_read_blocking(i2c, sid, data, len, false);
}

void i2c_send_bytes_at(i2c_t *i2c, int sid, uint8_t reg, const uint8_t* values, int size)
{
	i2c_write_blocking((i2c_inst_t *) i2c, sid, &reg, 1, true);
	i2c_write_blocking((i2c_inst_t *) i2c, sid, values, size, false);
}


void i2c_send_byte_at(i2c_t *i2c, int _address, int reg, uint8_t value)
{
	i2c_send_bytes_at(i2c, _address, reg, &value, 1);

}

void i2c_recv_bytes_at(i2c_t *i2c, int sid, uint8_t reg , uint8_t* values, int size)
{
	i2c_write_blocking(i2c, sid, &reg, 1, true);
	i2c_read_blocking(i2c, sid, values, size, false);
}

uint8_t i2c_recv_byte_at(i2c_t *i2c, int sid, uint8_t reg )
{
	uint8_t value;
	i2c_recv_bytes_at(i2c, sid, reg, &value, 1);
	return value;
}

void i2c_ship(i2c_t *i2c, uint8_t addr, const uint8_t *w, size_t wn)
{
	i2c_write_blocking(i2c, addr, w, wn, false);
}
