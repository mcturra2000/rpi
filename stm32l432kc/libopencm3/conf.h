#pragma once

#define STM32L4

//#ifndef STM32L432xx
//#define STM32L432xx
//#endif

//#include "stm32l4xx.h"

#include <inttypes.h>

//static inline uint32_t  SystemCoreClock = 4000000;
extern uint32_t  SystemCoreClock;


static inline void __attribute__((optimize("O0"))) nop(void)
{
	asm volatile("nop");
}

void __attribute__((optimize("O0"))) nops(uint32_t n);
