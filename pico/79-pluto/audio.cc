#include "pluto.h"

#include <hardware/pwm.h>

#include "pace.h"

#define SPK 19

unsigned int slice_num; // determined in play_music()
//const bool use_pwm = 1;

#define ALARM 0
#define DELAY (1000000/16000)


volatile uint8_t dbuf[512*2];
volatile int dbuf_offset = -1; // either -1: don't do anything; 0: offset 0; 512: offset 512

/* FN song_buffer_poll 
 *
 * play songe
 *
 * Returns:
 * true: more to play
 * false: song finished
 */
bool song_buffer_poll(file32_t *file)
{
	if(dbuf_offset == -1) return true;
	volatile uint8_t* dst = dbuf + dbuf_offset;
	int n = file32_read(file, (uint8_t*) dst);
	if(n<512) {
		for(int i=n; i< 512; i++) *(dst+i) = 0;
		//file32_seek0(file); // repeat the song
		return false;
	}
	dbuf_offset = -1;
	return true;
}

uint8_t __not_in_flash_func(get_vol)()
{
	volatile static int playing = 0, bidx = 0;
	uint8_t vol = *(dbuf + 512*playing + bidx++);
	if(bidx>=512) {
		bidx = 0;
		dbuf_offset = 512*playing;
		playing = 1-playing;
	}
	return vol;
}

void __not_in_flash_func(sound_set_level)()
{
	pi_alarm_rearm(ALARM, DELAY);
	static volatile uint8_t vol = 0; // helps to stagger? possibly
	pwm_set_gpio_level(SPK, vol);
	vol = get_vol();
}

void sound_init(void)
{
	pi_alarm_init(ALARM, sound_set_level, DELAY);
	pace_config_pwm(&slice_num, SPK, 16000, 255);
}


void  sound_deinit (void)
{
	irq_set_enabled(TIMER_IRQ_0, false);
	pwm_set_enabled(slice_num, false);
}




void play_song (const char *filename)
{
	//char filename[] = "song-16k.raw";
	print("PLAY FILE: %s\n", filename);
	file32_t file;
	file32_init(&file, filename);
	if(!file32_found(&file)) {
		print("ERR: file not found: %s\n", filename);
		return;
	}
	print("File found. Should be good to go.\n");
	sound_init();

	print("Entering while loop\n");
	while(song_buffer_poll(&file));
	sound_deinit();
	print("Finished\n");
}



