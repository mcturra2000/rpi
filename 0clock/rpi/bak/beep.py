from enum import Enum
import threading
import time

import digitalio
import board
import time

#tick = 0
def btn():
	pass





##################################################################

class BuzEv(Enum):
	START = 1
	TICK = 2
	STOP = 3

class BuzSt(Enum):
	IDLE =1
	TIMING =2

st = BuzSt.IDLE

buzzer = digitalio.DigitalInOut(board.D16)
buzzer.direction = digitalio.Direction.OUTPUT
#btn.pull = digitalio.Pull.UP


buz_count = 0 # updated every 1ms
def do_buz(ev):
	global buz_count, st
	if st == BuzSt.IDLE:
		if ev == BuzEv.START:
			buz_count = 0
			st = BuzSt.TIMING
	elif st == BuzSt.TIMING:
		if ev == BuzEv.TICK:
			secs5 = buz_count % 5000
			buz_count += 1
			on = (secs5 < 250) or (secs5 >500 and secs5 < 500 + 250)
			buzzer.value = on
		elif ev == BuzEv.STOP:
			st = BuzSt.IDLE
			buzzer.value = 0
	else:
		raise 100


##################################################################



more = True
t1 = None

def beep_thr():
	global more, tick
	while more:
		btn()
		buz(Buz.UPDATE);
		time.sleep(0.001)
		#tick += 1


def start():
	global t1, more
	more = True
	t1 = threading.Thread(target=beep_thr)
	t1.start()

def stop():
	global more, t1
	more = False
	buzzer.value = 0
	t1.join()	

#beep =  Beep()
def main():
	do_buz(BuzEv.START)
	try: 
		while True:
			do_buz(BuzEv.TICK)
			time.sleep(0.001)
	finally:
		do_buz(BuzEv.STOP)
	return

	try: 
		start()
		time.sleep(15)
		#beep_stop()
		#s = input("Type anything")
	finally:
		stop()
#beep.del()
#while True:
#	time.sleep(5)



if __name__ == "__main__":
	main()
