#pragma once

#ifndef STM32L432xx
#define STM32L432xx
#endif

#include "stm32l4xx.h"

extern uint32_t SystemCoreClock; // provided by jagan/init.c


static inline void __attribute__((optimize("O2"))) nop(void)
{
	asm volatile("nop");
}

static inline void __attribute__((optimize("O2"))) nops(uint32_t n)
{
	for(uint32_t i = 0; i < n; i++) nop();
}
