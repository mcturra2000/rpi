#include "pi.h"

#define PIN 17
#define LED  25 // GPIO of built-in LED

volatile uint32_t tick = 0;

//void SysTick_Handler(void)
void isr_systick(void)
{
	//pi_gpio_toggle(PIN);
	uint32_t rem = tick % 1000;
	if(rem == 0) {
	    pi_gpio_clr(LED);
		pi_gpio_clr(PIN);
	}
	if(rem == 900) {
		pi_gpio_set(LED);
		pi_gpio_set(PIN);
	}
	tick++;
}

int main() 
{

	pi_gpio_out(LED);
	pi_gpio_out(PIN);

	systick_init();

	for(;;);

	return 0;
}

