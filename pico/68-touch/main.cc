#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "hardware/adc.h"
#include "pi.h"

#define LED	16

			
void activate(void)
{
	pi_gpio_high(LED);
	sleep_ms(1000);
	pi_gpio_low(LED);
}

// seems to stay on too long
void method1()
{
#define ADC 	0
	adc_init();
	adc_gpio_init(26+ADC);
	adc_select_input(ADC);

	while(1) {
		uint16_t adc = adc_read();
		if(adc > 2500) 	activate();
		
	}
}


// works
void method2()
{
#define TOUCH 26
	pi_gpio_init(TOUCH, INPUT); // works: INPUT. INPUT_PULLDOWN. fails: INPUT_PULLUP
	while(1) {
		if(pi_gpio_is_high(TOUCH)) activate();
	}

}

int main() 
{
	stdio_init_all();
	pi_gpio_init(LED, OUTPUT);
	method2();

	return 0;
}

