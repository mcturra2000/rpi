#include <stdarg.h>
#include <stddef.h>
#include "uart.h"

int vsnprintf(char *str, size_t size, const char *format, va_list ap);

int printf2(const char *format, ...)
{
	static int init =0;
	if(init == 0) {
		init = 1;
		uart2_init();
	}
	
	char msg[132];
	va_list ap;
	va_start(ap, format);
	int n = vsnprintf(msg, sizeof(msg), format, ap);
	va_end(ap);

	uart2_send_blocking(msg, n);

	return n;
}

int putchar(int c)
{
	printf2("%c", c);
	return c;
}

int puts(const char *s)
{
	printf2("%s\n", s);
	return 1;
}

