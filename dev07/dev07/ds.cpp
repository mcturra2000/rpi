#include "main.h"

DS3231 myRTC;

bool century = false;
bool h12 = false;
bool pm;

void ds_get_str(char *msg, int len)
{
  snprintf(msg, len, "%d.%02d.%02d %02d:%02d:%02d\003\r\n",2000+myRTC.getYear(), myRTC.getMonth(century), myRTC.getDate(), 
    myRTC.getHour(h12, pm), myRTC.getMinute(), myRTC.getSecond());
}
int ds_hhmm()
{
    int year = 2000+myRTC.getYear();
    int month = myRTC.getMonth(century)-1;
    int day = myRTC.getDate();
    int hour = myRTC.getHour(h12, pm);
    int minute = myRTC.getMinute();
    int sec = myRTC.getSecond();
    adjbst(&year, &month, &day, &hour);
    return hour*100 + minute;
}   

 
int DS3231_reduce(long long *timeval) //, int scale)
{
  const int scale = 100;
  int val = *timeval  % scale;
  *timeval /= scale;
  return val;
}

void DS3231_set_int(long long val)
{
  long long timeval = val;
  int wday = DS3231_reduce(&timeval);
  if(wday==0) wday = 7;
  myRTC.setDoW(wday);
  volatile int sec = DS3231_reduce(&timeval);
  myRTC.setSecond(sec);
  volatile int minute = DS3231_reduce(&timeval);
  myRTC.setMinute(minute);
  volatile int hr = DS3231_reduce(&timeval);
  myRTC.setHour(hr);  
  volatile int mday = DS3231_reduce(&timeval);
  myRTC.setDate(mday);
  volatile int month = DS3231_reduce(&timeval);
  myRTC.setMonth(month);
  volatile long long year = timeval-2000;
  myRTC.setYear(year);
}

  
