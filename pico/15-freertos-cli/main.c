#include <FreeRTOS.h>
#include <FreeRTOS_CLI.h>
#include <task.h>

#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"


#include "pi.h"

void gpio17_task()
{
	const uint PIN = 17;
	pi_gpio_out(PIN);

	while(1) {
		pi_gpio_set(PIN);
		vTaskDelay(250);
		pi_gpio_clr(PIN);
		vTaskDelay(250);
	}
}

void led_task()
{
	const uint PIN = PICO_DEFAULT_LED_PIN; // 25
	pi_gpio_out(PIN);

	while(1) {
		pi_gpio_set(PIN);
		vTaskDelay(100);
		pi_gpio_clr(PIN);
		vTaskDelay(900);
	}
}


extern void vRegisterSampleCLICommands(); // defined in Sample-CLI-commands.c

#define MAX_INPUT_LENGTH 132
#define MAX_OUTPUT_LENGTH 132
void cmd_task(void *pvParameters)
{
	//fwrite("hello", 3, 1, 0);
	char *str = "hello\n";
	printf(str);
	int8_t c, cInputIndex = 0;
	BaseType_t more;
	static int8_t pcOutputString[ MAX_OUTPUT_LENGTH ], pcInputString[ MAX_INPUT_LENGTH ];
	//Peripheral_Descriptor_t xConsole;
	//FreeRTOS_write(xConsole, str, sizeof(str));
	for(;;) {
		c = getchar();
		putchar(c);
		if(c == '\r') {
			putchar('\n');
			c = '\n';
		}

		if(c == '\n') {
			puts("\r\n");
			do {
				more =  FreeRTOS_CLIProcessCommand(pcInputString, pcOutputString, MAX_OUTPUT_LENGTH);
				puts(pcOutputString);
			} while( more != pdFALSE);
			cInputIndex = 0;
	    		memset( pcInputString, 0x00, MAX_INPUT_LENGTH );
		} else {
			if(c == '\r') {
				// ignore
			} else if(c == '\b') {
				if(cInputIndex > 0)
				{
					cInputIndex--;
					pcInputString[cInputIndex] = 0;
				}
			} else {
				if(cInputIndex < MAX_INPUT_LENGTH)
				{
					pcInputString[cInputIndex] = c;
					cInputIndex++;
				}
			}
		}

		//printf("%d \n", c);

	}
}

int main() 
{
	stdio_init_all();
	vRegisterSampleCLICommands();
	xTaskCreate(led_task, "LED", 256, NULL, 1, NULL);
	xTaskCreate(gpio17_task, "GP17", 256, NULL, 1, NULL);
	xTaskCreate(cmd_task, "CMD", 256, NULL, 0, NULL);
	vTaskStartScheduler();
}

