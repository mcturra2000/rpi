/* Generate a tone on PA0 using TIM5_CH1, AF2
*/

#include "basal.h"
//#include <string.h>

#include "gpio.h"

void init_pwm_pa0_tim5_ch1(void)
{
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN; 
	gpio_alt(GPIOA, 0, 2); // PB6 PWM alt fn
	RCC->APB1ENR |= RCC_APB1ENR_TIM5EN; // enable time clock
	//memset(TIM5, 0, sizeof(TIM5)); // seems to be important
	TIM5->CR1 |= TIM_CR1_ARPE; // buffer the ARR register
	TIM5->PSC = SystemCoreClock/1000000-1; // scale to 1us
	TIM5->CCMR1 |= (0b110 << TIM_CCMR1_OC1M_Pos); // PWM mode 1
	TIM5->CCMR1 |= TIM_CCMR1_OC1PE; // enable preload for channel 1
	TIM5->CCER |= TIM_CCER_CC1E; // Enable Capture/Compare for channel 1
	TIM5->CR1 |= TIM_CR1_CEN; // enable the counter
	TIM5->CNT = 0;
}


/* duty s/b [0..99] */
void set_pwm_pa0_tim5_ch1(uint32_t freq, uint32_t duty)
{
	uint32_t arr = 1000000/freq-1;
	TIM5->ARR = arr; 
	uint32_t ccr1 =  (arr+1) * duty /100 -1;
	TIM5->CCR1 = ccr1; // Duty cycle on Cap/Compare Reg
	TIM5->EGR |= TIM_EGR_UG; // generate update (required!)
}

int main (void) 
{   
	init_pwm_pa0_tim5_ch1();

	volatile TIM_TypeDef * tim5 = TIM5; // for debugging purposes

	while (1) {
		set_pwm_pa0_tim5_ch1(440, 10);
		delayish(500);
		set_pwm_pa0_tim5_ch1(600, 10);
		delayish(500);
	}
}
