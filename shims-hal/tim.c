#include <tim.h>

#ifdef HAL_TIM_MODULE_ENABLED

void tim_set_frequency(TIM_HandleTypeDef *htim, int granularity, int freq)
{
	__HAL_TIM_SET_PRESCALER(htim, SystemCoreClock/granularity-1);
	int arr = granularity/freq-1;
	__HAL_TIM_SET_AUTORELOAD(htim, arr);
	htim->Instance->EGR  |= TIM_EGR_UG; // update the registers (required!)
}


#endif // HAL_TIM_MODULE_ENABLED
