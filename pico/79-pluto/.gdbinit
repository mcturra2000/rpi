cd build

# quit without confirmation
define hook-quit
    set confirm off 
end

set pagination off
file app.elf
target remote localhost:3333
load
#b cmd_del
#b main
#b main.c:30
b uart_rx_handler
#b kbd_init
#b parse_line
monitor reset init
echo RUNNING...\n
c
