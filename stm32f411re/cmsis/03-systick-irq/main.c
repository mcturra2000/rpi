#include "basal.h"
#include "gpio.h"
#include "systick.h"

#define LED	GPIOA, 5

volatile uint32_t  ticks = 0; // must be volatile to prevent compiler optimisations

void  SysTick_Handler(void)
{
	ticks++;
	if(ticks%512 == 0)
		gpio_toggle(LED);
}


void delay_ms(int ms)
{
	uint32_t started = ticks;
	while((ticks-started)<=ms); // rollover-safe (within limits)
}


int main (void) 
{   
	gpio_out(LED);
	systick_init_1ms();
	//for(;;);
}
