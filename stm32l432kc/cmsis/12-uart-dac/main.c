#include "conf.h"

#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include "delay.h"
#include "uart.h"

/*
// TODO place somewhere else
void *memset(void *s, int c, size_t n)
{
	unsigned char* s1 = s;
	while(n--) {
		*s1++ = c;
	}
	return s;
}
*/

/* baudrate
 * 115200 *2 = 230400
 */

#define SFREQ 8000 // sampling frequency

#define CB_CAP 512 // capacity
uint32_t cb_data[CB_CAP];
volatile int cb_read_index;
volatile int cb_write_index;
volatile int cb_size;

bool cb_write(uint32_t value)
{
	if(cb_size == CB_CAP) return false;
	cb_data[cb_write_index] = value;
	if(++cb_write_index == CB_CAP) cb_write_index = 0;
	cb_size++;
	return true;
}

bool cb_read(uint32_t* value)
{
	*value = 0;
	if(cb_size == 0) return false;
	*value = cb_data[cb_read_index];
	if(++cb_read_index == CB_CAP) cb_read_index = 0;
	cb_size--;
	return true;
}

void cb_init(void)
{
	cb_read_index = 0;
	cb_write_index = 0;
	cb_size = 0;
	memset(cb_data, 0, sizeof(cb_data));
}


void TIM2_IRQHandler(void)
{	
	if(TIM2->SR & TIM_SR_UIF)
		TIM2->SR = ~TIM_SR_UIF; // clear flag

#if 0
	uint32_t val;
	if(cb_read(&val)) {
		val <<= 4;
		DAC1->DHR12R1 = val;
	}
#else
	static int val1 = 0;
	val1 = (1 << 13) -1 -val1; // toggle between extremes
	DAC1->DHR12R1 = val1;
#endif
}

void tim2_init(int freq)
{
	// TIM2->ARPRE somewhere ??
	RCC->APB1ENR1 |= RCC_APB1ENR1_TIM2EN; // enable TIM2 clock
	TIM2->PSC = SystemCoreClock/1000000-1; // pre-scale to 1us
	TIM2->ARR = 1000000/freq-1; // Auto Reload Value to a given frequency
	TIM2->EGR |= TIM_EGR_UG; // generate update even to reset timer and apply settings
	TIM2->CR1 |= TIM_CR1_CEN; // enable timer counter
	TIM2->DIER |= TIM_DIER_UIE; // enable update interrupt
	NVIC_EnableIRQ(TIM2_IRQn);
}

int main()
{
	uart2_init();
	uart2_set_baudrate(115200*1);
	//tim2_init(SFREQ);
	//cb_init();

	// init dac
	RCC->APB1ENR1 |= RCC_APB1ENR1_DAC1EN;
	DAC1->CR |= DAC_CR_EN1;
	DAC1->DHR12R1 = 0;

	while(1) {
		uint8_t val = uart2_recv_blocking();
		char c = val;
		//DAC1->DHR12R1 =val<<4;
		//continue;
		//NVIC_DisableIRQ(TIM2_IRQn);
		//char c = cb_write(val) ? '1' : '0';
		//NVIC_EnableIRQ(TIM2_IRQn);
		uart2_send_blocking(&c, 1);
	}
}
