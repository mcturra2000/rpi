#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <Arduino.h>
#include <functional>

#include "picol.h"
#include "ds3231.h"
#include "cal.h"


struct picolInterp interp;

char clibuf[132];
int clibuf_len = 0;

BaseType_t xReturned;
TaskHandle_t xHandle = NULL;

#define STACK_SIZE 10000

int set_val(std::function<void(struct ts *t, int newval)> fn, int argc, char**argv)
{
	if (argc != 2) return picolArityErr(&interp,argv[0]);
	int n = atoi(argv[1]);
	struct ts t;
	DS3231_get(&t);
	fn(&t, n); // function that sets the new value
	t.sec = n;
	DS3231_set(t);
	return PICOL_OK;
}

int picolCommandYear(struct picolInterp *i, int argc, char **argv, void *pd) 
{
	auto f = [](struct ts *t, int newval) { t->year = newval; };
	return set_val(f, argc, argv);
}

int picolCommandMonth(struct picolInterp *i, int argc, char **argv, void *pd) 
{
	auto f = [](struct ts *t, int newval) { t->mon = newval; };
	return set_val(f, argc, argv);
}

int picolCommandDay(struct picolInterp *i, int argc, char **argv, void *pd) 
{
	auto f = [](struct ts *t, int newval) { t->mday = newval; };
	return set_val(f, argc, argv);
}


int picolCommandHelp(struct picolInterp *i, int argc, char **argv, void *pd) 
{
	if (argc != 1) return picolArityErr(i,argv[0]);
	const char *cmds[] = {
		"date  - print date adjusted for timezone",
		"day N - change day to N",
		"help  - print this help",
		"hr N  - change local hour to N",
		"min N - change minutes to N",
		"mon N - change month to N [1..12]",
		"sec N - change seconds to N",
		"yr N  - change year to N",
		0
	};
	char **cmd = (char**) cmds;
	while(*cmd) {
		Serial.println(*cmd++);
		//*cmd++;
	}
	return PICOL_OK;
}

int picolCommandDate(struct picolInterp *i, int argc, char **argv, void *pd) 
{
	if (argc != 1) return picolArityErr(i,argv[0]);
	struct ts t;
	DS3231_get_uk(&t);
	Serial.printf("%d.%02d.%02d %02d:%02d:%02d (LOCAL)\n", t.year, t.mon, t.mday, t.hour, t.min, t.sec);
	return PICOL_OK;
}




int picolCommandSec(struct picolInterp *i, int argc, char **argv, void *pd) 
{
	auto f = [](struct ts *t, int newval) { t->sec = newval; };
	return set_val(f, argc, argv);
}

int picolCommandHour(struct picolInterp *i, int argc, char **argv, void *pd) 
{
	auto f = [](struct ts *t, int newval) { 
		if(isbst(t->year, t->mon-1, t->mday, t->hour))
			newval--;
		t->hour = newval; 
		};
	return set_val(f, argc, argv);
}

int picolCommandMinute(struct picolInterp *i, int argc, char **argv, void *pd) 
{
	auto f = [](struct ts *t, int newval) { t->min = newval; };
	return set_val(f, argc, argv);
}

bool picol_read(void)
{
	int c;	
	bool eol = false;
	int n = Serial.available(); // maximum is 64
	if (n == 0)
		return false;
	while (n--)
	{
		c = Serial.read();
		Serial.write(c);
		if (c == '\r')
		{
			Serial.print('\n');
			Serial.println("eol found");
			clibuf[clibuf_len] = 0;
			return true;
		}
		else
			clibuf[clibuf_len++] = c;
	}
	// clibuf[clibuf_len] = 0;
yield:
	taskYIELD();
	return false;
}
void picol_repl(void *pvParameters)
{

	while (1)
	{
		int retcode;
		int n = 0;
		clibuf_len = 0;
		Serial.print("picol> ");
		// fflush(stdout);
		bool eoi = false;
		while (!picol_read())
			;
		if (clibuf_len > 0)
		{
			Serial.println("non-empty buffer found");
			Serial.println(clibuf);
			retcode = picolEval(&interp, clibuf);
			if (interp.result[0] != '\0')
			printf("[%d] %s\n", retcode, interp.result);
		}
	yield:
		taskYIELD();
	}
}



void reg(const char *name, picolCmdFunc f)
{
	picolRegisterCommand(&interp, (char*) name, f, NULL);

}
void init_picol(void)
{
	picolInitInterp(&interp);
	picolRegisterCoreCommands(&interp);
	
	reg("help", picolCommandHelp);
	reg("date", picolCommandDate);
	reg("yr", picolCommandYear);
	reg("mon", picolCommandMonth);
	reg("day", picolCommandDay);
	reg("sec", picolCommandSec);
	reg("min", picolCommandMinute);
	reg("hr", picolCommandHour);

	/* Create the task, storing the handle. */
	xReturned = xTaskCreate(
		picol_repl,		  /* Function that implements the task. */
		"PICOL REPL",	  /* Text name for the task. */
		STACK_SIZE,		  /* Stack size in words, not bytes. */
		(void *)1,		  /* Parameter passed into the task. */
		tskIDLE_PRIORITY, /* Priority at which the task is created. */
		&xHandle);		  /* Used to pass out the created task's handle. */
}