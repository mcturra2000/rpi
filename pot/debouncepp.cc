#include <debounce.hh>

debounce::debounce(GPIO_TypeDef *GPIOx, uint32_t GPIO_Pin)
{
	gpio_inpull(GPIOx, GPIO_Pin);
	//deb_t deb;
	deb_init(&deb, GPIOx, GPIO_Pin);
}
            
debounce::~debounce() {}
 
bool debounce::falling()
{
	return deb_falling(&deb);
}
