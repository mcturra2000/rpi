//#include <byteswap.h>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include "pico/rand.h"
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "hardware/structs/rosc.h"

#include "ili9341.h"
#include "mode0.h"
#include "pi.h"


typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;

void offset_init();

extern "C" const char crown[];
extern "C" const char flag[];
u16* fg = (u16*) crown;
u16* bg = (u16*) flag;

volatile u32 tick = 0;
extern "C" void __not_in_flash_func(isr_systick)()
{
	//return;
	mode0_refresh_screen();
	tick++;
	if(tick > 8000) {
		offset_init();
		tick = 0;
		std::swap(fg, bg);
	}
}



// colours of the form RGB 5-6-5
#define RED	__builtin_bswap16(0b1111100000000000)
#define GREEN 	__builtin_bswap16(0b0000011111100000)
#define BLUE	__builtin_bswap16(0b0000000000011111)



#define W 320
//#define forw() for(int w = 0; w<W; w++)
int voffs[W]; // contrived so that <= 0
int vmax = 0; // >= 0
void offset_init()
{
	int seed = 0;
	for(int i = 0; i < 32; i++ ) {
		seed <<= 1;
		seed |= (rosc_hw->randombit & 1);
	}

	srandom(seed);
	int voff = 0;  vmax = 0;
	for(int x = 0; x<320; x++) {
		voff += ((random() %3) -1)*8;
		//if(voff > 0) voff = 0;
		vmax = std::max(vmax, voff);
		//if(voff < vin) vmin = voff;
		voffs[x] = voff;
	}

	for(int x = 0; x<320; x++) {
		voffs[x] -= vmax;
		assert(voffs[x] <= 0);
	}


	//forw() voffs[w] -= vmax;

}


u16 get_pixel(u16* bmp, int x, int y)
{
	//u16* fg = (u16*) crown;
	u32 addr = (320*x + y) * 2;
	addr = (240*x + y)*1; // works, assuming no offset
	u16 lo = bmp[addr];
	u16 hi = bmp[addr +1];
	return bmp[addr];
	return lo | (hi <<8);

}

void ili_line_filler(uint16_t* buffer, int x)
{
	//int thresh = -vmin - voffs[x] -tick/40; // we time-adjust the offset
	int off = voffs[x] + tick/10;
	assert(1==1);
	for(int y = 0 ; y < 240; y++) {
		int y1 = off + y;
		u16 val;
		if(y1>240) {
			//val = RED;
			val = get_pixel(bg, x, y);
		} else 	if(off < 0) { 
			val =  get_pixel(fg, x, y);
		} else {
			val = get_pixel(fg, x, y1);
		}
		buffer[y] = val;

	}
}


int main() 
{
	stdio_init_all();
	// while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 

	// always keep the backlight on
	const int ili_led = 8;
	pi_gpio_out(ili_led);
	pi_gpio_high(ili_led);

	offset_init();
	ili9341_set_filler(ili_line_filler);	
	ili9341_init();
	systick_init();

	for(;;);
}

