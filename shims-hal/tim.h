#pragma once

#include <shims.h>

#ifdef HAL_TIM_MODULE_ENABLED

/* Set the frequency for a timer. E.g. to set htim5 to 400Hz,
	tim_set_frequency(&htim5, 1000000, 400);
The granulirity sets how often the CNT increases. So 1,000,000 would be every
microsecond. 

This does not start the timer, though. You still need to do e.g.
		HAL_TIM_Base_Start(&htim5);

NB Many counters are 16-bit (7b4), giving a min. freq of ~15Hz. If you want more,
then you need to reduce the granularity, to say 1,000. So a frequency of 1 Hz
might look like this:
	tim_set_frequency(&htim11, 1000, 1);
*/
void tim_set_frequency(TIM_HandleTypeDef *htim, int granularity, int freq);


#endif // HAL_TIM_MODULE_ENABLED
