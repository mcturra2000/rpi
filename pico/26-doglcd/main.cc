#include <stdio.h>
//#include <string.h>
#include "hardware/irq.h"
#include "hardware/uart.h"
#include "pico/sync.h"
//#include "pico/time.h"

#include <pi.h>
#include "doglcd.h"
//#include "circular.h"


/*
 * power connections:
 * VIN pin of LCD should go to Pico pin 40 (VBUS). being 5V
 * GND of LCD to any GND pin of Pico
 *
 * 2023-02-02	Confirmed
 */


int main() 
{

	sleep_ms(10); // a little delay seems to help for the Tiny
	doglcd_init();

	char msg[100];
	int i = 0;
	for(;;) {
		doglcd_gotoxy(0, 0);
		sprintf(msg, "%d", i++);
		doglcd_write_str(msg);
		sleep_ms(1000);
	}
}

