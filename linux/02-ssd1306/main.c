#include <stdio.h>
#include <unistd.h>


#include "i2c.h"
#include "ssd1306.h"

int main()
{	
	//i2c_init(SID);
	puts("init display ...");
	const int SID = 0x3C;
	init_display(64, SID);

	int counter = 0;
	while(1) {
		ssd1306_printf("Counter = %d\n", counter++);
		//puts("show scr...");
		show_scr();
		//for(int i = 0 ; i< 100; i++) ssd1306_display_cell();
		sleep(1);
	}

	//i2c_deinit();
	return 0;
}
