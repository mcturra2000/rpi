(use-modules (ice-9 format))

( define hdr "
#define STM32F411xE
#include \"stm32f4xx.h\"
#include \"delay.h\"
#include \"gpio.h\"


uint32_t SystemCoreClock = 16000000;
volatile uint32_t  ticks = 0; // must be volatile to prevent compiler optimisations


void  SysTick_Handler(void)
{
	ticks++;
}	



int main (void) 
{   
	 // might as well turn on all the port clocks
	 RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIOBEN | RCC_AHB1ENR_GPIOCEN;
	SysTick_Config(SystemCoreClock/1000); // set tick to every 1ms

")	 

(define output-pins '())

(define main hdr)
(define init "")
(define body "")

(define-syntax-rule (+= var e ...)
		    (set! var (string-append var e ...)))



(define (nop)
  ( += body "\tasm (\"nop\");\n"))


; function that calls some kind of function on an output pin
(define (gpio-out func-name port pin)
  (define gpio (string-append "GPIO" (symbol->string port)))
  (string-upcase! gpio)
  (set! pin (number->string pin))
  (set! output-pins (cons (list gpio pin) output-pins))
  (+= body func-name "(" gpio ", " pin ");\n"))

(define (toggle port pin)
  (gpio-out "gpio_toggle" port pin))

(define (on port pin)
  (gpio-out "gpio_set" port pin))

(define (off port pin)
  (gpio-out "gpio_clr" port pin))

(define (delayish ms)
  (+= body "\tdelayish(" (number->string ms) ");\n"))


(define-syntax-rule (forever e ...)
		    (begin
		      (+= body "while(1) {\n")
		      e ...
		      (+= body "};\n")))

(define sym-id 0)

(define (gensym)
  (set! sym-id (+ 1 sym-id))
  sym-id)

(define (body+ . args)
  (+= body (string-concatenate args)))

(define (gnat-delay start-time ms end set-ptr)
  (let* ((next-step (format #f "gnat~a" (gensym))))
	 (body+ "// gnat-delay begin\n")
	 (body+ "if(ticks - " start-time "< " (number->string ms) ") { goto " end ";}\n")
	 (body+ start-time "= ticks;\n")
	 ;(body+ (set-ptr next-step))
	 (set-ptr next-step)
	 (body+ next-step ":\n")
	 (body+ "// gnat-delay end\n")
	 #t))

(define-syntax-rule (gnat-loop w e ...)
		    (let* ((id (gensym))
			   (id-str (format #f "gnat_~a" id))
			   (ptr (format #f "gnat_ptr_~a" id))
			   (start-time (format #f "~a_start_time" id-str))
			   (end (format #f "~a_end" id-str))
			   (start-label (string-append id-str "_start"))
			   (set-ptr (lambda (x) (body+ ptr " = &&" x ";\n")))
			   (w (lambda (x) (gnat-delay start-time x end set-ptr))))
		      (body+ "static void *" ptr " = &&" start-label ";\n")
		      (body+ "static uint64_t " start-time " = 0;\n")
		      (body+ "goto *" ptr ";\n")
		      (body+ start-label ":\n")
		      e ...
		      (body+ ptr " = &&" start-label ";\n")
		      (body+ end ":\n")
		      (nop)
		      #t))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; actual user code

(nop) ; don't do anything

;(blinker 'a 5 100 900)

(forever 
  (gnat-loop  wait
	      (on 'a 5)
	      (wait 100)
	      (off 'a 5)
	      (wait 900))
  (gnat-loop wait
	     (on 'c 13)
	     (wait 200)
	     (off 'c 13)
	     (wait 200))
  #t)

;(forever 
;  (on 'a 5)
;  (delayish 100)
;  (off 'a 5)
;  (delayish 900))




;; end of user code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(let loop ((pins output-pins))
  (unless (null? pins) 
    (let* ((port-pin (car pins))
	   (gpio (car port-pin))
	   (pin (cadr port-pin)))
      (+= init gpio "->MODER |= GPIO_MODER_MODER" pin "_0;\n")
      (loop (cdr pins)))))


(+= main init "\n" body "\n}\n")

(let ((output-port (open-file "main.c" "w")))
  (display main output-port)
  (newline output-port)
  (close output-port))
