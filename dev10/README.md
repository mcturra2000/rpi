# dev10

ESP-WROOM getter/setter for ds3231 rtcs.

Use the [app](../pico/22-ds3231-set/prog) for Linux-side getting and setting


PERIS: ds3231

## See Also

Schematic: db07.186

## Status

2023-07-13	Started. Works
