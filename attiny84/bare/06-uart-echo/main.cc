//#define F_CPU 8000000UL

//#include <util/delay.h>

//#include <avr/iotn4.h> // for CLKPR
#include <avr/io.h>
#include <avr/interrupt.h>

#include <gpio.h> // my HAL
#include <delay.h>


#define GPA(x) GPIOA, (1<<x)
#define GPB(x) GPIOB, (1<<x)

#define RX	GPB(2)
#define TX 	GPA(7) 

ISR(TIM0_COMPA_vect)
{
}

// whenever P_INT0 falls
ISR(INT0_vect) 	
{
}

const uint8_t delay_count = 130; // works out at about 104us // ori 128. 130 works better

unsigned char uart_recv_blocking()
{
	unsigned char c = 0;
	while(gpio_get(RX));
	delayish_us(delay_count/2); // skew the start bit
	for(uint8_t i = 0; i < 8; i++) {
		delayish_us(delay_count);
		c >>= 1;
		if(gpio_get(RX)) c |= (1<<7);
	}
	delayish_us(delay_count); // skip the stop bit
	while(gpio_get(RX) == 0);
	return c;
}

void uart_send_blocking(unsigned char c)
{
	// send start bit
	gpio_clr(TX);
	delayish_us(delay_count);
	
	for(uint8_t i = 0; i<8; i++) {
		gpio_put(TX, c & 1);
		delayish_us(delay_count);
		c >>= 1;
	}

	// send stop bit
	gpio_set(TX);
	delayish_us(delay_count);
}

int main()
{
	// set clock 8MHz
	//cli();
	//CLKPR = (1<<7); // prepare for change
	//CLKPR = 0;

	gpio_out(TX);
	gpio_in(RX);

	char msg[] = "hello world ... ";
	char* ptr = msg;
	for(;;) {

		char c = uart_recv_blocking();
		uart_send_blocking(c);
		if(c=='\r') uart_send_blocking('\n');
		continue;
		if(*ptr == 0) ptr = msg;
		uart_send_blocking(*ptr++);
		//uart_send_blocking('X');
		//if(c=='\r') uart_send_blocking('\n');
		delayish(100);
	}
}
