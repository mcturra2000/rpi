/* test macro expansion:
as -alm=blob.lst blob.S
*/

.macro pre name
	.global \name\()_start
	.balign 4
\name\()_start:
.endm

.macro post name
\name\()_end:
	.global \name\()_size
	.balign 4
\name\()_size:
	.int \name\()_end - \name\()_start
.endm


.macro drum name file
pre \name
	.incbin "\file"
post \name
.endm


/////////////////////////////////////////////////////////////////////////////////

	.section .rodata

drum bass 	../bass.raw
drum crash 	../crash.raw
drum hatc 	../hatc.raw
drum snare 	../snare.raw
drum tom ../tom.raw
