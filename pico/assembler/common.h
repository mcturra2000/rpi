#pragma once

#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <setjmp.h>

typedef enum yytype_e { T_ID = 256 , T_STAR, 
	T_RN, // a register 
	T_DIR, // a directive (.e.g .align)
	T_LABEL, // something of the form =foo, as in LDR r6, =myval
	T_LR, T_SP ,  T_PC, 
	T_NUM, // a number, without a '#' prefix
	T_IMM // a number, prefixed by a '#'
} yytype_e;

extern enum yytype_e yytype;
extern int yylval;
extern int arg[4]; // arguments to the assembler opcode
extern char *yystart; // start of token
extern uint16_t opcode0; //regular opcode
extern int yylen; // length of token
extern int address;

void emit_opcode0(char *name);
void emit_opcode (char * name, uint16_t value);
int yylex();
void syn (char *msg);
bool yyfind(char* keyword);
void expect (yytype_e expected_yytype, char *msg);

bool match_code();

// in labels

/* FN jtype_e  - jump type */
typedef enum {
	J_8 = 1, // SignExtend(imm8:'0', 32); used by Bnn
	J_11, // SignExtend(imm11:'0', 32); used by B
	J_BL, // Froody type reserved for BL instruction
} jtype_e;

/* FN str_t */
typedef struct {
	char *data; // pointer to beginning of string
	int len; // length of string
} str_t;

/* FN label_t */
typedef struct label_t {
	struct label_t* prev;
	str_t yytext; // the label
	//char *offset; // offset from source base. Effectively yystart at definition time
	//int len;
	int address; // address where any hole needs to be filled
	jtype_e jtype;
} label_t;

extern label_t* hd_label;
extern label_t* hd_holes; // unresolved forward branching

void init_labels();
void deinit_labels();
bool matched_label_def();
//void print_label (label_t *lab, char *post);
void print_label (label_t *lab, char *post);
void print_labels();
label_t *find_label(str_t *str);
void add_hole (char *start, int len, int address, jtype_e jtype);
void fill_holes();
uint32_t rel_offset (int here, int there, jtype_e jtype);


// branching
bool match_branch();
