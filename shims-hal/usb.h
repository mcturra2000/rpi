#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>


extern char usb_linebuff[]; // capable of holding more than one line
	
bool usb_linebuff_is_ready();
void usb_linebuff_consume();

// only to be used by the USB driver:
void usb_writen(uint8_t *buf, uint32_t len);



#ifdef __cplusplus
}
#endif


