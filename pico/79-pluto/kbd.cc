#include "pluto.h"


#ifdef KBD_UART
#include <hardware/gpio.h>
#include <hardware/uart.h>

extern "C" void  process_char (char ch) ;

// doesn't seem to be called
extern "C" void uart_rx_handler() 
{
	char c = (uart_get_hw(uart0)->dr) & 0xFF;
	process_char(c);
}


void kbd_init()
{
	uart_init(uart0, 115200);
	gpio_set_function(1, GPIO_FUNC_UART);
	irq_set_exclusive_handler(UART0_IRQ , uart_rx_handler);
	uart_set_irq_enables(uart0, true, false); // handle something in RX buffer
	irq_set_enabled(UART0_IRQ, true); // necessary
}


void kbd_task()
{
	if(!uart_is_readable(uart0)) return;
	uint8_t c;
	uart_read_blocking(uart0, &c, 1);
	process_char(c);
}

#else // use USB

#include "bsp/board.h"
#include "tusb.h"
extern void hid_app_task(void);

void kbd_init()
{
	board_init();
	tusb_init();
}

void kbd_task()
{
	// 2023-01-22 takes about 1.5us
	tuh_task(); // tinyusb host task
	// 2023-01-22 takes about 48ns. Really fast
	hid_app_task();
}
#endif
