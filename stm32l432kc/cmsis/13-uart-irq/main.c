#include <string.h>
#include <stdbool.h>

#include "conf.h"

#include "delay.h"
#include "uart.h"

#define CB_CAP 512 // capacity
uint32_t cb_data[CB_CAP];
volatile int cb_read_index;
volatile int cb_write_index;
volatile int cb_size;

bool cb_write(uint32_t value)
{
	if(cb_size == CB_CAP) return false;
	cb_data[cb_write_index] = value;
	if(++cb_write_index == CB_CAP) cb_write_index = 0;
	cb_size++;
	return true;
}

bool cb_read(uint32_t* value)
{
	*value = 0;
	if(cb_size == 0) return false;
	*value = cb_data[cb_read_index];
	if(++cb_read_index == CB_CAP) cb_read_index = 0;
	cb_size--;
	return true;
}

void cb_init(void)
{
	cb_read_index = 0;
	cb_write_index = 0;
	cb_size = 0;
	memset(cb_data, 0, sizeof(cb_data));
}


// this will only be triggered on a receive
void USART2_IRQHandler(void)
{
	volatile static int c =-1;
	if(USART2->ISR & USART_ISR_RXNE)  {
		c = USART2->RDR; // this also clears the RXNE flag
		//USART2->CR1 |= USART_CR1_TXEIE; // TX empty interrupt enable
		//while((USART2->ISR & USART_ISR_TXE) == 0);
		//while((USART2->ISR & USART_ISR_TC) == 0);
		//USART2->TDR = c;
		//cb_write(c);
		/*
		while((USART2->ISR & USART_ISR_TXE) == 0);
		volatile static const char* s = "0123456789\n";
		volatile static int idx = 0;
		USART2->TDR = s[idx++];
		if(idx > 10) idx = 0;
		*/


		//return;
	}

	//return;
	// can only rite when TX is empty
	if((USART2->ISR & USART_ISR_TXE) && (c>-1)) { 
		//uint32_t c1;
		//if(cb_read(&c1)) USART2->TDR = c1;
		//USART2->CR1 &= ~USART_CR1_TXEIE;
		USART2->TDR = c;
		c = -1;
	}

}

int main()
{
	cb_init();
	uart2_init();

	int baud = 0;
	switch(2) {
		case 1: baud = 9600; break;
		case 2: baud = 115200; break;
		case 3: baud = 115200*2; break; // 230400
	}
	uart2_set_baudrate(baud);

	USART2->CR1 |= USART_CR1_RXNEIE; // RX non-empty interrupt enable
	USART2->CR1 |= USART_CR1_TXEIE; // TX empty interrupt enable
	NVIC_EnableIRQ(USART2_IRQn); // enable the handler

	for(;;);
}
