//"""Library for the ST7567 128x64 SPI LCD."""

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#ifdef RPI // needs refactoring because it only works on RPI
#include <linux/spi/spidev.h>
#include <sys/ioctl.h>


#include "gpio.h"
#include "delay.h"

//#include "sn3218.h"
#include "st7567.h"

typedef uint8_t u8;
static bool rotated = false;
static u8 buf[128*64/8] = {0};
static int fd = -1;
static pin_t rst_pin; // reset pin, GPIO5
static pin_t dc_pin; // Data/Command pin, GPIO6
static u8 contrast = 58;

static void command(u8 *cmds, int n);
static void command1(u8 cmd);
static void command2(u8 v1, u8 v2);
static void data(u8 *data, int n);
static void setup();
static void init();

#define count_of(array) (sizeof(array) / sizeof(array[0]))
#define CMD(arr) command(arr, count_of(arr))


#define WIDTH  128
#define HEIGHT  64
#define PIN_CS  8
#define PIN_RST  5
#define PIN_DC  6
#define ST7567_PAGESIZE  128
#define ST7567_DISPOFF  0xae         // 0xae: Display OFF (sleep mode) */
#define ST7567_DISPON  0xaf          // 0xaf: Display ON in normal mode */
#define ST7567_SETSTARTLINE  0x40    // 0x40-7f: Set display start line */
#define ST7567_STARTLINE_MASK  0x3f
#define ST7567_REG_RATIO  0x20
#define ST7567_SETPAGESTART  0xb0    // 0xb0-b7: Set page start address */
#define ST7567_PAGESTART_MASK  0x07
#define ST7567_SETCOLL  0x00         // 0x00-0x0f: Set lower column address */
#define ST7567_COLL_MASK  0x0f
#define ST7567_SETCOLH  0x10         // 0x10-0x1f: Set higher column address */
#define ST7567_COLH_MASK  0x0f
#define ST7567_SEG_DIR_NORMAL  0xa0  // 0xa0: Column address 0 is mapped to SEG0 */
#define ST7567_SEG_DIR_REV  0xa1     // 0xa1: Column address 128 is mapped to SEG0 */
#define ST7567_DISPNORMAL  0xa6      // 0xa6: Normal display */
#define ST7567_DISPINVERSE  0xa7     // 0xa7: Inverse display */
#define ST7567_DISPRAM  0xa4         // 0xa4: Resume to RAM content display */
#define ST7567_DISPENTIRE  0xa5      // 0xa5: Entire display ON */
#define ST7567_BIAS_1_9  0xa2        // 0xa2: Select BIAS setting 1/9 */
#define ST7567_BIAS_1_7  0xa3        // 0xa3: Select BIAS setting 1/7 */
#define ST7567_ENTER_RMWMODE  0xe0   // 0xe0: Enter the Read Modify Write mode */
#define ST7567_EXIT_RMWMODE  0xee    // 0xee: Leave the Read Modify Write mode */
#define ST7567_EXIT_SOFTRST  0xe2    // 0xe2: Software RESET */
#define ST7567_SETCOMNORMAL  0xc0    // 0xc0: Set COM output direction, normal mode */
#define ST7567_SETCOMREVERSE  0xc8   // 0xc8: Set COM output direction, reverse mode */
#define ST7567_POWERCTRL_VF  0x29    // 0x29: Control built-in power circuit */
#define ST7567_POWERCTRL_VR  0x2a    // 0x2a: Control built-in power circuit */
#define ST7567_POWERCTRL_VB  0x2c    // 0x2c: Control built-in power circuit */
#define ST7567_POWERCTRL  0x2f       // 0x2c: Control built-in power circuit */
#define ST7567_REG_RES_RR0  0x21     // 0x21: Regulation Resistior ratio */
#define ST7567_REG_RES_RR1  0x22     // 0x22: Regulation Resistior ratio */
#define ST7567_REG_RES_RR2  0x24     // 0x24: Regulation Resistior ratio */
#define ST7567_SETCONTRAST  0x81     // 0x81: Set contrast control */
#define ST7567_SETBOOSTER  0xf8      // Set booster level */
#define ST7567_SETBOOSTER4X  0x00    // Set booster level */
#define ST7567_SETBOOSTER5X  0x01    // Set booster level */
#define ST7567_NOP  0xe3             // 0xe3: NOP Command for no operation */
#define ST7565_STARTBYTES  0


static int cursorx = 0, cursory = 0;

void st7567_putchar(char c)
{
	//static int page = 0; // [0,8)
	//static int col = 0; //column [0, 128)

	const int pages = 8, height = 64;

	if(c == '\n') {
		cursorx = 0;
		cursory += 8;
		return;
	}
	if(cursorx >= 128) {
		cursorx = 0;
		cursory += 8;
	}

	if(cursory >= height) { // scroll up
		uint8_t *pixels = buf;
		//putchar('.');
		memmove(pixels, pixels + 128, (pages-1)*128);
		memset(pixels + (pages-1)*128 , 0, 128);
		cursorx = 0;
		cursory = (pages-1)*8;
	}

	if(c< ' ' || c>  0x7F) c = '?'; // 0x7F is the DEL key
	int offset = 4 + (c - ' ' )*6;
	u8 bitmap[8] = {0};
	memcpy(bitmap, ssd1306_font6x8 + offset, 6);
	//bitmap[6] = bitmap[7] = 0;
	memcpy(buf + cursory*128/8 + cursorx, bitmap, 8);
	cursorx += 8;
}

void st7567_printstr(char* str)
{
	while(*str) st7567_putchar(*str++);
}

void st7567_set_col(int page, int c, int value)
{
	buf[page*128+c] = value;
}

void st7567_show()
{
	setup();
	command1(ST7567_ENTER_RMWMODE); // E0
	for(int page =0; page <8; page++) {
		int offset = page * ST7567_PAGESIZE;		// pagesize is 128
		u8 cmds[] = {
			ST7567_SETPAGESTART | page,  	// B0 | page
			ST7567_SETCOLL, 			// 00
			ST7567_SETCOLH};			// 10
		CMD(cmds);
		data(buf+offset, ST7567_PAGESIZE);
	}
	command1(ST7567_EXIT_RMWMODE);				// EE

}

void reset()
{
	gpio_put(&rst_pin, 0);
	delay(10);
	gpio_put(&rst_pin, 1);
	delay(100);
}

void setup()
{
	// reset();
}

void command1(u8 cmd)
{
	u8 vals[] = {cmd};
	CMD(vals);
}

void command2(u8 v1, u8 v2)
{
	u8 vals[] = {v1, v2};
	CMD(vals);
}

void st7567_set_contrast(u8 value)
{
	setup();
	command2(ST7567_SETCONTRAST, value);
}


#define CHIO(err) assert(err != -1);


void writebytes(u8 *data, int n)
{
	struct spi_ioc_transfer tr[1] = {
		{
			.tx_buf = (unsigned long)data,
			.len = n,
			.speed_hz = 500000,
			.bits_per_word = 8,
			//.delay_usecs = 10,
		},
	};
	int err = ioctl(fd, SPI_IOC_MESSAGE(1), &(tr));
	CHIO(err);

}


void set_pin_dc(int val)
{
	gpio_put(&dc_pin, val);
}

void data(u8 *data, int n)
{
	set_pin_dc(1);
	writebytes(data, n);
}

void command(u8 *cmds, int n)
{
	set_pin_dc(0);
	writebytes(cmds, n);
}


void st7567_init(spi_t *spi)
{
	fd = spi->fd;
	gpio_out(&dc_pin, 6);
	gpio_out(&rst_pin, 5);
	reset();
	init();
	setup();
}


void init()
{
	//contrast = 10;
	//contrast = 0xff/2;
	//contrast = 0x3f;
	contrast = 45;
	contrast = 37;
	u8 cmds[] = {
		ST7567_BIAS_1_7,          	// A3 Bais 1/7 (0xA2 = Bias 1/9)
		ST7567_SEG_DIR_NORMAL,		// A0
		ST7567_SETCOMREVERSE,     	// C8 Reverse COM - vertical flip
		ST7567_DISPNORMAL | 0,        	// A6 Inverse display (0xA6 normal)
						//ST7567_DISPNORMAL | 1,        	// A6 Invert display
		ST7567_SETSTARTLINE | 0,	// 40 Start at line 0
		ST7567_POWERCTRL,		// 2F	
		ST7567_REG_RATIO | 3,		// 20 | 3
		ST7567_DISPON,			// AF	
						//
		ST7567_SETCONTRAST,       	// 81 Set contrast (0b1000'0001) instruction 18
						// Contrast value is EV5:EV0 (max is dec 63, hex 3f)
		contrast                        // Contrast value
	};
	CMD(cmds);

}

void st7567_set_pixel(int x, int y, int value)
{
	if(rotated) {
		x = (WIDTH - 1) - x;
		y = (HEIGHT - 1) - y;
	}
	int offset = ((y / 8) * WIDTH) + x;	
	int bit = y % 8;
	buf[offset] &= ~(1 << bit);
	buf[offset] |= (value & 1) << bit;

	//memset(buf, 1, WIDTH*HEIGHT/8); // TODO remove
}

#endif

