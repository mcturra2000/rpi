//#ifdef HAL_I2C_MODULE_ENABLED // this has to come after the include

/** @addtogroup ssd1306

Use an OLED display.

Sending the whole screen at once takes
91ms at 100k (standard mode)
23ms at 400k (fast mode)

`ssd1306_display_cell()` takes about 117us to transfer a cell at a transfer rate of 1Mbps. 
It is intended to be used in the main program loop where you don't want the transfer
blocking for too long. See project `ssd1306` for an example of its use.

The repo from which this code is derived used "vertical addressing mode", which I found to be inconvenient.
I have therefore used horizontal addressing mode. This is the default, but you can set it manually
as follows:
@code
	write_cmd(SET_MEM_ADDR); // 0x20
	//write_cmd(0b01); // vertical addressing mode
	write_cmd(0b00); // horizontal addressing mode

	write_cmd(SET_COL_ADDR); // 0x21
	write_cmd(0);
	write_cmd(127);

	write_cmd(SET_PAGE_ADDR); // 0x22
	write_cmd(0);
	write_cmd(pages()-1);
@endcode
see also: function reset_addressing().

*/

//#include <cstdint>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include <i2c.h>
#include <pot/fonts.h>
#include <delay.h>
#include <pot/ssd1306.h>

// You need to initialise I2C yourself, and implement the following function:
//extern void i2c_ship(uint32_t i2c, uint8_t sid, const uint8_t *w, size_t wn);
extern void i2c_ship(uint32_t i2c, uint8_t sid, const uint8_t *data, size_t len);

//extern int vsnprintf(char *str, size_t size, const char *format, va_list ap);

//const uint8_t ssd1306_font6x8[];

#ifdef PICO_BOARD
#include <hardware/i2c.h>
typedef  i2c_inst_t I2C_HandleTypeDef;
//extern I2C_HandleTypeDef i2c0;
#define ptr_hi2c1 i2c0
#else
extern I2C_HandleTypeDef hi2c1;
#define ptr_hi2c1 &hi2c1
#endif

//#define OLED_128x32

/*
#ifdef OLED_128x32
const uint8_t height = 32;
#else
const uint8_t height = 64;
#endif
*/

static uint32_t i2c;
static uint8_t height = 32;
//const uint8_t SID = (height == 64) ? 0x3C : 0x3D; // different height displays have different addr
const uint8_t SID = 0x3C ; // different height displays have different addr
const uint8_t width = 128;

int pages() { return height/8; }

//uint8_t scr[pages*width+1]; // extra byte holds data send instruction
//uint8_t ssd1306_scr[1025] = {[0] = 0x40}; // being: 8 pages (max) * 128 width + 1 I2C command byte




struct ssd1306_screen_t ssd1306_screen = {.cmd = 0x40};

#define SET_CONTRAST 0x81
#define SET_ENTIRE_ON 0xA4
#define SET_NORM_INV 0xA6
#define SET_DISP 0xAE
#define SET_MEM_ADDR 0x20
#define SET_COL_ADDR 0x21
#define SET_PAGE_ADDR 0x22
#define SET_DISP_START_LINE 0x40
#define SET_SEG_REMAP 0xA0
#define SET_MUX_RATIO 0xA8
#define SET_COM_OUT_DIR 0xC0
#define SET_DISP_OFFSET 0xD3
#define SET_COM_PIN_CFG 0xDA	// s10.1.18 page 40
#define SET_DISP_CLK_DIV 0xD5
#define SET_PRECHARGE 0xD9
#define SET_VCOM_DESEL 0xDB
#define SET_CHARGE_PUMP 0x8D


void ssd1306_test()
{
	
	ssd1306_init_display(64, (uint32_t) ptr_hi2c1);
	int counter = 0;

	while(1) {
		// You
		uint32_t ms = millis();
		ssd1306_printf("Counter %d\n", counter++);

		// the following section could be replaced by a simple ssd1306_show_scr(), but I show it the hard way for illustration purposes

		while(millis() - ms < 1000) {
			ssd1306_display_cell();
			delay(1);
		}
	}
}

static void send_data(uint8_t *data, int nbytes)
{
	//extern void i2c_ship(uint32_t i2c, uint8_t sid, const uint8_t *data, size_t len);
	i2c_ship(i2c, SID, data, nbytes);
}

void write_cmd(uint8_t cmd);

void write_cmd_pair(uint8_t cmd, uint8_t value)
{
	write_cmd(cmd);
	write_cmd(value);
}


void fill_scr(uint8_t v)
{
	memset(ssd1306_screen.pixels, v, sizeof(ssd1306_screen.pixels));
}

void clear_scr(void)
{
	setCursorxy(0,0);
	fill_scr(0);
}


void send2(uint8_t v1, uint8_t v2)
{
	uint8_t buf[2];
	buf[0] = v1;
	buf[1] = v2;
	send_data(buf, 2);
}

/** @brief send current cell to OLED

A "cell" is an 8x8 block.

The function keeps an internal position of the cell it wants to
write. This is independent of the screen cursor.

You would typically use this function in your main loop, where
it is important that functions don't block too long.

*/ 
void ssd1306_display_cell (void)
{
	//show_scr(); return;

	static int offset = 0;
	//if(offset == -1) return;
	uint8_t block[1+8]; // the data command, plus 8 column bytes
	block[0] = 0x40;
	//memcpy(block+1, (uint8_t*)&ssd1306_screen + offset + 1, 8);
	memcpy(block+1, ssd1306_screen.pixels + offset, 8);
	send_data(block, 9);
	offset += 8;
	if(offset >= width * pages())  {
		offset = 0;
		//offset = -1;
	}
}


/** @brief send screen to the display itself

Seems to take about 100ms at 100kps tfr rate, which is disappointing.


*/

// declared weak in case you want to overwrite it (e.g. for using DMA)
void  __attribute__((weak))  ssd1306_show_scr (void)
{
#if 0
	write_cmd(SET_MEM_ADDR); // 0x20
	//write_cmd(0b01); // vertical addressing mode
	write_cmd(0b00); // horizontal addressing mode

	write_cmd(SET_COL_ADDR); // 0x21
	write_cmd(0);
	write_cmd(127);

	write_cmd(SET_PAGE_ADDR); // 0x22
	write_cmd(0);
	write_cmd(pages()-1);
#endif

#if 0
	scr[0] = 0x40; // the data instruction	
	int size = pages()*width +1;
	send_data(scr, size);
#endif
#if 0
	send_data((uint8_t*)&ssd1306_screen, sizeof(ssd1306_screen));
	//send_data(&ssd1306_screen, 512+1);
#else
	uint8_t buf[9];
	buf[0] = 0x40;
	int nblocks = pages()*width/ 8;
	for(int i = 0; i < nblocks; i++) {
		memcpy(buf+1, ssd1306_screen.pixels + 1 * i*8, 8);
		send_data(buf, 9);
	}
#endif
}



void write_cmd(uint8_t cmd) 
{ 
	send2(0x80, cmd);
}


void ssd1306_write_cmd(uint8_t cmd) { send2(0x80, cmd); }

void poweroff() { write_cmd(SET_DISP | 0x00); }

void poweron() { write_cmd(SET_DISP | 0x01); }

void contrast(uint8_t contrast) { write_cmd(SET_CONTRAST); write_cmd(contrast); }

void invert(uint8_t invert) { write_cmd(SET_NORM_INV | (invert & 1)); }



void reset_addressing(void)
{
	write_cmd(SET_MEM_ADDR); // 0x20
	//write_cmd(0b01); // vertical addressing mode
	write_cmd(0b00); // horizontal addressing mode

	write_cmd(SET_COL_ADDR); // 0x21
	write_cmd(0);
	write_cmd(127);

	write_cmd(SET_PAGE_ADDR); // 0x22
	write_cmd(0);
	write_cmd(pages()-1);

}

void ssd1306_init_display(int h, uint32_t i2c_port)
{
	i2c = i2c_port;
	height = h;
	//uint8_t pin_cfg_height = (height == 32 ? 0x02 : 0x12);
	write_cmd_pair(SET_COM_PIN_CFG, height == 32 ? 0x02 : 0x12);
	write_cmd_pair(SET_MUX_RATIO /* 0xA8 */, height - 1);

	static uint8_t cmds[] = {
		SET_DISP | 0x00,  // display off 0x0E | 0x00

		SET_MEM_ADDR, // 0x20
		0x00,  // horizontal

		//# resolution and layout
		SET_DISP_START_LINE | 0x00, // 0x40
		SET_SEG_REMAP | 0x01,  //# column addr 127 mapped to SEG0

		//SET_MUX_RATIO, // 0xA8
		//(uint8_t)(height - 1),

		SET_COM_OUT_DIR | 0x08,  //# scan from COM[N] to COM0  (0xC0 | val)
		SET_DISP_OFFSET, // 0xD3
		0x00,

		//SET_COM_PIN_CFG, // 0xDA
		//0x02 if self.width > 2 * self.height else 0x12,
		//width > 2*height ? 0x02 : 0x12,
		//SET_COM_PIN_CFG, pin_cfg_height,

		//# timing and driving scheme
		SET_DISP_CLK_DIV, // 0xD5
		0x80,

		SET_PRECHARGE, // 0xD9
		//0x22 if self.external_vcc else 0xF1,
		//external_vcc ? 0x22 : 0xF1,
		0xF1,

		SET_VCOM_DESEL, // 0xDB
		//0x30,  //# 0.83*Vcc
		0x40, // changed by mcarter

		//# display
		SET_CONTRAST, // 0x81
		0xFF,  //# maximum

		SET_ENTIRE_ON,  //# output follows RAM contents // 0xA4
		SET_NORM_INV,  //# not inverted 0xA6

		SET_CHARGE_PUMP, // 0x8D
		//0x10 if self.external_vcc else 0x14,
		//external_vcc ? 0x10 : 0x14,
		0x14,

		SET_DISP | 0x01
	};

	// write all the commands
	for(int i=0; i<sizeof(cmds); i++)
		write_cmd(cmds[i]);
	clear_scr();
	reset_addressing();
	//ssd1306_show_scr();
}


void draw_pixel(int16_t x, int16_t y, int color) 
{
	if(x<0 || x >= width || y<0 || y>= height) return;

	int page = y/8;
	int bit = 1<<(y % 8);
	//int xincr = 8;
	//xincr =	height/8;
	//uint8_t* ptr = scr + x*xincr + page  + 1; 
	//uint8_t* ptr = screen.display[page] + x; 
	//uint8_t* ptr = (uint8_t*) &screen + page*8 + x +1; 
	uint8_t* ptr = ssd1306_screen.pixels + page*128 + x;

	switch (color) {
		case 1: // white
			*ptr |= bit;
			break;
		case 0: // black
			*ptr &= ~bit;
			break;
		case -1: //inverse
			*ptr ^= bit;
			break;
	}

}

void drawBitmap(int16_t x, int16_t y, const uint8_t bitmap[], int16_t w,
		int16_t h, uint16_t color) 
{
	int16_t byteWidth = (w + 7) / 8; // Bitmap scanline pad = whole byte
	uint8_t byte = 0;

	for (int16_t j = 0; j < h; j++, y++) {
		for (int16_t i = 0; i < w; i++) {
			if (i & 7)
				byte <<= 1;
			else
				byte = bitmap[j * byteWidth + i / 8];
			if (byte & 0x80)
				draw_pixel(x + i, y, color);
		}
	}
}

void draw_letter_at (uint8_t x, uint8_t y, char c)
{
	if(c< ' ' || c>  0x7F) c = '?'; // 0x7F is the DEL key

	int offset = 4 + (c - ' ' )*6;
	for(int col = 0 ; col < 6; col++) {
		uint8_t line =  ssd1306_font6x8[offset+col];
		for(int row =0; row <8; row++) {
			draw_pixel(x+col, y+row, line & 1);
			line >>= 1;
		}
	}

	for(int row = 0; row<8; row++) {
		draw_pixel(x+6, y+row, 0);
		draw_pixel(x+7, y+row, 0);
	}

}


static int cursorx = 0, cursory = 0;

int getx(void)
{
	return cursorx;
}

void ssd1306_home(void)
{
	cursorx = 0;
	cursory = 0;
}

void ssd1306_putchar(char c)
{
	if(c == '\n') {
		cursorx = 0;
		cursory += 8;
		return;
	}
	if(cursorx >= 128) {
		cursorx = 0;
		cursory += 8;
	}
	if(cursory >= height) { // scroll up
		uint8_t *pixels = ssd1306_screen.pixels;
		memmove(pixels, pixels + 128, (pages()-1)*128);
		memset(pixels + (pages()-1)*128 , 0, 128);
		cursorx = 0;
		cursory = (pages()-1)*8;
	}
	draw_letter_at(cursorx, cursory, c);
	cursorx += 8;
}

/** @brief Print at current cursor position
*/

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wparentheses"
//void __attribute__((noparentheses)) ssd1306_print(const char* str)
void ssd1306_print(const char* str)
{
	char c;
	while(c = *str) {
		ssd1306_putchar(c);
		str++;
	}
}
#pragma GCC diagnostic pop

void ssd1306_puts(const char* str)
{
	ssd1306_print(str);
	ssd1306_putchar('\n');
}

void ssd1306_printf(const char *format, ...)
{
	char str[512];
	va_list ap;
	va_start(ap, format);
	vsnprintf(str, sizeof(str), format, ap);
	va_end(ap);
	str[511] = 0;
	ssd1306_print(str);
}


/** @brief Print at a stated cursor position

Position should be given in terms of character positions, not pixel

*/

void ssd1306_print_at(int x, int y, const char* str)
{
        setCursorx(x);
        setCursory(y);
        ssd1306_print(str);
}

void setCursorx(int x)
{
	const int pos = 8;
	cursorx = pos * x;
}


void setCursory(int y)
{
	const int pos = 8;
	cursory = pos * y;
}

void setCursorxy(int x, int y)
{
	setCursorx(0);
	setCursory(0);
}

void print_char_at(char c, int scale, int xoff, int yoff)
{
	ssd1306_home();
	int x = xoff;
	for(int col=0; col < 6; col++) {
		for(int row=0; row < 8; row++) {
			//printf("start setting:%c, %d\n", c, c);

			int setting = font_pixel(c, col, row);
			//puts("done setting");
			for(int x1 = 0; x1<scale; x1++) {
				for(int y1=0; y1<scale; y1++) {
					draw_pixel(x + x1, row*scale + y1+yoff, setting);
				}
			}
		}
		x += scale;
	}
	//printf("...print_char: x = %d\n", x);
	//setCursorx(x);
	//printf("...cursor is now %d\n", getx());
}

void print_str_at(const char *str, int scale, int x, int y)
{
	while(*str) {
		print_char_at(*str, scale, x, y);
		str++;
		x += 6*scale;
	}
}

void show_str(char* str)
{
	setCursorx(0);
	setCursory(0);
	print_str_at(str, 4, 0, 0);
	//show_scr();
}

void ssd1306_printf_at(int scale, int x, int y, const char *format, ...)
{
        char msg[100];
        va_list ap;
        va_start(ap, format);
        vsnprintf(msg, sizeof(msg), format, ap);
        va_end(ap);
        print_str_at(msg, scale, x, y);
}


int font_pixel(char c, int col, int row)
{
	int idx = (c-32)*6 + 4 + col;
	//printf("c:%d, idx:%d\n", c, idx);
	return (ssd1306_font6x8[idx] >> row) & 1;

}
