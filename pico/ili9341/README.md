# ili9341 display

Uses SPI


## See also

* db7.160
* [original source code](https://github.com/shawnhyam/pico)
* (xpt2046)[../xpt2046) - touch screen counterpart


## Status

2023-01-22	Changed mode0. You now need to explicitly call 
		mode0_refresh_screen() to refresh screen
		
2023-01-02	Separated out the code into a library. See project 78 
		for example usage.

2022-12-30	Pin reshuffle. Working

2021-11-24	Started. Working.
