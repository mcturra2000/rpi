// a clapperboard that momentarily turns the green LED and buzzer on
static constexpr int BTN = PB4;
static constexpr int GREEN = PB1;
static constexpr int BZR = PB2;




typedef struct {
  unsigned int init: 1, en : 1, 
         seek_high :1 ,
         //ack_falling : 1, ack_rising:1, 
         falling:1, rising:1, // prev:8, 
         count:8;
} deb_t;

void deb_update(deb_t *deb, int high);
bool deb_falling(deb_t* deb);
bool deb_rising(deb_t* deb);

#define THRESH 10

void deb_update(deb_t* deb, int high)
{
  if(deb->en == 0) return;

  if(deb->init == 0) {
    deb->init = 1;
    deb->count = THRESH;
    //deb->prev = THRESH;
    return;
  }

  if(high && (deb->count < THRESH) ) {
    deb->count++;
    if((deb->count == THRESH) && (deb->seek_high == 1)) {
      deb->rising = 1;
      deb->seek_high = 0;
    }
  } else if((high ==0) && (deb->count > 0)) {
    deb->count--;
    if((deb->count == 0) && (deb->seek_high == 0)) {
      deb->falling = 1;
      deb->seek_high = 1;
    }
  }
}



bool deb_falling(deb_t* deb)
{
  //debounce_update(deb);
  if(deb->falling) {
    deb->falling = false;
    return true;
  }
  return false;
}


bool deb_rising(deb_t* deb)
{
  //debounce_update(deb);
  if(deb->rising) {
    deb->rising = false;
    return true;
  }
  return false;
}


deb_t btn_deb;

void setup() {
  pinMode(BTN, INPUT_PULLUP);
  btn_deb.en = 1;
  pinMode(GREEN, OUTPUT);
  pinMode(BZR, OUTPUT);
}

void loop() {
  //btn.update(digitalRead(BTN));
  deb_update(&btn_deb, digitalRead(BTN));
  if(deb_falling(&btn_deb)) {
    //if(digitalRead(GREEN)) digitalWrite(GREEN, 0); else digitalWrite(GREEN, 1);
    digitalWrite(GREEN, 1);
    digitalWrite(BZR, 1);
    delay(200);
    digitalWrite(GREEN, 0);
    digitalWrite(BZR, 0);
  }
  delay(4);
}
