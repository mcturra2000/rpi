# I2C

## shims

Taken from [here](https://github.com/eziya/STM32_LL_EXAMPLES/blob/main/STM32F4_LL_I2C_MASTER_BME280/Core/Src/main.c)

There are plenty of other I2C functions, too.

```
void i2c_ship(uint32_t i2c, uint8_t sid, const uint8_t *data, size_t len)
{
	I2C_TypeDef *_i2c = (I2C_TypeDef *) i2c; // e.g. i2c == I2C1
	LL_I2C_GenerateStartCondition(_i2c);
	while(!LL_I2C_IsActiveFlag_SB(_i2c));

	LL_I2C_TransmitData8(_i2c, (sid << 1) | 0x00);
	while(!LL_I2C_IsActiveFlag_ADDR(_i2c));
	LL_I2C_ClearFlag_ADDR(_i2c);

	for(int i = 0; i < len; i++) {
		while(!LL_I2C_IsActiveFlag_TXE(_i2c));
		LL_I2C_TransmitData8(_i2c, *data++);
	}

	while(!LL_I2C_IsActiveFlag_BTF(_i2c)); // 7. wait BTF flag (TXE flag set & empty DR condition)


	LL_I2C_GenerateStopCondition(_i2c);
}
```

