/*
 * om.c
 *
 *  Created on: May 14, 2024
 *      Author: pi
 */

//#include "main.h"
#include <stdbool.h>

#include <gpio.h>
#include <delay.h>
#include <pot/om.h>

#ifdef USE_HAL_DRIVER
#define BZR GPIOB, GPIO_PIN_6
#endif

#ifdef PICO_BOARD
#define BZR 0, 6
#endif

static bool  om_en = false; // is it running?
static int om_start_time;
static int bzr_mode = 0; // see om_init() for meaning
const static int duration = 30  * 60 * 1000; // how long before counter expiry

//void om_mode_toggle_1_2();
void om_set_mode(int mode);


//static inline int millis() { return HAL_GetTick(); }

// FN bzr 
static void bzr (int on)
{
	gpio_write(BZR, on);
}

/* FN om_init 
 * Call this before anything else
 * It sets the BZR to MODE
 * mode:
 * 0 : completely silent
 * 1 : only buzz after timer expired
 * 2 : buzz continuously
 */
void om_init(int mode)
{
	gpio_out(BZR);
	om_set_mode(mode);
	//bzr_mode = mode;
}

#if 0
// FN om_mode_toggle_1_2
// If in mode 1, switch to mode 2, and vice versa. 
// Don't change if in mode 0
// This isn't quite right
void om_mode_toggle_1_2()
{
	//if(bzr_mode) om_set_mode(3-bzr_mode);
	
	switch (bzr_mode) {
		case 1 : om_set_mode(2); break;
		case 2 : om_set_mode(1); break;
	}
	

}
#endif

// FN om_get_mode 
int om_get_mode(int mode)
{
	return bzr_mode;
}

// FN om_set_mode 
void om_set_mode(int mode)
{
	bzr(0);
	bzr_mode = mode;
	om_update();

}


/* If it's on, turn it off, and vice versa
 *
 */
void om_toggle()
{
	if(om_en)
		om_stop();
	else
		om_start();

}
void om_start()
{
	om_en = true;
	om_start_time = millis();

}

// FN om_running 
// Determines if om is running or not
bool om_running()
{
	return om_en;
}

void om_stop()
{
	bzr(0);
	if(!om_en) return;
	om_en = false;
}

// FN om_millis 
// ms since start (assumes on)
uint32_t om_millis()
{
	return millis() - om_start_time;
}


// FN om_secs 
// secs since start (assumes on)
uint32_t om_secs()
{
	return om_millis()/1000;
}

// FN om_update 
void om_update()
{
	if(!om_en) return;
	if(bzr_mode == 0) return;
	int m = om_millis();
	bool expired = (m>= duration);
	if(bzr_mode == 1 && !expired) return;
	int slice = m % 5000; // parcel up the time in terms of 5 seconds
	bool on = false;
	if(slice < 50) on = true;
	if(expired && (100 < slice) && (slice< 150)) on = true;
	bzr(on);

}
