#pragma once
#include <stdint.h> // for the likes of uint8_t


// #define __IO volatile
#define REG(x) volatile unsigned char x

#define nop() __asm__ __volatile__("nop" )
/*
static inline void __attribute__((optimize("O0"))) nop(void)
{
        //asm volatile("nop");
        __asm__ __volatile__("nop" );
}
*/

