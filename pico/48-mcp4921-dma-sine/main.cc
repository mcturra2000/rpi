#include <stdio.h>
#include <string.h>
//#define PARAM_ASSERTIONS_ENABLE_ALL 1
#include "pico/stdlib.h"
#include "pico/assert.h"
//#include "hardware/clocks.h"
#include "hardware/gpio.h"
#include "hardware/irq.h"
#include "hardware/spi.h"
#include "hardware/dma.h"
#include <math.h>
// #include "tusb.h" // if you want to use tud_cdc_connected()

#define SPI spi1
#define SCK 	10
#define MOSI	11
#define CS	13

#define LEDA 14
#define LEDB 15

#define NSAMPS 256
const auto clk_hz = 125'000'000;
// 44100 will come out as 44107
// 22000 as 22003
const auto fs_raw = 1 ? 44100 : 22000;
const auto fs_raw_denom = clk_hz/ fs_raw;
const auto fs = clk_hz/fs_raw_denom; 
const auto fw = 500; // wave frequency
const auto TOP = 4095;
static_assert(TOP <= 0xffff); // it's only a 16-bit register

unsigned int slice_num;
int dma_chan_a, dma_chan_b;

//volatile uint16_t val = 0;

volatile uint16_t buf_a[NSAMPS] __attribute__((aligned(NSAMPS * sizeof(uint16_t))));
volatile uint16_t buf_b[NSAMPS] __attribute__((aligned(NSAMPS * sizeof(uint16_t))));

constexpr uint log2(uint n)
{
	if(n>1)
		return 1 + log2(n>>1);
	else
		return 0;
}

static const uint size_bits = log2(NSAMPS * sizeof(uint16_t));  // 9, basically
static_assert((1<<size_bits) == NSAMPS * sizeof(uint16_t));


void gpio_toggle(uint pin)
{
	gpio_put(pin, gpio_get(pin) ? 0 : 1);
}

// From
// http://www.mclimatiano.com/faster-sine-approximation-using-quadratic-curve/

// // doesn't seem better than normal sin

// Low Precision version
float SinLP(float x)
{
	if (x < -3.14159265f)
		x += 6.28318531f;
	else if (x >  3.14159265f)
		x -= 6.28318531f;

	if ( x < 0 )
		return x * ( 1.27323954f + 0.405284735f * x );
	else
		return x * ( 1.27323954f - 0.405284735f * x );
}

// High Precision version
float SinHP( float x )
{
        float sin = 0;
        if (x < -3.14159265f)
                x += 6.28318531f;
        else
                if (x >  3.14159265f)
                        x -= 6.28318531f;

        if ( x < 0 )
        {
                sin = x * ( 1.27323954f + 0.405284735f * x );

                if ( sin < 0 )
                        sin = sin * ( -0.255f * ( sin + 1 ) + 1 );
                else
                        sin = sin * ( 0.255f * ( sin - 1 ) + 1 );
        }
        else
        {
                sin = x * ( 1.27323954f - 0.405284735f * x );

                if ( sin < 0 )
                        sin = sin * ( -0.255f * ( sin + 1 ) + 1 );
                else
                        sin = sin * ( 0.255f * ( sin - 1 ) + 1 );
        }

        return sin;

}


void fill_buff(uint16_t* buf, uint pin)
{
	static volatile float x=0, y=0;
	const float pi2 = 6.27319;
	const float dx = pi2 * fw/fs;
	gpio_put(pin, 1);
	for(int i =0; i<NSAMPS; i++) {
		buf[i] = ((uint16_t) y) | (0b0111<<12);
		x+= dx;
		if(x > pi2) x-= pi2;
		//y = (Sin(x)+1.0)*TOP/2.0;
		y = (sin(x)+1.0)*TOP/2.0;
		//y = 0;
	}
	gpio_put(pin, 0);
}



void dma_handler()
{
	if(dma_hw->intr & (1u<<dma_chan_a)) { // channel a complete?
		dma_hw->ints0=1u<<dma_chan_a; // clear the interrupt request
		fill_buff((uint16_t*) buf_a, LEDA); // buf a transferred, so refill it
	}
	if(dma_hw->intr & (1u<<dma_chan_b)) { // channel b complete?
		dma_hw->ints0=1u<<dma_chan_b; // clear the interrupt request
		fill_buff((uint16_t*) buf_b, LEDB); // buf b transferred, so refill it
	}

}


void init_spi(void)
{
	uint baud = 8'000'000;
	spi_init(SPI, baud);
	spi_set_format(SPI, 16, SPI_CPOL_0, SPI_CPHA_0, SPI_MSB_FIRST);
	gpio_set_function(SCK,  GPIO_FUNC_SPI);
	gpio_set_function(MOSI, GPIO_FUNC_SPI);
	gpio_set_function(CS, GPIO_FUNC_SPI);
}


void dma_setup_chan(int dma_chan, int dma_chan_chain, volatile uint16_t* buf)
{
	dma_channel_config cfg = dma_channel_get_default_config(dma_chan);
	channel_config_set_transfer_data_size(&cfg, DMA_SIZE_16);
	channel_config_set_read_increment(&cfg, true);
	channel_config_set_ring(&cfg, false, size_bits);
	// channel_config_set_dreq(&cfg, DREQ_PWM_WRAP0 + slice_num); // write data at pwm frequency 
	channel_config_set_dreq(&cfg, 0x3b); // timer pacing using Timer 0
	channel_config_set_chain_to(&cfg, dma_chan_chain); // start chan b when chan a completed
	//channel_config_set_irq_quiet(&cfg_a, false);
	dma_channel_configure(
			dma_chan,          // Channel to be configured
			&cfg,            // The configuration we just created
			&spi_get_hw(SPI)->dr, // write address
			buf,           // The initial read address
			NSAMPS, // Number of transfers
			false           // Start immediately?
			);
}

int main() 
{
	stdio_init_all();
	// while(!tud_cdc_connected()) sleep_ms(250); // wait for usb serial 
	puts("\nwsine wave via dma and spi and chaining");

	auto led_mask =  (1<<LEDA) | (1<<LEDB);
	gpio_init_mask(led_mask);
	gpio_set_dir_out_masked(led_mask);

	init_spi();

	// set up DMA ping-pong buffers
	dma_chan_a = dma_claim_unused_channel(true);
	dma_chan_b = dma_claim_unused_channel(true);
	dma_setup_chan(dma_chan_a, dma_chan_b, buf_a);
	dma_setup_chan(dma_chan_b, dma_chan_a, buf_b);
	irq_set_exclusive_handler(DMA_IRQ_0,dma_handler);
	dma_set_irq0_channel_mask_enabled((1<<dma_chan_a) | (1<<dma_chan_b), true);
	irq_set_enabled(DMA_IRQ_0,true);

	const int dma_timer = 0; // dma_claim_unused_timer(true); // panic upon failure
	dma_timer_claim(dma_timer); // panic if fail
	dma_timer_set_fraction(dma_timer, 1, fs_raw_denom);

	dma_channel_start(dma_chan_a); // seems to start something

	while(1) {
		tight_loop_contents();
	}
}

