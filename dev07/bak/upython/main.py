# MicroPython TM1637 quad 7-segment LED display driver examples

# WeMos D1 Mini -- 4 Digit Display
# D1 (GPIO5) ----- CLK
# D2 (GPIO4) ----- DIO
# 3V3 ------------ VCC
# G -------------- GND

import utime
import tm1637
from machine import Pin
tm = tm1637.TM1637(clk=Pin(0), dio=Pin(2))
tm.brightness(1)

from machine import Pin,I2C
from time import sleep_ms
from bme688 import *
import ds3231
i2c = I2C(0, sda=Pin(4), scl=Pin(5))

sensor = BME680_I2C(i2c)
rtc = ds3231.RTC(i2c, sid = 0x68)
TSW = Pin(7, Pin.IN, Pin.PULL_UP) # test switch
#i = 0

class Every:
    def __init__(self, interval_ms, func = None ):
        self.start = utime.ticks_ms()
        self.interval_ms = interval_ms
        self.func = func
        
    def rising(self):
        now =utime.ticks_ms()
        if now<self.start: self.start = now
        if now - self.start < self.interval_ms: return False
        self.start = now
        return True
    
    def update(self):
        if not self.rising(): return
        #print("Updating")
        self.func()
        

do_show_time  = True
def react_button():
    global do_show_time
    do_show_time = not do_show_time

grate = 0 
def btn_check():
    global grate
    if TSW.value() == 1:
        grate = 0
        return
    if TSW.value() == 0:
        grate += 1
    
    if grate >= 8: 
    	grate = 8 # restrict
    	return
    
    #grate += 1
    if grate == 7:
        react_button()
        
ev_btn = Every(4, btn_check)

def show_temp():
    #gas = sensor.gas
    temper = sensor.temperature
    temper = round(temper)
    #humidity = sensor.humidity
    #pressure = sensor.pressure
    tm.temperature(temper)
    
def show_time():
    global rtc
    second, minute, hour, weekday, day, month, year = rtc.ReadTime(0)
    hour = int(hour)
    minute = int(minute)
    day, hour = ds3231.adjbst(year, month, day, hour)
    #print("{0:>02d}:{1:>02d}".format(hour, minute))
    tm.numbers(hour, minute, True)
    #print(t)
    

while True:
    if do_show_time:
        show_time()
    else:
        show_temp()
    ev_btn.update()
    #if TSW.value() == 0 : tm.show("test")
    sleep_ms(1)

    # heartbeat
    #segs = tm.show("----")
    #sleep_ms(100)

