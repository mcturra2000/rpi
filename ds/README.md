# DS3231 clock setting

Set the thing going. In the ds, type something like:
```
./ds -h # for help
./ds -r # read the time on the device
./ds -s # set the time to utc
./ds -t # set a test time of 21 Jan 2021
```

PERIS: ds3231

## See also

dev10 - for an ESP getter/setter

~~pico/22-ds3231-set - for rp2040 counterpart~~

Can be used in projects:
* /home/pi/mcus/STM32-workspaces/workspace_1.14.0/dev13-f401
* /home/pi/mcus/STM32-workspaces/workspace_1.14.0/dev13-f411
* dev07

## Status

2024-03-06 Made conformal to struct tm

2023-07-19  Added ETX fix-up for getter/setter

2023-06-06	Started. Works. 
