# Create a sine wave using DMA and PWM

Ping-pong buffering sine wave

## See also

* [fprum](https://forums.raspberrypi.com/viewtopic.php?t=329066)


## Status

2022-02-05	Produces sine wave

2022-02-04	Started. Partially working. Square wave.
