#include "hardware/gpio.h"

#include <gpioxx.h>

GpioOut::GpioOut(uint gpio)
{
	gpio_init(gpio);
	gpio_set_dir(gpio, GPIO_OUT);
	m_gpio = gpio;
}

void GpioOut::on(void)
{
	gpio_put(m_gpio, true);
}
void GpioOut::off(void)
{
	gpio_put(m_gpio, false);
}

