/* this reproduces the standard boot2 code
See
/home/pi/pico/pico-sdk/src/rp2_common/boot_stage2
boot2_w25q080.S
and in subdir
/home/pi/pico/pico-sdk/src/rp2_common/boot_stage2/asminclude/boot2_helpers
exit_from_boot2.S
*/

#define PADS_QSPI_BASE 	0x40020000
#define XIP_SSI_BASE	0x18000000

#define SSI_CTRLR0_DFS_32_LSB			16
#define SSI_CTRLR0_TMOD_VALUE_TX_AND_RX		0
#define SSI_CTRLR0_TMOD_LSB			8


#define MODE_CONTINUOUS_READ 0xa0

#define XIP_BASE 0x10000000
#define PPB_BASE 0xe0000000


#define M0PLUS_VTOR_OFFSET 0x0000ed08 // defined in src/rp2040/hardware_regs/include/hardware/regs/m0plus.h


.syntax unified
.cpu cortex-m0plus
.thumb

.section .text

// The exit point is passed in lr. If entered from bootrom, this will be the
// flash address immediately following this second stage (0x10000100).
// Otherwise it will be a return address -- second stage being called as a
// function by user code, after copying out of XIP region. r3 holds SSI base,
// r0...2 used as temporaries. Other GPRs not used.
.global _stage2_boot
.type _stage2_boot,%function
.thumb_func
_stage2_boot:
push	{lr}
ldr	r3, =PADS_QSPI_BASE
movs	r0, #33	// 0x21
str	r0, [r3, #4]
ldr	r0, [r3, #8]
movs	r1, #2
bics	r0, r1
str	r0, [r3, #8]
str	r0, [r3, #12]
str	r0, [r3, #16]
str	r0, [r3, #20]
//ldr	r3, [pc, #184]	// (0xd0)
ldr r3, =XIP_SSI_BASE
movs	r1, #0
str	r1, [r3, #8]
movs	r1, #2
str	r1, [r3, #20]
movs	r1, #1
movs	r2, #240	// 0xf0
str	r1, [r3, r2]

program_sregs:
#define CTRL0_SPI_TXRX \
    (7 << SSI_CTRLR0_DFS_32_LSB) | /* 8 bits per data frame */ \
    (SSI_CTRLR0_TMOD_VALUE_TX_AND_RX << SSI_CTRLR0_TMOD_LSB)
//ldr	r1, [pc, #172]	// (0xd4)
ldr r1, =(CTRL0_SPI_TXRX)
str	r1, [r3, #0]
movs	r1, #1
str	r1, [r3, #8]
movs	r0, #53	// 0x35
bl	read_flash_sreg
movs	r2, #2
cmp	r0, r2
beq.n	skip_sreg_programming
movs	r1, #6
str	r1, [r3, #96]	// 0x60
bl	wait_ssi_ready
ldr	r1, [r3, #96]	// 0x60
movs	r1, #1
str	r1, [r3, #96]	// 0x60
movs	r0, #0
str	r0, [r3, #96]	// 0x60
str	r2, [r3, #96]	// 0x60
bl	wait_ssi_ready
ldr	r1, [r3, #96]	// 0x60
ldr	r1, [r3, #96]	// 0x60
ldr	r1, [r3, #96]	// 0x60
//poll status register for write completion
1:
movs	r0, #5
bl	read_flash_sreg	
movs	r1, #1
tst	r0, r1
bne.n	1b

skip_sreg_programming:
movs	r1, #0
str	r1, [r3, #8]

dummy_read:
#define SSI_CTRLR0_SPI_FRF_VALUE_QUAD 0x2
#define FRAME_FORMAT SSI_CTRLR0_SPI_FRF_VALUE_QUAD
#define SSI_CTRLR0_SPI_FRF_LSB 21
#define SSI_CTRLR0_TMOD_VALUE_EEPROM_READ 0x3
#define CTRLR0_ENTER_XIP \
    (FRAME_FORMAT                          /* Quad I/O mode */                \
        << SSI_CTRLR0_SPI_FRF_LSB) |                                          \
    (31 << SSI_CTRLR0_DFS_32_LSB)  |       /* 32 data bits */                 \
    (SSI_CTRLR0_TMOD_VALUE_EEPROM_READ     /* Send INST/ADDR, Receive Data */ \
        << SSI_CTRLR0_TMOD_LSB)
//ldr	r1, [pc, #108]	// (0xd8)
ldr r1, =(CTRLR0_ENTER_XIP)
str	r1, [r3, #0]
movs	r1, #0
str	r1, [r3, #4]
#define ADDR_L 8
#define SSI_SPI_CTRLR0_ADDR_L_LSB 2
#define WAIT_CYCLES 4
#define SSI_SPI_CTRLR0_WAIT_CYCLES_LSB 11
#define SSI_SPI_CTRLR0_INST_L_VALUE_8B 0x2
#define SSI_SPI_CTRLR0_INST_L_LSB 8
#define SSI_SPI_CTRLR0_TRANS_TYPE_VALUE_1C2A 0x1
#define SSI_SPI_CTRLR0_TRANS_TYPE_LSB 0
#define SPI_CTRLR0_ENTER_XIP \
    (ADDR_L << SSI_SPI_CTRLR0_ADDR_L_LSB) |     /* Address + mode bits */ \
    (WAIT_CYCLES << SSI_SPI_CTRLR0_WAIT_CYCLES_LSB) | /* Hi-Z dummy clocks following address + mode */ \
    (SSI_SPI_CTRLR0_INST_L_VALUE_8B \
        << SSI_SPI_CTRLR0_INST_L_LSB) |        /* 8-bit instruction */ \
    (SSI_SPI_CTRLR0_TRANS_TYPE_VALUE_1C2A      /* Send Command in serial mode then address in Quad I/O mode */ \
        << SSI_SPI_CTRLR0_TRANS_TYPE_LSB)
//ldr	r1, [pc, #104]	// (0xdc)
#define SSI_SPI_CTRLR0_OFFSET 0x000000f4
ldr r1, =(SPI_CTRLR0_ENTER_XIP)
//ldr	r0, [pc, #108]	// (0xe0)
ldr r0, =(XIP_SSI_BASE + SSI_SPI_CTRLR0_OFFSET)  // SPI_CTRL0 Register
str	r1, [r0, #0]
movs	r1, #1
str	r1, [r3, #8]
movs	r1, #235	// 0xeb
str	r1, [r3, #96]	// 0x60
movs	r1, #160	// 0xa0
str	r1, [r3, #96]	// 0x60
bl	wait_ssi_ready
movs	r1, #0
str	r1, [r3, #8]

configure_ssi:
#define SSI_SPI_CTRLR0_XIP_CMD_LSB	24
#define SSI_SPI_CTRLR0_INST_L_VALUE_NONE	0
#define SSI_SPI_CTRLR0_TRANS_TYPE_VALUE_2C2A	0x2
#define SPI_CTRLR0_XIP \
    (MODE_CONTINUOUS_READ                      /* Mode bits to keep flash in continuous read mode */ \
        << SSI_SPI_CTRLR0_XIP_CMD_LSB) | \
    (ADDR_L << SSI_SPI_CTRLR0_ADDR_L_LSB) |    /* Total number of address + mode bits */ \
    (WAIT_CYCLES << SSI_SPI_CTRLR0_WAIT_CYCLES_LSB) |    /* Hi-Z dummy clocks following address + mode */ \
    (SSI_SPI_CTRLR0_INST_L_VALUE_NONE          /* Do not send a command, instead send XIP_CMD as mode bits after address */ \
        << SSI_SPI_CTRLR0_INST_L_LSB) | \
    (SSI_SPI_CTRLR0_TRANS_TYPE_VALUE_2C2A      /* Send Address in Quad I/O mode (and Command but that is zero bits long) */ \
        << SSI_SPI_CTRLR0_TRANS_TYPE_LSB)
//ldr	r1, [pc, #88]	// (0xe4)
ldr	r1, =(SPI_CTRLR0_XIP)
//ldr	r0, [pc, #80]	// (0xe0)
ldr r0, =(XIP_SSI_BASE + SSI_SPI_CTRLR0_OFFSET)
str	r1, [r0, #0]
movs	r1, #1
str	r1, [r3, #8]


// /home/pi/pico/pico-sdk/src/rp2_common/boot_stage2/asminclude/boot2_helpers/exit_from_boot2.S
// begin

// If entered from the bootrom, lr (which we earlier pushed) will be 0,
// and we vector through the table at the start of the main flash image.
// Any regular function call will have a nonzero value for lr.

check_return:
pop	{r0}
cmp	r0, #0
beq.n	vector_into_flash
bx	r0

vector_into_flash:
ldr	r0, =(XIP_BASE + 0x100) // r0 := 0x1000'0100, where the vector table is stored (in flash)
ldr r1, =(PPB_BASE + M0PLUS_VTOR_OFFSET) // r1 := 0xe000'ed08, arm's address for where the vector table is 
str	r0, [r1, #0] // tell arm where the vector table is
ldmia	r0, {r0, r1} // r0 := location of SP, r1 := reset fn
msr	MSP, r0 // set the SP to the top of the stack (i.e. top of SRAM)
bx	r1 // jump to the reset function
//end exit_from_boot2.S


wait_ssi_ready:
push	{r0, r1, lr}
2:
ldr	r1, [r3, #40]	// 0x28
movs	r0, #4
tst	r1, r0
beq.n	2b
movs	r0, #1
tst	r1, r0
bne.n	2b
pop	{r0, r1, pc}

read_flash_sreg:
push	{r1, lr}
str	r0, [r3, #96]	// 0x60
str	r0, [r3, #96]	// 0x60
bl	wait_ssi_ready
ldr	r0, [r3, #96]	// 0x60
ldr	r0, [r3, #96]	// 0x60
pop	{r1, pc}

//.align 4
.global literals
literals:
/*
//.word	0x40020000
//.word	0x18000000
//.word	0x00070000
//.word	0x005f0300
//.word	0x00002221
//.word	0x180000f4
//.word	0xa0002022
//.word	0x10000100
.word	0xe000ed08
*/

.ltorg

.end
