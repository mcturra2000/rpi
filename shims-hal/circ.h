/*
 * circ.h
 * 2024-07-23	mcarter	more functions
 * 2023-11-08	mcarter created
 *
 */

#ifndef INC_CIRC_H_
#define INC_CIRC_H_

#include <stdint.h>
#include <stdbool.h>
//#include "main.h"

bool circ_write(uint8_t value);
bool circ_read(uint8_t *value);
int circ_get(); // will return -1 if nothing avail
bool circ_avail();
void circ_writen(uint8_t *buf, uint32_t len);





#endif /* INC_CIRC_H_ */
